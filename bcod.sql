/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : bcod

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-09-22 02:36:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cat_dependencias`
-- ----------------------------
DROP TABLE IF EXISTS `cat_dependencias`;
CREATE TABLE `cat_dependencias` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `NOMENCLATURE` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_dependencias
-- ----------------------------
INSERT INTO `cat_dependencias` VALUES ('1', 'Secretaría de Administración', 'SA');
INSERT INTO `cat_dependencias` VALUES ('2', 'Secretaría de Finanzas y Tesorería General del Estado', 'SF');
INSERT INTO `cat_dependencias` VALUES ('3', 'Secretaría General de Gobierno', 'SG');
INSERT INTO `cat_dependencias` VALUES ('4', 'Dependencia de Prueba', 'DDP');

-- ----------------------------
-- Table structure for `cat_roles`
-- ----------------------------
DROP TABLE IF EXISTS `cat_roles`;
CREATE TABLE `cat_roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_roles
-- ----------------------------
INSERT INTO `cat_roles` VALUES ('1', 'SUPERADMIN');
INSERT INTO `cat_roles` VALUES ('2', 'ADMIN');
INSERT INTO `cat_roles` VALUES ('3', 'Administrador de Dependencia');
INSERT INTO `cat_roles` VALUES ('4', 'Administrador de Unidad Administrativa');
INSERT INTO `cat_roles` VALUES ('5', 'Administrador de Subnidad Administrativa');
INSERT INTO `cat_roles` VALUES ('6', 'Usuario');
INSERT INTO `cat_roles` VALUES ('7', 'Asistente');

-- ----------------------------
-- Table structure for `cat_status`
-- ----------------------------
DROP TABLE IF EXISTS `cat_status`;
CREATE TABLE `cat_status` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESC` varchar(255) DEFAULT NULL,
  `BEFORE_SENT` tinyint(4) DEFAULT NULL,
  `NOTIFICATION` varchar(255) DEFAULT NULL,
  `ALERT` varchar(255) DEFAULT NULL,
  `ICON` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_status
-- ----------------------------
INSERT INTO `cat_status` VALUES ('1', 'Completado', '0', null, null, null);
INSERT INTO `cat_status` VALUES ('3', 'Enterado', '0', null, null, null);
INSERT INTO `cat_status` VALUES ('4', 'Rechazado', '0', 'Documento fue rechazado', null, null);
INSERT INTO `cat_status` VALUES ('5', 'Pendiente de Firma', '1', 'Un documento ocupa firmarse antes de', 'Firmar antes de', 'mdi-pencil');
INSERT INTO `cat_status` VALUES ('10', 'Con término de respuesta', '1', 'Recibiste un correo que ocupa respuesta', 'Responder antes de', null);
INSERT INTO `cat_status` VALUES ('11', 'Urgente', '1', 'Documento urgente', 'Urgente', null);
INSERT INTO `cat_status` VALUES ('12', 'Sin acción requerida', '1', '-', '-', null);

-- ----------------------------
-- Table structure for `cat_sub_u_administrativas`
-- ----------------------------
DROP TABLE IF EXISTS `cat_sub_u_administrativas`;
CREATE TABLE `cat_sub_u_administrativas` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_DEPENDENCIA` int(11) DEFAULT NULL,
  `ID_UA` int(11) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `NOMENCLATURE` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_sub_u_administrativas
-- ----------------------------
INSERT INTO `cat_sub_u_administrativas` VALUES ('1', '1', '6', 'Dirección de Sistemas Gubernamentales', 'DSG');
INSERT INTO `cat_sub_u_administrativas` VALUES ('2', '1', '6', 'Dirección de Infraestructura', 'DI');
INSERT INTO `cat_sub_u_administrativas` VALUES ('3', '1', '7', 'Dirección de Administración', 'DA');
INSERT INTO `cat_sub_u_administrativas` VALUES ('4', '1', '7', 'Dirección de Recursos Humanos', 'RH');
INSERT INTO `cat_sub_u_administrativas` VALUES ('5', '1', '8', 'Dirección de Adquisiciones', 'DA');
INSERT INTO `cat_sub_u_administrativas` VALUES ('6', '1', '8', 'Dirección de Mantenimiento y Servicios Generales', 'DM');
INSERT INTO `cat_sub_u_administrativas` VALUES ('7', '1', '8', 'Dirección de Concursos', 'DC');
INSERT INTO `cat_sub_u_administrativas` VALUES ('8', '5', '8', 'jhk', 'jhkj');

-- ----------------------------
-- Table structure for `cat_types_mail`
-- ----------------------------
DROP TABLE IF EXISTS `cat_types_mail`;
CREATE TABLE `cat_types_mail` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_types_mail
-- ----------------------------
INSERT INTO `cat_types_mail` VALUES ('1', 'Directo');
INSERT INTO `cat_types_mail` VALUES ('2', 'Copiado');
INSERT INTO `cat_types_mail` VALUES ('3', 'Re-envio');
INSERT INTO `cat_types_mail` VALUES ('4', 'Turnado');
INSERT INTO `cat_types_mail` VALUES ('5', 'Respuesta');
INSERT INTO `cat_types_mail` VALUES ('6', 'Propio');

-- ----------------------------
-- Table structure for `cat_types_notification`
-- ----------------------------
DROP TABLE IF EXISTS `cat_types_notification`;
CREATE TABLE `cat_types_notification` (
  `ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `ICON` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_types_notification
-- ----------------------------
INSERT INTO `cat_types_notification` VALUES ('1', 'Documento recibido', 'mdi-email-plus-outline');
INSERT INTO `cat_types_notification` VALUES ('2', 'Documendo copiado', 'mdi-email-plus-outline');
INSERT INTO `cat_types_notification` VALUES ('3', 'Documento firmado', 'mdi-pencil');

-- ----------------------------
-- Table structure for `cat_u_administrativas`
-- ----------------------------
DROP TABLE IF EXISTS `cat_u_administrativas`;
CREATE TABLE `cat_u_administrativas` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_DEPENDENCIA` int(11) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `NOMENCLATURE` varchar(10) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `TELEPHONE_1` varchar(255) DEFAULT NULL,
  `TELEPHONE_2` varchar(255) DEFAULT NULL,
  `TELEPHONE_3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_u_administrativas
-- ----------------------------
INSERT INTO `cat_u_administrativas` VALUES ('1', '1', 'Dirección Jurídica y de Control', 'DJC', 'Biblioteca Central, piso 9. Calle Zuazua 655 Sur, Zona Centro. Monterrey, Nuevo León CP 64000', '8120201699', null, null);
INSERT INTO `cat_u_administrativas` VALUES ('2', '1', 'Secretaría Técnica', 'ST', 'Biblioteca Central, piso 11. Calle Zuazua 655 Sur, Zona Centro. Monterrey, Nuevo León CP 64000', '8120201683', null, null);
INSERT INTO `cat_u_administrativas` VALUES ('3', '1', 'Dirección General Ejecutiva', 'DGE', 'Biblioteca Central, piso 11. Calle Zuazua 655 Sur, Zona Centro. Monterrey, Nuevo León CP 64000', '8120201703', null, null);
INSERT INTO `cat_u_administrativas` VALUES ('4', '1', 'Coordinación de Comunicación, Imagen y Difusión', 'CC', 'Biblioteca Central, piso 11. Calle Zuazua 655 Sur, Zona Centro. Monterrey, Nuevo León CP 64000', '8120201703', '8120201703', '8120201703');
INSERT INTO `cat_u_administrativas` VALUES ('5', '1', 'Subsecretaría de Tecnologías', 'SST', 'Calle 5 de Mayo 505 Oriente, Zona Centro, Monterrey, Nuevo León CP 64000', '8120201110', '8120201100', null);
INSERT INTO `cat_u_administrativas` VALUES ('6', '1', 'Subsecretaría de Administración', 'SSA', 'Biblioteca Central, piso 10. Calle Zuazua 655 Sur, Zona Centro. Monterrey, Nuevo León CP 64000', '8120201705', null, null);
INSERT INTO `cat_u_administrativas` VALUES ('7', '1', 'Dirección General de Adquisiciones y Servicios Generales', 'DGA', 'Biblioteca Central, piso 4. Calle Zuazua 655 Sur, Zona Centro. Monterrey, Nuevo León CP 64000', '8120201750', null, null);
INSERT INTO `cat_u_administrativas` VALUES ('9', '3', 'kkkkkj', 'j', 'j', 'j', 'j', 'j');
INSERT INTO `cat_u_administrativas` VALUES ('10', '3', 'f', 'f', 'f', 'f', 'f', 'f');

-- ----------------------------
-- Table structure for `new_r_mail_to`
-- ----------------------------
DROP TABLE IF EXISTS `new_r_mail_to`;
CREATE TABLE `new_r_mail_to` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SUB_THREAD_KEY` varchar(20) DEFAULT NULL,
  `ID_TO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of new_r_mail_to
-- ----------------------------

-- ----------------------------
-- Table structure for `new_t_documents`
-- ----------------------------
DROP TABLE IF EXISTS `new_t_documents`;
CREATE TABLE `new_t_documents` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_TYPE` tinyint(4) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `BODY` longtext,
  `OWNER` bigint(20) DEFAULT NULL,
  `FOLIO` varchar(255) DEFAULT NULL,
  `FOLIO_2` varchar(255) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `ID_AUTHOR` bigint(20) DEFAULT NULL,
  `FOR` longtext,
  `CC` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of new_t_documents
-- ----------------------------
INSERT INTO `new_t_documents` VALUES ('1', '1', 'a', 'sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss[SEPARATOR]sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', 'SF/DJC-2019-13', null, '2019-09-20 11:56:01', '2', 'x', 'x');
INSERT INTO `new_t_documents` VALUES ('2', '1', '1Q84', '1Q84 (????????? Ichi-Ky?-Hachi-Yon) is a dystopian novel written by Japanese writer Haruki Murakami, first published in three volumes in Japan in 2009–10.[1] It covers a fictionalized year of 1984 in parallel with a \"real\" one. The novel is a story of how a woman named Aomame begins to notice strange changes occurring in the world. She is quickly enraptured in a plot involving Sakigake, a religious cult, her childhood love, Tengo, and embarks on a journey to discover what is \"real\". Its first printing sold out on the day it was released and sales reached a million within a month.[2] The English-language edition of all three volumes, with the first two volumes translated by Jay Rubin and the third by Philip Gabriel, was released in North America and the United Kingdom on October 25, 2011.[3][4][5][6] An excerpt from the novel, \"Town of Cats\", appeared in the September 5, 2011 issue of The New Yorker magazine.[7] The first chapter of 1Q84 had also been read as an excerpt in the Selected Shorts series at Symphony Space in New York.[SEPARATOR]Background information\r\nMurakami spent four years writing the novel after coming up with the opening sequence and title.[11] The title is a play on the Japanese pronunciation of the year 1984 and a reference to George Orwell\'s Nineteen Eighty-Four. The letter Q and ?, the Japanese number for 9, (typically romanized as \"ky?\", but as \"kew\" on the book\'s Japanese cover) are homophones, which are often used in Japanese wordplay.\r\n\r\nBefore the publication of 1Q84, Murakami stated that he would not reveal anything about the book, following criticism that leaks had diminished the novelty of his previous books. 1Q84 was noted for heavy advance orders despite this secrecy.[12]', '2', 'SF/DJC-2019-14', null, '2019-09-20 12:44:26', '2', 'x', 'x');
INSERT INTO `new_t_documents` VALUES ('3', '1', '1Q84', '1Q84 (????????? Ichi-Ky?-Hachi-Yon) is a dystopian novel written by Japanese writer Haruki Murakami, first published in three volumes in Japan in 2009–10.[1] It covers a fictionalized year of 1984 in parallel with a \"real\" one. The novel is a story of how a woman named Aomame begins to notice strange changes occurring in the world. She is quickly enraptured in a plot involving Sakigake, a religious cult, her childhood love, Tengo, and embarks on a journey to discover what is \"real\". Its first printing sold out on the day it was released and sales reached a million within a month.[2] The English-language edition of all three volumes, with the first two volumes translated by Jay Rubin and the third by Philip Gabriel, was released in North America and the United Kingdom on October 25, 2011.[3][4][5][6] An excerpt from the novel, \"Town of Cats\", appeared in the September 5, 2011 issue of The New Yorker magazine.[7] The first chapter of 1Q84 had also been read as an excerpt in the Selected Shorts series at Symphony Space in New York.', '22', 'SF/DJC-2019-14', null, '2019-09-20 15:24:00', '2', 'x', 'x');
INSERT INTO `new_t_documents` VALUES ('4', '1', '1Q84', '1Q84 (????????? Ichi-Ky?-Hachi-Yon) is a dystopian novel written by Japanese writer Haruki Murakami, first published in three volumes in Japan in 2009–10.[1] It covers a fictionalized year of 1984 in parallel with a \"real\" one. The novel is a story of how a woman named Aomame begins to notice strange changes occurring in the world. She is quickly enraptured in a plot involving Sakigake, a religious cult, her childhood love, Tengo, and embarks on a journey to discover what is \"real\". Its first printing sold out on the day it was released and sales reached a million within a month.[2] The English-language edition of all three volumes, with the first two volumes translated by Jay Rubin and the third by Philip Gabriel, was released in North America and the United Kingdom on October 25, 2011.[3][4][5][6] An excerpt from the novel, \"Town of Cats\", appeared in the September 5, 2011 issue of The New Yorker magazine.[7] The first chapter of 1Q84 had also been read as an excerpt in the Selected Shorts series at Symphony Space in New York.[SEPARATOR]Background information\r\nMurakami spent four years writing the novel after coming up with the opening sequence and title.[11] The title is a play on the Japanese pronunciation of the year 1984 and a reference to George Orwell\'s Nineteen Eighty-Four. The letter Q and ?, the Japanese number for 9, (typically romanized as \"ky?\", but as \"kew\" on the book\'s Japanese cover) are homophones, which are often used in Japanese wordplay.\r\n\r\nBefore the publication of 1Q84, Murakami stated that he would not reveal anything about the book, following criticism that leaks had diminished the novelty of his previous books. 1Q84 was noted for heavy advance orders despite this secrecy.[12]', '8', 'SF/DJC-2019-14', null, '2019-09-20 17:42:07', '2', 'x', 'x');
INSERT INTO `new_t_documents` VALUES ('6', '1', 'La peregrinación mágica de Cepillín', 'Hey guys I recently got enough coins for the event savanose gear and weapons but I\'m not sure what to pick..\r\n\r\nI have a scout (SR/Sheriff/Enchanter) and a cleric (Exo/Druid/Diev) that I might want to interchange the armours between the two at least until I get better gear. Should I stick to plate armour then get gems/awakening to increase the magic defence or should I go the opposite route with cloth and phy gems?\r\n\r\nSimilar question with velcoffer gear, plate leather or cloth?[SEPARATOR]A mysterious visitor knocks at the door in the dead of night. A hauntingly monotone voice can be heard from the hallway. \"Please let me in? I need help.\" the child insists. Meredith Rhodes is the maternal sort; a mother of two, and would normally never hesitate to help a child in need, but there\'s something not right . . . she can feel it in her gut. What she discovers on the other side, she will soon learn, is just one of many disturbing reports of late-night visits from black-eyed children. Who are they, and what do they want? The answer ties into a global conspiracy that runs all the way to to the heart of the Vatican. At the crux of this war of light and shadow, a mysterious Dark Man that has already infiltrated the highest levels of government and Hollywood influence, and his greatest threat is Meredith\'s 10-year-old daughter Ashley.\r\n\r\nDoes this young girl somehow hold the key to stopping this demonic madman, or will her family be the first of many to fall in the coming apocalypse?[SEPARATOR]Welcome to your new role at the H31 Facility - and congratulations!\r\n\r\nIf you have been recruited that means you are among an elite few with the necessary skills and talents to work at this state of the art correctional facility.\r\n\r\nYou will be joining a team of 300 select officers tasked with the incarceration and study of nine of the most challenging and unique subjects throughout the entire judicial system. Please do not allow the disparity of this figure to fool you into complacency - these individuals are extremely dangerous to your physical, mental and emotional wellbeing.\r\n\r\nAs such, there follows a list of 40 prime directives that must not and cannot be ignored at any cost. Familiarizing yourself with and memorizing these rules will ensure that you settle into your position here at the H31 Facility as smoothly as possible.[SEPARATOR]All officers in the H31 Facility are issued a personal security kit consisting of body armor, protective helmet, comms devices, riot baton, sidearm, flare, gas mask, lockbox keys, and crucifix. These must be worn at all times when within the H31 Facility.\r\n\r\nShould any items from your personal security kit be mislaid it must be reported to your senior officer and the Facility is to be placed into Code Blue Lockdown immediately.\r\n\r\nThe doors to the H31 facility open for 20 minutes on three occasions each day (5.00, 13.00, and 20.00) to allow for the arrival of the new shift of officers and departure of the last. Nobody may pass through the door outside of these times.', '2', 'SF/DJC-2019-16', 'I-EAT-ASS-69/2019', '2019-09-21 21:11:12', '2', 'Señor Bronco\r\nDueño de todo Nuevo León', 'CC - \r\nCC - Para pitufo - Patriarca de Aldea Pitufo\r\nCC - Pitufina - La única mujer de Aldea Pitufo\r\nCC - Pitufo Fortachón - Nomás porque sí\r\nCC - Doug Dimmadome dueño del Dimmadomo de Dimmsdale');
INSERT INTO `new_t_documents` VALUES ('7', '1', 'La peregrinación mágica de Cepillín', 'Hey guys I recently got enough coins for the event savanose gear and weapons but I\'m not sure what to pick..\r\n\r\nI have a scout (SR/Sheriff/Enchanter) and a cleric (Exo/Druid/Diev) that I might want to interchange the armours between the two at least until I get better gear. Should I stick to plate armour then get gems/awakening to increase the magic defence or should I go the opposite route with cloth and phy gems?\r\n\r\nSimilar question with velcoffer gear, plate leather or cloth?[SEPARATOR]A mysterious visitor knocks at the door in the dead of night. A hauntingly monotone voice can be heard from the hallway. \"Please let me in? I need help.\" the child insists. Meredith Rhodes is the maternal sort; a mother of two, and would normally never hesitate to help a child in need, but there\'s something not right . . . she can feel it in her gut. What she discovers on the other side, she will soon learn, is just one of many disturbing reports of late-night visits from black-eyed children. Who are they, and what do they want? The answer ties into a global conspiracy that runs all the way to to the heart of the Vatican. At the crux of this war of light and shadow, a mysterious Dark Man that has already infiltrated the highest levels of government and Hollywood influence, and his greatest threat is Meredith\'s 10-year-old daughter Ashley.\r\n\r\nDoes this young girl somehow hold the key to stopping this demonic madman, or will her family be the first of many to fall in the coming apocalypse?[SEPARATOR]Welcome to your new role at the H31 Facility - and congratulations!\r\n\r\nIf you have been recruited that means you are among an elite few with the necessary skills and talents to work at this state of the art correctional facility.\r\n\r\nYou will be joining a team of 300 select officers tasked with the incarceration and study of nine of the most challenging and unique subjects throughout the entire judicial system. Please do not allow the disparity of this figure to fool you into complacency - these individuals are extremely dangerous to your physical, mental and emotional wellbeing.\r\n\r\nAs such, there follows a list of 40 prime directives that must not and cannot be ignored at any cost. Familiarizing yourself with and memorizing these rules will ensure that you settle into your position here at the H31 Facility as smoothly as possible.[SEPARATOR]All officers in the H31 Facility are issued a personal security kit consisting of body armor, protective helmet, comms devices, riot baton, sidearm, flare, gas mask, lockbox keys, and crucifix. These must be worn at all times when within the H31 Facility.\r\n\r\nShould any items from your personal security kit be mislaid it must be reported to your senior officer and the Facility is to be placed into Code Blue Lockdown immediately.\r\n\r\nThe doors to the H31 facility open for 20 minutes on three occasions each day (5.00, 13.00, and 20.00) to allow for the arrival of the new shift of officers and departure of the last. Nobody may pass through the door outside of these times.', '8', 'SF/DJC-2019-16', 'I-EAT-ASS-69/2019', '2019-09-21 21:12:50', '2', 'Señor Bronco\r\nDueño de todo Nuevo León', 'CC - \r\nCC - Para pitufo - Patriarca de Aldea Pitufo\r\nCC - Pitufina - La única mujer de Aldea Pitufo\r\nCC - Pitufo Fortachón - Nomás porque sí\r\nCC - Doug Dimmadome dueño del Dimmadomo de Dimmsdale');
INSERT INTO `new_t_documents` VALUES ('8', '1', 'La peregrinación mágica de Cepillín', 'Hey guys I recently got enough coins for the event savanose gear and weapons but I\'m not sure what to pick..\r\n\r\nI have a scout (SR/Sheriff/Enchanter) and a cleric (Exo/Druid/Diev) that I might want to interchange the armours between the two at least until I get better gear. Should I stick to plate armour then get gems/awakening to increase the magic defence or should I go the opposite route with cloth and phy gems?\r\n\r\nSimilar question with velcoffer gear, plate leather or cloth?[SEPARATOR]A mysterious visitor knocks at the door in the dead of night. A hauntingly monotone voice can be heard from the hallway. \"Please let me in? I need help.\" the child insists. Meredith Rhodes is the maternal sort; a mother of two, and would normally never hesitate to help a child in need, but there\'s something not right . . . she can feel it in her gut. What she discovers on the other side, she will soon learn, is just one of many disturbing reports of late-night visits from black-eyed children. Who are they, and what do they want? The answer ties into a global conspiracy that runs all the way to to the heart of the Vatican. At the crux of this war of light and shadow, a mysterious Dark Man that has already infiltrated the highest levels of government and Hollywood influence, and his greatest threat is Meredith\'s 10-year-old daughter Ashley.\r\n\r\nDoes this young girl somehow hold the key to stopping this demonic madman, or will her family be the first of many to fall in the coming apocalypse?[SEPARATOR]Welcome to your new role at the H31 Facility - and congratulations!\r\n\r\nIf you have been recruited that means you are among an elite few with the necessary skills and talents to work at this state of the art correctional facility.\r\n\r\nYou will be joining a team of 300 select officers tasked with the incarceration and study of nine of the most challenging and unique subjects throughout the entire judicial system. Please do not allow the disparity of this figure to fool you into complacency - these individuals are extremely dangerous to your physical, mental and emotional wellbeing.\r\n\r\nAs such, there follows a list of 40 prime directives that must not and cannot be ignored at any cost. Familiarizing yourself with and memorizing these rules will ensure that you settle into your position here at the H31 Facility as smoothly as possible.[SEPARATOR]All officers in the H31 Facility are issued a personal security kit consisting of body armor, protective helmet, comms devices, riot baton, sidearm, flare, gas mask, lockbox keys, and crucifix. These must be worn at all times when within the H31 Facility.\r\n\r\nShould any items from your personal security kit be mislaid it must be reported to your senior officer and the Facility is to be placed into Code Blue Lockdown immediately.\r\n\r\nThe doors to the H31 facility open for 20 minutes on three occasions each day (5.00, 13.00, and 20.00) to allow for the arrival of the new shift of officers and departure of the last. Nobody may pass through the door outside of these times.', '25', 'SF/DJC-2019-16', 'I-EAT-ASS-69/2019', '2019-09-21 21:12:51', '2', 'Señor Bronco\r\nDueño de todo Nuevo León', 'CC - \r\nCC - Para pitufo - Patriarca de Aldea Pitufo\r\nCC - Pitufina - La única mujer de Aldea Pitufo\r\nCC - Pitufo Fortachón - Nomás porque sí\r\nCC - Doug Dimmadome dueño del Dimmadomo de Dimmsdale');
INSERT INTO `new_t_documents` VALUES ('10', '1', 's', 's', '2', 'SF/DJC-2019-17', 's', '2019-09-21 21:23:51', '2', 'S', 's');
INSERT INTO `new_t_documents` VALUES ('11', '1', 's', 's', '8', 'SF/DJC-2019-17', 's', '2019-09-21 21:24:07', '2', 'S', 's');
INSERT INTO `new_t_documents` VALUES ('12', '1', 'Dolor', 'Mucho dolor', '2', 'SF/DJC-2019-18', 'DOLOR-2019', '2019-09-21 21:38:49', '2', 'Lord Dolor\r\nDe Billy y Mandy', 'CC - Billy\r\nCC - Mandy\r\nCC - Puro Hueso');
INSERT INTO `new_t_documents` VALUES ('13', '1', 'Dolor', 'Mucho dolor', '8', 'SF/DJC-2019-18', 'DOLOR-2019', '2019-09-21 21:39:04', '2', 'Lord Dolor\r\nDe Billy y Mandy', 'CC - Billy\r\nCC - Mandy\r\nCC - Puro Hueso');
INSERT INTO `new_t_documents` VALUES ('14', '1', '546456', '545645', '2', 'SF/DJC-2019-19', '45456', '2019-09-22 08:55:22', '2', 'Pepe', 'cc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X');
INSERT INTO `new_t_documents` VALUES ('15', '1', '546456', '545645', '8', 'SF/DJC-2019-19', '45456', '2019-09-22 08:55:36', '2', 'Pepe', 'cc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X\r\ncc- Mr X');

-- ----------------------------
-- Table structure for `new_t_documents_signed`
-- ----------------------------
DROP TABLE IF EXISTS `new_t_documents_signed`;
CREATE TABLE `new_t_documents_signed` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_NAME` varchar(255) NOT NULL,
  `SIGNED_DATE` datetime NOT NULL,
  `DIGITAL_SIGNATURE` varchar(255) NOT NULL,
  `ID_DOCUMENT` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of new_t_documents_signed
-- ----------------------------
INSERT INTO `new_t_documents_signed` VALUES ('1', 'Mitzy Anahí Macias Garza', '2019-09-20 17:42:31', 'aea54a7f995be460fd2c09edae718d4b3763d10c7bba941fb5fbd463060b13b0', '4');
INSERT INTO `new_t_documents_signed` VALUES ('2', 'Mitzy Anahí Macias Garza', '2019-09-21 21:14:35', '68a8c8e4c72a64867f712685427bbb58fb86b475a3ab0c05fdb8df74a39fa3d0', '7');
INSERT INTO `new_t_documents_signed` VALUES ('3', 'Mitzy Anahí Macias Garza', '2019-09-21 21:39:25', '1341a2f8f6fc35c5388ded6f1953cef648bf0864ed8592ca5e7dd56b4dd3c1a0', '13');

-- ----------------------------
-- Table structure for `new_t_document_draft`
-- ----------------------------
DROP TABLE IF EXISTS `new_t_document_draft`;
CREATE TABLE `new_t_document_draft` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_DOCUMENT` bigint(20) DEFAULT NULL,
  `ID_EDITOR` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of new_t_document_draft
-- ----------------------------

-- ----------------------------
-- Table structure for `new_t_document_from`
-- ----------------------------
DROP TABLE IF EXISTS `new_t_document_from`;
CREATE TABLE `new_t_document_from` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_DOCUMENT_RECIEVED` bigint(20) DEFAULT NULL,
  `ID_FROM_USER` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of new_t_document_from
-- ----------------------------
INSERT INTO `new_t_document_from` VALUES ('1', '3', '2');
INSERT INTO `new_t_document_from` VALUES ('2', '4', '2');
INSERT INTO `new_t_document_from` VALUES ('3', '7', '2');
INSERT INTO `new_t_document_from` VALUES ('4', '8', '2');
INSERT INTO `new_t_document_from` VALUES ('5', '11', '2');
INSERT INTO `new_t_document_from` VALUES ('6', '13', '2');
INSERT INTO `new_t_document_from` VALUES ('7', '15', '2');

-- ----------------------------
-- Table structure for `new_t_document_mail`
-- ----------------------------
DROP TABLE IF EXISTS `new_t_document_mail`;
CREATE TABLE `new_t_document_mail` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DOCUMENT_THREAD` varchar(20) DEFAULT NULL,
  `ID_MAIL` bigint(20) DEFAULT NULL,
  `ID_DOCUMENT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of new_t_document_mail
-- ----------------------------
INSERT INTO `new_t_document_mail` VALUES ('1', null, '1', '2');
INSERT INTO `new_t_document_mail` VALUES ('2', null, '2', '3');
INSERT INTO `new_t_document_mail` VALUES ('3', null, '3', '2');
INSERT INTO `new_t_document_mail` VALUES ('4', null, '4', '4');
INSERT INTO `new_t_document_mail` VALUES ('5', null, '5', '6');
INSERT INTO `new_t_document_mail` VALUES ('6', null, '6', '7');
INSERT INTO `new_t_document_mail` VALUES ('7', null, '7', '8');
INSERT INTO `new_t_document_mail` VALUES ('8', null, '8', '10');
INSERT INTO `new_t_document_mail` VALUES ('9', null, '9', '11');
INSERT INTO `new_t_document_mail` VALUES ('10', null, '10', '12');
INSERT INTO `new_t_document_mail` VALUES ('11', null, '11', '13');
INSERT INTO `new_t_document_mail` VALUES ('12', null, '12', '14');
INSERT INTO `new_t_document_mail` VALUES ('13', null, '13', '15');

-- ----------------------------
-- Table structure for `new_t_document_replies`
-- ----------------------------
DROP TABLE IF EXISTS `new_t_document_replies`;
CREATE TABLE `new_t_document_replies` (
  `ID` bigint(20) NOT NULL,
  `ID_THREAD` varchar(255) DEFAULT NULL,
  `ID_DOCUMENT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of new_t_document_replies
-- ----------------------------

-- ----------------------------
-- Table structure for `new_t_document_status`
-- ----------------------------
DROP TABLE IF EXISTS `new_t_document_status`;
CREATE TABLE `new_t_document_status` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_THREAD_OWNER` bigint(20) DEFAULT NULL,
  `ID_HEAD_DOCUMENT` bigint(20) DEFAULT NULL,
  `MAIN_THREAD` varchar(255) DEFAULT NULL,
  `SUB_THREAD` varchar(255) DEFAULT NULL,
  `DUE_DATE` datetime DEFAULT NULL,
  `LAST_UPDATED` datetime DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `SEEN` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of new_t_document_status
-- ----------------------------
INSERT INTO `new_t_document_status` VALUES ('1', '2', '2', 'MjIwLjQxNTUzMjAwIDE1NjkwMTEwNDA', 'MkRPQ1VNRU5UMjAuNDE1NTUxMDAgMTU2OTAxMTA0MDI', null, '2019-09-20 15:24:00', '0', '0');
INSERT INTO `new_t_document_status` VALUES ('2', '22', '3', 'MjIwLjQxNTUzMjAwIDE1NjkwMTEwNDA', 'M0RPQ1VNRU5UMjAuNjYwNTMxMDAgMTU2OTAxMTA0MDIy', null, '2019-09-20 15:24:00', '0', '0');
INSERT INTO `new_t_document_status` VALUES ('3', '2', '2', 'MjIwLjM2NTI0NjAwIDE1NjkwMTkzMjc', 'MkRPQ1VNRU5UMjAuMzY1MjU5MDAgMTU2OTAxOTMyNzI', null, '2019-09-20 17:42:07', '0', '0');
INSERT INTO `new_t_document_status` VALUES ('4', '8', '4', 'MjIwLjM2NTI0NjAwIDE1NjkwMTkzMjc', 'NERPQ1VNRU5UMjAuNjM4MDIzMDAgMTU2OTAxOTMyNzg', null, '2019-09-20 17:42:07', '0', '1');
INSERT INTO `new_t_document_status` VALUES ('5', '2', '6', 'MjYwLjgxMjQzOTAwIDE1NjkxMTgzNzA', 'NkRPQ1VNRU5UMjAuODEyNDcyMDAgMTU2OTExODM3MDI', null, '2019-09-21 21:12:50', '0', '1');
INSERT INTO `new_t_document_status` VALUES ('6', '8', '7', 'MjYwLjgxMjQzOTAwIDE1NjkxMTgzNzA', 'N0RPQ1VNRU5UMjAuMDY4Mjg1MDAgMTU2OTExODM3MTg', null, '2019-09-21 21:12:50', '0', '1');
INSERT INTO `new_t_document_status` VALUES ('7', '25', '8', 'MjYwLjgxMjQzOTAwIDE1NjkxMTgzNzA', 'OERPQ1VNRU5UMjAuMzQ2Mjk1MDAgMTU2OTExODM3MTI1', null, '2019-09-21 21:12:50', '0', '0');
INSERT INTO `new_t_document_status` VALUES ('8', '2', '10', 'MjEwMC42MjgxNjYwMCAxNTY5MTE5MDQ3', 'MTBET0NVTUVOVDIwLjYyODIwMTAwIDE1NjkxMTkwNDcy', null, '2019-09-21 21:24:07', '0', '0');
INSERT INTO `new_t_document_status` VALUES ('9', '8', '11', 'MjEwMC42MjgxNjYwMCAxNTY5MTE5MDQ3', 'MTFET0NVTUVOVDIwLjg5MDQxOTAwIDE1NjkxMTkwNDc4', null, '2019-09-21 21:24:07', '0', '0');
INSERT INTO `new_t_document_status` VALUES ('10', '2', '12', 'MjEyMC40ODQxNDIwMCAxNTY5MTE5OTQ0', 'MTJET0NVTUVOVDIwLjQ4NDE5MjAwIDE1NjkxMTk5NDQy', null, '2019-09-21 21:39:04', '0', '1');
INSERT INTO `new_t_document_status` VALUES ('11', '8', '13', 'MjEyMC40ODQxNDIwMCAxNTY5MTE5OTQ0', 'MTNET0NVTUVOVDIwLjY2ODI5NTAwIDE1NjkxMTk5NDQ4', null, '2019-09-21 21:39:04', '0', '1');
INSERT INTO `new_t_document_status` VALUES ('12', '2', '14', 'MjE0MC42MjAyMTEwMCAxNTY5MTM1MzM2', 'MTRET0NVTUVOVDIwLjYyMDIzMjAwIDE1NjkxMzUzMzYy', null, '2019-09-22 08:55:36', '0', '1');
INSERT INTO `new_t_document_status` VALUES ('13', '8', '15', 'MjE0MC42MjAyMTEwMCAxNTY5MTM1MzM2', 'MTVET0NVTUVOVDIwLjc3MDA4NzAwIDE1NjkxMzUzMzY4', null, '2019-09-22 08:55:36', '0', '1');

-- ----------------------------
-- Table structure for `new_t_mail`
-- ----------------------------
DROP TABLE IF EXISTS `new_t_mail`;
CREATE TABLE `new_t_mail` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MAIN_THREAD_KEY` varchar(255) DEFAULT NULL,
  `SUB_THREAD_KEY` varchar(255) DEFAULT NULL,
  `SUBJECT` varchar(255) DEFAULT NULL,
  `FROM` bigint(20) NOT NULL DEFAULT '0',
  `CC` tinyint(4) NOT NULL DEFAULT '0',
  `BCC` tinyint(4) NOT NULL DEFAULT '0',
  `BODY` longtext,
  `DATE_RECEIVED` datetime DEFAULT NULL COMMENT 'ss',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of new_t_mail
-- ----------------------------
INSERT INTO `new_t_mail` VALUES ('1', 'TUFJTjIwLjEyNzAwNDAwIDE1NjkwMTEwNDAMmFzdW50bw', 'U1VCMjAuMTI3MDM2MDAgMTU2OTAxMTA0MDJhc3VudG8', 'asunto', '0', '0', '0', 'asunto', '2019-09-20 15:24:00');
INSERT INTO `new_t_mail` VALUES ('2', 'TUFJTjIwLjEyNzAwNDAwIDE1NjkwMTEwNDAMmFzdW50bw', 'U1VCMjIwLjEyNzA0MjAwIDE1NjkwMTEwNDAyMjJhc3VudG8', 'asunto', '2', '0', '0', 'asunto', '2019-09-20 15:24:00');
INSERT INTO `new_t_mail` VALUES ('3', 'TUFJTjIwLjE1MjE4NTAwIDE1NjkwMTkzMjcMjFxODQ', 'U1VCMjAuMTUyMTk1MDAgMTU2OTAxOTMyNzIxcTg0', '1q84', '0', '0', '0', '1q84', '2019-09-20 17:42:07');
INSERT INTO `new_t_mail` VALUES ('4', 'TUFJTjIwLjE1MjE4NTAwIDE1NjkwMTkzMjcMjFxODQ', 'U1VCODAuMTUyMjAwMDAgMTU2OTAxOTMyNzI4MXE4NA', '1q84', '2', '0', '0', '1q84', '2019-09-20 17:42:07');
INSERT INTO `new_t_mail` VALUES ('5', 'TUFJTjIwLjQyNzQ3MDAwIDE1NjkxMTgzNzAMlBlcmVncmluYWNpw7NuIE3DoWdpY2E', 'U1VCMjAuNDI3NDg1MDAgMTU2OTExODM3MDJQZXJlZ3JpbmFjacOzbiBNw6FnaWNh', 'Peregrinación Mágica', '0', '0', '0', '', '2019-09-21 21:12:50');
INSERT INTO `new_t_mail` VALUES ('6', 'TUFJTjIwLjQyNzQ3MDAwIDE1NjkxMTgzNzAMlBlcmVncmluYWNpw7NuIE3DoWdpY2E', 'U1VCODAuNDI3NDkxMDAgMTU2OTExODM3MDI4UGVyZWdyaW5hY2nDs24gTcOhZ2ljYQ', 'Peregrinación Mágica', '2', '0', '0', '', '2019-09-21 21:12:50');
INSERT INTO `new_t_mail` VALUES ('7', 'TUFJTjIwLjQyNzQ3MDAwIDE1NjkxMTgzNzAMlBlcmVncmluYWNpw7NuIE3DoWdpY2E', 'U1VCMjUwLjQyNzQ5NjAwIDE1NjkxMTgzNzAyMjVQZXJlZ3JpbmFjacOzbiBNw6FnaWNh', 'Peregrinación Mágica', '2', '1', '0', '', '2019-09-21 21:12:50');
INSERT INTO `new_t_mail` VALUES ('8', 'TUFJTjIwLjIyNDU2OTAwIDE1NjkxMTkwNDcMnM', 'U1VCMjAuMjI0NTgwMDAgMTU2OTExOTA0NzJz', 's', '0', '0', '0', 's', '2019-09-21 21:24:07');
INSERT INTO `new_t_mail` VALUES ('9', 'TUFJTjIwLjIyNDU2OTAwIDE1NjkxMTkwNDcMnM', 'U1VCODAuMjI0NTg0MDAgMTU2OTExOTA0NzI4cw', 's', '2', '0', '0', 's', '2019-09-21 21:24:07');
INSERT INTO `new_t_mail` VALUES ('10', 'TUFJTjIwLjI3ODE2ODAwIDE1NjkxMTk5NDQMmRvbG9yc2l0bw', 'U1VCMjAuMjc4MTc2MDAgMTU2OTExOTk0NDJkb2xvcnNpdG8', 'dolorsito', '0', '0', '0', 'dddd', '2019-09-21 21:39:04');
INSERT INTO `new_t_mail` VALUES ('11', 'TUFJTjIwLjI3ODE2ODAwIDE1NjkxMTk5NDQMmRvbG9yc2l0bw', 'U1VCODAuMjc4MTgxMDAgMTU2OTExOTk0NDI4ZG9sb3JzaXRv', 'dolorsito', '2', '0', '0', 'dddd', '2019-09-21 21:39:04');
INSERT INTO `new_t_mail` VALUES ('12', 'TUFJTjIwLjUxMzEzNTAwIDE1NjkxMzUzMzYMmE', 'U1VCMjAuNTEzMTQwMDAgMTU2OTEzNTMzNjJh', 'a', '0', '0', '0', 'a', '2019-09-22 08:55:36');
INSERT INTO `new_t_mail` VALUES ('13', 'TUFJTjIwLjUxMzEzNTAwIDE1NjkxMzUzMzYMmE', 'U1VCODAuNTEzMTQ0MDAgMTU2OTEzNTMzNjI4YQ', 'a', '2', '0', '0', 'a', '2019-09-22 08:55:36');

-- ----------------------------
-- Table structure for `new_t_mail_threads`
-- ----------------------------
DROP TABLE IF EXISTS `new_t_mail_threads`;
CREATE TABLE `new_t_mail_threads` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_THREAD_OWNER` bigint(20) DEFAULT NULL,
  `MAIN_THREAD_KEY` varchar(255) DEFAULT NULL,
  `SUB_THREAD_KEY` varchar(255) DEFAULT NULL,
  `LAST_UPDATED` datetime DEFAULT NULL,
  `ID_LAST_MAIL` bigint(20) NOT NULL,
  `READ` tinyint(4) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of new_t_mail_threads
-- ----------------------------
INSERT INTO `new_t_mail_threads` VALUES ('1', '2', 'TUFJTjIwLjEyNzAwNDAwIDE1NjkwMTEwNDAMmFzdW50bw', 'U1VCMjAuMTI3MDM2MDAgMTU2OTAxMTA0MDJhc3VudG8', '2019-09-20 15:24:00', '1', '1');
INSERT INTO `new_t_mail_threads` VALUES ('2', '22', 'TUFJTjIwLjEyNzAwNDAwIDE1NjkwMTEwNDAMmFzdW50bw', 'U1VCMjIwLjEyNzA0MjAwIDE1NjkwMTEwNDAyMjJhc3VudG8', '2019-09-20 15:24:00', '2', '0');
INSERT INTO `new_t_mail_threads` VALUES ('3', '2', 'TUFJTjIwLjE1MjE4NTAwIDE1NjkwMTkzMjcMjFxODQ', 'U1VCMjAuMTUyMTk1MDAgMTU2OTAxOTMyNzIxcTg0', '2019-09-20 17:42:07', '3', '1');
INSERT INTO `new_t_mail_threads` VALUES ('4', '8', 'TUFJTjIwLjE1MjE4NTAwIDE1NjkwMTkzMjcMjFxODQ', 'U1VCODAuMTUyMjAwMDAgMTU2OTAxOTMyNzI4MXE4NA', '2019-09-20 17:42:07', '4', '0');
INSERT INTO `new_t_mail_threads` VALUES ('5', '2', 'TUFJTjIwLjQyNzQ3MDAwIDE1NjkxMTgzNzAMlBlcmVncmluYWNpw7NuIE3DoWdpY2E', 'U1VCMjAuNDI3NDg1MDAgMTU2OTExODM3MDJQZXJlZ3JpbmFjacOzbiBNw6FnaWNh', '2019-09-21 21:12:50', '5', '1');
INSERT INTO `new_t_mail_threads` VALUES ('6', '8', 'TUFJTjIwLjQyNzQ3MDAwIDE1NjkxMTgzNzAMlBlcmVncmluYWNpw7NuIE3DoWdpY2E', 'U1VCODAuNDI3NDkxMDAgMTU2OTExODM3MDI4UGVyZWdyaW5hY2nDs24gTcOhZ2ljYQ', '2019-09-21 21:12:50', '6', '0');
INSERT INTO `new_t_mail_threads` VALUES ('7', '25', 'TUFJTjIwLjQyNzQ3MDAwIDE1NjkxMTgzNzAMlBlcmVncmluYWNpw7NuIE3DoWdpY2E', 'U1VCMjUwLjQyNzQ5NjAwIDE1NjkxMTgzNzAyMjVQZXJlZ3JpbmFjacOzbiBNw6FnaWNh', '2019-09-21 21:12:50', '7', '0');
INSERT INTO `new_t_mail_threads` VALUES ('8', '2', 'TUFJTjIwLjIyNDU2OTAwIDE1NjkxMTkwNDcMnM', 'U1VCMjAuMjI0NTgwMDAgMTU2OTExOTA0NzJz', '2019-09-21 21:24:07', '8', '1');
INSERT INTO `new_t_mail_threads` VALUES ('9', '8', 'TUFJTjIwLjIyNDU2OTAwIDE1NjkxMTkwNDcMnM', 'U1VCODAuMjI0NTg0MDAgMTU2OTExOTA0NzI4cw', '2019-09-21 21:24:07', '9', '0');
INSERT INTO `new_t_mail_threads` VALUES ('10', '2', 'TUFJTjIwLjI3ODE2ODAwIDE1NjkxMTk5NDQMmRvbG9yc2l0bw', 'U1VCMjAuMjc4MTc2MDAgMTU2OTExOTk0NDJkb2xvcnNpdG8', '2019-09-21 21:39:04', '10', '1');
INSERT INTO `new_t_mail_threads` VALUES ('11', '8', 'TUFJTjIwLjI3ODE2ODAwIDE1NjkxMTk5NDQMmRvbG9yc2l0bw', 'U1VCODAuMjc4MTgxMDAgMTU2OTExOTk0NDI4ZG9sb3JzaXRv', '2019-09-21 21:39:04', '11', '0');
INSERT INTO `new_t_mail_threads` VALUES ('12', '2', 'TUFJTjIwLjUxMzEzNTAwIDE1NjkxMzUzMzYMmE', 'U1VCMjAuNTEzMTQwMDAgMTU2OTEzNTMzNjJh', '2019-09-22 08:55:36', '12', '1');
INSERT INTO `new_t_mail_threads` VALUES ('13', '8', 'TUFJTjIwLjUxMzEzNTAwIDE1NjkxMzUzMzYMmE', 'U1VCODAuNTEzMTQ0MDAgMTU2OTEzNTMzNjI4YQ', '2019-09-22 08:55:36', '13', '0');

-- ----------------------------
-- Table structure for `new_t_reply`
-- ----------------------------
DROP TABLE IF EXISTS `new_t_reply`;
CREATE TABLE `new_t_reply` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_TO` bigint(20) DEFAULT NULL,
  `ID_MAIL_REPLY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of new_t_reply
-- ----------------------------

-- ----------------------------
-- Table structure for `r_procuration`
-- ----------------------------
DROP TABLE IF EXISTS `r_procuration`;
CREATE TABLE `r_procuration` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_REPRESENTATIVE` int(11) NOT NULL,
  `ID_REPRESENTED` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of r_procuration
-- ----------------------------
INSERT INTO `r_procuration` VALUES ('1', '13', '8');
INSERT INTO `r_procuration` VALUES ('2', '13', '9');
INSERT INTO `r_procuration` VALUES ('3', '13', '14');
INSERT INTO `r_procuration` VALUES ('4', '13', '15');
INSERT INTO `r_procuration` VALUES ('5', '26', '8');
INSERT INTO `r_procuration` VALUES ('6', '26', '9');
INSERT INTO `r_procuration` VALUES ('7', '26', '2');
INSERT INTO `r_procuration` VALUES ('8', '26', '1');

-- ----------------------------
-- Table structure for `r_user_roles`
-- ----------------------------
DROP TABLE IF EXISTS `r_user_roles`;
CREATE TABLE `r_user_roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_ROLE` int(11) DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of r_user_roles
-- ----------------------------
INSERT INTO `r_user_roles` VALUES ('2', '3', '8');
INSERT INTO `r_user_roles` VALUES ('18', '1', '1');
INSERT INTO `r_user_roles` VALUES ('19', '1', '2');
INSERT INTO `r_user_roles` VALUES ('20', '2', '1');
INSERT INTO `r_user_roles` VALUES ('21', '2', '2');
INSERT INTO `r_user_roles` VALUES ('26', '5', '13');
INSERT INTO `r_user_roles` VALUES ('27', '6', '13');
INSERT INTO `r_user_roles` VALUES ('28', '7', '13');
INSERT INTO `r_user_roles` VALUES ('29', '7', '26');
INSERT INTO `r_user_roles` VALUES ('30', '6', '26');
INSERT INTO `r_user_roles` VALUES ('31', '7', '27');
INSERT INTO `r_user_roles` VALUES ('32', '6', '27');
INSERT INTO `r_user_roles` VALUES ('33', '7', '28');
INSERT INTO `r_user_roles` VALUES ('34', '6', '28');
INSERT INTO `r_user_roles` VALUES ('35', '1', '1');
INSERT INTO `r_user_roles` VALUES ('36', '7', '29');

-- ----------------------------
-- Table structure for `system_log`
-- ----------------------------
DROP TABLE IF EXISTS `system_log`;
CREATE TABLE `system_log` (
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of system_log
-- ----------------------------

-- ----------------------------
-- Table structure for `t_documents_consecutive`
-- ----------------------------
DROP TABLE IF EXISTS `t_documents_consecutive`;
CREATE TABLE `t_documents_consecutive` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_DEP` int(11) NOT NULL,
  `COUNT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Primero el UPDATE +1, luego select para obtener el valor';

-- ----------------------------
-- Records of t_documents_consecutive
-- ----------------------------
INSERT INTO `t_documents_consecutive` VALUES ('2', '1', '19');
INSERT INTO `t_documents_consecutive` VALUES ('3', '2', '19');
INSERT INTO `t_documents_consecutive` VALUES ('4', '4', '4');

-- ----------------------------
-- Table structure for `t_documents_due_date`
-- ----------------------------
DROP TABLE IF EXISTS `t_documents_due_date`;
CREATE TABLE `t_documents_due_date` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_DOCUMENT` int(11) DEFAULT NULL,
  `DATE` datetime DEFAULT NULL,
  `COMPLETED` tinyint(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_documents_due_date
-- ----------------------------

-- ----------------------------
-- Table structure for `t_documents_signed`
-- ----------------------------
DROP TABLE IF EXISTS `t_documents_signed`;
CREATE TABLE `t_documents_signed` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USER` int(11) NOT NULL,
  `ID_DOCUMENT` int(11) NOT NULL,
  `DATE_SIGNED` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_documents_signed
-- ----------------------------
INSERT INTO `t_documents_signed` VALUES ('1', '8', '7', '2019-08-22 11:49:14');
INSERT INTO `t_documents_signed` VALUES ('2', '8', '8', '2019-08-22 11:59:45');
INSERT INTO `t_documents_signed` VALUES ('3', '8', '9', '2019-08-22 12:01:47');
INSERT INTO `t_documents_signed` VALUES ('4', '8', '10', '2019-08-22 12:04:40');
INSERT INTO `t_documents_signed` VALUES ('5', '8', '11', '2019-08-22 12:13:41');
INSERT INTO `t_documents_signed` VALUES ('6', '8', '12', '2019-08-22 12:13:51');
INSERT INTO `t_documents_signed` VALUES ('7', '8', '15', '2019-08-22 12:15:08');

-- ----------------------------
-- Table structure for `t_drafts`
-- ----------------------------
DROP TABLE IF EXISTS `t_drafts`;
CREATE TABLE `t_drafts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(255) DEFAULT NULL,
  `BODY` longtext,
  `DOC_TITLE` varchar(255) DEFAULT NULL,
  `DOC_BODY` longtext,
  `DATE_CREATED` datetime DEFAULT NULL,
  `LAST_EDIT` datetime DEFAULT NULL,
  `TO` varchar(255) DEFAULT NULL,
  `CC` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_drafts
-- ----------------------------
INSERT INTO `t_drafts` VALUES ('1', 'jkjhkjlh', 'kjhkjhkjh', 'hjkjhkj', 'hjkjhk', null, null, null, null);

-- ----------------------------
-- Table structure for `t_notifications`
-- ----------------------------
DROP TABLE IF EXISTS `t_notifications`;
CREATE TABLE `t_notifications` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USER` int(11) DEFAULT NULL,
  `ID_TYPE` int(11) NOT NULL,
  `ID_DOC` int(11) DEFAULT NULL,
  `DATE` datetime NOT NULL,
  `SEEN` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_notifications
-- ----------------------------
INSERT INTO `t_notifications` VALUES ('1', '9', '1', '2', '2019-07-23 12:25:37', '0');
INSERT INTO `t_notifications` VALUES ('2', '8', '2', '3', '2019-07-23 12:25:37', '0');
INSERT INTO `t_notifications` VALUES ('3', '9', '1', '5', '2019-08-22 11:29:50', '0');
INSERT INTO `t_notifications` VALUES ('4', '8', '2', '6', '2019-08-22 11:29:50', '0');
INSERT INTO `t_notifications` VALUES ('5', '1', '3', '6', '2019-08-22 11:49:14', '0');
INSERT INTO `t_notifications` VALUES ('6', '1', '3', '6', '2019-08-22 11:59:45', '0');
INSERT INTO `t_notifications` VALUES ('7', '1', '3', '6', '2019-08-22 12:01:47', '0');
INSERT INTO `t_notifications` VALUES ('8', '1', '3', '6', '2019-08-22 12:04:40', '0');
INSERT INTO `t_notifications` VALUES ('9', '9', '1', '14', '2019-08-22 12:15:00', '0');

-- ----------------------------
-- Table structure for `t_status`
-- ----------------------------
DROP TABLE IF EXISTS `t_status`;
CREATE TABLE `t_status` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_DOCUMENT` int(11) NOT NULL,
  `STATUS` int(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_status
-- ----------------------------
INSERT INTO `t_status` VALUES ('1', '2', '2');
INSERT INTO `t_status` VALUES ('2', '5', '2');
INSERT INTO `t_status` VALUES ('3', '14', '2');

-- ----------------------------
-- Table structure for `t_turned`
-- ----------------------------
DROP TABLE IF EXISTS `t_turned`;
CREATE TABLE `t_turned` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MOTIVO` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_turned
-- ----------------------------

-- ----------------------------
-- Table structure for `t_users`
-- ----------------------------
DROP TABLE IF EXISTS `t_users`;
CREATE TABLE `t_users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(255) DEFAULT NULL,
  `NAMES` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `MAIDEN_NAME` varchar(255) DEFAULT NULL,
  `CURP` varchar(255) DEFAULT NULL,
  `RFC` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `DEPENDENCIA` int(11) DEFAULT NULL,
  `UNIDAD_ADMINISTRATIVA` int(11) DEFAULT NULL,
  `SUBUNIDAD_ADMINISTRATIVA` int(11) DEFAULT NULL,
  `PUESTO` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_users
-- ----------------------------
INSERT INTO `t_users` VALUES ('1', 'superadmin', 'S.A. Pruebín', 'Provakov', 'Prestibald', 'PPPDPD', 'PDPDPDPDPDP', 'admin', 'test@test.com', '1', '7', '3', 'Algo');
INSERT INTO `t_users` VALUES ('2', 'admin', 'A. Pustulio', 'Clarencio', 'Malvanoda', 'MPC12345', 'MPC12345JRR', 'admin', 'ggggggg@gmail.com', '2', '1', '2', 'Algo');
INSERT INTO `t_users` VALUES ('3', null, '', '', '', 'RUTR720120HDFDRN01', 'RUTR720120HDFDRN01', 'admin', 'rutr@gmail.com', '0', '0', '0', 'Algo');
INSERT INTO `t_users` VALUES ('8', 'mitzy', 'Mitzy Anahí', 'Macias', 'Garza', 'MAGM880908MTSCRT09', 'mitan', 'mitzy', 'mitzy.macias@nuevoleon.gob.mx', '1', '5', '1', 'Directora de Tecnologías');
INSERT INTO `t_users` VALUES ('9', 'paco', 'Jesús Francisco', 'Caballero', 'Jiménez', 'CAJJ841104HVZBMS06', 'jefran', 'paco', '', '3', '4', '4', 'Paco');
INSERT INTO `t_users` VALUES ('13', 'secre', 'Secretaria de', 'Mitzy', 'o Paco', 'SEMO190826MNLCTPZ7', null, 'secre', 'd', '3', '3', '3', 'Asistente');
INSERT INTO `t_users` VALUES ('14', 'arturo', 'Arturo Felipe', 'López', 'Rodríguez', 'LORA681206HNLPDR04', null, 'arturo', 'arturo.lopez@nuevoleon.gob.mx', null, null, null, null);
INSERT INTO `t_users` VALUES ('15', 'julian', 'Julián', 'Quiroga', 'Almaguer', 'QUAJ650709HNLRLL05', null, 'julian', 'julian.quiroga@nuevoleon.gob.mx', null, null, null, null);
INSERT INTO `t_users` VALUES ('16', 'carlos', 'Carlos', 'Comsille', 'Villarreal', 'COVC710311HTSMLR06', null, 'carlos', 'carlos.comsille@nuevoleon.gob.mx', null, null, null, null);
INSERT INTO `t_users` VALUES ('17', 'irma', 'Irma Marcela', 'Flores', 'Rodríguez', 'FORI720912MNLLDR02', null, 'irma', 'irma.flores@nuevoleon.gob.mx', null, null, null, null);
INSERT INTO `t_users` VALUES ('18', 'erika', 'Erika Vanesa', 'Cruz', 'Torres', 'CUTE890529MNLRRR03', null, 'erika', 'erika.cruz@nuevoleon.gob.mx', null, null, null, null);
INSERT INTO `t_users` VALUES ('19', 'ramon', 'Ramón', 'López', 'Serrano', '', null, 'ramon', 'ramon.lopez@nuevoleon.gob.mx', null, null, null, null);
INSERT INTO `t_users` VALUES ('20', 'adrian', 'Adrián Raymundo', 'Granados', 'De Anda', 'GAAA850705HNLRND09', null, 'adrian', 'adrian.granados@nuevoleon.gob.mx', null, null, null, null);
INSERT INTO `t_users` VALUES ('21', 'elvia', 'Elvia Ivette', 'Ham', 'Rivas', 'HARE740207MDFMVL03', null, 'elvia', 'ivette.ham@nuevoleon.gob.mx', null, null, null, null);
INSERT INTO `t_users` VALUES ('22', 'rosalinda', 'Rosalinda', 'Reyna', 'Gómez', 'REGR690824MNLYMS04', null, 'rosalinda', 'rosalinda.reyna@nuevoleon.gob.mx', null, null, null, null);
INSERT INTO `t_users` VALUES ('23', 'gloria', 'Gloria Yolanda', 'Garza', 'De León', 'GAL6721210MNLRNL00', null, 'gloria', 'gloria.garza@nuevoleon.gob.mx', null, null, null, null);
INSERT INTO `t_users` VALUES ('24', 'celina', 'Celina Edith', 'Pérez', 'Mireles', 'PEMC760612MNLRRL04', null, 'celina', 'celina.perez@nuevoleon.gob.mx', null, null, null, null);
INSERT INTO `t_users` VALUES ('25', 'liliana', 'Liliana Leticia', 'Daniel', 'Quintanilla', 'DAQL820206MNLNNL02', null, 'liliana', 'liliana.daniel@nuevoleon.gob.mx', null, null, null, null);
INSERT INTO `t_users` VALUES ('26', 'assistA', 'Asistente', 'A', 'A', 'AAAAAAAAAAAAAAAAAAAA', null, 'assistA', 'a', '4', '4', '3', 'Asistente');
INSERT INTO `t_users` VALUES ('28', 'assC', 'Asistente', 'C', 'C', 'CCCCCCCCCCCCCCCC', null, 'assC', 'c', '4', '3', '7', 'Asistente');
INSERT INTO `t_users` VALUES ('29', 'f', 'f', 'f', 'f', 'f', null, 'f', 'f', '5', '4', '3', 'f');

-- ----------------------------
-- Table structure for `_plataforma`
-- ----------------------------
DROP TABLE IF EXISTS `_plataforma`;
CREATE TABLE `_plataforma` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SLOGAN` varchar(255) DEFAULT NULL,
  `INITIAL_DATE` datetime DEFAULT NULL,
  `FINAL_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _plataforma
-- ----------------------------
INSERT INTO `_plataforma` VALUES ('1', 'La Nueva Dependencia', '2019-06-02 16:11:04', '2019-06-18 16:13:38');
INSERT INTO `_plataforma` VALUES ('2', 'La Nueva Dependencia 2', '2019-06-18 16:13:38', '2019-06-18 16:14:11');
INSERT INTO `_plataforma` VALUES ('3', 'La Nueva Dependencia 3', '2019-06-18 16:14:11', '2019-06-26 12:42:16');
INSERT INTO `_plataforma` VALUES ('4', 's', '2019-06-26 12:42:16', '2019-06-26 12:42:42');
INSERT INTO `_plataforma` VALUES ('5', 'ss', '2019-06-26 12:42:42', '2019-06-26 12:44:49');
INSERT INTO `_plataforma` VALUES ('6', 'pikachu', '2019-06-26 12:44:49', '2019-09-18 14:33:49');
INSERT INTO `_plataforma` VALUES ('7', 'La Nueva Dependencia', '2019-09-18 14:33:49', '2019-09-20 16:51:07');
INSERT INTO `_plataforma` VALUES ('8', '2019, AÑO DE LA LUCHA CONTRA LA VIOLENCIA HACIA LAS MUJERES', '2019-09-20 16:51:07', '2019-09-20 16:52:19');
INSERT INTO `_plataforma` VALUES ('9', '\"2019, AÑO DE LA LUCHA CONTRA LA VIOLENCIA HACIA LAS MUJERES\"', '2019-09-20 16:52:19', null);
