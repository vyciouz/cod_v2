
<?php
class Mail_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload if needed
	public function inbox($ID_user){
	    $query = $this->db->query("SELECT
        new_t_mail_threads.ID,
        new_t_mail_threads.SUB_THREAD_KEY,
        CONCAT(sender.NAMES, ' ',sender.LAST_NAME, ' ' ,sender.MAIDEN_NAME) AS Remitente,
        new_t_mail.`SUBJECT` AS Asunto,
        new_t_mail.BODY AS Correo,
        new_t_mail_threads.LAST_UPDATED AS Recibido
        FROM
        new_t_mail_threads
        INNER JOIN new_t_mail ON new_t_mail_threads.ID_LAST_MAIL = new_t_mail.ID
        LEFT JOIN t_users AS sender ON new_t_mail.`FROM` = sender.ID
        WHERE
        new_t_mail_threads.ID_THREAD_OWNER = $ID_user AND
        NOT new_t_mail.`FROM` = 0
        ;");
	    $result = $query->result_array();
	    return $result;
    }

    public function delegatedInbox($ID_user){
        $query = $this->db->query("SELECT
        t_users.`NAMES`,
        t_users.LAST_NAME,
        t_users.ID,
        Count(new_t_mail_threads.`READ`) AS NEW_MAIL
        FROM
        r_procuration
        INNER JOIN t_users ON r_procuration.ID_REPRESENTED = t_users.ID
        LEFT JOIN new_t_mail_threads ON t_users.ID = new_t_mail_threads.ID_THREAD_OWNER
        WHERE
        r_procuration.ID_REPRESENTATIVE = $ID_user AND
        new_t_mail_threads.READ = 0
        GROUP BY
        t_users.`NAMES`,
        t_users.LAST_NAME        
        ;");
	    $result = $query->result_array();
	    return $result;
    }

    public function sent($ID_user){
	    $query = $this->db->query("SELECT
        new_t_mail_threads.SUB_THREAD_KEY,
        new_t_mail.`SUBJECT` AS `Título`,
        new_t_mail.BODY AS Cuerpo,
        new_t_mail.ID
        FROM
        new_t_mail_threads
        INNER JOIN new_t_mail ON new_t_mail_threads.SUB_THREAD_KEY = new_t_mail.SUB_THREAD_KEY
        LEFT JOIN t_users AS sender ON new_t_mail.`FROM` = sender.ID
        WHERE
        new_t_mail_threads.ID_THREAD_OWNER = $ID_user AND
        new_t_mail.`FROM` = 0
        ;");
	    $result = $query->result_array();
	    return $result;
    }

    public function subthread($subThreadKey){
	    $query = $this->db->query("SELECT
        new_t_mail.`SUBJECT`,
        new_t_mail.BODY,
        new_t_mail.ID,
        new_t_mail.`FROM`,
        CONCAT(sender.NAMES, ' ',sender.LAST_NAME, ' ' ,sender.MAIDEN_NAME) AS Remitente
        FROM
        new_t_mail_threads
        INNER JOIN new_t_mail ON new_t_mail_threads.SUB_THREAD_KEY = new_t_mail.SUB_THREAD_KEY
        LEFT JOIN t_users AS sender ON new_t_mail.`FROM` = sender.ID
        WHERE
        new_t_mail_threads.SUB_THREAD_KEY = '$subThreadKey'
        ;");
	    $result = $query->result_array();
	    return $result;
    }

    public function lastThreadUpdate($subThreadKey){
	    $query = $this->db->query("SELECT
        new_t_mail_threads.LAST_UPDATED,
        new_t_mail_threads.ID_LAST_MAIL,
        new_t_mail.`SUBJECT`
        FROM
        new_t_mail_threads
        INNER JOIN new_t_mail ON new_t_mail_threads.ID_LAST_MAIL = new_t_mail.ID
        WHERE
        new_t_mail_threads.SUB_THREAD_KEY = '$subThreadKey'
        ;");
	    $result = $query->row_array();
	    return $result;
    }
    
}
?>