<?php
class Dependencies_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload if needed
	public function get_dependencies(){
	    $query = $this->db->query("SELECT * FROM cat_dependencias ORDER BY NAME ASC;");
	    $result = $query->result_array();
	    return $result;
	}

	public function get_dependency_by_id($id){
		$query = $this->db->query("SELECT * FROM cat_dependencias WHERE ID = $id;");
	    $result = $query->result_array();
	    return $result;
    }
}
?>