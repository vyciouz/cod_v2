
<?php
class Notifications_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload if needed
	public function get_alerts($ID_user){
	    $query = $this->db->query("SELECT
        t_documents.CURRENT_OWNER,
        t_documents.ID,
        t_documents.TITLE,
        t_status.`STATUS`,
        t_documents_due_date.ID_DOCUMENT,
        t_documents_due_date.DATE,
        cat_status.NOTIFICATION AS notification,
        cat_status.ALERT AS alert,
        cat_status.ICON AS icon
        FROM
        t_documents
        INNER JOIN t_documents_due_date ON t_documents.ID = t_documents_due_date.ID_DOCUMENT
        INNER JOIN t_status ON t_documents_due_date.ID_DOCUMENT = t_status.ID_DOCUMENT
        INNER JOIN cat_status ON t_status.`STATUS` = cat_status.ID
        WHERE
                COMPLETED = 0 AND
                t_documents.CURRENT_OWNER = $ID_user AND
                t_documents_due_date.DATE BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 DAY)
        ORDER BY
                t_documents_due_date.DATE ASC
        
        ;");
	    $result = $query->result_array();
	    return $result;
    }
    
    public function get_dated_documents($ID_user){
	    $query = $this->db->query("SELECT
        t_documents.CURRENT_OWNER,
        t_documents.ID,
        t_documents.TITLE,
        t_status.`STATUS`,
        t_documents_due_date.ID_DOCUMENT,
        t_documents_due_date.DATE,
        cat_status.NOTIFICATION AS notification,
        cat_status.ALERT AS alert,
        cat_status.ICON AS icon
        FROM
        t_documents
        INNER JOIN t_documents_due_date ON t_documents.ID = t_documents_due_date.ID_DOCUMENT
        INNER JOIN t_status ON t_documents_due_date.ID_DOCUMENT = t_status.ID_DOCUMENT
        INNER JOIN cat_status ON t_status.`STATUS` = cat_status.ID
        WHERE
        COMPLETED = 0 AND
        t_documents.CURRENT_OWNER = $ID_user
        ORDER BY
        t_documents_due_date.DATE ASC
        ;");
	    $result = $query->result_array();
	    return $result;
    }

    public function Notifications($ID_user){
        $query = $this->db->query("SELECT
        t_notifications.ID_USER,
        t_notifications.ID_TYPE,
        t_notifications.ID_DOC,
        t_notifications.DATE,
        cat_types_notification.DESCRIPTION,
        cat_types_notification.ICON
        FROM
        t_notifications
        INNER JOIN cat_types_notification ON t_notifications.ID_TYPE = cat_types_notification.ID
        WHERE
        t_notifications.ID_USER = $ID_user
        
        ;");
        $result = $query->result_array();
	    return $result;
	}
}
?>