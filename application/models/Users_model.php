<?php
class Users_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload if needed

	public function get_users(){
	    $query = $this->db->query("SELECT
		t_users.`NAMES` AS Nombres,
		t_users.LAST_NAME AS `Primer Apellido`,
		t_users.MAIDEN_NAME AS `Segundo Apellido`,
		-- t_users.CURP AS CURP,
		t_users.EMAIL AS `e-mail`,
		t_users.PUESTO AS Puesto,
		cat_dependencias.`NAME` AS Dependencia,
		cat_u_administrativas.`NAME` AS `U. Administrativa`,
		cat_sub_u_administrativas.`NAME` AS `S. Administrativa`,
		t_users.ID
		FROM
		t_users
		LEFT JOIN cat_sub_u_administrativas ON t_users.SUBUNIDAD_ADMINISTRATIVA = cat_sub_u_administrativas.ID
		LEFT JOIN cat_u_administrativas ON t_users.UNIDAD_ADMINISTRATIVA = cat_u_administrativas.ID
		LEFT JOIN cat_dependencias ON t_users.DEPENDENCIA = cat_dependencias.ID		
		");
	    $result = $query->result_array();
	    return $result;
	}

	public function get_user_by_id($id){
		$query = $this->db->query("SELECT * FROM t_users WHERE ID = $id;");
	    $result = $query->result_array();
	    return $result;
    }
    
    public function get_user_by_curp($curp){
		$query = $this->db->query("SELECT * FROM t_users WHERE CURP = '$curp';");
	    $result = $query->result_array();
	    return $result;
    }
    
    public function get_user_by_rfc($rfc){
		$query = $this->db->query("SELECT * FROM t_users WHERE RFC = '$rfc';");
	    $result = $query->result_array();
	    return $result;
	}

	public function get_user_by_names($names){
		$query = $this->db->query("SELECT * FROM t_users WHERE NAMES LIKE '$names%';");
	    $result = $query->result_array();
	    return $result;
	}

	public function get_users_by_dependency($dep){
		$query = $this->db->query("SELECT * FROM t_users WHERE DEPENDENCIA = $dep;");
	    $result = $query->result_array();
	    return $result;
	}

	public function get_5_users_by_ua($ua){
		$query = $this->db->query("SELECT * FROM t_users WHERE UNIDAD_ADMINISTRATIVA = $ua;");
	    $result = $query->result_array();
	    return $result;
	}

	public function get_5_users_by_sua($sua){
		$query = $this->db->query("SELECT * FROM t_users WHERE SUBUNIDAD_ADMINISTRATIVA = $sua;");
	    $result = $query->result_array();
	    return $result;
	}

	public function get_5_users_by_dependency($dep){
		$query = $this->db->query("SELECT * FROM t_users WHERE DEPENDENCIA = $dep;");
	    $result = $query->result_array();
	    return $result;
	}

	public function get_5_users(){
		$query = $this->db->query("SELECT * FROM t_users LIMIT 5;");
	    $result = $query->result_array();
	    return $result;
	}

	public function editors($ID_Draft){
		$query = $this->db->query("SELECT
			t_users.`NAMES`,
			t_users.LAST_NAME,
			t_users.MAIDEN_NAME,
			CONCAT(t_users.`NAMES`,' ' ,SUBSTRING(t_users.LAST_NAME,1,1), '. ' ,SUBSTRING(t_users.MAIDEN_NAME,1,1), '.') AS Editor
			FROM
			r_drafts
			INNER JOIN t_users ON r_drafts.ID_EDITOR = t_users.ID
			WHERE
			r_drafts.ID_DRAFT = $ID_Draft"
		);
		$result = $query->result_array();
	    return $result;
	}

	public function assistants(){
		$query = $this->db->query("SELECT
		t_users.`NAMES` AS `Nombre(s)`,
		t_users.LAST_NAME AS `Primer Apellido`,
		t_users.MAIDEN_NAME AS `Segundo Apellido`,
		t_users.ID,
		cat_dependencias.`NAME` AS Dependencia
		FROM
		t_users
		INNER JOIN r_user_roles ON t_users.ID = r_user_roles.ID_USER
		INNER JOIN cat_dependencias ON t_users.DEPENDENCIA = cat_dependencias.ID
		WHERE
		r_user_roles.ID_ROLE = 7		
		");
		$result = $query->result_array();
		return $result;
	}

	public function procuration($id){
		$query = $this->db->query("SELECT
		t_users.`NAMES`,
		t_users.LAST_NAME,
		t_users.MAIDEN_NAME,
		t_users.ID,
		CONCAT(t_users.`NAMES`, ' ', t_users.LAST_NAME) as fullName 
		FROM
		r_procuration
		INNER JOIN t_users ON r_procuration.ID_REPRESENTED = t_users.ID
		WHERE
		r_procuration.ID_REPRESENTATIVE = $id
		ORDER BY
		t_users.`NAMES` ASC
		");
		$result = $query->result_array();
		return $result;
	}
}
?>