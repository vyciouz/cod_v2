<?php
class Status_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload if needed
	public function get_statuses(){
	    $query = $this->db->get("cat_status");
	    $result = $query->result_array();
	    return $result;
    }
    
    public function before_sent_statuses(){
		$this->db->order_by("DESC", "ASC");
	    $result = $this->db->get_where('cat_status', array('BEFORE_SENT =' => 1))->result_array();
	    // $result = $query->result_array();
	    return $result;
	}

	public function after_sent_statuses($id){
		$query = $this->db->query("SELECT * FROM cat_roles WHERE ID = $id;");
	    $result = $query->result_array();
	    return $result;
    }

    public function get_roles_by_user($id){
		$query = $this->db->query("SELECT * FROM r_user_roles WHERE ID_USER = $id;");
	    $result = $query->result_array();
	    return $result;
    }
}
?>