<?php
class Adminsubunit_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload if needed
	public function get_sub_units(){
	    $query = $this->db->query("SELECT
		cat_sub_u_administrativas.ID,
		cat_sub_u_administrativas.`NAME` AS Nombre,
		cat_sub_u_administrativas.NOMENCLATURE AS Nomenclatura,
		cat_dependencias.`NAME` AS Dependencia,
		cat_u_administrativas.`NAME` AS `U. Administrativa`
		FROM
		cat_sub_u_administrativas
		INNER JOIN cat_dependencias ON cat_sub_u_administrativas.ID_DEPENDENCIA = cat_dependencias.ID
		INNER JOIN cat_u_administrativas ON cat_sub_u_administrativas.ID_UA = cat_u_administrativas.ID		
		");
	    $result = $query->result_array();
	    return $result;
    }
    
    public function get_sub_unit_by_id($id){
	    $query = $this->db->query("SELECT * FROM cat_sub_u_administrativas WHERE ID = $id;");
	    $result = $query->result_array();
	    return $result;
	}
}
?>