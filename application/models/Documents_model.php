<?php
class Documents_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}
	
	public function drafts($ID_owner){
		$query = $this->db->query("SELECT
		new_t_documents.ID,
		new_t_documents.TITLE AS `Título`,
		new_t_documents.BODY AS `Texto/Descripción`
		FROM
		new_t_document_draft
		INNER JOIN new_t_documents ON new_t_document_draft.ID_DOCUMENT = new_t_documents.ID
		WHERE
		new_t_document_draft.ID_EDITOR = $ID_owner
		");
	    $result = $query->result_array();
	    return $result;
	}

	public function getAll(){
		$query = $this->db->query("SELECT
		new_t_documents.ID,
		new_t_documents.TITLE AS `Título`,
		new_t_documents.BODY AS `Texto/Descripción`,
		new_t_documents.`ID_AUTHOR` AS `Autor`,
		new_t_documents.`OWNER`,
		new_t_documents.FOLIO AS Folio,
		new_t_documents.DATE_CREATED AS `Fecha creado`,
		CONCAT(t_users.NAMES,' ' ,t_users.LAST_NAME) AS `Dueño`
		FROM
		new_t_documents
		INNER JOIN t_users ON new_t_documents.`OWNER` = t_users.ID
		");
	    $result = $query->result_array();
	    return $result;
	}

	public function receivedDocuments($ID_owner){
		$query = $this->db->query("SELECT
		new_t_document_status.THREAD,
		new_t_document_status.ID,
		new_t_document_status.`STATUS`,
		new_t_documents.TITLE
		FROM
		new_t_document_status
		INNER JOIN new_t_documents ON new_t_document_status.ID_HEAD_DOCUMENT = new_t_documents.ID
		WHERE
		new_t_document_status.THREAD_OWNER = $ID_owner
		");
		$result = $query->result_array();
	    return $result;
	}

	public function myDocuments($ID_user){
		$query = $this->db->query("SELECT
			new_t_documents.ID,
			new_t_documents.TITLE AS `Título`,
			new_t_documents.BODY AS Texto,
			new_t_documents.FOLIO AS Folio,
			new_t_documents.DATE_CREATED AS `Fecha Creación`
			FROM
			new_t_documents
			WHERE
			new_t_documents.`OWNER` = $ID_user
			ORDER BY
			new_t_documents.TITLE ASC
		");
	    $result = $query->result_array();
	    return $result;
	}

	public function signedDocument($digitalSignature){
		$query = $this->db->query("SELECT
		new_t_documents_signed.USER_NAME,
		new_t_documents_signed.SIGNED_DATE,
		new_t_documents_signed.DIGITAL_SIGNATURE,
		new_t_documents.TITLE
		FROM
		new_t_documents_signed
		INNER JOIN new_t_documents ON new_t_documents_signed.ID_DOCUMENT = new_t_documents.ID
		WHERE
		new_t_documents_signed.DIGITAL_SIGNATURE = '$digitalSignature'
		
		");
	    $result = $query->row_array();
	    return $result;
	}

	public function myDocumentsSigned($ID_user){
		$query = $this->db->query("SELECT
		new_t_documents.`OWNER`,
		new_t_documents_signed.ID_DOCUMENT AS `ID_DOC`,
		new_t_documents_signed.ID AS `ID_SIG`,
		new_t_documents.TITLE AS `Título`,
		new_t_documents.`ID_AUTHOR` AS `Autor`,
		new_t_documents.BODY AS Texto,
		new_t_documents.FOLIO AS Folio
		FROM
		new_t_documents_signed
		INNER JOIN new_t_documents ON new_t_documents_signed.ID_DOCUMENT = new_t_documents.ID
		WHERE
		new_t_documents.`OWNER` = $ID_user
		
		");
	    $result = $query->result_array();
	    return $result;
	}

	public function tracking($ID_user){
		$query = $this->db->query("SELECT
		new_t_document_status.ID,
		new_t_document_status.SUB_THREAD,
		new_t_documents.TITLE AS `Título`
		FROM
		new_t_document_status
		INNER JOIN new_t_documents ON new_t_document_status.ID_HEAD_DOCUMENT = new_t_documents.ID
		WHERE
		new_t_document_status.ID_THREAD_OWNER = $ID_user		
		");
	    $result = $query->result_array();
	    return $result;
	}

	public function getDocument($ID_document){
		$query = $this->db->query("SELECT
			new_t_documents.TITLE AS `Título`,
			new_t_documents.BODY AS Texto,
			new_t_documents.FOLIO_2 AS Folio,
			new_t_documents.FOR,
			new_t_documents.CC,
			new_t_documents.OWNER,
			new_t_documents.`ID_AUTHOR` AS `Autor`,
			new_t_documents.DATE_CREATED AS `Fecha Creación`,
			new_t_documents.ID
			FROM
			new_t_documents
			WHERE
			new_t_documents.ID = $ID_document
		");
	    $result = $query->row_array();
	    return $result;
	}
}
?>