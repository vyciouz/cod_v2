<?php
class Roles_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload if needed
	public function get_roles(){
	    $query = $this->db->query("SELECT * FROM cat_roles ORDER BY NAME ASC;");
	    $result = $query->result_array();
	    return $result;
    }
    
    public function get_roles_no_admin(){
	    $query = $this->db->query("SELECT * FROM cat_roles WHERE NOT ID = 1 AND NOT ID = 2 ORDER BY NAME ASC;");
	    $result = $query->result_array();
	    return $result;
	}

	public function get_role_by_id($id){
		$query = $this->db->query("SELECT * FROM cat_roles WHERE ID = $id;");
	    $result = $query->result_array();
	    return $result;
    }

    public function get_roles_by_user($id){
		$query = $this->db->query("SELECT * FROM r_user_roles WHERE ID_USER = $id;");
	    $result = $query->result_array();
	    return $result;
    }
}
?>