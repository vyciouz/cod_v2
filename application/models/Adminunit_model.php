<?php
class Adminunit_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload if needed
	public function get_units(){
	    $query = $this->db->query("SELECT
		cat_u_administrativas.ID,
		cat_u_administrativas.`NAME` AS Nombre,
		cat_u_administrativas.NOMENCLATURE AS Nomenclatura,
		cat_dependencias.`NAME` AS Dependencia,
		cat_u_administrativas.ADDRESS AS `Dirección`,
		cat_u_administrativas.TELEPHONE_1 AS `Tel. 1`,
		cat_u_administrativas.TELEPHONE_2 AS `Tel. 2`,
		cat_u_administrativas.TELEPHONE_3 AS `Tel. 3`
		FROM
		cat_u_administrativas
		INNER JOIN cat_dependencias ON cat_u_administrativas.ID_DEPENDENCIA = cat_dependencias.ID		
		");
	    $result = $query->result_array();
	    return $result;
    }
    
    public function get_unit_by_id($id){
	    $query = $this->db->query("SELECT * FROM cat_u_administrativas WHERE ID = $id;");
	    $result = $query->result_array();
	    return $result;
	}
}
?>