<?php
class Admin_unit extends CI_Controller{
    /*
    Controlador para acciones correspondientes al usuario personal.
    */
    public function index()
	{
		#Page Information
        $data['title'] = "Inicio";
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		$this->load->view('home/login');
		$this->load->view('templates/footer');
    }


    public function delete($id){
        $this->load->database();
		$data['table'] = "cat_u_administrativas";
		$data['id'] = $id;
		$data['redirect'] = "index.php/admin_units/list";
		$this->load->view('templates/delete',$data);
	}

    public function new()
	{
        $this->load->database();
        # Camcpos a editar
        $data['columnsToUse'] = [
            "NAME" => [
                'name' => "Nombre",
                'type' => 'text',
                'dependency' => null
            ],
            'ID_DEPENDENCIA' => [
                'name' => "Dependencia",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_dependencias",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ]
            ],
            "NOMENCLATURE" => [
                'name' => "Nomenclatura",
                'type' => 'text',
                'dependency' => null
            ],
            "ADDRESS" => [
                'name' => "Dirección",
                'type' => 'text',
                'dependency' => null
            ],
            "TELEPHONE_1" => [
                'name' => "Teléfono 1",
                'type' => 'text',
                'dependency' => null
            ],
            "TELEPHONE_2" => [
                'name' => "Teléfono 2",
                'type' => 'text',
                'dependency' => null
            ],
            "TELEPHONE_3" => [
                'name' => "Teléfono 3",
                'type' => 'text',
                'dependency' => null
            ]
        ];
        $data['formAction'] = "";
        $data['table'] = "cat_u_administrativas";
        $data['controllerName'] = "admin_units";
		#Page Information
        $data['title'] = "Editar información de usuario";
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
        # Sólo mostrar si se edita el usuario con sesión
        # iniciada o si es un admin
		$this->load->view('templates/new',$data);
		$this->load->view('templates/footer');
    }

    public function view($id)
	{
        $this->load->model('adminunit_model');
        $this->load->model('users_model');
        $data['id'] = $id;
        $data['users'] = $this->users_model->get_5_users_by_ua($id);
        $data['dependency'] = $this->adminunit_model->get_unit_by_id($id);
        $data['columnsToUse'] = [
            "NAME" => [
                'name' => "Nombres",
                'type' => 'text',
                'dependency' => null
            ],
            "NOMENCLATURE" => [
                'name' => "Nomenclatura",
                'type' => 'text',
                'dependency' => null
            ]
        ];
		#Page Information
        $data['title'] = $data['dependency'][0]['NAME'];
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		$this->load->view('admin/admin_unit',$data);
		$this->load->view('templates/footer');
    }

    public function edit($id)
	{
        $this->load->database();
        # Camcpos a editar
        $this->load->model('adminunit_model');
        $data['ua'] = $this->adminunit_model->get_unit_by_id($id);
        $data['id'] = $id;
        $data['editFields'] = [
            "NAME" => [
                'name' => "Nombre",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['ua'][0]['NAME']
            ],
            'ID_DEPENDENCIA' => [
                'name' => "Dependencia",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_dependencias",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ],
                'val' => $data['ua'][0]['ID_DEPENDENCIA']
            ],
            "NOMENCLATURE" => [
                'name' => "Nomenclatura",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['ua'][0]['NOMENCLATURE']
            ],
            "ADDRESS" => [
                'name' => "Dirección",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['ua'][0]['ADDRESS']
            ],
            "TELEPHONE_1" => [
                'name' => "Teléfono 1",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['ua'][0]['TELEPHONE_1']
            ],
            "TELEPHONE_2" => [
                'name' => "Teléfono 2",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['ua'][0]['TELEPHONE_2']
            ],
            "TELEPHONE_3" => [
                'name' => "Teléfono 3",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['ua'][0]['TELEPHONE_3']
            ]
        ];
        $data['table'] = "cat_u_administrativas";
        
		#Page Information
        $data['title'] = "Editar información de usuario";
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
        # Sólo mostrar si se edita el usuario con sesión
        # iniciada o si es un admin
		$this->load->view('templates/edit',$data);
		$this->load->view('templates/footer');
    }

    public function logos($id){
        #Page Information
        $data['title'] = "Editar logos";
        $data['id']  = $id;
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
		$this->load->view('unidad_administrativa/images',$data);
		$this->load->view('templates/footer');
    }


}
?>