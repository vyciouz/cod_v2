<?php
class Dependencies extends CI_Controller{
    /*
    Controlador para acciones correspondientes al usuario personal.
    */
    public function index()
	{
		#Page Information
        $data['title'] = "Documentos";
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		
		$this->load->view('templates/footer');
    }

    public function list(){
        $this->load->model('dependencies_model');
        $data['dataArray'] = $this->dependencies_model->get_dependencies();
        $data['index'] = 'ID';
        $data['orderByCol'] = 1;
		$data['controllerName'] = 'dependency';
        $data['title'] = "Listado de dependecias";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('templates/list',$data);
		$this->load->view('templates/footer');
    }

    public function new()
    {
        #Page Information
        $data['title'] = "Nueva dependecia";
        $data['columnsToUse'] = [
            "NAME" => [
                'name' => "Nombre",
                'type' => 'text',
                'dependency' => null
            ]
        ];
        $data['table'] = "cat_dependencias";
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
        $this->load->view('templates/new');
        $this->load->view('templates/footer');
    }
    
}
?>