<?php
class General extends CI_Controller{

    public function admin(){
        $this->load->database();
        $data['title'] = "Administración Dependencia";
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
        $this->load->view('templates/getUserData',$data);
        $this->load->view('admin/general_front');
        $this->load->view('templates/footer');

    }

    public function admin_back($method){
        $data['method'] = $method;
        $this->load->view('admin/general_back',$data);
    }
}
?>