<?php
class Restrict extends CI_Controller{

    public function index()
    {
        try {
            #Page Information
            $data['title'] = "Acceso Denegado";
            $this->load->view('templates/_utils');
            $this->load->view('templates/header',$data);
 
            $this->load->view('templates/restrict',$data);
            $this->load->view('templates/footer');
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

}
?>