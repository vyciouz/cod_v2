<?php
class Documents extends CI_Controller{
    /*
    Controlador para acciones correspondientes al usuario personal.
    */
    public function index()
	{
		#Page Information
        $data['title'] = "Documentos";
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		
		$this->load->view('templates/footer');
    }

    public function all(){
        $this->load->model('documents_model');
        $data['documentsType'] = "all";
        $data['index'] = 'ID';
        $data['orderByCol'] = 0;
		$data['controllerName'] = 'document';
        $data['title'] = "Listado de documentos";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('document/list',$data);
		$this->load->view('templates/footer');
    }

    

    public function signed_documents(){
        $this->load->model('documents_model');
        $data['dataArray'] = $this->documents_model->documents();
        $data['index'] = 'ID';
        $data['orderByCol'] = 0;
		$data['controllerName'] = 'document';
        $data['title'] = "Listado de documentos";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('templates/list',$data);
		$this->load->view('templates/footer');
    }

    
    
    public function new(){
        $this->load->model('users_model');
        $data['users'] = $this->users_model->get_users();
        $data['title'] = "Nuevo Documento";
		$this->load->view('templates/header',$data);
		$this->load->view('document/new');
		$this->load->view('templates/footer');
    }

    public function receivedDocuments(){
        $this->load->model('documents_model');
        $data['documentsType'] = "received";
        $data['index'] = 'ID';
        $data['orderByCol'] = 0;
		$data['controllerName'] = 'document';
        $data['title'] = "Documentos Recibidos";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('document/list',$data);
		$this->load->view('templates/footer');
    }

    public function myDocuments(){
        $this->load->model('documents_model');
        $data['documentsType'] = "personal";
        $data['index'] = 'ID';
        $data['orderByCol'] = 0;
		$data['controllerName'] = 'document';
        $data['title'] = "Listado de documentos";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('document/list',$data);
		$this->load->view('templates/footer');
    }

    public function mySignedDocuments(){
        $this->load->model('documents_model');
        $data['documentsType'] = "signed";
        $data['index'] = 'ID';
        $data['orderByCol'] = 0;
		$data['controllerName'] = 'document';
        $data['title'] = "Listado de documentos";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('document/list',$data);
		$this->load->view('templates/footer');
    }

    public function myDrafts(){
        $this->load->model('documents_model');
        $data['documentsType'] = "drafts";
		$data['controllerName'] = 'document';
        $data['title'] = "Listado de borradores";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('document/list',$data);
		$this->load->view('templates/footer');
    }

    public function tracking(){
        $this->load->database();
        $data['title'] = "Seguimiento";
        $data['documentsType'] = "tracking";
        $data['controllerName'] = 'document';
        
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		$this->load->view('document/list',$data);
		$this->load->view('templates/footer');
    }

    
    

    public function upload_document(){
        $this->load->database();
        $this->load->model('users_model');
        if (!empty($_POST) && !empty($_FILES)) {
            $data['currentUser'] = $this->users_model->get_user_by_id($_POST['ID_OWNER']);
            $data['title'] = "Subiendo PDF";
        }else{
            $data['title'] = "ERROR";
        }
        
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		$this->load->view('document/upload_pdf',$data);
		$this->load->view('templates/footer');
    }
}
?>