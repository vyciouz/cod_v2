<?php
class Admin_subunits extends CI_Controller{
    /*
    Controlador para acciones correspondientes al usuario personal.
    */
    public function index()
	{
		#Page Information
        $data['title'] = "Inicio";
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		$this->load->view('home/login');
		$this->load->view('templates/footer');
    }

    public function list(){
        $this->load->model('adminsubunit_model');
        $data['dataArray'] = $this->adminsubunit_model->get_sub_units();
        $data['controllerName'] = 'admin_subunit';
        $data['index'] = 'ID';
        $data['orderByCol'] = 0;
        $data['title'] = "Listado de Subunidades Administrativas";
        //Extra CSS needed
		$data['css'] = [
            "libraries/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "libraries/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('templates/list',$data);
		$this->load->view('templates/footer');
    }

    public function logos($id){
        #Page Information
        $data['title'] = "Editar logos";
        $data['id']  = $id;
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
		$this->load->view('dependencies/images',$data);
		$this->load->view('templates/footer');
    }


}
?>