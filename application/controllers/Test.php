<?php
class Test extends CI_Controller{
    /*
    Controlador para acciones correspondientes al usuario personal.
    */
    public function list(){
        
        $data['title'] = "Configuración de usuario";
        $data['eJS'] = [
            "assets/plugins/jQuery-slimScroll-master/jquery.slimscroll.js",
		];
        $this->load->view('templates/header',$data);
		$this->load->view('tests/scroll_bar');
        $this->load->view('templates/footer');
        
    }

}
?>