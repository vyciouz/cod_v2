<?php
class Document extends CI_Controller{
    /*
    Controlador para acciones correspondientes al usuario personal.
    */
    public function index()
	{
		#Page Information
        $data['title'] = "Documentos";
        $this->load->view('templates/getUserData');
		$this->load->view('templates/header',$data);
		
		$this->load->view('templates/footer');
    }

    /*
    Función que verifica la validez de un documento firmado.
    */
    public function validate($digtalSignature){
        $this->load->database();
        $this->load->model('documents_model');
        $data['document'] = $this->documents_model->signedDocument($digtalSignature);
        $data['title'] = "Información documento";
        $this->load->view('templates/getUserData');
        $this->load->view('document/validate',$data);
    }

    public function view($ID_document){
        $this->load->database();
        $this->load->model('documents_model');
        $data['docID'] = $ID_document;
        $data['document'] = $this->documents_model->getDocument($ID_document);
        $data['title'] = "Información documento";
        $this->load->view('templates/header',$data);
        $this->load->view('templates/getUserData');
        $this->load->view('document/new_view',$data);
		$this->load->view('templates/footer');
    }

    public function view_back($id){
        $this->load->view('document/view_back',$data);
    }

    public function read($ID_document, $ID_signature = null){
        $this->load->model('documents_model');
        $data['ID_signature'] = $ID_signature;
        $data['docID'] = $ID_document;
        $data['document'] = $this->documents_model->getDocument($ID_document);
        $this->load->view('templates/getUserData');
        $this->load->view('document/read',$data);
    }

    public function new(){
        $this->load->model('users_model');
        $data['mode'] = "new";
        $data['users'] = $this->users_model->get_users();
        $data['title'] = "Nuevo Documento";
        $data['css'] = [
            "assets/plugins/select2/css/select2.min.css",
            // "assets/scss/_forms.scss",
            // "assets/scss/_reboot.scss"
        ];
        $data['eJS'] = [
            "assets/plugins/select2/js/select2.min.js"
        ];
		$this->load->view('templates/header',$data);
		$this->load->view('document/new_front');
		$this->load->view('templates/footer');
    }

    public function new_back($method){
        $this->load->database();
        $data['method'] = $method;
        $this->load->view('templates/getUserData');
        $this->load->view('document/new_back', $data);
    }

    public function new_upload(){
        $this->load->model('users_model');
        
        $data['users'] = $this->users_model->get_users();
        $data['title'] = "Nuevo Documento";
        $data['css'] = [
            "assets/plugins/select2/css/select2.min.css",
            // "assets/scss/_forms.scss",
            // "assets/scss/_reboot.scss"
        ];
        $data['eJS'] = [
            "assets/plugins/select2/js/select2.min.js"
        ];
		$this->load->view('templates/header',$data);
		$this->load->view('document/upload_front');
		$this->load->view('templates/footer');
    }
    
    public function upload_back(){
        $this->load->database();
        $data['title'] = "Nuevo Documento";
        $this->load->view('templates/getUserData');
		$this->load->view('document/upload_back',$data);
    }

    public function sign($id, $procurator = null){
        $this->load->model('documents_model');
        $data['ID_Document'] = $id;
        $data['procurator'] = $procurator;
        $data['document'] = $this->documents_model->getDocument($id);
        $data['title'] = "Firma de documento";
		$this->load->view('templates/header',$data);
        $this->load->view('document/sign_front',$data);
		$this->load->view('templates/footer');
    }

    public function sign_back($id, $method = null){
        $this->load->database();
        $this->load->model('documents_model');
        $data['document'] = $this->documents_model->getDocument($id);
        $data['ID_Document'] = $id;
        $data['method'] = $method;
        $this->load->view('templates/_utils');
        $this->load->view('templates/getUserData');
        $this->load->view('document/sign_back',$data);

    }

    public function create_back($method, $ID_Draft = null){
        $this->load->database();
        // $this->load->view('templates/getUserData');
        $data['method'] = $method;
        $data['ID_Draft'] = $ID_Draft;
        $this->load->view('templates/getUserData',$data);
		$this->load->view('document/create_back', $data);
    }

    public function track($thread = null){
        $this->load->database();
        $data['title'] = "Seguimiento";
        $data['sub_thread'] = $thread;
        $data['documentsType'] = "tracking";
        $data['controllerName'] = 'document';
        
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		$this->load->view('document/track_front', $data);
		$this->load->view('templates/footer');
    }

    public function draft($ID_document = null){
        $this->load->model('documents_model');
        $data['document'] = $this->documents_model->getDocument($ID_document);

        $data['mode'] = "edit";
        $data['ID_document'] = $ID_document;
        
        $data['title'] = "Editar Borrador";
        $data['css'] = [
            "assets/plugins/select2/css/select2.min.css",
            // "assets/scss/_forms.scss",
            // "assets/scss/_reboot.scss"
        ];
        $data['eJS'] = [
            "assets/plugins/select2/js/select2.min.js"
        ];
		$this->load->view('templates/header', $data);
		$this->load->view('document/new_front', $data);
		$this->load->view('templates/footer');
    }

    public function draft_back($method, $ID_Draft){
		#Page Information
        $this->load->database();
        $data['method'] = $method;
        $data['ID_Draft'] = $ID_Draft;
        $this->load->view('templates/getUserData',$data);
		$this->load->view('document/new_back', $data);
    }

    
}
?>