<?php
class Users extends CI_Controller{
    /*
    Controlador para acciones correspondientes al los usuarios en general.
    */

    public function list(){
        $this->load->model('users_model');
        $data['dataArray'] = $this->users_model->get_users();
        $data['index'] = 'ID';
        $data['orderByCol'] = 1;
		$data['controllerName'] = 'user';
        $data['title'] = "Listado de usuarios";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('templates/list',$data);
		$this->load->view('templates/footer');
    }


    public function json_get_users(){
        $this->load->model('users_model');
        $data['users'] = $this->users_model->get_users();
        $this->load->view('json_response/users',$data);
    }

    public function json_get_users_by_name($names){
        $this->load->model('users_model');
        $data['users'] = $this->users_model->get_user_by_names($names);
        $this->load->view('json_response/users',$data);
    }

    public function json_get_users_by_name_test(){
        $this->load->database();
        $this->load->model('users_model');
        $this->load->view('json_response/users_test');
    }

    public function json_users($method){
        $this->load->database();
        $data['method'] = $method;
        $this->load->model('users_model');
        $this->load->view('json_response/users', $data);
    }
}
?>