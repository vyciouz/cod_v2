<?php
class JsonResponse extends CI_Controller{

    public function jsonMyDocuments(){
        session_start();
        $this->load->database();
        $ID_user = $_SESSION['ID'];
        $query = $this->db->query("SELECT
        new_t_documents.ID,
        new_t_documents.TITLE,
        new_t_documents.BODY ,
        new_t_documents.FOLIO,
        new_t_documents.DATE_CREATED
        FROM
        new_t_documents
        WHERE
        new_t_documents.`OWNER` = $ID_user
        ORDER BY
        new_t_documents.TITLE ASC
        ");
	    $result = $query->result_array();
	    echo json_encode($result);
    }

    public function jsonMySignedDocuments(){
        session_start();
        $this->load->database();
        $ID_user = $_SESSION['ID'];
        $query = $this->db->query("SELECT
        new_t_documents.TITLE,
        new_t_documents_signed.ID,
        new_t_documents_signed.DIGITAL_SIGNATURE,
        new_t_documents_signed.USER_NAME,
        new_t_documents_signed.ID_DOCUMENT
        FROM
        new_t_documents_signed
        INNER JOIN new_t_documents ON new_t_documents_signed.ID_DOCUMENT = new_t_documents.ID
        WHERE
		new_t_documents.`OWNER` = $ID_user
		");
	    $result = $query->result_array();
	    echo json_encode($result);
    }

    public function jsonMyDocumentsArgs($args){
        session_start();
        $this->load->database();
        $ID_user = $_SESSION['ID'];
        $query = $this->db->query("SELECT
        new_t_documents.ID,
        new_t_documents.TITLE,
        new_t_documents.BODY ,
        new_t_documents.FOLIO,
        new_t_documents.DATE_CREATED
        FROM
        new_t_documents
        WHERE
        new_t_documents.`OWNER` = $ID_user
        AND new_t_documents.TITLE LIKE '%$args%'
        ORDER BY
        new_t_documents.TITLE ASC
        ");
        $result = $query->result_array();
	    echo json_encode($result);
    }

    public function jsonMySignedDocumentsArgs($args){
        session_start();
        $this->load->database();
        $ID_user = $_SESSION['ID'];
        $query = $this->db->query("SELECT
        new_t_documents.TITLE,
        new_t_documents_signed.ID,
        new_t_documents_signed.DIGITAL_SIGNATURE,
        new_t_documents_signed.USER_NAME
        FROM
        new_t_documents_signed
        INNER JOIN new_t_documents ON new_t_documents_signed.ID_DOCUMENT = new_t_documents.ID
        WHERE
        new_t_documents.`OWNER` = $ID_user
        AND new_t_documents.TITLE LIKE '%$args%'
        ORDER BY
        new_t_documents.TITLE ASC
        ");
        $result = $query->result_array();
	    echo json_encode($result);
    }

    public function jsonUsersArgs($args){
        session_start();
        $this->load->database();
        $ID_user = $_SESSION['ID'];
        $query = $this->db->query("SELECT
        t_users.ID,
        t_users.LAST_NAME,
        t_users.MAIDEN_NAME,
        t_users.`NAMES`
        FROM `t_users`
        WHERE
        (t_users.`NAMES` LIKE '%$args%' OR
        t_users.LAST_NAME LIKE '%$args%' OR
        t_users.MAIDEN_NAME LIKE '%$args%') 
        -- AND t_users.ID <> $ID_user
        ORDER BY NAMES ASC
        ");
        $result = $query->result_array();
	    echo json_encode($result);
    }

    public function jsonStatuses(){
        $this->load->database();
        $ID_user = $_SESSION['ID'];
        $query = $this->db->query("SELECT
        cat_status.ID,
        cat_status.`DESC`
        FROM `cat_status`
        WHERE
        cat_status.BEFORE_SENT = 1        
        ");
        $result = $query->result_array();
	    echo json_encode($result);
    }

    public function admin_back($method){
        $data['method'] = $method;
        $this->load->view('admin/general_back',$data);
    }
}
?>