<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['title'] = "Inicio";
		$this->load->view('home/login_front_1');
	}

	public function user()
	{
		$data['title'] = "Inicio";
		$this->load->view('home/login_front_2');
	}

	// Login backend: Credentials
	public function lb_c()
	{
		$this->load->database();
		$data['title'] = "Inicio";
		$this->load->view('templates/_utils');
		$this->load->view('home/login_back_1');
	}

	// Login backend: Username
	public function lb_u()
	{
		$this->load->database();
		$data['title'] = "Inicio";
		$this->load->view('home/login_back_2');
	}

	public function logout(){
		$data['title'] = "Cerrando sesión";
		$this->load->view('home/logout',$data);
	}
}

