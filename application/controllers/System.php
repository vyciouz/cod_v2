<?php
class System extends CI_Controller{
    /*
    Controlador para acciones correspondientes al usuario personal.
    */

    public function index(){
        $this->load->database();
		#Page Information
        $data['title'] = "System";
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		$this->load->view('system/dashboard_front',$data);
		$this->load->view('templates/footer');
    }

    public function dashboard_back($method){
        $data['method'] = $method;
        $this->load->view('system/dashboard_back',$data);
    }
    // Borra documentos y registro de documentos
    public function delete_all_documents(){
        
    }

    // Borra solamente el registro de los documentos
    public function delete_document_registry(){
        
    }

    public function error_404(){
		#Page Information
        $data['heading'] = "404";
        $data['message'] = "Página no encontrada";
		$this->load->view('errors/cli/error_404', $data);
    }

    public function check_u(){
        $this->load->view('json_response/check_unique');
    }
}
?>