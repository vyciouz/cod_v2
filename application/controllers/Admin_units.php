<?php
class Admin_units extends CI_Controller{
    /*
    Controlador para acciones correspondientes al usuario personal.
    */
    public function index()
	{
		#Page Information
        $data['title'] = "Inicio";
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		$this->load->view('home/login');
		$this->load->view('templates/footer');
    }

    public function list(){
        $this->load->model('adminunit_model');
        $data['dataArray'] = $this->adminunit_model->get_units();
        $data['index'] = 'ID';
        $data['orderByCol'] = 0;
		$data['controllerName'] = 'admin_unit';
        $data['title'] = "Listado de Unidades Administrativas";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('templates/list',$data);
		$this->load->view('templates/footer');
    }

    public function edit($id)
	{
        $this->load->database();
        # Camcpos a editar
        $this->load->model('adminunit_model');
        $data['ua'] = $this->adminunit_model->get_unit_by_id($id);
        $data['id'] = $id;
        $data['editFields'] = [
            "NAME" => [
                'name' => "Nombre",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['ua'][0]['NAME']
            ],
            'ID_DEPENDENCIA' => [
                'name' => "Dependencia",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_dependencias",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ],
                'val' => $data['ua'][0]['ID_DEPENDENCIA']
            ],
            "NOMENCLATURE" => [
                'name' => "Nomenclatura",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['ua'][0]['NOMENCLATURE']
            ],
            "ADDRESS" => [
                'name' => "Dirección",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['ua'][0]['ADDRESS']
            ],
            "TELEPHONE_1" => [
                'name' => "Teléfono 1",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['ua'][0]['TELEPHONE_1']
            ],
            "TELEPHONE_2" => [
                'name' => "Teléfono 2",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['ua'][0]['TELEPHONE_2']
            ],
            "TELEPHONE_3" => [
                'name' => "Teléfono 3",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['ua'][0]['TELEPHONE_3']
            ]
        ];
        $data['table'] = "cat_u_administrativas";
        
		#Page Information
        $data['title'] = "Editar información de usuario";
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
        # Sólo mostrar si se edita el usuario con sesión
        # iniciada o si es un admin
		$this->load->view('templates/edit',$data);
		$this->load->view('templates/footer');
    }

    


}
?>