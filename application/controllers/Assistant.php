<?php
class User extends CI_Controller{
    /*
    Controlador para acciones correspondientes al usuario personal.
    */
    public function index()
	{
		#Page Information
        $data['title'] = "Inicio";
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		$this->load->view('home/login');
		$this->load->view('templates/footer');
    }

    public function edit_info($id){
        $this->load->model('users_model');
        $this->load->model('roles_model');
        $data['userRoles'] = $this->roles_model->get_roles_by_user($id);
        $data['roles'] = $this->roles_model->get_roles_no_admin();
        $data['user'] = $this->users_model->get_user_by_id($id);
        $data['id'] = $id;
        $data['editFields'] = [
            "NAMES" => [
                'name' => "Nombres",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['user'][0]['NAMES']
            ],
            'LAST_NAME' => [
                'name' => "Apellido paterno",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['user'][0]['LAST_NAME']
            ],
            'MAIDEN_NAME' => [
                'name' => "Apellido materno",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['user'][0]['MAIDEN_NAME']
            ],
            'DEPENDENCIA' => [
                'name' => "Dependencia",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_dependencias",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ],
                'val' => $data['user'][0]['DEPENDENCIA']
            ],
            'UNIDAD_ADMINISTRATIVA' => [
                'name' => "Unidad Administrativa",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_u_administrativas",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ],
                'val' => $data['user'][0]['UNIDAD_ADMINISTRATIVA']
            ],
            'SUBUNIDAD_ADMINISTRATIVA' => [
                'name' => "Subunidad Administrativa",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_sub_u_administrativas",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ],
                'val' => $data['user'][0]['SUBUNIDAD_ADMINISTRATIVA']
            ],
            'PUESTO' => [
                'name' => "Puesto",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['user'][0]['PUESTO']
            ],
            'EMAIL' => [
                'name' => "Correo electrónico",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['user'][0]['EMAIL']
            ]
        ];
        #Page Information
        $data['title'] = "Editar información de usuario";
        // $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
        # Sólo mostrar si se edita el usuario con sesión
        # iniciada o si es un admin
		$this->load->view('user/edit',$data);
		$this->load->view('templates/footer');

    }


    public function edit_roles($id){
        $this->load->database();
        $data['id'] = $id;
        $data['title'] = "Editar información de usuario";
        $this->load->view('templates/header',$data);
        # Sólo mostrar si se edita el usuario con sesión
        # iniciada o si es un admin
		$this->load->view('user/edit_roles',$data);
		$this->load->view('templates/footer');
    }

    public function edit($id){
        $this->load->database();
        # Campos a editar
        $this->load->model('users_model');
        $data['user'] = $this->users_model->get_user_by_id($id);
        $data['id'] = $id;
        $data['editFields'] = [
            "NAMES" => [
                'name' => "Nombres",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['user'][0]['NAMES']
            ],
            'LAST_NAME' => [
                'name' => "Apellido paterno",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['user'][0]['LAST_NAME']
            ],
            'MAIDEN_NAME' => [
                'name' => "Apellido materno",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['user'][0]['MAIDEN_NAME']
            ],
            'DEPENDENCIA' => [
                'name' => "Dependencia",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_dependencias",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ],
                'val' => $data['user'][0]['DEPENDENCIA']
            ],
            'UNIDAD_ADMINISTRATIVA' => [
                'name' => "Unidad Administrativa",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_u_administrativas",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ],
                'val' => $data['user'][0]['UNIDAD_ADMINISTRATIVA']
            ],
            'SUBUNIDAD_ADMINISTRATIVA' => [
                'name' => "Subunidad Administrativa",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_sub_u_administrativas",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ],
                'val' => $data['user'][0]['SUBUNIDAD_ADMINISTRATIVA']
            ],
            'PUESTO' => [
                'name' => "Puesto",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['user'][0]['PUESTO']
            ],
            'EMAIL' => [
                'name' => "Correo electrónico",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['user'][0]['EMAIL']
            ],
            'PASSWORD' => [
                'name' => "Contraseña",
                'type' => 'password',
                'dependency' => null,
                'val' => $data['user'][0]['PASSWORD']
            ],
        ];
        $data['table'] = "t_users";
        
		#Page Information
        $data['title'] = "Editar información de usuario";
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
        # Sólo mostrar si se edita el usuario con sesión
        # iniciada o si es un admin
		$this->load->view('templates/edit',$data);
		$this->load->view('templates/footer');
    }

    public function new(){
        $this->load->database();
		#Page Information
        $data['title'] = "Nuevo usuario";
        $this->load->view('templates/header',$data);
		$this->load->view('user/new_front',$data);
		$this->load->view('templates/footer');
    }

    public function new_back(){
        $this->load->view('user/new_back');
    }

    public function delete($id){
        $this->load->database();
		$data['table'] = "t_users";
		$data['id'] = $id;
		$data['redirect'] = "index.php/users/list";
		$this->load->view('templates/delete',$data);
    }

    public function settings(){
        $this->load->database();
        $data['title'] = "Configuración de usuario";
        $this->load->view('templates/header',$data);
		$this->load->view('user/settings_front');
		$this->load->view('templates/footer');
    }

    public function settings_back($method){
        $this->load->database();
        $data['method'] = $method;
		$this->load->view('user/settings_back',$data);
    }
    
    public function profile($id = 0){
        // Arreglar esto, quitar session_start de aquí
        session_start();
        $this->load->model('documents_model');
        $this->load->model('users_model');
        $this->load->model('roles_model');
        // Arreglar esto, quitar id de aquí
        $id = (empty($id)) ? $_SESSION['ID'] : $id;
        $data['roles'] = $this->roles_model->get_roles_by_user($id);
        $data['user'] = $this->users_model->get_user_by_id($id);
        $data['myDocuments'] = $this->documents_model->my_documents_small($id);
        $data['inbox'] = $this->documents_model->inbox_small($id);
        $data['outbox'] = $this->documents_model->outbox_small($id);
        $data['catalogDependency'] = array(
			"SENT_FROM"=>array(
				"TABLE"=>"t_users","searchInCOL"=>"ID","resultInCOL"=>"NAMES"
            )
        );
        $data['title'] = "Perfil";
        $this->load->view('templates/header',$data);
        $this->load->view('templates/getUserData');
        $this->load->view('user/profile',$data);
		$this->load->view('templates/footer');
    }

    public function upload_certificate(){
        $data['title'] = "OK";
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
        # Sólo mostrar si se edita el usuario con sesión
        # iniciada o si es un admin
		$this->load->view('user/upload_certificate');
		$this->load->view('templates/footer');
    }


    public function my_documents(){
        $data['mail'] = "personal";
        $data['columnsToUse'] = [
            'TITLE'=>'Título',
            'SENT_FROM'=>"Remitente",
            'DATE_RECEIVED'=>"Fecha",
            // 'DATE_RECEIVED'=>"Fecha de Recibido"
        ];
		$data['catalogDependency'] = array(
			"ORIGINAL_OWNER"=>array(
				"TABLE"=>"t_users","searchInCOL"=>"ID","resultInCOL"=>"NAMES"
            ),
            "CURRENT_OWNER"=>array(
				"TABLE"=>"t_users","searchInCOL"=>"ID","resultInCOL"=>"NAMES"
            ),
            "SENT_FROM"=>array(
				"TABLE"=>"t_users","searchInCOL"=>"ID","resultInCOL"=>"NAMES"
            )
        );
		$data['controllerName'] = 'document';
        $data['title'] = "Mis documentos";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
            "assets/css/tables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('user/documents',$data);
		$this->load->view('templates/footer');
    }
    public function inbox_2(){
        $data['mail'] = "inbox";
        $this->load->view('templates/header',$data);
        $this->load->view('user/documents',$data);
		$this->load->view('templates/footer');
    }

    public function inbox(){
        $data['mail'] = "inbox";
        $data['columnsToUse'] = [
            'TITLE'=>'Título',
            'ORIGINAL_OWNER'=>'Dueño original',
            'SENT_FROM'=>"Remitente",
            'CURRENT_OWNER'=>"Dueño actual",
            'DATE_CREATED'=>"Fecha de Creación"
        ];
		$data['catalogDependency'] = array(
			"ORIGINAL_OWNER"=>array(
				"TABLE"=>"t_users","searchInCOL"=>"ID","resultInCOL"=>"NAMES"
            ),
            "CURRENT_OWNER"=>array(
				"TABLE"=>"t_users","searchInCOL"=>"ID","resultInCOL"=>"NAMES"
            ),
            "SENT_FROM"=>array(
				"TABLE"=>"t_users","searchInCOL"=>"ID","resultInCOL"=>"NAMES"
            )
        );
		$data['controllerName'] = 'document';
        $data['title'] = "Bandeja de Entrada";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('user/documents',$data);
		$this->load->view('templates/footer');
    }



   



}
?>