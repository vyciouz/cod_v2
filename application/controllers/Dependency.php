<?php
class Dependency extends CI_Controller{
    /*
    Controlador para acciones correspondientes al usuario personal.
    */
    public function index()
	{
		#Page Information
        $data['title'] = "Documentos";
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		
		$this->load->view('templates/footer');
    }

    public function view($id)
	{
        $this->load->model('dependencies_model');
        $this->load->model('users_model');
        $data['id'] = $id;
        $data['users'] = $this->users_model->get_5_users_by_dependency($id);
        $data['dependency'] = $this->dependencies_model->get_dependency_by_id($id);
        $data['columnsToUse'] = [
            "NAME" => [
                'name' => "Nombres",
                'type' => 'text',
                'dependency' => null
            ],
            "NOMENCLATURE" => [
                'name' => "Nomenclatura",
                'type' => 'text',
                'dependency' => null
            ]
        ];
        #Page Information
        if ($data['dependency']) {
            $data['title'] = $data['dependency'][0]['NAME'];
        } else {
            $data['title'] = "N/A";
        }
        $this->load->view('templates/_utils');
        $this->load->view('templates/getUserData');
		$this->load->view('templates/header',$data);
		$this->load->view('admin/dependency_front',$data);
		$this->load->view('templates/footer');
    }

    public function new(){
        $this->load->database();
        # Camcpos a editar
        $data['table'] = "cat_dependencias";
        $data['columnsToUse'] = [
            "NAME" => [
                'name' => "Nombres",
                'type' => 'text',
                'dependency' => null
            ],
            "NOMENCLATURE" => [
                'name' => "Nomenclatura",
                'type' => 'text',
                'dependency' => null
            ]
        ];
        $data['controllerName']  = "dependencies";
		#Page Information
        $data['title'] = "Nueva dependencia";
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		$this->load->view('templates/new');
		$this->load->view('templates/footer');
    }

    public function dashboard(){
        $data['title'] = "Administración Dependencia";
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
        $this->load->view('templates/getUserData',$data);
        $this->load->view('admin/dependency_front');
        $this->load->view('templates/footer');
    }

    public function dependency_back($method, $ID_Dep){
        $data['title'] = "Administración Dependencia";
        $data['method'] = $method;
        $data['ID_Dep'] = $ID_Dep;
        $this->load->view('admin/dependency_back', $data);
    }
    
    public function delete($id){
        $this->load->database();
		$data['table'] = "cat_dependencias";
		$data['id'] = $id;
		$data['redirect'] = "index.php/dependencies/list";
		$this->load->view('templates/delete',$data);
    }

    public function edit($id){
        $this->load->database();
        # Camcpos a editar
        $this->load->model('dependencies_model');
        $data['dependency'] = $this->dependencies_model->get_dependency_by_id($id);
        $data['id'] = $id;
        $data['editFields'] = [
            "NAME" => [
                'name' => "Nombres",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['dependency'][0]['NAME']
            ],
            'NOMENCLATURE' => [
                'name' => "Nomenclatura",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['dependency'][0]['NOMENCLATURE']
            ]
        ];
        $data['table'] = "cat_dependencias";
        $data['action'] = null;
        $data['redirect'] = null;
        
		#Page Information
        $data['title'] = "Editar información de usuario";
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
        # Sólo mostrar si se edita el usuario con sesión
        # iniciada o si es un admin
		$this->load->view('templates/edit',$data);
		$this->load->view('templates/footer');
    }
    
}
?>

