<?php
class Admin_subunit extends CI_Controller{
    /*
    Controlador para acciones correspondientes al usuario personal.
    */
    public function index()
	{
		#Page Information
        $data['title'] = "Inicio";
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		$this->load->view('home/login');
		$this->load->view('templates/footer');
    }


    public function delete($id){
        $this->load->database();
		$data['table'] = "cat_sub_u_administrativas";
		$data['id'] = $id;
		$data['redirect'] = "index.php/admin_subunits/list";
		$this->load->view('templates/delete',$data);
	}

    public function new()
	{
        $this->load->database();
        # Camcpos a editar
        $data['columnsToUse'] = [
            "NAME" => [
                'name' => "Nombre",
                'type' => 'text',
                'dependency' => null
            ],
            "NOMENCLATURE" => [
                'name' => "Nomenclatura",
                'type' => 'text',
                'dependency' => null
            ],
            'ID_DEPENDENCIA' => [
                'name' => "Dependencia",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_dependencias",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ]
            ],
            'ID_UA' => [
                'name' => "Unidad Administrativa",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_u_administrativas",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ]
            ]
        ];
        $data['formAction'] = "";
        $data['table'] = "cat_sub_u_administrativas";
        $data['controllerName'] = "admin_subunits";
		#Page Information
        $data['title'] = "Nueva Subunidad Administrativa";
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
        # Sólo mostrar si se edita el usuario con sesión
        # iniciada o si es un admin
		$this->load->view('templates/new',$data);
		$this->load->view('templates/footer');
    }

    public function edit($id)
	{
        $this->load->database();
        # Camcpos a editar
        $this->load->model('adminsubunit_model');
        $data['sua'] = $this->adminsubunit_model->get_sub_unit_by_id($id);
        $data['id'] = $id;
        $data['editFields'] = [
            "NAME" => [
                'name' => "Nombre",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['sua'][0]['NAME']
            ],
            "NOMENCLATURE" => [
                'name' => "Nomenclatura",
                'type' => 'text',
                'dependency' => null,
                'val' => $data['sua'][0]['NOMENCLATURE']
            ],
            'ID_DEPENDENCIA' => [
                'name' => "Dependencia",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_dependencias",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ],
                'val' => $data['sua'][0]['ID_DEPENDENCIA']
            ],
            'ID_UA' => [
                'name' => "Unidad Administrativa",
                'type' => 'select',
                'dependency' => [
                    "table" => "cat_u_administrativas",
                    "inCol" => "ID",
                    "outCol" => "NAME"
                ],
                'val' => $data['sua'][0]['ID_UA']
            ]
        ];
        $data['table'] = "cat_sub_u_administrativas";
        
		#Page Information
        $data['title'] = "Editar información de usuario";
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
        # Sólo mostrar si se edita el usuario con sesión
        # iniciada o si es un admin
		$this->load->view('templates/edit',$data);
		$this->load->view('templates/footer');
    }

    public function view($id)
	{
        $this->load->model('adminsubunit_model');
        $this->load->model('users_model');
        $data['id'] = $id;
        $data['users'] = $this->users_model->get_5_users_by_sua($id);
        $data['dependency'] = $this->adminsubunit_model->get_sub_unit_by_id($id);
        $data['columnsToUse'] = [
            "NAME" => [
                'name' => "Nombres",
                'type' => 'text',
                'dependency' => null
            ],
            "NOMENCLATURE" => [
                'name' => "Nomenclatura",
                'type' => 'text',
                'dependency' => null
            ]
        ];
		#Page Information
        $data['title'] = $data['dependency'][0]['NAME'];
        $this->load->view('templates/_utils');
		$this->load->view('templates/header',$data);
		$this->load->view('admin/admin_subunit',$data);
		$this->load->view('templates/footer');
    }

    public function logos($id){
        #Page Information
        $data['title'] = "Editar logos";
        $data['id']  = $id;
        $this->load->view('templates/_utils');
        $this->load->view('templates/header',$data);
		$this->load->view('dependencies/images',$data);
		$this->load->view('templates/footer');
    }


}
?>