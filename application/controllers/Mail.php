<?php
class Mail extends CI_Controller{
    /*
    Controlador para acciones correspondientes al usuario personal.
    */
  
    public function compose(){
		#Page Information
        $this->load->model('users_model');
        $this->load->model('status_model');
        $data['statuses']   = $this->status_model->before_sent_statuses();
        $data['users']      = $this->users_model->get_users();
        $data['title']      = "Nuevo Correo 📧";
        $data['css'] = [
            "assets/plugins/select2/css/select2.min.css",
            // "assets/scss/_forms.scss",
            // "assets/scss/_reboot.scss"
        ];
        $data['eJS'] = [
            "assets/plugins/select2/js/select2.min.js"
        ];
        $this->load->view('templates/header',$data);
        $this->load->view('mail/compose_front copy', $data);
        $this->load->view('templates/footer');
    }

    public function compose_back(){
        $this->load->database();
        $this->load->view('mail/compose_back copy');
    }

    public function inbox(){
        $data['mail'] = "inbox";
        $this->load->model('mail_model');
        $data['title'] = "Bandeja de Entrada";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('mail/list',$data);
		$this->load->view('templates/footer');
    }

    public function d_inbox($delegated_user){
        $data['mail'] = "d_inbox";
        $data['d_user'] = $delegated_user;
        $this->load->model('mail_model');
        $data['title'] = "Bandeja de Entrada";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('mail/list',$data);
		$this->load->view('templates/footer');
    }

    public function sent(){
        $data['mail'] = "sent";
        $this->load->model('mail_model');
        $data['title'] = "Bandeja de Entrada";
        //Extra CSS needed
		$data['css'] = [
            "assets/plugins/datatables/datatables.css",
		];
		//Extra JS needed
		$data['eJS'] = [
            "assets/plugins/datatables/datatables.js",
		];
		$this->load->view('templates/header',$data);
        $this->load->view('mail/list',$data);
		$this->load->view('templates/footer');
    }

    public function read($subThreadKey){
        $this->load->model('mail_model');
        
        $data['thread']     = $this->mail_model->subthread($subThreadKey);
        $data['lastUpdate'] = $this->mail_model->lastThreadUpdate($subThreadKey);
        $data['title']      = "Bandeja de Entrada";
		$this->load->view('templates/header',$data);
        $this->load->view('mail/read_front',$data);
		$this->load->view('templates/footer');
    }

    

    
}
?>