<?php
// print_r($thread);

?>

<div class="content-wrapper">							
    <div class="content">							
        <div class="row">
            
            <div class="col-lg-12">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2>
                            <?= $lastUpdate['SUBJECT']?>
                        </h2>
                    </div>
                    <div class="card-body">
                        <div id="accordion2" class="accordion accordion-shadow">

                        <?php printThread($thread); ?>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    sendBack(".reply-form", "<?= base_url('index.php/mail/reply')?>");
});

function sendBack(form, url, button, subThread){
    $(form).on('submit', function(e){
        var url = url;
        var form = new FormData(this);
        for (var value of form.values()) {
            console.log(value); 
        }
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            beforeSend : function(data){
                var buttonLoading = 
                '<div class="sk-circle" style= "margin: auto">'
                    + '<div class="sk-circle1 sk-child"></div>'
                    + '<div class="sk-circle2 sk-child"></div>'
                    + '<div class="sk-circle3 sk-child"></div>'
                    + '<div class="sk-circle4 sk-child"></div>'
                    + '<div class="sk-circle5 sk-child"></div>'
                    + '<div class="sk-circle6 sk-child"></div>'
                    + '<div class="sk-circle7 sk-child"></div>'
                    + '<div class="sk-circle8 sk-child"></div>'
                    + '<div class="sk-circle9 sk-child"></div>'
                    + '<div class="sk-circle10 sk-child"></div>'
                    + '<div class="sk-circle11 sk-child"></div>'
                    + '<div class="sk-circle12 sk-child"></div>'
                    + '</div>';
                $("#create").html(buttonLoading);
                $("#create").attr("disabled", true);
            },
            success: function(data){
                <?php $url = base_url("index.php/documents/mydocuments/") ?>
                if (data.result) {
                    window.location.href = "<?php echo $url?>";    
                }
            },
            complete: function(){
            },
            error: function(e){
            }
        });
    });
}
</script>
<?php

function printThread($thread){
    $ci =& get_instance();

    foreach ($thread as $key => $value):
        $queryStringFindMail = "SELECT
        new_t_documents.TITLE,
        new_t_documents.ID
        FROM
        new_t_document_mail
        INNER JOIN new_t_documents ON new_t_document_mail.ID_DOCUMENT = new_t_documents.ID
        WHERE
        new_t_document_mail.ID_MAIL = " . $thread[$key]['ID'];
        $queryFindMail = $ci->db->query($queryStringFindMail);
	    $resultFindMail = $queryFindMail->result_array();
        
        $name = ($thread[$key]['Remitente']) ? $thread[$key]['Remitente'] : "Yo" ;
        $attachmentsDirectory = "./data/userdata/" . $_SESSION['ID'] . "/mail/" . $thread[$key]['ID'] . "/";
        $fileList = (is_dir($attachmentsDirectory)) ? Attachments($attachmentsDirectory) : false ;
              
    ?>
        <div class="card">
            <div class="card-header" id="<?= "header_" . $thread[$key]['ID'] ?>">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#<?= "collapse_" . $thread[$key]['ID'] ?>" aria-expanded="false" aria-controls="<?= "collapse_" . $thread[$key]['ID'] ?>">
                    
                    <?= $name . ":"?>
                </button>
            </div>

            <div id="<?= "collapse_" . $thread[$key]['ID'] ?>" class="collapse" aria-labelledby="<?= "header_" . $thread[$key]['ID'] ?>" data-parent="#accordion2" style="">
                <div class="card-body">
                    <?= $thread[$key]['BODY'] ?>

                    
                    <?php
                        AttachedDocuments($resultFindMail, $thread[$key]['ID']);
                        if ($fileList):
                            ShowAttachments($fileList, $thread[$key]['ID']);
                        endif;?>
                    
                </div>
            </div>
        </div>
        <?php
    endforeach;
    ?>
<?php
}
function AttachedDocuments($resultFindMail, $ID_mail){
    $ci =& get_instance();
    
    if ($resultFindMail) {
        echo "<p> Documentos Oficiales Adjuntos </p>";
        foreach ($resultFindMail as $key => $value) {
            echo "<p>";
            echo "<a href = '" . base_url('index.php/document/view/' . $value['ID']) . "'>";
            echo '<i class="far fa-file"></i> ' .  $value['TITLE'];
            echo "</a>";
            echo "</p>";
            $querySignedDocument = $ci->db->query("SELECT
            new_t_mail_signatures.ID_MAIL,
            new_t_documents_signed.USER_NAME,
            new_t_documents_signed.ID
            FROM
            new_t_mail_signatures
            INNER JOIN new_t_documents_signed ON new_t_mail_signatures.ID_DIGITAL_SIGNATURE = new_t_documents_signed.ID
            WHERE
            new_t_mail_signatures.ID_MAIL = $ID_mail
            ");
            $resultSignedDocument = $querySignedDocument->result_array();
            foreach ($resultSignedDocument as $key => $document):
                $documentID = $value['ID'];
                $signatureID = $document['ID'];
                echo "<p>";
                echo '<a href = "' . base_url("index.php/document/read/$documentID/$signatureID") . '">';
                echo  '<i class="fas fa-file-signature"></i> ' . $value['TITLE'] . " - " . $document['USER_NAME'];
                echo "</a>";
                echo "</p>";
            endforeach;
            
        }
    }
    

}

function Attachments($dir){
    $files = scandir($dir);
    $fileList = [];
    array_shift($files); #Remove ./
    array_shift($files); #Remove ../
    // print_r($files);
    
    $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type
    foreach ($files as $key => $value) {
        $fileList[$key]['name'] = $value;
        $fileList[$key]['displayName'] = (strlen($value) <= 24) ? $value : substr($value, 0, 14) . "..." . substr($value, -8) ;
        $fileList[$key]['type'] = finfo_file($finfo,  $dir ."/" .$value);
        $string = $fileList[$key]['type'];
        switch (true) {
            case stristr($string, "image"):
                $fileList[$key]['icon'] = '<i class="mdi mdi-file-image font-size-20"></i>';
                break;
            
            default:
                $fileList[$key]['icon'] = '<img src="https://img.icons8.com/material/24/000000/question-mark.png">';
                break;
        }
    }
    
    return $fileList;
}

function ShowAttachments($files, $docID){
    $attachmentsDirectory = "data/userdata/" . $_SESSION['ID'] . "/mail/$docID/";
    ?>
    <hr>
      
    <?php
    foreach ($files as $key => $value):?>
    <div class="col-md-6 col-lg-3 col-xl-3">
        <div class="row">
            <div class="col-md-2">
                <?php echo $value['icon'];?>
            </div>
            <div class="col-md-9">
                <a href="<?= base_url() . $attachmentsDirectory . $value['name']?>">
                    <?php echo $value['displayName'];?>
                </a>
            </div>
        </div>
    </div>
    <?php
    endforeach;
    ?>

<?php
}

?>