<?php
$today = new DateTime(date("Y-m-d"));
?>
<style>
hr{
    margin: auto;
    width : 98%;
}

input[type=date], select{
    /* width: 40%; */
    display: inline-block !important;
}

.hidden{
    display: none;
}
.addDocToList, .remove-this-li{
    color: #669;
    font-weight: 600;
}
.addDocToList:hover{
    background-color: #4a7;
    color: white;
    font-weight: bold;
    border-radius: 5px;
    padding-left: 5px;
    padding-right: 5px;
}
.remove-this-li:hover{
    background-color: #c44;
    color: white;
    font-weight: bold;
    border-radius: 5px;
    padding-left: 5px;
    padding-right: 5px;
}
.removeDT{
    color: gray;
}
.removeDT:hover{
    color: red;
    font-weight: bold;
}
.b-r{
    border-right:1px solid #ccf;
}

textarea{
    overflow:hidden;
    resize:none;
    border: 0
}
html {
    color: -internal-root-color;
}
</style>

<?php
$today = new DateTime(date("Y-m-d"));
?>


<div class="content-wrapper">
    <div class="content">
        <div class="row">
			<div class="col-lg-12">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2>Nuevo Correo</h2>
                    </div>
                    <div class="card-body">
                        <form id = "compose">
                            <div class="form-group">
                                <div class = "pseudo-input" id = "recipient-holder">
                                    <span id = "for">
                                    Para:
                                    </span>
                                    
                                    <div id = "rec_1" style ="display: inline">
                                    </div>
                                    <div id = "rec_2" style ="display: inline">
                                        <input type="text" id ="find-user" class = "crouching-div-hidden-input">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Recipientes"> -->
                                <div class = "pseudo-input" id = "copied-holder">
                                    <span id = "CC">CC:</span>
                                    
                                    <div id = "cc_1" style ="display: inline">
                                    </div>
                                    <div id = "cc_2" style ="display: inline">
                                        <input type="text" id ="cc" class = "crouching-div-hidden-input">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <input type="text" class="form-control" name="subject" id = "subject" placeholder = "Asunto" required >
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" id="body" name = "body" rows="3"></textarea>
                            </div>

                            <div class="form-group row" id = "document-holder">
                                
                            </div>
                            <div class="form-group row" id = "attachments-holder">
                                
                            </div>
                            <div class="form-group">
                                <a href="#" data-toggle="modal" data-target="#exampleModal" style = "font-weight: bold; color: #8a909d">
                                    <i class = "mdi mdi-attachment"></i> Agregar Documento Oficial
                                </a>
                            </div>
                            <div class="form-group">
                                <a href="#" id = "AddAttachmentInput" style = "font-weight: bold; color: #8a909d">
                                    <i class = "mdi mdi-attachment"></i> Adjuntar archivos
                                </a>
                            </div>
                            
                            <div class="form-footer pt-4 pt-5 mt-4 border-top">
                            
                                <button type="submit" id = "send-mail" class="btn btn-primary btn-default">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- <div class="modal fade show" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: block; padding-right: 17px;" aria-modal="true"> -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Compartir Borrador</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Buscar Documento:</label>
                            <input type="text" class="form-control" name="searchDocument" id = "searchDocument" placeholder = "Buscar" >
                        </div>
                        <ul class="list-group" id = "searchResult">
						</ul>
                    </div>
                    <div class="col-lg-6">
                        <div class = "col-12">
                            <div class="form-group">
                                <label for="">Agregados</label>
                                <ul id = "temp-doc-list">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id = "add-documents" type="button" class="btn btn-primary btn-pill">Agregar</button>
                <button type="cancel-add" class="btn btn-danger btn-pill" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>



<script>
$(document).ready(function(){
    
    const form = document.querySelector('form');

   
    
    $("#compose").on('submit', function(e){
        var url = "<?php echo base_url('index.php/mail/compose_back')?>";
        var form = new FormData(this);
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            beforeSend : function(data){
                //$("#preview").fadeOut();
                // $("#err").fadeOut();
                // console.log(data);
                var buttonLoading = '<div class="sk-circle" style= "margin: auto">'
                    + '<div class="sk-circle1 sk-child"></div>'
                    + '<div class="sk-circle2 sk-child"></div>'
                    + '<div class="sk-circle3 sk-child"></div>'
                    + '<div class="sk-circle4 sk-child"></div>'
                    + '<div class="sk-circle5 sk-child"></div>'
                    + '<div class="sk-circle6 sk-child"></div>'
                    + '<div class="sk-circle7 sk-child"></div>'
                    + '<div class="sk-circle8 sk-child"></div>'
                    + '<div class="sk-circle9 sk-child"></div>'
                    + '<div class="sk-circle10 sk-child"></div>'
                    + '<div class="sk-circle11 sk-child"></div>'
                    + '<div class="sk-circle12 sk-child"></div>'
                + '</div>';
                $("#send-mail").html(buttonLoading);
                $("#send-mail").attr("disabled", true);
            },
            success: function(data){
                <?php $url = base_url() . "index.php/user/profile/" . $_SESSION['ID'];?>
                if (data.result) {
                    window.location.href = "<?php echo $url?>";    
                }else{
                    switch (data.error) {
                        case 1:
                            alert(data.msg);
                            break;
                    
                        default:
                            break;
                    }
                }
                
                // console.log(data);
            },
            complete: function(){
                $('#send-mail').removeAttr("disabled");
                $("#send-mail").html("Enviar");
            },
            error: function(e){
                // $("#err").html(e).fadeIn();
            }
        });
    });
    
    $(".remove").click(function(){
        // CheckForRecipient("#recipient-holder");
        this.remove();
    })
    $( "#recipient-holder input[type=text]" ).focus(function() {
        CheckForRecipient("#recipient-holder");
    });

    
    $("#searchDocument").on('input', function(){
        var searchVal = $(this).val();
        var urlAll = "<?= base_url('index.php/JsonResponse/jsonMyDocuments') ?>";
        var urlMyDocs = "<?= base_url('index.php/JsonResponse/jsonMyDocumentsArgs/') ?>" + searchVal;
        if (searchVal.length == 0) {
            getMyDocuments();
        }
        if (searchVal.length > 1) {
            $("#searchResult").text("");
            $.ajax({
                url: urlMyDocs,
                method: "post",
                data:  {
                },
                dataType: 'json',
                success: function(data){
                    if (data.length > 10) {
                        limit = 10;
                    }else{
                        limit = data.length;
                    }
                    for (var key = 0; key < limit; key++) {
                        console.log(data[key].Título);
                        hlId = "add_" + data[key].ID;
                        hl = "<a href='#' class = ' addDocToList' id = '" + hlId + "' title='" + data[key].Título +  "' > + " + data[key].Título + " </a>";
                        $("#searchResult").append( "<li>" + hl + "</li>");
                    }
                    AddDocToList();
                },
                error: function(e){
                    // $("#err").html(e).fadeIn();
                }          
            });
        }
    });
    getMyDocuments();

    $( "#find-user" ).autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: "<?php echo base_url().'index.php/Users/json_get_users_by_name_test/'; ?>",
                dataType: "json",
                method: "GET",
                data: {
                    name : request.term
                },
                success: function( data ) {
                    // response( availableTags );
                    // console.log(data);
                    var d = $.map(data,function(names){
                        return{
                            label: names['NAMES'] + " " + names['LAST_NAME'],
                            value: names['ID']
                        }     
                    });
                    response( d );
                    // console.log(d);
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            // console.log( "Selected: " + ui.item.value + " aka " + ui.item.label );
            $("#rec_1").append("<div class='recipient-user'>" + ui.item.label + "   <a href='#' class='remove'>×</a> <input class='user-id-value' type='text' name='to-" + ui.item.value + "' value='"+ ui.item.value +"'></div>");
            // CheckForRecipient("#recipient-holder");
            this.value = "";
            $(".remove").click(function( event ) {
                event.preventDefault();
                $(this).parent().remove();
                // console.log("hello");
            })
            return false;
        }
    });
    $( "#cc" ).autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: "<?php echo base_url().'index.php/Users/json_get_users_by_name_test/'; ?>",
                dataType: "json",
                method: "GET",
                data: {
                    name : request.term
                },
                success: function( data ) {
                    // response( availableTags );
                    // console.log(data);
                    var d = $.map(data,function(names){
                        return{
                            label: names['NAMES'] + " " + names['LAST_NAME'],
                            value: names['ID']
                        }     
                    });
                    response( d );
                    // console.log(d);
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            // console.log( "Selected: " + ui.item.value + " aka " + ui.item.label );
            $("#cc_1").append("<div class='recipient-user'>" + ui.item.label + "   <a href='#' class='remove'>×</a> <input class='user-id-value' type='text' name='cc-" + ui.item.value + "' value='"+ ui.item.value +"'></div>");
            this.value = "";
            $(".remove").click(function( event ) {
                event.preventDefault();
                $(this).parent().remove();
            })
            return false;
        }
    });

    $("#delete_first-file").on('click', function() { 
        event.preventDefault();
        $("#user-file-1").val(''); 
    });

    $( "#recipient-holder" ).click(function() {
        $( "#find-user" ).focus();
    });
    $( "#copied-holder" ).click(function() {
        $( "#cc" ).focus();
    });

    $( "#share-holder" ).click(function() {
        $( "#share-to" ).focus();
    });
    
    CheckboxToggleDisplay("#with-due-date", "#dueDate");
    CheckboxToggleDisplay("#with-status", "#status-select");
    AttachDocumentsToMail();
    AddAttachmentInput();


});

function getMyDocuments(){
    $("#searchResult").html("");
    var urlAll = "<?= base_url('index.php/JsonResponse/jsonMyDocuments') ?>";
    var button = "";
    var limit = 0;
    $.ajax({
        url: urlAll,
        dataType: 'json',
        success: function(data){
            if (data.length > 10) {
                limit = 10;
            }else{
                limit = data.length;
            }
            for (var key = 0; key < limit; key++) {
                hlId = "add_" + data[key].ID;
                hl = "<a href='#' class = ' addDocToList' id = '" + hlId + "' title='" + data[key].Título +  "' > + " + data[key].Título + " </a>";
                $("#searchResult").append( "<li>" + hl + "</li>");
            }
            AddDocToList();
        },
        error: function(e){
            // $("#err").html(e).fadeIn();
        }          
    });
}

function AttachDocumentsToMail(){
    $("#add-documents").on('click', function(e){
        $(".remove-this-li").each(function () {
            var title = $(this).attr("title");
            var did = $(this).attr("did");;
            var docAdded = 
                "<div class = 'col-12 row' en = 'dt_" + did + "'>"
                + "<div class = 'col-11'>"
                + "<div class = 'row'>"
                + "<div class = 'col-12'>"
                + "<span class = ''>"
                + title
                + "<input class= 'hidden' name = 'doc_" + did + "' type='text' value = '" + did + "' />"
                + "</span>"
                + "<span class = 'float-right'>"
                // + "Requiere acción? <input name = 'check_" + did + "' type = 'checkbox'>"
                + "</span>"
                + "</div>" // div 12 input título y checkbox
                + "<div class = 'col-12'>"
                + "<span class = ''>"
                // + "<select style = 'width: 40%;' class = 'form-control' name='stat_" + did + "' id=''><option value='0'> - </option></select>"
                + "</span>"
                + "<span class = 'float-right'>"
                // + "<input type='date' class = 'form-control' name = 'date_" + did + "' min='<?= $today->format("Y-m-d")?>'>"
                + "</span>"
                + "</div>" // div 12 select
                + "</div>" //div row
                + "</div>" //div 11
                // Aquí abajo va el select con los estados posibles para un documento
                + "<div class = 'col-1' style = 'font-size: 45px;'>"
                + "<a href='#' class = 'removeDT' en = 'dt_" + did + "'>"
                + "<i class = ' mdi mdi-trash-can-outline'></i>"
                + "</a>"
                + "</div>"
                + "</div>"
                ;
            $("#document-holder").append(docAdded);

            $(".removeDT").on('click', function(e){
                var attributeValue = $(this).attr("en");
                $("div[en='" + attributeValue + "']").remove();
            });
        });
        $('#exampleModal').modal('toggle');
    });
}

function GetStatuses(){

}

function AddDocToList(){
    $(".addDocToList").on('click', function(e){
        e.preventDefault();
        // temp-doc-list
        var docElementID = "id-doc-" + $(this).attr("id");
        var docElementTitle = $(this).attr("title");
        var did = 0;
        did = docElementID.replace(/\D/g,'');
        did = parseInt(did, 10);
        var docElementLI = "<li ><a href='#' did = '" + did + "' title = '" + docElementTitle + "' class = 'remove-this-li'> X " + docElementTitle + "<a/></li>";
        if (!$('[did="' + did + '"]').length){
            $("#temp-doc-list").append(docElementLI);
        }

        $(".remove-this-li").on('click', function(e){
            $(this).parent().remove();
        });
    });
}


function SendBack(method){
    url = "<?= base_url()?>index.php/test/draft_back/" + method;
    event.preventDefault();
    var form = $("#create_new_document")[0];
    var formData = new FormData(form);
    console.log(formData);
    $.ajax({
        url: url,
        method: "post",
        data:  formData,
        contentType: false,
        cache: false,
        processData:false,
        dataType: 'text',
        beforeSend : function(data){
        },
        success: function(data){
            console.log(data);
        },
        error: function(e){
            // $("#err").html(e).fadeIn();
        }          
    });
}

function AddAttachmentInput(){
    $("#AddAttachmentInput").click(function(event) {
        event.preventDefault();
        counter = $("#attachments-holder input").length;
        counter++;
        
        
        var newInput = 
            "<div class = 'col-12 row ' >"
                + "<div class = 'col-11'>"
                
                + "<input type='file' class='form-control-file input_div' name = 'file_" + counter +"'> "
                
                
                + "</div>" //div 11
            // Aquí abajo va el select con los estados posibles para un documento
                + "<div class = 'col-1' style = 'font-size: 45px;'>"
                    + "<a href='#' class = 'row del_input' >"
                    + "<i class = ' mdi mdi-trash-can-outline'></i>"
                    + "</a>"
                + "</div>"
            + "</div>"
            ;

        $("#attachments-holder").append(newInput);
        $(".del_input").on('click', function(e){
            $(this).parent().parent().remove();
            var inputDiv = 1;
            $( ".input_div" ).each(function() {
                $(this).prop("name", "file_" + inputDiv);
                console.log(inputDiv);
                inputDiv++;
            });
        });
        // $(this).remove();
    });
}

function CheckboxToggleDisplay(ID_checkbox, ID_inputField ){
    $(ID_checkbox).click(function(){
        if($(this).prop("checked") == true){
            $(ID_inputField).toggle();
            $(ID_inputField).prop("disabled", false);
        }
        else if($(this).prop("checked") == false){   
            $(ID_inputField).toggle();
            $(ID_inputField).prop("disabled", true);
        }
    });
}

function CheckForRecipient(holder){
    // console.log("hola");
    // if ($(holder).find('div.recipient-user')) {
        // $(holder).children('span').text("");
    // }else{
        // $(holder).children('span').text("Para:");
    // }
}
</script>