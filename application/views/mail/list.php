<?php
// "My documents" siempre mostrará los documentos de quien tenga sesión iniciada.
$ci =& get_instance();
$url = base_url("index.php/mail/read/");
switch ($mail) {
    case 'personal':
        $dataArray = $ci->documents_model->my_documents($_SESSION['ID']);
        $index = "SUB_THREAD_KEY";
        $orderByCol = 2;
        $actions = true;
        break;
    case 'sent':
        $dataArray = $ci->mail_model->sent($_SESSION['ID']);
        // $index se refiere al identificador del registro sobre cual actuar
        $index = "SUB_THREAD_KEY";
        $orderByCol = 4;
        $actions = true;
        break;
    case 'inbox':
        $dataArray = $ci->mail_model->inbox($_SESSION['ID']);
        $index = "SUB_THREAD_KEY";
        $orderByCol = 4;
        $actions = true;
        break;
    case 'd_inbox':
        $dataArray = $ci->mail_model->inbox($d_user);
        $index = "SUB_THREAD_KEY";
        $orderByCol = 4;
        $actions = true;
        break;
    case 'drafts':
        $dataArray = $ci->documents_model->my_drafts($_SESSION['ID']);
        $index = "ID";
        $orderByCol = 1;
        $actions = false;
        $url = base_url("index.php/document/draft/");
        break;
    
    default:
        $index = "ID";
        break;
}

DrawTable($dataArray, $index, $actions);?> 

<script>
$(function () {
    $('#documents').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        "pageLength": 25,
        // "order": [[ <?= $orderByCol ?>, "desc" ]],
        "language": {
            "search": "Búsqueda:",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "info":           "Mostrando de _START_ a _END_ entradas de un total de _TOTAL_ resultados",
            "infoEmpty":      "Mostrando de 0 a 0 entradas de 0 resultados",
        },
    })
})
  
$(".mail-element").click(function () {
    var id = $(this).prop("id");
    console.log(id);
    var url = "<?= $url ?>" + id;
    window.location.href = url;
});

</script>


<?php
/********************************************************************************************/
/* ************************************** FUNCTIONS  ************************************** */
/********************************************************************************************/

function DrawTable($data, $index, $actions){
    $controller = "mail";
    // print_r($data);
    echo '<table class="table table-hover mail table-striped" id ="documents">';
    echo "<thead>";
    
    if ($data) {
        foreach ($data[0] as $key => $value) {
            echo ( $key !== "ID" && $key !== "SENT_FROM" && $key !== "ID_PARENT_DOCUMENT" && $key !== "CURRENT_OWNER" && $key !== "READ" && $key !== "SUB_THREAD_KEY") ? "<th>" . $key . "</th>" : "" ;
        }

        echo ($actions) ? "<th> Acciones </th>" : "" ;
        echo "</thead>";
        echo "<tbody>";
    
        $odd = true;
        $trClass = "odd";
        foreach ($data as $key => $value):
            $read = (isset($value['READ']) && $value['READ']) ? "read" : "" ;
            echo "<tr class = '$controller $trClass $read mail-element' id = '" . $value[$index] . "'>";
            foreach ($value as $k => $v):
                $v = (strcmp($v,"")) ? $v : "--" ;
                echo ( $k !== "ID" && $k !== "SENT_FROM" && $k !== "ID_PARENT_DOCUMENT" && $k !== "CURRENT_OWNER" && $k !== "READ" && $k !== "SUB_THREAD_KEY") ? "<td>" . $v . "</td>" : "" ;
            endforeach;
            if ($actions) {
                echo "<td>";
                echo "<a href='" .base_url("index.php/$controller/read/" . $value[$index]) . "' class='btn btn-xs'><i class='fa fa-search'></i></a>";
                // echo "<a href='" .base_url("index.php/$controller/send/" . $value[$index]) . "'  class='btn btn-xs'><i class='fas fa-paper-plane'></i></a>";
                echo "</td>";
            }
            echo "</tr>";
            $odd = ($odd) ? false : true ;
            $trClass = ($odd) ? "odd" : "even" ;
        endforeach;
    }else{
        echo "<th>Tabla vacía</th>";
        echo "</thead>";
        echo "<tbody>";
        echo "<tr>";
        echo "<td>No hay información</td>";
        echo "</tr>";
    }
    
    echo "</tbody>";
    echo "</table>";
}

?>