<?php
// BACK
// Correo
session_start();
// print_r($_POST);
// Información general que va con cada correo enviado
$dateCurrent                = new DateTime(date("Y-m-d H:i:s"));
$data['DATE_RECEIVED']      = $dateCurrent->format("Y-m-d H:i:s");
$data['FROM']               = $_SESSION['ID'];
$data['MAIN_THREAD_KEY']    = URL_Encode("MAIN" . $_SESSION['ID'] . microtime()) . URL_Encode($_SESSION['ID'] . $_POST['subject']);
$data["SUBJECT"]            = $_POST['subject'];
$data["BODY"]               = $_POST['body'];


// Si no lleva un destinatario, el proceso se para y regresa un mensaje de error al usuario
$to = false;
foreach ($_POST as $key => $value) {
    if(strpos($key,'to') !== false){
        $to = true;
        $_POST['to_self'] = $_SESSION['ID'];
    }
}
if (!$to) {
    $result = [
        "result" => 0,
        "msg" => "No hay destinatario.",
        "error" => 1
    ];
    echo json_encode($result);
    exit;
}
/* Consulta la información de los documentos a enviar
Recibe los ID de los documentos sin firmar y las firmas digitales.
Se compara la información de oficios firmados y sin firmar para
evitar enviar información repetida a los remitentes. */
$documents = PrepareDocuments();
/* Si hay archivos adjuntos, son subidos a una carpeta temporal
 creada en el momento donde se guardarán para luego ser copiados a
 cada remitente. Una vez finalizado esto se eliminará dicha carpeta junto
 con sus contenido. */
$temp_folder = PrepareFiles();
SendData($data, $documents, $temp_folder);


function PrepareDocuments(){
    $documents = [];
    $ci =& get_instance();

    foreach ($_POST as $key => $value) {
        if(strpos($key,'sigd') !== false){
            # Proceso para documentos firmados
            $querySignedDocument = $ci->db->get_where(
                'new_t_documents_signed',
                ["DIGITAL_SIGNATURE" => $value]
            );
            $resultSignedDocument = $querySignedDocument->row_array();
            $index = $resultSignedDocument["ID_DOCUMENT"];
            $queryOriginalDocument = $ci->db->get_where(
                'new_t_documents',
                ["ID" => $index]
            );
            $resultOriginalDocument = $queryOriginalDocument->row_array();
            # Para no sobre-escribir algún resultado anterior
            if (!isset($documents[$index])) {
                $documents[$index] = $resultOriginalDocument;
            }
            
            if (!isset($documents[$index]['SIGNED'])) {
                $documents[$index]['SIGNED'] = [];
            }
            $documents[$index]['MAIN_THREAD'] = URL_Encode($_SESSION['ID'] . $index .  microtime() );
            array_push($documents[$index]['SIGNED'], $resultSignedDocument);
        }
    }

    foreach ($_POST as $key => $value) {
        if(strpos($key,'nsd') !== false){
            if(!isset($documents[$value])){
                $queryOriginalDocument = $ci->db->get_where(
                    'new_t_documents',
                    ["ID" => $value]
                );
                $resultOriginalDocument = $queryOriginalDocument->row_array();
                $documents[$value] = $resultOriginalDocument;
                $documents[$value]['MAIN_THREAD'] = URL_Encode($_SESSION['ID'] . $value .  microtime() );
            }
        }
    }
    // echo json_encode($documents);
    // print_r($documents);
    return $documents;
}

function PrepareFiles(){
    if ($_FILES) {
        $fileSrc = [];
        $folder = "./data/temp/";
        $temp_folder = $folder . $_SESSION['ID'] ."_" .  microtime() . "/";
        (file_exists ( $folder )) ? "" : mkdir($folder) ;
        (file_exists ( $temp_folder )) ? "" : mkdir($temp_folder) ;
        foreach ($_FILES as $key => $value) {
            if ($_FILES[$key]['error'] == 0) {
                $fileName = $_FILES[$key]['name'];
                $file_tmp = $_FILES[$key]['tmp_name'];
                $file = $temp_folder . $fileName;
                array_push($fileSrc, [
                    "NAME" => $fileName,
                    "SRC" => $file
                    ]);
                move_uploaded_file($file_tmp, $file);
            } else {
                if ($_FILES[$key]['error'] == 4){
                    // echo "No archivos selecionados. \n";
                }   
            }
        }
        
        return $temp_folder;
    }
}

/* El enviar el correo consta de ciclos determinados por
Remitentes > Documentos
Por cada remitente, correr ciclo principal:
- registrar correo
- copiar archivos adjuntos
> Por cada docmuento, correr ciclo de registro documentos:
> - registrar documento a correo
> - registrar firma a documento registrado en caso de tener una o varias */
function SendData($data, $documents, $temp_folder = null){
    $ci =& get_instance();
    $to = false;
    $from = $_SESSION['ID'];
    foreach ($_POST as $key => $recipientID):
        $send = false;
        $ccRecipient = 0;
        if(strpos($key,'to') !== false){
            $send = true;
        }
        if(strpos($key,'cc') !== false){
            $send = true;
            $ccRecipient = 1;
        }
        if ($send):
            $self = ($recipientID == $_SESSION['ID']) ? true : false ;
            $read = ($self) ? 1 : 0 ;
            $from = ($self) ? 0 : $_SESSION['ID'] ;
            $subThreadKeyMail = URL_Encode(
                "SUB"
                . $recipientID
                . microtime()
                . $_SESSION['ID']
                . $recipientID
                . $_POST['subject']
            );
            /* Añadir registro a la base de datos en las tablas correspondientes:
            new_t_mail
            new_t_mail_threads
            new_t_documents
            new_t_document_from
            new_t_document_status
            new_t_documents_signed
            new_t_document_mail
            */

            // Registrar entrada de correo.
            $dataMail = [
                "MAIN_THREAD_KEY"   => $data['MAIN_THREAD_KEY'],
                "SUB_THREAD_KEY"    => $subThreadKeyMail,
                "SUBJECT"           => $data["SUBJECT"],
                'FROM'              => $from,
                "CC"                => $ccRecipient,
                "BODY"              => $data["BODY"],
                "DATE_RECEIVED"     => $data['DATE_RECEIVED']
            ];
            $ci->db->insert('new_t_mail', $dataMail);
            $recentIdMail = $ci->db->insert_id();

            $dataMailThreads = [
                "ID_THREAD_OWNER"   => $recipientID,
                "MAIN_THREAD_KEY"   => $data['MAIN_THREAD_KEY'],
                "SUB_THREAD_KEY"    => $subThreadKeyMail,
                "LAST_UPDATED"      => $data['DATE_RECEIVED'],
                'ID_LAST_MAIL'      => $recentIdMail,
                "READ"              => $read
            ];
            $ci->db->insert('new_t_mail_threads', $dataMailThreads);

            foreach ($documents as $key => $document) {
                /* Después de registar el correo, inicia el ciclo para relacionar
                los documentos con el correo que se envió. */
                // Resgitrar documento(s) a usuario
                $subThread = URL_Encode(
                    $document['ID'] . "DOCUMENT" . $_SESSION['ID'] . microtime() . $recipientID
                );

                $dataDocument = [
                    "ID_TYPE"       => $document['ID_TYPE'],
                    "TITLE"         => $document['TITLE'],
                    "OWNER"         => $recipientID,
                    "BODY"          => $document['BODY'],
                    "ID_AUTHOR"     => $document['ID_AUTHOR'],
                    "CC"            => $document['CC'],
                    "FOR"           => $document['FOR'],
                    "FOLIO_2"       => $document['FOLIO_2'],
                    "FOLIO"         => $document['FOLIO'],
                    "DATE_CREATED"  => $data['DATE_RECEIVED']
                ];
                if ($self) {
                    $recentIdDocument = $document['ID'];
                }else{
                    $ci->db->insert('new_t_documents', $dataDocument);
                    $recentIdDocument = $ci->db->insert_id();
                }
                
                
                $dataDocumentFrom = [
                    "ID_DOCUMENT_RECEIVED" => $recentIdDocument,
                    "ID_FROM_USER" => $from
                ];
                $ci->db->insert('new_t_document_from', $dataDocumentFrom);
                

                $dataDocumentMail = [
                    "DOCUMENT_THREAD" => $subThread,
                    "ID_MAIL" => $recentIdMail,
                    "ID_DOCUMENT" => $recentIdDocument
                ];
                $ci->db->insert('new_t_document_mail', $dataDocumentMail);

                // Registrar entrada de status.
                $dataStatus = [
                    "ID_THREAD_OWNER"   => $recipientID,
                    "ID_HEAD_DOCUMENT"  => $recentIdDocument,
                    "MAIN_THREAD"       => $document['MAIN_THREAD'],
                    'SUB_THREAD'        => $subThread,
                    "LAST_UPDATE"       => $data['DATE_RECEIVED'],
                    "STATUS"            => 0,
                    "SEEN"              => 0
                ];

                /* Si en el documento tiene uno o varios registros de firma, se agregan
                al documento ya registrado.
                */
                if (isset($document['SIGNED'])) {
                    foreach ($document['SIGNED'] as $key => $digitalSignature) {
                        $dataDocumentSigned = [
                            "USER_NAME" => $digitalSignature['USER_NAME'],
                            "SIGNED_DATE" => $digitalSignature['SIGNED_DATE'],
                            "DIGITAL_SIGNATURE" => $digitalSignature['DIGITAL_SIGNATURE'],
                            "ID_DOCUMENT" => $recentIdDocument
                        ];
                        $ci->db->insert('new_t_documents_signed', $dataDocumentSigned);
                        $recentIdSignature = $ci->db->insert_id();
                        
                        $dataMailSignatures = [
                            "ID_MAIL" => $recentIdMail,
                            "ID_DIGITAL_SIGNATURE" => $recentIdSignature
                        ];
                        $ci->db->insert('new_t_mail_signatures', $dataMailSignatures);
                    }
                }
            }
            
            // print_r($dataMail);
        endif; //send

        /* Si hay archivos adjuntos, se crea una carpeta con la estructura
        /data/userdata/ID_DE_USUARIO/mail/ID_DE_CORREO/
        */
        if ($temp_folder) {
            $routeDir = "./data/userdata/$recipientID/mail/$recentIdMail/";
            (file_exists ( "./data/userdata/$recipientID/" )) ? "" : mkdir("./data/userdata/$recipientID/") ;
            (file_exists ( "./data/userdata/$recipientID/mail/" )) ? "" : mkdir("./data/userdata/$recipientID/mail/") ;
            (file_exists ( "./data/userdata/$recipientID/mail/$recentIdMail/" )) ? "" : mkdir("./data/userdata/$recipientID/mail/$recentIdMail/") ;
            $attachmentsList = array_diff(scandir($temp_folder), array('.','..'));
            foreach ($attachmentsList as $key => $file) {
                copy($temp_folder . $file, $routeDir . $file);
            }
        }
    endforeach;
    if ($temp_folder) {
        foreach ($attachmentsList as $file) {
            (is_dir("$temp_folder/$file")) ? delTree("$temp_folder/$file") : unlink("$temp_folder/$file");
        }
        rmdir($temp_folder);
    }
    $result = [
        "result" => 1,
        "msg" => "Correo enviado."
    ];
    echo json_encode($result);
}

function URL_Encode($data){
    return rtrim( strtr( base64_encode( $data ), '+/', '-_'), '=');
}
?>