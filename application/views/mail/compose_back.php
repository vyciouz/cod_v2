<?php
// BACK
// Correo
session_start();
// print_r($_POST);
RegisterMail();
// Error type
// 1: no hay destinatario

function RegisterMail(){
    // En este arreglo se agrega la información a registrar.
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $to = false;
    $data = [];
    $dataMailTable = [];
    $dataMailThreads = [];
    $dataDocument = [];
    $documents = [];
    
    $dateCurrent                = new DateTime(date("Y-m-d H:i:s"));
    $data['DATE_RECEIVED']      = $dateCurrent->format("Y-m-d H:i:s");
    $data['FROM']               = $_SESSION['ID'];
    $data['MAIN_THREAD_KEY']    = URL_Encode("MAIN" . $_SESSION['ID'] . microtime()) . URL_Encode($_SESSION['ID'] . $_POST['subject']);
    $data["SUBJECT"]            = $_POST['subject'];
    $data["BODY"]               = $_POST['body'];

    $dataMailTable[$data['FROM']] = [
        "MAIN_THREAD_KEY"   => $data['MAIN_THREAD_KEY'],
        "SUB_THREAD_KEY"    => URL_Encode("SUB" . $data['FROM'] . microtime() . $data['FROM'] . $_POST['subject']),
        "DATE_RECEIVED"     => $data['DATE_RECEIVED'],
        "SUBJECT"           => $data["SUBJECT"],
        "BODY"              => $data["BODY"],
    ];
    $dataMailThreads[$data['FROM']] = [
        "MAIN_THREAD_KEY"   => $dataMailTable[$data['FROM']]['MAIN_THREAD_KEY'],
        "SUB_THREAD_KEY"    => $dataMailTable[$data['FROM']]['SUB_THREAD_KEY'],
        "ID_THREAD_OWNER"   => $data['FROM'],
        "LAST_UPDATED"      => $dataMailTable[$data['FROM']]['DATE_RECEIVED'],
        "READ"              => 1
    ];

    foreach ($_POST as $key => $value) {
        if(strpos($key,'to') !== false){
            $to = true;
            $dataMailTable[$value] = 
            [
                "MAIN_THREAD_KEY"   => $data['MAIN_THREAD_KEY'],
                "SUB_THREAD_KEY"    => URL_Encode("SUB" . $value . microtime() . $_SESSION['ID'] . $value . $_POST['subject']),
                "DATE_RECEIVED"     => $data['DATE_RECEIVED'],
                'FROM'              => $data['FROM'],
                "SUBJECT"           => $data["SUBJECT"],
                "BODY"              => $data["BODY"],
            ];
            $dataMailThreads[$value] = [
                "MAIN_THREAD_KEY"   => $dataMailTable[$value]['MAIN_THREAD_KEY'],
                "SUB_THREAD_KEY"    => $dataMailTable[$value]['SUB_THREAD_KEY'],
                "ID_THREAD_OWNER"   => $value,
                "LAST_UPDATED"      => $dataMailTable[$value]['DATE_RECEIVED']
            ];
        }
        if(strpos($key,'cc') !== false){
            $dataMailTable[$value] = 
            [
                "MAIN_THREAD_KEY"   => $data['MAIN_THREAD_KEY'],
                "SUB_THREAD_KEY"    => URL_Encode("SUB" . $value . microtime() . $_SESSION['ID'] . $value . $_POST['subject']),
                "DATE_RECEIVED"     => $data['DATE_RECEIVED'],
                'FROM'              => $data['FROM'],
                "CC"                => 1,
                "SUBJECT"           => $data["SUBJECT"],
                "BODY"              => $data["BODY"],
            ];
            $dataMailThreads[$value] = [
                "MAIN_THREAD_KEY"   => $dataMailTable[$value]['MAIN_THREAD_KEY'],
                "SUB_THREAD_KEY"    => $dataMailTable[$value]['SUB_THREAD_KEY'],
                "ID_THREAD_OWNER"   => $value,
                "LAST_UPDATED"      => $dataMailTable[$value]['DATE_RECEIVED']
            ];
        }
        if(strpos($key,'doc') !== false){
            // echo "Documento no.: $value\n";
            array_push($documents, $value);
        }
    }

    if (!$to) {
        $result = [
            "result" => 0,
            "msg" => "No hay destinatario.",
            "error" => 1
        ];
        echo json_encode($result);
        exit;
    }
    
    // print_r($data);
    // print_r($dataMailTable);
    foreach ($dataMailTable as $key => $value) {
        // echo $key;
        $ci->db->insert('new_t_mail', $value);
        $recentId = $ci->db->insert_id();
        $dataMailThreads[$key]["ID_LAST_MAIL"] = $recentId;
        // print_r($value);
    }

    foreach ($dataMailThreads as $key => $value) {
        // print_r($value);
        $ci->db->insert('new_t_mail_threads', $value);
        // $recentId = $ci->db->insert_id();
    }

    if ($_FILES) {
        $fileSrc = [];
        $folder = "./data/temp/";
        $temp_folder = $folder . $_SESSION['ID'] . "/";
        (file_exists ( $folder )) ? "" : mkdir($folder) ;
        (file_exists ( $temp_folder )) ? "" : mkdir($temp_folder) ;
        foreach ($_FILES as $key => $value) {
            if ($_FILES[$key]['error'] == 0) {
                $fileName = $_FILES[$key]['name'];
                $file_tmp = $_FILES[$key]['tmp_name'];
                $file = $temp_folder . $fileName;
                array_push($fileSrc, [
                    "NAME" => $fileName,
                    "SRC" => $file
                    ]);
                move_uploaded_file($file_tmp, $file);
            } else {
                if ($_FILES[$key]['error'] == 4){
                    // echo "No archivos selecionados. \n";
                }   
            }
        }
        foreach ($dataMailThreads as $key => $value) {
            
            $own = $dataMailThreads[$key]['ID_THREAD_OWNER'];
            $mail = $dataMailThreads[$key]['ID_LAST_MAIL'];
            $routeDir = "./data/userdata/$own/mail/$mail/";
            (file_exists ( "./data/userdata/$own/" )) ? "" : mkdir("./data/userdata/$own/") ;
            (file_exists ( "./data/userdata/$own/mail/" )) ? "" : mkdir("./data/userdata/$own/mail/") ;
            (file_exists ( "./data/userdata/$own/mail/$mail/" )) ? "" : mkdir("./data/userdata/$own/mail/$mail/") ;
            foreach ($fileSrc as $fkey => $fvalue) {
                copy($fvalue['SRC'], $routeDir . $fvalue['NAME']);
            }
        }
    }

    // Union MAIL THREAD con DOCUMENTO
    if ($documents) {
        foreach ($documents as $key => $HeadDocument) {
            $mainthread = URL_Encode($_SESSION['ID'] . $HeadDocument .  microtime() );
            $DocumentStatus = (isset($_POST["stat_" . $HeadDocument])) ? $_POST["stat_" . $HeadDocument] : 0 ;
            $DocumentDate = (isset($_POST["date_" . $HeadDocument])) ? $_POST["stat_" . $HeadDocument] : null ;
            $OriginalDocumentID = $HeadDocument;
            

            # Ciclo repetido en base a destinatarios
            # $k es el ID del destinatario
            foreach ($dataMailThreads as $k => $v) {
                if ($dataMailThreads[$k]['ID_THREAD_OWNER'] != $_SESSION['ID']) {
                    // Insert doc, get recent Id
                    $HeadDocument = MakeDocumentCopy($HeadDocument, $_SESSION['ID'], $k); #ID regresada de la copia creada
                }else{
                    $HeadDocument = $OriginalDocumentID;
                }

                $threadOwner = $dataMailThreads[$k]['ID_THREAD_OWNER'];
                $subThread = URL_Encode(
                    $HeadDocument . "DOCUMENT" . $_SESSION['ID'] . microtime() . $threadOwner
                );

                $dataDocumentStatus = [
                    "ID_THREAD_OWNER"   => $threadOwner,
                    "ID_HEAD_DOCUMENT"  => $HeadDocument,
                    "MAIN_THREAD"       => $mainthread,
                    "SUB_THREAD"        => $subThread,
                    "DUE_DATE"          => $DocumentDate,
                    "LAST_UPDATED"      => $data['DATE_RECEIVED'],
                    "STATUS"            => $DocumentStatus
                ];
                $dataDocumentStatus['SEEN'] = ($_SESSION['ID'] == $threadOwner) ? 1 : 0 ;
                $dataDocumentMailRelation = [
                    "ID_MAIL" => $dataMailThreads[$k]["ID_LAST_MAIL"],
                    "ID_DOCUMENT" => $HeadDocument
                ];
                
                $ci->db->insert('new_t_document_status', $dataDocumentStatus);
                $ci->db->insert('new_t_document_mail', $dataDocumentMailRelation);
                // print_r($value);
            }
        }
    }

    $result = [
        "result" => 1,
        "msg" => "Correo enviado."
    ];
    echo json_encode($result);
}//RegisterMail

function URL_Encode($data){
    return rtrim( strtr( base64_encode( $data ), '+/', '-_'), '=');
}

function MakeDocumentCopy($ID_originalD, $from, $newOwner){
    $ci =& get_instance();
    $dateCurrent = new DateTime(date("Y-m-d H:i:s"));

    
    $queryOriginalDoc = $ci->db->get_where('new_t_documents', ["ID" => $ID_originalD]);
    $resultOriginal = $queryOriginalDoc->row_array();



    /* BORRAR DESPUÉS */
    $querySignedDoc = $ci->db->get_where('new_t_documents_signed', ["ID_DOCUMENT" => $ID_originalD]);
    $resultSigned = $querySignedDoc->row_array();
    /* /BORRAR DESPUÉS */



    // Si es un registo completo en la base de datos o
    // si refiere a un PDF subido a la plataforma
    if ($resultOriginal['ID_TYPE'] == 2) {
        // Hacer una copia del archivo pdf para el destinatario
    }

    $dataCopyDocument = [
        "ID_TYPE" => $resultOriginal['ID_TYPE'],
        "TITLE" => $resultOriginal['TITLE'],
        "OWNER" => $newOwner,
        "BODY" => $resultOriginal['BODY'],
        "ID_AUTHOR" => $resultOriginal['ID_AUTHOR'],
        "CC" => $resultOriginal['CC'],
        "FOR" => $resultOriginal['FOR'],
        "FOLIO_2" => $resultOriginal['FOLIO_2'],
        
        "FOLIO" => $resultOriginal['FOLIO'],
        "DATE_CREATED" => $dateCurrent->format("Y-m-d H:i:s")
    ];
    // Register in doc table
    $ci->db->insert('new_t_documents', $dataCopyDocument);
    $recentId = $ci->db->insert_id();

    // Register in doc_from table
    $dataDocumentFrom = [
        "ID_DOCUMENT_RECIEVED" => $recentId,
        "ID_FROM_USER" => $from
    ];
    $ci->db->insert('new_t_document_from', $dataDocumentFrom);




    /* BORRAR DESPUÉS */
    if ($resultSigned) {
        $dataSigned = [
            "USER_NAME" => $resultSigned['USER_NAME'],
            "SIGNED_DATE" => $resultSigned['SIGNED_DATE'],
            "DIGITAL_SIGNATURE" => $resultSigned['DIGITAL_SIGNATURE'],
            "ID_DOCUMENT" => $recentId
        ];
        $ci->db->insert('new_t_documents_signed', $dataSigned);
    }
    /* /BORRAR DESPUÉS */
    
    return $recentId;
}
?>