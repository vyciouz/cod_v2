<?php
$today = new DateTime(date("Y-m-d"));
?>
<style>
hr{
    margin: auto;
    width : 98%;
}

input[type=date], select{
    /* width: 40%; */
    display: inline-block !important;
}

.hidden{
    display: none;
}
.addDocToList, .remove-this-li{
    color: #669;
    font-weight: 400;
}
.addDocToList:hover{
    background-color: #4a7;
    color: white;
    font-weight: bold;
    border-radius: 5px;
    padding-left: 5px;
    padding-right: 5px;
}
.remove-this-li:hover{
    background-color: #c44;
    color: white;
    font-weight: bold;
    border-radius: 5px;
    padding-left: 5px;
    padding-right: 5px;
}
.removeDT{
    color: gray;
}
.removeDT:hover{
    color: red;
    font-weight: bold;
}
.b-r{
    border-right:1px solid #ccf;
}

textarea{
    overflow:hidden;
    resize:none;
    border: 0
}
html {
    color: -internal-root-color;
}
</style>

<?php
$today = new DateTime(date("Y-m-d"));
?>


<div class="content-wrapper">
    <div class="content">
        <div class="row">
			<div class="col-lg-12">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2>Nuevo Correo</h2>
                        <input type="number" id = "is-signed" value = 0 disabled style="display: none">
                    </div>
                    <div class="card-body">
                        <form id = "compose">
                            <div class="form-group">
                                <div class = "pseudo-input" id = "recipient-holder">
                                    <span id = "for">
                                    Para:
                                    </span>
                                    
                                    <div id = "rec_1" style ="display: inline">
                                    </div>
                                    <div id = "rec_2" style ="display: inline">
                                        <input type="text" id ="find-user" class = "crouching-div-hidden-input">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Recipientes"> -->
                                <div class = "pseudo-input" id = "copied-holder">
                                    <span id = "CC">CC:</span>
                                    
                                    <div id = "cc_1" style ="display: inline">
                                    </div>
                                    <div id = "cc_2" style ="display: inline">
                                        <input type="text" id ="cc" class = "crouching-div-hidden-input">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <input type="text" class="form-control" name="subject" id = "subject" placeholder = "Asunto" required >
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" id="body" name = "body" rows="3"></textarea>
                            </div>
                            
                            <div class="form-group row" id = "document-holder">
                                <div class = "col-12">
                                    <h5>- Documentos -</h5>
                                </div>
                            </div>
                            <div class="form-group row" id = "attachments-holder">
                                
                            </div>
                            <div class="form-group">
                                <a href="#" data-toggle="modal" id = "attach-not-signed" class = "attach-doc" style = "font-weight: bold; color: #8a909d">
                                    <i class = "mdi mdi-attachment"></i> Agregar Oficio
                                </a>
                            </div>
                            <div class="form-group">
                                <a href="#" data-toggle="modal" id = "attach-signed" class = "attach-doc" style = "font-weight: bold; color: #8a909d">
                                    <i class = "mdi mdi-attachment"></i> Agregar Oficio Firmado
                                </a>
                            </div>
                            <div class="form-group">
                                <a href="#" id = "AddAttachmentInput" style = "font-weight: bold; color: #8a909d">
                                    <i class = "mdi mdi-attachment"></i> Adjuntar archivos
                                </a>
                            </div>
                            
                            <div class="form-footer pt-4 pt-5 mt-4 border-top">
                            
                                <button type="submit" id = "send-mail" class="btn btn-primary btn-default">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php DrawModal(); ?>




<script>
$(document).ready(function(){
    
    const form = document.querySelector('form');
    
    $("#compose").on('submit', function(e){
        var url = "<?php echo base_url('index.php/mail/compose_back')?>";
        var form = new FormData(this);
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            beforeSend : function(data){
                //$("#preview").fadeOut();
                // $("#err").fadeOut();
                // console.log(data);
                var buttonLoading = '<div class="sk-circle" style= "margin: auto">'
                    + '<div class="sk-circle1 sk-child"></div>'
                    + '<div class="sk-circle2 sk-child"></div>'
                    + '<div class="sk-circle3 sk-child"></div>'
                    + '<div class="sk-circle4 sk-child"></div>'
                    + '<div class="sk-circle5 sk-child"></div>'
                    + '<div class="sk-circle6 sk-child"></div>'
                    + '<div class="sk-circle7 sk-child"></div>'
                    + '<div class="sk-circle8 sk-child"></div>'
                    + '<div class="sk-circle9 sk-child"></div>'
                    + '<div class="sk-circle10 sk-child"></div>'
                    + '<div class="sk-circle11 sk-child"></div>'
                    + '<div class="sk-circle12 sk-child"></div>'
                + '</div>';
                $("#send-mail").html(buttonLoading);
                $("#send-mail").attr("disabled", true);
            },
            success: function(data){
                <?php $url = base_url() . "index.php/user/profile/" . $_SESSION['ID'];?>
                if (data.result) {
                    window.location.href = "<?php echo $url?>";    
                }else{
                    switch (data.error) {
                        case 1:
                            alert(data.msg);
                            break;
                    
                        default:
                            break;
                    }
                }
                
                // console.log(data);
            },
            complete: function(){
                $('#send-mail').removeAttr("disabled");
                $("#send-mail").html("Enviar");
            },
            error: function(e){
                // $("#err").html(e).fadeIn();
            }
        });
    });
    
    $(".remove").click(function(){
        // CheckForRecipient("#recipient-holder");
        this.remove();
    })
    $( "#recipient-holder input[type=text]" ).focus(function() {
        CheckForRecipient("#recipient-holder");
    });

    
    

    $( "#find-user" ).autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: "<?php echo base_url().'index.php/Users/json_get_users_by_name_test/'; ?>",
                dataType: "json",
                method: "GET",
                data: {
                    name : request.term
                },
                success: function( data ) {
                    // response( availableTags );
                    // console.log(data);
                    var d = $.map(data,function(names){
                        return{
                            label: names['NAMES'] + " " + names['LAST_NAME'],
                            value: names['ID']
                        }     
                    });
                    response( d );
                    // console.log(d);
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            // console.log( "Selected: " + ui.item.value + " aka " + ui.item.label );
            $("#rec_1").append("<div class='recipient-user'>" + ui.item.label + "   <a href='#' class='remove'>×</a> <input class='user-id-value' type='text' name='to-" + ui.item.value + "' value='"+ ui.item.value +"'></div>");
            // CheckForRecipient("#recipient-holder");
            this.value = "";
            $(".remove").click(function( event ) {
                event.preventDefault();
                $(this).parent().remove();
                // console.log("hello");
            })
            return false;
        }
    });
    $( "#cc" ).autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: "<?php echo base_url().'index.php/Users/json_get_users_by_name_test/'; ?>",
                dataType: "json",
                method: "GET",
                data: {
                    name : request.term
                },
                success: function( data ) {
                    // response( availableTags );
                    // console.log(data);
                    var d = $.map(data,function(names){
                        return{
                            label: names['NAMES'] + " " + names['LAST_NAME'],
                            value: names['ID']
                        }     
                    });
                    response( d );
                    // console.log(d);
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            // console.log( "Selected: " + ui.item.value + " aka " + ui.item.label );
            $("#cc_1").append("<div class='recipient-user'>" + ui.item.label + "   <a href='#' class='remove'>×</a> <input class='user-id-value' type='text' name='cc-" + ui.item.value + "' value='"+ ui.item.value +"'></div>");
            this.value = "";
            $(".remove").click(function( event ) {
                event.preventDefault();
                $(this).parent().remove();
            })
            return false;
        }
    });

    $("#delete_first-file").on('click', function() { 
        event.preventDefault();
        $("#user-file-1").val(''); 
    });

    $( "#recipient-holder" ).click(function() {
        $( "#find-user" ).focus();
    });
    $( "#copied-holder" ).click(function() {
        $( "#cc" ).focus();
    });

    $( "#share-holder" ).click(function() {
        $( "#share-to" ).focus();
    });
    
    AttachDocuments();
    AddAttachmentInput();


});

function AttachDocuments(){
    $(".attach-doc").on('click', function(e){
        $("#temp-doc-list").html("");
        $("#searchResult").text("");
        $("#searchDocument").val("");
        $('#exampleModal').modal('show');
        signed = ($(this).attr("id") == "attach-signed") ? 1 : 0;
        urlMyDocs = (signed) ? "<?= base_url('index.php/JsonResponse/jsonMySignedDocuments/') ?>": "<?= base_url('index.php/JsonResponse/jsonMyDocuments/') ?>";
        $("#is-signed").val(signed);
        SearchDocuments();
        getDocumentsData(urlMyDocs);
    });
}

function SearchDocuments(signed){
    $("#searchDocument").on('input', function(){
        searchVal = $(this).val();
        signed = $("#is-signed").val();
        urlMyDocs = "";
        attrId = "";
        urlMyDocs = (signed) ? "<?= base_url('index.php/JsonResponse/jsonMySignedDocumentsArgs/') ?>": "<?= base_url('index.php/JsonResponse/jsonMyDocumentsArgs/') ?>";
        urlMyDocs += searchVal;
        if (searchVal.length == 0) {
            urlMyDocs = (signed) ? "<?= base_url('index.php/JsonResponse/jsonMySignedDocuments/') ?>": "<?= base_url('index.php/JsonResponse/jsonMyDocuments/') ?>";
        }
        // functions
        getDocumentsData(urlMyDocs);
    });
}

function getDocumentsData(urlMyDocs){
    $("#searchResult").text("");
    signed = $("#is-signed").val();
    $.ajax({
        url: urlMyDocs,
        method: "post",
        data:  {
        },
        dataType: 'json',
        success: function(data){
            divHolder = $("#ul-holder").html("");
            divHolder.append("<ul id = 'searchResult'></ul>");
            limit = (data.length > 10) ? 10: data.length;
            for (var key = 0; key < limit; key++) {
                elementHTML = "<li><a href='#' class = ' addDocToList' id = '[ID]' title = '[TITLE]' value = '[VALUE]'>"
                    + "<i class='mdi mdi-plus'></i> [DISPLAY_NAME]"
                    + "</a></li>";
                if (signed == 1) {
                    // elementHTML = elementHTML.replace('[DISPLAY_NAME]', data[key].TITLE);
                    elementHTML = elementHTML.replace('[DISPLAY_NAME]', data[key].TITLE + "(" + data[key].USER_NAME + ")");
                    elementHTML = elementHTML.replace('[TITLE]', data[key].TITLE + "(" + data[key].USER_NAME + ")");
                    elementHTML = elementHTML.replace('[ID]', "sigd_" + data[key].ID_DOCUMENT + "_" + data[key].ID);
                    elementHTML = elementHTML.replace('[VALUE]', data[key].DIGITAL_SIGNATURE);
                }else{
                    elementHTML = elementHTML.replace('[DISPLAY_NAME]', data[key].TITLE);
                    elementHTML = elementHTML.replace('[TITLE]', data[key].TITLE);
                    elementHTML = elementHTML.replace('[ID]', "nsd_" + data[key].ID);
                    elementHTML = elementHTML.replace('[VALUE]', data[key].ID);
                }
                $("#searchResult").append(elementHTML);
            }
            delete data;
            AddDocToList();
        },
        error: function(e){
            // $("#err").html(e).fadeIn();
        }          
    });
}
// 
function AddDocToList(){
    $(".addDocToList").on('click', function(e){
        signed = $("#is-signed").val();
        e.preventDefault();
        // temp-doc-list
        ElementID = $(this).attr("id");
        docElementTitle = $(this).attr("title");
        thisElementValue = $(this).attr("value");
        elementHTML = "<li class = '[CLASS]'>"
            + "<a href='#' title = '[TITLE]' class = 'remove-this-li' value = '[VALUE]'>"
            + "<i class='mdi mdi-delete'></i>[ICON] [DISPLAY_NAME]<a/></li>";
            '<i class="fas fa-file-signature"></i>'
        if (signed == 1) {
            icon = '<i class="fas fa-file-signature"></i>';
        }else{
            icon = '<i class="far fa-file"></i>';
        }
        elementHTML = elementHTML.replace('[CLASS]',ElementID);
        elementHTML = elementHTML.replace('[DISPLAY_NAME]', docElementTitle);
        elementHTML = elementHTML.replace('[VALUE]', thisElementValue);
        elementHTML = elementHTML.replace('[ICON]',icon);
        elementHTML = elementHTML.replace('[TITLE]', docElementTitle);
        if (!$("#temp-doc-list").length) {
            $("#temp-doc-holder").append("<ul id = 'temp-doc-list' ></ul>");
        }
        if (!$('.' + ElementID).length){
            $("#temp-doc-list").append(elementHTML);
            AttachDocumentsToMail();
        }
        
        $(".remove-this-li").on('click', function(e){
            $(this).parent().remove();
        });
    });
}


function AttachDocumentsToMail(){
    $("#add-documents").on('click', function(e){
        $(".remove-this-li").each(function () {
            thisElementClasses  = $(this).parent().attr("class");
            signed = $("#is-signed").val();
            title = $(this).attr("title");
            thisElementValue = $(this).attr("value");
            thisElementInputName = thisElementClasses;
            if (signed == 1) {
                icon = '<i class="fas fa-file-signature"></i>';
            }else{
                icon = '<i class="far fa-file"></i>';
            }

            var docAdded = 
                "<div class = 'col-12 row " + thisElementClasses + "'>"
                + "<div class = 'col-11'>"
                + "<div class = 'row'>"
                + "<div class = 'col-12'>"
                + "<span class = ''>[ICON] "
                + title
                + "<input class= 'hidden' name = '[INPUT_NAME]' type='text' value = '" + thisElementValue + "' />"
                + "</span>"
                + "<span class = 'float-right'>"
                // + "Requiere acción? <input name = 'check_" + did + "' type = 'checkbox'>"
                + "</span>"
                + "</div>" // div 12 input título y checkbox
                + "<div class = 'col-12'>"
                + "<span class = ''>"
                // + "<select style = 'width: 40%;' class = 'form-control' name='stat_" + did + "' id=''><option value='0'> - </option></select>"
                
                
                // + "<input type='date' class = 'form-control' name = 'date_" + did + "' min='<?= $today->format("Y-m-d")?>'>"
                + "</span>"
                + "</div>" // div 12 select
                + "</div>" //div row
                + "</div>" //div 11
                // Aquí abajo va el select con los estados posibles para un documento
                + "<div class = 'col-1'>"
                
                + "<a href='#' class = 'removeDT' >"
                + "<i class = ' mdi mdi-trash-can-outline'></i>"
                + "</a>"
                
                + "</div>"
                + "</div>"
                ;
            docAdded = docAdded.replace('[ICON]',icon);
            docAdded = docAdded.replace('[INPUT_NAME]',thisElementInputName);
            $("#document-holder").append(docAdded);

            $(".removeDT").on('click', function(e){
                var attributeValue = $(this).attr("en");
                $(this).parent().parent().remove();
            });
        });
        $("#temp-doc-list").remove();
        $('#exampleModal').modal('toggle');
    });
}


function SendBack(method){
    url = "<?= base_url()?>index.php/test/draft_back/" + method;
    event.preventDefault();
    var form = $("#create_new_document")[0];
    var formData = new FormData(form);
    console.log(formData);
    $.ajax({
        url: url,
        method: "post",
        data:  formData,
        contentType: false,
        cache: false,
        processData:false,
        dataType: 'text',
        beforeSend : function(data){
        },
        success: function(data){
            console.log(data);
        },
        error: function(e){
            // $("#err").html(e).fadeIn();
        }          
    });
}

function AddAttachmentInput(){
    $("#AddAttachmentInput").click(function(event) {
        event.preventDefault();
        counter = $("#attachments-holder input").length;
        counter++;
        
        var newInput = 
            "<div class = 'col-12 row'>"
                + "<div class = col-11 >"
                + "<input type='file' class='form-control-file input_div' name = 'file_" + counter +"'> "
                + "</div>" //div 11
                + "<div class = col-1 >"
                
                    + "<a href='#' class = 'row del_input' >"
                    + "<i class = ' mdi mdi-trash-can-outline'></i>"
                    + "</a>"
                
                + "</div>" //div 1
            + "</div>"
            ;

        $("#attachments-holder").append(newInput);
        $(".del_input").on('click', function(e){
            $(this).parent().parent().remove();
            var inputDiv = 1;
            $( ".input_div" ).each(function() {
                $(this).prop("name", "file_" + inputDiv);
                inputDiv++;
            });
        });
        // $(this).remove();
    });
}

function CheckboxToggleDisplay(ID_checkbox, ID_inputField ){
    $(ID_checkbox).click(function(){
        if($(this).prop("checked") == true){
            $(ID_inputField).toggle();
            $(ID_inputField).prop("disabled", false);
        }
        else if($(this).prop("checked") == false){   
            $(ID_inputField).toggle();
            $(ID_inputField).prop("disabled", true);
        }
    });
}

function CheckForRecipient(holder){
    // console.log("hola");
    // if ($(holder).find('div.recipient-user')) {
        // $(holder).children('span').text("");
    // }else{
        // $(holder).children('span').text("Para:");
    // }
}
</script>

<?php
function DrawModal(){
    ?>
<!-- <div class="modal fade show" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: block; padding-right: 17px;" aria-modal="true"> -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Documento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Buscar Documento:</label>
                            <input type="text" class="form-control" name="searchDocument" id = "searchDocument" placeholder = "Buscar" >
                        </div>
                        <div id = "ul-holder">
                        <!-- <ul class="list-group" id = "searchResult">
						</ul> -->
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class = "col-12">
                            <div class="form-group" id = "temp-doc-holder">
                                <label for="">Agregados</label>
                                
                                <ul id = "temp-doc-list">
                                </ul>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id = "add-documents" type="button" class="btn btn-primary btn-pill">Agregar</button>
                <button type="cancel-add" class="btn btn-danger btn-pill" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
    <?php
}

?>