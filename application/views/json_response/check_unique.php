<?php
session_start();
$result = [];

if (!isset($_POST['V']) || !isset($_POST['C']) || !isset($_POST['T'])) {
    echo "error";
    // exit;
    print_r($_POST);
    exit;
}

$ID = $_SESSION['ID'];
$column     = $_POST['C'];
$table      = $_POST['T'];
$value      = $_POST['V'];
$ci =& get_instance();
$ci->db->where($column, $value);
$query = $ci->db->get($table);
$result = $query->row_array();
$available = (!$result) ? 1 : 0 ;
$result['available'] = $available;

echo json_encode($result,JSON_UNESCAPED_UNICODE);
?>