<?php
session_start();
// print_r($_POST);

// if (CheckUnique('t_users', 'CURP', $_POST['CURP'])) {
//     ShowError(1);
// }
(CheckUnique('t_users', 'USERNAME', $_POST['USERNAME'])) ? 1 : ShowError(1);
(CheckUnique('t_users', 'CURP', $_POST['CURP'])) ? 1 : ShowError(2);
(CheckUnique('t_users', 'EMAIL', $_POST['EMAIL'])) ? 1 : ShowError(3);

RegisterUser();






// -------------------------------------------------------------------------------------
function CheckUnique($table, $column, $value){
    $ci =& get_instance();
    $query = $ci->db->get_where($table, [$column => $value]);;
    $result = $query->row_array();
    $result = ($result) ? false : true;
    return $result;
}

function ShowError($type){
    $msg = '';
    switch ($type) {
        case 1:
            $msg = "Ese nombre de usuario ya ha sido utilizado.";
            break;
        
        case 2:
            $msg = "Ese CURP de usuario ya ha sido utilizado.";
            break;

        case 3:
            $msg = "Ese correo de usuario ya ha sido utilizado.";
            break;
        
        default:
            # code...
            break;
    }
    $error = ['error' => $type, 'msg' => $msg];
    $error = json_encode($error);
    echo $error;
    exit;
}



function RegisterUser(){
    $roles = [];
    $dataArray = [
        'USERNAME'                  => $_POST['USERNAME'],
        'NAMES'                     => $_POST['NAMES'],
        'LAST_NAME'                 => $_POST['LAST_NAME'],
        'MAIDEN_NAME'               => $_POST['MAIDEN_NAME'],
        'CURP'                      => $_POST['CURP'],
        'PASSWORD'                  => $_POST['PASSWORD'],
        'EMAIL'                     => $_POST['EMAIL'],
        'DEPENDENCIA'               => $_POST['DEPENDENCIA'],
        'UNIDAD_ADMINISTRATIVA'     => $_POST['UNIDAD_ADMINISTRATIVA'],
        'SUBUNIDAD_ADMINISTRATIVA'  => $_POST['SUBUNIDAD_ADMINISTRATIVA'],
        'PUESTO'                    => $_POST['PUESTO']
    ];
    $ci =& get_instance();
    $ci->db->insert('t_users', $dataArray);
    $recentId = $ci->db->insert_id();
    
    foreach ($_POST as $key => $value) {
        if(strpos($key,'role') !== false){
            array_push($roles,$value);
        }
    }
    RegisterRoles($recentId, $roles);
}

function RegisterRoles($ID_user, $rolesArray){
    $dataArray = [
        'ID_USER'                     => $ID_user
    ];
    $ci =& get_instance();
    
    foreach ($rolesArray as $key => $value) {
        $dataArray['ID_ROLE'] = $value;
        $ci->db->insert('r_user_roles', $dataArray);
    }

    $result = [ "result" => 1,
        "msg" => "Usuario registrado exitosamente.",
        "ID" => $ID_user];
    echo json_encode($result);
}
?>