<?php
$dependencia = GetDependencyInfo($user[0]['DEPENDENCIA']);
$unidadAdmin = GetAdminUnitInfo($user[0]['UNIDAD_ADMINISTRATIVA']);
$userActiveRoles = GetRoles($ID_user);
?>
<div class="row justify-content-center mt-5">
    <div class="col-xl-4">
        <div class="card card-default">
            <div class="card-header card-header-border-bottom">
                <h2> <i class="mdi mdi-account-card-details"></i> INFORMACION DEL USUARIO</h2>
            </div>
            <div class="card-body">
                <p class="text-dark font-weight-medium pt-4 mb-2 nav-text">Nombres</p>
                <p><?=$user[0]['NAMES']?></p>
                <p class="text-dark font-weight-medium pt-4 mb-2 nav-text">Primer Apellido</p>
                <p><?=$user[0]['LAST_NAME']?></p>
                <p class="text-dark font-weight-medium pt-4 mb-2 nav-text">Segundo Apellido</p>
                <p><?=$user[0]['MAIDEN_NAME']?></p>
                <p class="text-dark font-weight-medium pt-4 mb-2 nav-text">Correo Electrónico</p>
                <p><?=$user[0]['EMAIL']?></p>
                <p class="text-dark font-weight-medium pt-4 mb-2 nav-text">Dependencia</p>
                <p><?=$dependencia[0]['NAME']?></p>
                <p class="text-dark font-weight-medium pt-4 mb-2 nav-text">Unidad Administrativa</p>
                <p><?=$unidadAdmin[0]['NAME']?></p>
                <p class="text-dark font-weight-medium pt-4 mb-2 nav-text">Puesto</p>
                <p><?=$user[0]['PUESTO']?></p>
                <p class="text-dark font-weight-medium pt-4 mb-2 nav-text">Roles</p>
                <?php PrintRoles($userActiveRoles)?>
                <?php
                if (isset($userActiveRoles[7])):?>
                <p><a href = "<?= base_url('index.php/user/assign/' . $user[0]['ID'])?>" class="btn btn-primary" style = "color: white">Asignar</a></p>
                <?php
                endif;
                ?>
            </div>
        </div>
    </div>
</div>

<?php

function GetRoles($ID_user){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $ci->db->where('ID_USER', $ID_user);
    $userActiveRoles = [];
    

    $query = $ci->db->get('r_user_roles');
    $result = $query->result_array();
    // print_r($result);
    foreach ($result as $key => $value) {
        $userActiveRoles[$value['ID_ROLE']] = true;
    }
    // print_r($userActiveRoles);
    return $userActiveRoles;
}

function PrintRoles($roles){
    // print_r($roles);
    foreach ($roles as $key => $value) {
        // echo $value['ID_ROLE'];
        GetRoleName($key);
        echo (isset($roles[$key +1])) ? ", " : "" ;
    }
    $rol = [];
}

function GetRoleName($id){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $ci->db->where('ID', $id);
    $query = $ci->db->get('cat_roles');
    $row = $query->row();
    print_r($row->NAME);
    // return $result;
}



?>