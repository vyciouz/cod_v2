<?php
$roles = GetRoles();
$camposUsuario              = [
    "USERNAME"                  => [
        "name" => "Usuario: ",
        'type' => 'text',
        'dependency' => null,
        'unique' => false
    ],
    "NAMES"                     => [
        'name' => 'Nombre(s): ',
        'type' => 'text',
        'dependency' => null,
        'unique' => false
    ],
    "LAST_NAME"                 => [
        'name' => 'Apellido paterno: ',
        'type' => 'text',
        'dependency' => null,
        'unique' => false
    ],
    "MAIDEN_NAME"               => [
        'name' => 'Apellido materno: ',
        'type' => 'text',
        'dependency' => null,
        'unique' => false
    ],
    "CURP"                      => [
        'name' => 'CURP: ',
        'type' => 'text',
        'dependency' => null,
        'unique' => false
    ],
    "PASSWORD"                  => [
        'name' => 'Contraseña: ',
        'type' => 'password',
        'dependency' => null,
        'unique' => false
    ],
    "EMAIL"                     => [
        'name' => 'Correo Electrónico: ',
        'type' => 'text',
        'dependency' => null,
        'unique' => false
    ],
    "DEPENDENCIA"               => [
        'name' => 'Dependencia: ',
        'type' => 'select',
        'dependency' => [
            "table"     => "cat_dependencias",
            "inCol"     => "ID",
            "outCol"    => "NAME"
        ],
        'unique' => false
    ],
    "UNIDAD_ADMINISTRATIVA"     => [
        'name' => 'Unidad Administrativa: ',
        'type' => 'select',
        'dependency' => [
            "table"     => "cat_u_administrativas",
            "inCol"     => "ID",
            "outCol"    => "NAME"
        ],
        'unique' => false
    ],
    "SUBUNIDAD_ADMINISTRATIVA"  => [
        'name' => 'Subunidad Administrativa',
        'type' => 'select',
        'dependency' => [
            "table"     => "cat_sub_u_administrativas",
            "inCol"     => "ID",
            "outCol"    => "NAME"
        ],
        'unique' => false
    ],
    "PUESTO"                    => [
        'name' => 'Puesto',
        'type' => 'text',
        'dependency' => null,
        'unique' => false
    ]
];
?>

<style>
.invalid-feedback, .valid-feedback{
    display: block;
}
</style>
<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2>Nuevo Usuario</h2>
                    </div>
                    <div class="card-body">
                        <form id = "new-user">
                            <div class="form-group row">
                                <?php DrawEditFields($camposUsuario); ?>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-md-12 mb-3">
                                    <p class="text-dark font-weight-medium mb-3">Roles:</p>

                                    <ul class="list-unstyled list-inline">
                                        <?php
                                        foreach ($roles as $key => $value) {?>
                                        <li class="d-inline-block mr-4">
                                            <label class="control control-checkbox"><?php echo $value['NAME']?>
                                                <input type="checkbox" name = "role_<?php echo $value['ID']?>" value = "<?php echo $value['ID']?>">
                                                <div class="control-indicator"></div>
                                            </label>
                                        </li>
                                        <?php
                                        }
                                        ?>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="form-footer pt-4 pt-5 mt-4 border-top">
                                <button type="submit" class="btn btn-primary btn-default">Submit</button>
                                <button type="submit" class="btn btn-secondary btn-default">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    url = "<?php echo base_url()?>index.php/user/new_back";
    $( "#CURP" ).change(function(){
        var value = $("#CURP").val();
        $.ajax({
            url: "<?php echo base_url('/index.php/system/check_u') ?>",
            type: "POST",
            data:  {
                'V' : value,
                'T' : 't_users',
                'C' : 'CURP'
            },
            dataType: 'json',
            beforeSend : function(data){
            },
            success: function(data){
                if (data.available) {
                    $("#CURP").removeClass( "is-invalid" ).addClass( "is-valid" );
                }else{
                    $("#CURP").removeClass( "is-valid" ).addClass( "is-invalid" );
                }
            },
            error: function(e){
            }
        });
    });

    $( "#USERNAME" ).change(function(){
        var value = $("#USERNAME").val();
        $.ajax({
            url: "<?php echo base_url('/index.php/system/check_u') ?>",
            type: "POST",
            data:  {
                'V' : value,
                'T' : 't_users',
                'C' : 'USERNAME'
            },
            dataType: 'json',
            beforeSend : function(data){
            },
            success: function(data){
                if (data.available) {
                    $("#USERNAME").removeClass( "is-invalid" ).addClass( "is-valid" );
                }else{
                    $("#USERNAME").removeClass( "is-valid" ).addClass( "is-invalid" );
                }
            },
            error: function(e){
            }
        });
    });

    $("#new-user").on('submit', function(e){
      var form = new FormData(this);
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            beforeSend : function(data){
                //$("#preview").fadeOut();
                // $("#err").fadeOut();
                // console.log(data);
            },
            success: function(data)
            {
                if(data.result){
                    window.location.href = "<?= base_url() . 'index.php/user/view/'?>" + data.ID;
                }
                else{
                    // console.log(data);
                }
            }
        });
    });

});
</script>


<?php
function DrawEditFields($columnsToUse){
    
    foreach ($columnsToUse as $key => $value) {
        switch ($value['type']) {
            case 'text':
                DrawTextInput($columnsToUse[$key], $key);
                break;

            case 'select':
                DrawSelectInput($columnsToUse[$key], $key);
                break;

            case 'password':
                DrawPasswordInput($columnsToUse[$key], $key);
                break;
            
            default:
                # code...
                break;
        }
    }
}

function DrawTextInput($array, $colName){?>
    <div class="col-md-12 mb-3">
        <label for=""><?php echo $array['name']?></label>
        <input class = "form-control" type="text" name="<?php echo $colName?>" id="<?php echo $colName?>" required>
    </div>
<?php
}

function DrawSelectInput($array, $colName){?>
    <div class="col-md-12 mb-3">
        <label for=""><?php echo $array['name']?></label>
        <select  class ="form-control" name="<?php echo $colName?>" id="<?php echo $colName?>">
            <option value="0">N/A</option>
            <?php
            if ($array['dependency']!=null) {
                $ci =& get_instance();
                $ci->db->order_by($array['dependency']['outCol'], 'ASC');
                $values = $ci->db->get($array['dependency']['table']);
                foreach ($values->result_array() as $row) {?>
                    <option value="<?php echo $row[$array['dependency']['inCol']] ?>"><?php echo $row[$array['dependency']['outCol']] ?></option>
                <?php
                }
            }?>
        </select>
    </div>
<?php
}

function DrawPasswordInput($array, $colName){?>
<div class="col-md-12 mb-3">
    <label for=""><?php echo $array['name']?></label>
    <input class = "form-control" type="password" name="<?php echo $colName?>" id="<?php echo $colName?>">
</div>
<div class="col-md-12 mb-3">
    <label for="">Verificar <?php echo $array['name']?></label>
    <input class = "form-control" type="password" id="<?php echo $colName?>_verify">
    <div id = "CheckPasswordMatch" class = "invalid-feedback">
    </div>
</div>
    <script>
        function checkPasswordMatch() {
            var password = $("#<?php echo $colName?>").val();
            var confirmPassword = $("#<?php echo $colName?>_verify").val();
            if (password != confirmPassword){
                $("#CheckPasswordMatch").text("¡Contraseña no coincide!");
                }
            else{
                $("#CheckPasswordMatch").text("");
            }
        }

        $(document).ready(function () {
            $("#<?php echo $colName?>").change(checkPasswordMatch);
            $("#<?php echo $colName?>_verify").change(checkPasswordMatch);
        });
    </script>
<?php
}



function GetRoles(){
    $ci =& get_instance();
    $ci->db->order_by('NAME', 'ASC');
    $ci->db->where('ID != ',1,FALSE);
    $ci->db->where('ID != ',2,FALSE);
    $query = $ci->db->get('cat_roles');
    $result = $query->result_array();
    return $result;
}
?>