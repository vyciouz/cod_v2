<?php


if (empty($_SESSION)) {
    // echo $host;
    // echo base_url();
    header("Location: " . base_url());
    exit;
}

if (isset($_SESSION['ID']) ) {
    $id = (empty($id)) ? $_SESSION['ID'] : $id;
}else{
    $id = 0;
}

$UserInfo = getUserData($id);
$ci =& get_instance();
$orderByCol = 2;
$inboxArray = $ci->mail_model->inbox($_SESSION['ID']);
$sentArray = $ci->mail_model->sent($_SESSION['ID']);
// $delegatedInboxArray = $ci->mail_model->delegatedInbox($_SESSION['ID']);
$rolesArray             = ReturnRolesArray($roles);
$rolesNeededForInbox    = [7];
$assitantAccess         = CheckAccess($rolesArray, $rolesNeededForInbox);
$queryProcuration = $this->db->query("SELECT
r_procuration.ID,
r_procuration.ID_REPRESENTED,
t_users.`NAMES`,
t_users.LAST_NAME,
t_users.MAIDEN_NAME
FROM
r_procuration
INNER JOIN t_users ON r_procuration.ID_REPRESENTED = t_users.ID
WHERE
r_procuration.ID_REPRESENTATIVE = $id
");
$resultProcuration = $queryProcuration->result_array();

foreach ($resultProcuration as $key => $value) {
    $queryCount = $this->db->query("SELECT
        Count(new_t_mail_threads.`READ`) AS NEW_MAIL
        FROM
        new_t_mail_threads
        WHERE
        new_t_mail_threads.ID_THREAD_OWNER = " . $resultProcuration[$key]['ID_REPRESENTED']);
    $resultCount = $queryCount->row_array();
    $resultProcuration[$key]["NEW_MAIL"] = $resultCount['NEW_MAIL'];
}
$delegatedInboxArray = $resultProcuration;


?>

<div class="content-wrapper">
    <div class="content">
        <div class="bg-white border rounded">
            <div class="row no-gutters">
                <div class="col-lg-4 col-xl-3">
                    <div class="profile-content-left pt-5 pb-3 px-3 px-xl-5">
                        <div class="card text-center widget-profile px-0 border-0">
                            <div class="card-body">
                                <h4 class="py-2 text-dark"><?php echo $UserInfo['NAMES'] . " " . $UserInfo['LAST_NAME'] . " " . $UserInfo['MAIDEN_NAME']?></h4>
                                <p><?php echo $UserInfo['EMAIL']?></p>
                            </div>
                        </div>

                        <hr class="w-100">
                        <div class="contact-info ">
                            <p class="text-dark font-weight-medium pt-4 mb-2">Correo electrónico</p>
                            <p><?php echo $UserInfo['EMAIL']?></p>

                            <p class="text-dark font-weight-medium pt-4 mb-2">Roles</p>
                            <p><?php PrintRoles($roles)?></p>

                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-xl-9">
                    <div class="profile-content-right py-5">
                        <ul class="nav nav-tabs px-3 px-xl-5 nav-style-border" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="timeline-tab" data-toggle="tab" href="#timeline" role="tab" aria-controls="timeline" aria-selected="true">Entrada</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Salida</a>
                            </li>
                        </ul>
                        <div class="tab-content px-3 px-xl-5" id="myTabContent">
                            <div class="tab-pane fade show active" id="timeline" role="tabpanel" aria-labelledby="timeline-tab">
                                <div class="media mt-5 profile-timeline-media">
                                    <div class="media-body">
                                        <?php DrawTable($inboxArray, "SUB_THREAD_KEY", false); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="media mt-5 profile-timeline-media">
                                    <div class="media-body">
                                        <?php DrawTable($sentArray, "SUB_THREAD_KEY", false); ?>
                                    </div>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                </div>

            </div>
            <?php
            if ($assitantAccess):?>
                <div class="row">
                <div class="col-12">
                
                    <div class="col-12">
                        <hr>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <p>Correo delegado</p>
                                    <p class="mb-5">
                                        <?php DelegatedInbox($delegatedInboxArray) ?>
                                    </p>
                                </div>
                        
                            </div>
                        
                        </div>
                    </div>
                </div>
            
            </div>
            <?php
            endif; ?>
           
        </div>
    </div>
</div>

<script>
$(function () {
    $('#documents').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        "pageLength": 25,
        // "order": [[ <?= $orderByCol ?>, "desc" ]],
        "language": {
            "search": "Búsqueda:",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "info":           "Mostrando de _START_ a _END_ entradas de un total de _TOTAL_ resultados",
            "infoEmpty":      "Mostrando de 0 a 0 entradas de 0 resultados",
        },
    })
})
  
$(".mail-element").click(function () {
    var id = $(this).prop("id");
    console.log(id);
    var url = "<?= base_url('index.php/mail/read/') ?>" + id;
    window.location.href = url;
});

</script>
<style>
    label {
        display: inline-block;
        width: 5em;
    }
</style>

<?php

function ShowEmpty(){?>
    <div style="text-align:center">
        Vacio<i class="fas fa-envelope"></i>
    </div>
    <?php
}

function PrintRoles($roles){
    // print_r($roles);
    foreach ($roles as $key => $value) {
        // echo $value['ID_ROLE'];
        GetRoleName($value['ID_ROLE']);
        echo (isset($roles[$key +1])) ? ", " : "" ;
    }
    $rol = [];
}


function GetRoleName($id){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $ci->db->where('ID', $id);
    $query = $ci->db->get('cat_roles');
    $row = $query->row();
    print_r($row->NAME);
    // return $result;
}

function DrawTable($data, $index, $actions){
    // print_r($source);
    $controller = "mail";
    $counter = 0;
    // print_r($data);
    echo '<table class="table table-hover mail table-striped" id ="documents">';
    echo "<thead>";
    
    if ($data) {
        foreach ($data[0] as $key => $value) {
            echo ( $key !== "ID" && $key !== "SENT_FROM" && $key !== "ID_PARENT_DOCUMENT" && $key !== "CURRENT_OWNER" && $key !== "READ" && $key !== "SUB_THREAD_KEY") ? "<th>" . $key . "</th>" : "" ;
        }

        echo ($actions) ? "<th> Acciones </th>" : "" ;
        echo "</thead>";
        echo "<tbody>";
    
        $odd = true;
        $trClass = "odd";
        foreach ($data as $key => $value):
            $read = (isset($value['READ']) && $value['READ']) ? "read" : "" ;
            echo "<tr class = '$controller $trClass $read mail-element' id = '" . $value[$index] . "'>";
            foreach ($value as $k => $v):
                $v = (strcmp($v,"")) ? $v : "--" ;
                echo ( $k !== "ID" && $k !== "SENT_FROM" && $k !== "ID_PARENT_DOCUMENT" && $k !== "CURRENT_OWNER" && $k !== "READ" && $k !== "SUB_THREAD_KEY") ? "<td>" . $v . "</td>" : "" ;
            endforeach;
            if ($actions) {
                echo "<td>";
                echo "<a href='" .base_url("index.php/$controller/read/" . $value[$index]) . "' class='btn btn-xs'><i class='fa fa-search'></i></a>";
                // echo "<a href='" .base_url("index.php/$controller/send/" . $value[$index]) . "'  class='btn btn-xs'><i class='fas fa-paper-plane'></i></a>";
                echo "</td>";
            }
            echo "</tr>";
            $odd = ($odd) ? false : true ;
            $trClass = ($odd) ? "odd" : "even" ;
            $counter++;
            if ($counter == 5) {
                break;
            }
        endforeach;
    }else{
        echo "<th>Tabla vacía</th>";
        echo "</thead>";
        echo "<tbody>";
        echo "<tr>";
        echo "<td>No hay información</td>";
        echo "</tr>";
    }
    
    echo "</tbody>";
    echo "</table>";
}

function GetRelationdValue($value, $column, $dependencyArray){
    $ci =& get_instance();
    $ci->db->where($dependencyArray['dependency']['fieldSearch'], $value);
    $query = $ci->db->get($dependencyArray['dependency']['table']);
    $row = $query->row_array();
    
    return $row[$dependencyArray['dependency']['fieldValue']];
}

function DelegatedInbox($dataArray){
    ?>
    <table>

    <?php
    foreach ($dataArray as $key => $value) {
        echo "<tr>";
        echo "<td>";
        
        echo "<a href='" . base_url('index.php/mail/d_inbox/' . $value['ID_REPRESENTED']) . "'>";
        echo $value['NAMES'];
        echo "</a>";
        echo "</td>";
        
        echo "<td>";
        echo "<a href='" . base_url('index.php/mail/d_inbox/' . $value['ID_REPRESENTED']) . "'> <i class = 'mdi mdi-email-alert'></i>";
        echo $value['NEW_MAIL'];
        echo "</a>";
        echo "</td>";
        echo "</tr>";
    }
    ?>
    </table>
    <?php

}