<?php

$settings = [
    "password" => [
        "displayName" => "contraseña"
    ]
];
?>

<style>
.tab-pane .col-md-6{
    padding-top: 3vh;
    /* padding-bottom: 3vh; */
}

.tab-pane > .col-sm-12{
    /* padding-top: 3vh; */
    /* padding: 3vh; */
    padding-left: 30px;
    padding-right: 30px;

}
</style>

<div class="content-wrapper">
	<div class="content">
		<div class="row">
			<div class="col-lg-12">

				<div class="card card-default">
					<div class="card-header card-header-border-bottom">
						<h2>Configuración de cuenta de usuario</h2>
					</div>
					<div class="card-body" style = "padding: 0;">
						<div class="row ">
							<div class="col-sm-4">
								<ul class="nav nav-tabs nav-stacked flex-column">
									<li class="nav-item">
										<a href="#t_pw" class="nav-link active" data-toggle="tab" aria-expanded="true">
											<i class="mdi mdi-key"></i> Contraseña</a>
									</li>
									<!-- <li class="nav-item">
										<a href="#vtab2" class="nav-link" data-toggle="tab" aria-expanded="false">
											<i class="mdi mdi-account"></i> Profile</a>
									</li>
									<li class="nav-item">
										<a href="#vtab3" class="nav-link" data-toggle="tab">
											<i class="mdi mdi-phone"></i> Contact</a>
									</li> -->
								</ul>
							</div>
							<div class="tab-content col-sm-8 ">
								<div class="tab-pane fade active show" id="t_pw" aria-expanded="true">
									<form action="" id = "change-password">
										<div class="row">
											<div class = "col-md-6 col-sm-12">
												<div class="form-group">
													<label for="">Nueva Contraseña</label>
													<input type="password" class="form-control input-lg" id="new_password" placeholder="Password" name="newP">
												</div>
												<div class="form-group">
													<label for="">Confirmar Contraseña</label>
													<input type="password" class="form-control input-lg" id="confirm_password" placeholder="Password" name="confirmP">
												</div>
												
													<p id = "CheckPasswordMatch" class = "invalid-feedback" style = "display:block">
													
													</p>
												
												
												<div class="form-group">
													<label for="">Contraseña Actual</label>
													<input type="password" class="form-control input-lg" id="old_password" placeholder="Password" name="oldP">
												</div>
												<p id = "change-result" class = "valid-feedback" style = "display:block">
													
												</p>
												<div class="">
													<button type="submit" class="btn btn-lg btn-primary btn-block mb-4">Cambiar contraseña</button>
												</div>
											</div>
										
										</div>
									</form>
								</div>
								<div class="tab-pane fade" id="vtab2" aria-expanded="false">
									Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
									aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
								</div>
								<div class="tab-pane fade" id="vtab3" aria-expanded="false">
									Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
									Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
									aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function () {
	$("#new_password").change(checkPasswordMatch);
	$("#confirm_password").change(checkPasswordMatch);

	$("#change-password").on('submit', function(e){
		var url = "<?= base_url() . 'index.php/user/settings_back/password'?>";
      	var form = new FormData(this);
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            beforeSend : function(data){
                //$("#preview").fadeOut();
                // $("#err").fadeOut();
                // console.log(data);
            },
            success: function(data){
				if (data.result) {
					$( "#change-result" ).removeClass( "invalid-feedback" );
					$( "#change-result" ).addClass( "valid-feedback" );
				}else{
					$( "#change-result" ).removeClass( "valid-feedback" );
					$( "#change-result" ).addClass( "invalid-feedback" );
				}
				$("#change-result").text(data.msg);
            },
            error: function(e){
                // $("#err").html(e).fadeIn();
            }          
        });
    });

});

function checkPasswordMatch() {
	var password = $("#new_password").val();
	var confirmPassword = $("#confirm_password").val();
	if (password != confirmPassword){
		$("#CheckPasswordMatch").text("¡Contraseña no coincide!");
		}
	else{
		$("#CheckPasswordMatch").text("");
	}
}       
</script>