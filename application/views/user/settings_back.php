<?php
session_start();
switch($method){
    case 'password':
    ChangePassword();
    break;

    default:
    break;


}

function ChangePassword(){
    // print_r($_POST);
    $ci =& get_instance();
    $query = $ci->db->get_where("t_users", ["ID" => $_SESSION['ID'], "PASSWORD" => $_POST['oldP']]);;
    $queryResult = $query->row_array();
    $queryResult = ($queryResult) ? true : false;
    $empty = (!strcmp($_POST['newP'], '')) ? true : false;
    $noMatch = (strcmp($_POST['newP'], $_POST['confirmP'])) ? true : false ;
    if ($empty) {
        $result = [ "result" => 0,
            "msg" => "Nueva contraseña no puede estar vacia.",
        ];
        echo json_encode($result);
        exit;
    }
    if ($noMatch) {
        $result = [ "result" => 0,
        "msg" => "Las contraseñas no coinciden.",
        "val 1" => $_POST['newP'],
        "val 2" => $_POST['confirmP']];
        echo json_encode($result);
        exit;
    }
    if ($queryResult) {
        $dataArray = [
            "PASSWORD"           => $_POST['newP'],
        ];
        $ci->db->where('ID', $_SESSION['ID']);
        $ci->db->update('t_users', $dataArray);
        
        $result = [ "result" => 1,
        "msg" => "Contraseña cambiada."];
    }else{
        $result = [ "result" => 0,
        "msg" => "Contraseña actual incorrecta."];
    }
    echo json_encode($result);
}

?>