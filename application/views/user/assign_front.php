<?php

if (!isset($ID_user) || empty($ID_user)) {
    // Redirigir a un 404
    header('Location: ' . base_url() . "index.php/system/error_404");
    exit;
}
$userData = getUserData($ID_user);

?>
<style>
.add-this-user{
    color: #7a808d;
    
}
.add-this-user:hover{
    color: #37a;
    padding-left: 5px;
    font-weight: bold;
    font-size: 15px;
}
.delete-this-user{
    color: #7a808d;
}
.delete-this-user:hover{
    color: #f43;
    padding-left: 5px;
    font-weight: bold;
    font-size: 15px;
}
.hidden{
    display: none;
}

</style>
<div class="content-wrapper">
    <div class="content">
        <div class="row">
		    <div class="col-lg-12">
			    <div class="card card-default">
				    <div class="card-header card-header-border-bottom">
					    <h2>Basic Form Controls</h2>
					</div>
					<div class="card-body">
					    <form action="" id = "assign-assistant-to">
                            <div class="form-group">
                                <p >Asignar a <span style = "font-weight:bold"><?= $userData['NAMES'] . " " . $userData['LAST_NAME']?></span> como asistente para:</p>
                                <input type="text" class="form-control" id="search-user" placeholder="Buscar Usuario">
                                <span class="mt-2 d-block">Hacer click para agregar a <?= $userData['NAMES']?> como asistente para los siguientes usuarios.</span>
                            </div>
                            <div class="form-group" id = "user-result">
                            </div>
                            <hr>
                            <div class="form-group" id = "user-holder">
                            Se asignará a <?= $userData['NAMES'] . " " . $userData['LAST_NAME'] ?> como asistente de los siguientes usuarios:
                            </div>
                            
                            <div class="form-group" id = "user-holder">
                            </div>
                            <div class="form-group">
                                <button class = "btn btn-primary">Terminar de asignar</button>
                            </div>
                        </form>
						
					</div>
				</div>
            </div>
		</div>
    </div>
</div>

<script>
$(document).ready(function(){
    SearchUsers();
    AddThisUser();
    SendBack();
});

function SearchUsers(){
    $("#search-user").on('input', function(){
        var searchVal = $(this).val();
        var urlMyDocs = "<?= base_url('index.php/JsonResponse/jsonUsersArgs/') ?>" + searchVal;
        var usrID = "";
        var usrName = "";
        if (searchVal.length == 0) { $("#user-result").html(""); }
        if (searchVal.length > 2) {
            $("#searchResult").text("");
            $.ajax({
                url: urlMyDocs,
                method: "post",
                data:  {
                },
                dataType: 'json',
                success: function(data){
                    if (data.length > 10) { limit = 10; }
                    if (data.length < 10){ limit = data.length; }
                    if (data.length == 0) { $("#user-result").html(""); }
                    $("#user-result").html("");
                    for (var key = 0; key < limit; key++) {
                        
                        usrID = data[key].ID;
                        usrName = data[key].NAMES + " " + data[key].LAST_NAME + " " + data[key].MAIDEN_NAME;
                        usrRow = "<a href='#' class = ' add-this-user ' uname = '" + usrName + "' id = '" + usrID + "'><i class = 'mdi mdi-account-plus'></i> " + usrName+ " </a>";
                        $("#user-result").append( "<div>" + usrRow + "</div>");
                    }
                    AddThisUser();
                },
                error: function(e){
                    // $("#err").html(e).fadeIn();
                }          
            });
        }
    });
}

function SendBack(){
    $("#assign-assistant-to").on('submit', function(e){
        var form = new FormData(this);
        var url = "<?= base_url('index.php/user/assign_back/' . $ID_user)?>";
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            beforeSend : function(data){
                //$("#preview").fadeOut();
                // $("#err").fadeOut();
                // console.log(data);
            },
            success: function(data){
                if(data.result){
                    window.location.href = "<?= base_url() . 'index.php/user/view/'?>" + data.ID;
                }
                else{
                    // console.log(data);
                }
            }
        });
    });
}

function DeleteThisUser(){
    $(".delete-this-user").on('click', function(){
        $(this).remove();
    });
}

function AddThisUser(){
    $(".add-this-user").on('click', function(){
        var userID = $(this).attr("id");
        var usrName = $(this).attr("uname");
        usrRow = "<a href='#' class = ' delete-this-user ' id = '" + userID + "'>"
            + "<i class = 'mdi mdi-delete-outline'></i> " + usrName
            + " <input class = 'hidden' name = 'usr_" + userID + "' value = '" + userID + "' type='text'></a>";
        $("#user-holder").append( "<div>" + usrRow + "</div>");
        DeleteThisUser();
    });

}
</script>