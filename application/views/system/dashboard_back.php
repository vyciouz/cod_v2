<?php
switch ($method) {
    case 'server_time':
        ShowServerTime();
        break;

    case 'delete_documents':
        DeleteDocumentRecords();
        break;
    
    default:
        # code...
        break;
}



function ShowServerTime(){
    $time = date("H:i:s A");
    echo $time;
}

function DeleteDocumentRecords(){
$tables = ["r_drafts", "t_drafts", "t_documents", "t_documents_due_date", "t_documents_signed", "t_mail", "t_notifications", "t_status", "t_turned"];
$ci =& get_instance();

foreach ($tables as $table) {
    $ci->db->empty_table($table);
}

}
?>