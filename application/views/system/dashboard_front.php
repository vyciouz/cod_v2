<?php
$workPC = (file_exists("C:/xampp/apache/bin/openssl.exe")) ? true : false ;
$homePC = (file_exists("G:/xampp/apache/bin/openssl.exe")) ? true : false ;
$testServer = (file_exists("/usr/bin/openssl")) ? true : false ;
$timeZone = date_default_timezone_get();
$time = date("H:i:s A");
$directories = [
    "data/",
    "data/temp/",
    "data/userdata/",
];

$todo = [
    "Estado" => "Leído (abrió el correo o enlace), Enterado (Conscientemente admite que leyó el correo y presionó un botón de ENTERADO)",
    "Responder" => "Respuesta dirigida a la persona remitente del correo o documento a responder.",
    "Notificaciones"=>"Agregar notificaciones o avisos de documentos recibidos, turnados, copiados, etc.
        ID, Fecha, Tipo de notificación, ID Documento, ID Usuario, Vista o no (estilo FB)",
    ];
  
// echo $this->db->hostname;
// echo $this->db->username;
// echo $this->db->password;
// echo $this->db->database;
/*
Our php.ini contains the following settings:

display_errors = On
register_globals = Off
post_max_size = 8M
*/

function return_bytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    switch($last) {
        // The 'G' modifier is available since PHP 5.1.0
        case 'g':
            $val *= 1024;
        case 'm':
            // $val *= 1024;
        case 'k':
            // $val *= 1024;
    }

    return $val;
}
?>


<div class="content">
    <div class="content-wrapper">

        <div class="row">
			<div class=" col-xl-4 ">
                <div class="card card-default" id="analytics-device" data-scroll-height="580" style="height: 580px; overflow: hidden;">
                    <div class="card-header justify-content-between">
                        <h2>Información del servidor</h2>
                    </div>
                    <div class="card-body">
                        <div class="row no-gutters justify-content-center">
                            <div class="col-4 col-lg-3">
                                <div class="card card-icon-info text-center border-0">
                                    <i class="mdi mdi-desktop-mac"></i>
                                    <p class="pt-3 pb-1" style = "min-height: 70px">Casa</p>
                                    <h6>
                                        <?php
                                        if ($homePC) {?>
                                            <span class="badge badge-success">O</span><?php
                                        } else {?>
                                            <span class="badge badge-danger">X</span><?php
                                        }?>
                                    </h6>
                                </div>
                            </div>
                            <div class="col-4 col-lg-3">
                                <div class="card card-icon-info text-center border-0">
                                    <i class="mdi mdi-tablet-ipad"></i>
                                    <p class="pt-3 pb-1" style = "min-height: 70px">Laptop</p>

                                    <h6>
                                        <?php
                                        if ($workPC) {?>
                                            <span class="badge badge-success">O</span><?php
                                        } else {?>
                                            <span class="badge badge-danger">X</span><?php
                                        }?>
                                    </h6>
                                </div>
                            </div>
                            <div class="col-4 col-lg-3">
                                <div class="card card-icon-info text-center border-0">
                                    <i class="mdi mdi-cellphone-android fa-3x"></i>
                                    <p class="pt-3 pb-1" style = "min-height: 70px">Server de Prueba</p>
                                    <h6>
                                        <?php
                                        if ($testServer) {?>
                                            <span class="badge badge-success">O</span><?php
                                        } else {?>
                                            <span class="badge badge-danger">X</span><?php
                                        }?>
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutters justify-content-center">
                            <div class="col-12 col-lg-12">
                                <p class="mb-6 card-icon-info"><i class="mdi mdi-console"></i> <?php echo (defined('OPENSSL_ROUTE')) ? OPENSSL_ROUTE : "OpenSSL no encontrado.";?></p>
                                <p class="mb-6 card-icon-info"><i class="mdi mdi-alert"></i> <?php echo 'display_errors = ' . ini_get('display_errors'); ?></p>
                                <p class="mb-6 card-icon-info"><i class="mdi mdi-upload fa-3x"></i> <?php echo 'post_max_size = ' . ini_get('post_max_size'); ?></p>
                                <p class="mb-6 card-icon-info"><i class="mdi mdi-upload fa-3x"></i> <?php echo 'post_max_size in bytes = ' . return_bytes(ini_get('post_max_size')); ?></p>
                                <p class = "mb-6 card-icon-info"><i class="mdi mdi-globe-model fa-3x"></i> <?= $timeZone ?></p>
                                <p class = "mb-6 card-icon-info" id="server-time"><i class="mdi mdi-clock-outline fa-3x"></i> <?= $time ?></p>
                            </div>
                        </div>
                    </div>
                </div>
				    </div>

				    <div class=" col-xl-4 ">
                <!-- Page Views  -->
                <div class="card card-default table-borderless" id="page-views" data-scroll-height="580" style="height: 580px; overflow: hidden;">
                    <div class="card-header justify-content-between">
                        <h2>Permisos carpetas</h2>
                    </div>
                    <div id = "directories" class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><div class="card-body slim-scroll py-0" style="overflow: hidden; width: auto; height: 100%;">
                        <table class="table page-view-table ">
                            <thead>
                                  <tr>
                                    <th>Carpeta/Archivo</th>
                                    <th>Existe</th>
                                    <th>Permisos</th>
                                  </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($directories as $key => $value):?>
                                <tr>
                                    <td class="text-primary"><a class="link" href="analytics.html"><?php echo $value?></a></td>
                                    <td><?php echo (file_exists($value)) ? "O" : "X" ;?></td>
                                    <td>
                                        <?php
                                        if (file_exists($value)) {
                                            echo substr(sprintf('%o', fileperms($value)), -4);
                                            
                                        }?>
                                    </td>
                                </tr>
                                <?php
                                    $files = array_diff(scandir($value), array('.', '..'));
                                    foreach ($files as $fileKey => $fileValue):
                                        if (!is_dir($value . $fileValue)) {?>
                                            <tr>
                                                <td class="text-primary"><a class="link" href="analytics.html"><?php echo substr($fileValue, 0, 8) . "..." . substr($fileValue, -8);?></a></td>
                                                <td><?php echo (file_exists($value . $fileValue)) ? "O" : "X" ;?></td>
                                                <td>
                                            <?php
                                            if (file_exists($value . $fileValue)) {
                                                echo substr(sprintf('%o', fileperms($value . $fileValue)), -4);
                                            }?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    endforeach;
                                endforeach;
                                ?>
                                  
                                </tbody>
                              </table>
                            </div><div class="slimScrollBar" style="background: rgb(153, 153, 153); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 268.04px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                          </div>

				</div>
				<div class=" col-xl-4 ">
					<!-- Notification Table -->
					<div class="card card-default" data-scroll-height="580" style="height: 580px; overflow: hidden;">
						<div class="card-header justify-content-between">
							<h2>Borrar</h2>
							<div>
							
							</div>

						</div>

						<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><div class="card-body slim-scroll py-4" style="overflow: hidden; width: auto; height: 100%;">
							
                            <div class="media pb-4 align-items-center justify-content-between">
                                <button type="button" id ="delete_documents" class="mb-1 btn btn-block btn-danger ladda-button" data-style="expand-left" >
                                    Borrar Registros de Documentos
                                </button>
							</div>
						</div>
                    </div>
				</div>
			</div>
		</div>
         
    </div>
</div>
<script>

$( document ).ready(function() {
    urlServerTime = "<?= base_url()?>index.php/system/dashboard_back/server_time";

    $("#delete_documents").click(function(event) {
        url = "<?= base_url()?>index.php/system/dashboard_back/delete_documents";
        event.preventDefault();
        var form = $("#create_new_document")[0];
        var formData = new FormData(form);
        // console.log(formData);
        $.ajax({
            url: url,
            method: "post",
            data:  formData,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            beforeSend: function(data){
                var buttonLoading = '<div class="sk-circle" style= "margin: auto">'
                    + '<div class="sk-circle1 sk-child"></div>'
                    + '<div class="sk-circle2 sk-child"></div>'
                    + '<div class="sk-circle3 sk-child"></div>'
                    + '<div class="sk-circle4 sk-child"></div>'
                    + '<div class="sk-circle5 sk-child"></div>'
                    + '<div class="sk-circle6 sk-child"></div>'
                    + '<div class="sk-circle7 sk-child"></div>'
                    + '<div class="sk-circle8 sk-child"></div>'
                    + '<div class="sk-circle9 sk-child"></div>'
                    + '<div class="sk-circle10 sk-child"></div>'
                    + '<div class="sk-circle11 sk-child"></div>'
                    + '<div class="sk-circle12 sk-child"></div>'
                + '</div>';
                $("#delete_documents").html(buttonLoading);
            },
            success: function(data){
                console.log(data);
            
            },
            complete: function(){
                $("#delete_documents").html("Borrar Registros de Documentos");
            },
            error: function(e){
            }          
        });
    });

    executeQuery();
});

function executeQuery() {
  $.ajax({
    url: urlServerTime,
    success: function(data) {
        // console.log(data);
        $("#server-time").html('<i class="mdi mdi-clock-outline fa-3x"></i> ' + data);
    }
  });
  setTimeout(executeQuery, 1000); // you could choose not to continue on failure...
}
</script>