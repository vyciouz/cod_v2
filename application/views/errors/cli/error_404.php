<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Sleek Dashboard - Free Bootstrap 4 Admin Dashboard Template and UI Kit. It is very powerful bootstrap admin dashboard, which allows you to build products like admin panels, content management systems and CRMs etc.">


	<title>404</title>

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500" rel="stylesheet">
	<link href="https://cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css" rel="stylesheet">





	<!-- SLEEK CSS -->
	<link id="sleek-css" rel="stylesheet" href="<?php echo base_url() ?>assets/css/sleek.css">



	<script src="<?php echo base_url() ?>assets/plugins/nprogress/nprogress.js"></script>
</head>
<div class="content">						
	<div class="error-wrapper rounded border bg-white px-5">
		<div class="row justify-content-center">
			<div class="col-xl-4">
				<h1 class="text-primary bold error-title">404</h1>
				<p class="pt-4 pb-5 error-subtitle">Looks like something went wrong.</p>
				<a href="<?php echo base_url()?>" class="btn btn-primary btn-pill">Back to Home</a>
			</div>
			<div class="col-xl-6 pt-5 pt-xl-0 text-center">
				<img src="<?php echo base_url() ?>assets/img/lightenning.png" class="img-responsive" alt="Error Page Image">
			</div>
		</div>
	</div>
</div>