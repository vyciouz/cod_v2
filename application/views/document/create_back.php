<?php
session_start();
echo $ID_Draft;

/*
****** Agregar registro a t_mail¨******
TYPE es Respuesta, Re-envio, Directo (cat_status)
****** Agregar registro a t_notifications¨******
Backend para crear el PDF que será firmado digitalmente y envíado a
algún destinatario y CC en caso de ser requerido.

Métodos públicos:
    - FPDF

Métodos privados:
    - RegisterDocumentInDB()
    - SendAndAddToDB()
    - GenerarPDF()
    - getUserData()

Si son 10 copiados en CC, hacer más de 1 página en el pdf y colocarlos en 
la segunda página.
El código QR iría en la última página en ese caso.
Si son menos de 10 en CC, el formato quedará igual, en 1 página.
*/
# Librería para generar el PDF

switch ($method) {
    case 'save_draft':
        SaveDraft();
        break;

    case 'create_send':
        CreateSend($ID_Draft);
        break;

    case 'value':
        # code...
        break;
    
    default:
        # code...
        break;
}

use \setasign\Fpdi\Fpdi;

function SaveDraft(){
    $flagEmpty = true;
    foreach ($_POST as $key => $value) {
        $flagEmpty = ($value) ? 1 : 0 ;
        if ($flagEmpty) { break; }
    }
    if (!$flagEmpty) {
        $result = [ "result" => 0,
        "msg" => "Los campos no pueden estar vacios."];
        echo json_encode($result);
        exit;
    }

    $dataArray = [
        "TITLE"      => $_POST['subject'],
        "BODY"       => $_POST['textMail'],
        "DOC_TITLE"  => $_POST['title'],
        "DOC_BODY"   => $_POST['textBody']
    ];
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $ci->db->insert('t_drafts', $dataArray);
    $recentId = $ci->db->insert_id();
    $dataR_Drafts = [
        "ID_DRAFT"   => $recentId,
        "ID_OWNER"   => $_SESSION['ID'],
        "ID_EDITOR"  => $_SESSION['ID'],
    ];
    $ci->db->insert('r_drafts', $dataR_Drafts);
    $result = [ "result" => $recentId,
    "msg" => "Borrador guardado."];
    echo json_encode($result);
}

function CreateSend($ID_Draft = null){    
    include("./assets/plugins/fpdf/fpdf.php");
    include("./assets/plugins/qrlib/qrlib.php");
    
    $to         = 0;
    $cc         = [];
    $title      = "";
    $addressees = [];
    $sender     = (isset($_POST['REP']) && !empty($_POST['REP'])) ? $_POST['REP'] : $_SESSION['ID'] ;
    
    echo "post: ";
    print_r($_POST);
    
    foreach ($_POST as $key => $value) {
        // $key tiene "to"
        // $key tiene "cc"
        // $key tiene "textbody"
        // $key tiene "title"
        // echo $key . "\n";
        // array receiver = [id=> 0, cc =>true/false]
        if(strpos($key,'to') !== false){
            echo "Directo: $value\n";
            $to = $value;
            array_push($addressees,['ID'=>$value, 'CC' => 0]);
        }
        if(strpos($key,'cc') !== false){
            echo "Con copia: $value\n";
            array_push($cc,$value);
            array_push($addressees,['ID'=>$value, 'CC' => 1]);
        }
        if(strpos($key,'title') !== false){
            echo "Título: $value\n";
        }
        if(strpos($key,'textMail') !== false){
            echo "Texto Carta: $value\n";
        }
        if(strpos($key,'textBody') !== false){
            echo "Texto Documento: $value\n";
        }
    }
    if($to == 0){
        $error = [ "error" => 1,
        "msg" => "Elija destinatario"];
        echo json_encode($error);
        exit;
    }
    if (!$addressees) {
        SendError("Elija destinatarios.");
        exit;
    }
    
    /*
    Primero se checa el número de usuario copiados, si es un número mayor a X
    el PDF generado llevará N páginas. (Dividir un número de CC por página.)
    
    Se obtiene la información del usuario que envió el oficio, para incluirla
    en el mismo. Esta información es:
        - Nombre, Apellidos
        - Dependencia
        - U Admin
        - Sub U Admin
        - Cargo
    Una vez obtenia la información, el PDF es generado.
    Luego, hacer llenado del arreglo $document
    */
    
    $dateCurrent                    = new DateTime(date("Y-m-d H:i:s"));
    $document[0]['TITLE']           = ($_POST['title'] == "") ? $dateCurrent->format("Ymd_His") : $_POST['title'] ;
    $document[0]['ORIGINAL_OWNER']  = $sender;
    $document[0]['SENT_FROM']       = 0;
    $document[0]['NAME']            = str_replace(' ', '_', $_POST['title']) . ".pdf"; # pero cambiando ' ' por '_'
    $document[0]['CURRENT_OWNER']   = $sender;
    $document[0]['DATE_CREATED']    = $dateCurrent->format("Y-m-d H:i:s");
    $document[0]['BODY']            = $_POST['textBody'];
    
    // Registro del documento original.
    $document = GenerarPDF($document, $to);
    SendAndAddToDB ($document, $document[0]['ID'], $addressees);
    echo "hola";
    echo $ID_Draft;
    DeleteDraft($ID_Draft);
}

    
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************

// Generar PDF regresa el ID del documento generado. Este ID se utiliza para 
// diferenciar entradas con nombre del archivo o títulos similares
function GenerarPDF($document, $to){
    // Se registra la primer entrada del docmuento, la cual correponde
    // al emisor.
    $recentId   = RegisterDocumentInDB($document);
    $sender     = getUserData($document[0]['ORIGINAL_OWNER']);
    $addressee  = getUserData($to);
    $defaultNoLogo = "assets/img/system/no_logo.png";
    
    $logoDependency = "data/dependencies/". $sender['DEPENDENCIA']. "/logo.png";
    $logoD = (file_exists($logoDependency)) ? $logoDependency :$defaultNoLogo;
    $codLogo = "assets/img/system/campaign.png";
    $logoC = (file_exists($codLogo)) ? $codLogo :$defaultNoLogo;
    
    // Generar carpetas en caso de que no existan.
    if (!file_exists("./data/userdata/".$sender['ID']."/")) {
        mkdir("./data/userdata/".$sender['ID']."/", 0777);
    }
    if (!file_exists("./data/userdata/".$sender['ID']."/documents/")) {
        mkdir("./data/userdata/".$sender['ID']."/documents/", 0777);
    }
    $dest = $addressee['NAMES'] ." " . $addressee['LAST_NAME'] ." " .$addressee['MAIDEN_NAME'] ." " ."
        ". $addressee['PUESTO']. "
        ". $addressee['NAME_SUA']. "
        ". $addressee['NAME_UA']. "
        ". $addressee['NAME_DEP']. "
        PRESENTE.";

    $rem = $sender['NAMES'] ." " . $sender['LAST_NAME'] ." " .$sender['MAIDEN_NAME'] ." " ."
        ". $sender['PUESTO']. "
        ". $sender['NAME_SUA']. "
        ". $sender['NAME_UA']. "
        ". $sender['NAME_DEP'];
    $key = "MTIhYTYwkmsdvgnmdsofibmsdogfbkmoidfgkn
oidfkgodfgmnoidfgmnomdfgoin934jkengiun3r9
gmJNDIosxcvimsdv9aasfINASDINejfi09eJNIN0
9duvsincNJSOI9jkcvnkjansoafi893983KJNKNoo
miunNibsisdf8jdf8nf8edjns8jJ8edhfh7f7ws9jHH
H7wsdh8sf";

    $docInfo = "Monterrey, N.L. " . $document[0]['DATE_CREATED'] . "\nOficio Electrónico No. " . $sender['NOM_DEP'] ."/". $sender['NOM_UA'] ."-" .date("Y") ."-" .$recentId ."\nAsunto: " . $document[0]['TITLE'];
    $fileName = $document[0]['NAME'];
    $textBody = $document[0]['BODY'];

    $pdf = new FPDF();
    $pdf->AddPage('P', 'A4');
    $pdf->SetAutoPageBreak(true, 10);
    $pdf->SetFont('Arial', '', 12);
    $pdf->SetTopMargin(10);
    $pdf->SetLeftMargin(10);
    $pdf->SetRightMargin(10);

    // Imágenes
    $pdf->Image($logoD, 10, 20, 50, 25);
    $pdf->Image($logoC, 148, 20, 50, 25);
    $pdf->Image("./assets/img/system/qr.png", 15, 220, 40, 0);
    $pdf->Image("./assets/img/system/Logo_nuevo_leon.png", 155, 270, 45, 0);

    // Header
    $pdf->Cell(190, 12, 'La Nueva Independencia', 0, 1, 'C', false);

    // Infor del documento
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetY(50);
    $docInfo = iconv('UTF-8', 'windows-1252', $docInfo);
    $pdf->MultiCell(190,4,$docInfo ,0,'R',false);
    $pdf->Ln();

    // Destinatario
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(10,5,'',0,0,'C');
    $dest = iconv('UTF-8', 'windows-1252', $dest);
    $pdf->MultiCell(170,5,$dest ,0,'L',false);
    $pdf->Ln();

    // Cuerpo de la carta
    $pdf->SetFont('Courier','',10);
    $pdf->Cell(10,10,'',0,0,'C');
    $textBody = iconv('UTF-8', 'windows-1252', $textBody);
    $pdf->MultiCell(170,5,$textBody,0,'L',false);

    // Footer
    $pdf->SetY(220);
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(50,5,'',0,0,'C');
    $fe = iconv('UTF-8', 'windows-1252', "FIRMA ELECTRÓNICA");
    $pdf->Cell(80,5,$fe,0,0,'L');
    $pdf->Cell(60,5,'ATENTAMENTE',0,0,'R');
    $pdf->Ln();
    $pdf->SetY(230);
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(50,5,'',0,0,'C');
    $pdf->MultiCell(80,5,$key,0,'L',"");
    $pdf->SetFont('Arial','',10);
    $pdf->SetXY(140,230);
    $rem = iconv('UTF-8', 'windows-1252', $rem);
    $pdf->MultiCell(60,5,$rem,0,'R',"");
    $pdf->Ln();$pdf->Ln();

    // $pdf->MultiCell(140,5,'CCP Destinatario, CCP Destinatario, CCP Destinatario, CCP Destinatario, CCP Destinatario, CCP Destinatario, CCP Destinatario, CCP Destinatario, ',0,'L','');

    $savedFile = "./data/userdata/".$sender['ID']."/documents/$recentId" . "_$fileName";
    $pdf->Output($savedFile,'F');
    
    $document[0]['FOLIO'] = $sender['NOM_DEP'] ."/". $sender['NOM_UA'] ."/". $sender['NOM_SUA'] ."-" .date("Y") . "-".$recentId;
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $document[0]['ID'] = $recentId;
    $dataArray = [
        "FOLIO"           => $document[0]['FOLIO'],
    ];
    $ci->db->where('id', $recentId);
    $ci->db->update('t_documents', $dataArray);
    
    return $document;
}

/*
Esta función toma el documento previamente generado y cicla por los ID's de
los usarios a quienes les será enviado el documento con copia. */
function SendAndAddToDB ($document, $recentId, $addressee){
    $docTitle               = $document[0]['TITLE'];
    $document[0]['NAME']    = str_replace(' ', '_', $docTitle) . ".pdf";

    $docOriginalOwnerID = $document[0]['ORIGINAL_OWNER'];
    $baseFilesRoute     = "./data/userdata/"; #Ruta base donde se guardan los documentos
    # $newfile será la ruta donde se guardará para cada usuario.
    $document[0]['ID_PARENT_DOCUMENT']      = $recentId;
    $document[0]['ID_ORIGINAL_DOCUMENT']    = $recentId;
    $document[0]['SENT_FROM']               = $docOriginalOwnerID;
    // echo $document[0]['NAME'];
    $srcFile = $baseFilesRoute . "$docOriginalOwnerID/documents/" . $recentId . "_" . $document[0]['NAME'];
    echo $srcFile . "\n";
    foreach ($addressee as $key => $value) {
        echo "Nuevo dueño: " . $value['ID'] . "\n";
        $document[0]['CURRENT_OWNER']   = $value['ID'];
        $document[0]['CC']              = ($value['CC'] == 1) ? 1 : 0;
        // print_r($document);
        if (file_exists($srcFile)) {
            // echo "copiando\n";
            $newID = RegisterDocumentInDB($document);
            $destfile = $baseFilesRoute . $value['ID'] . "/documents/" . $newID . "_" . $document[0]['NAME'];
            AddRegistryMail($newID, $value['CC']);
            AddRegistryNotification($document, $newID);
            
            // Quitar este IF si se decide que el estatus se requiere en los CC
            if (!$value['CC']) {
                AddRegistryStatus($newID);
                if (isset($_POST['dueDate']) && !empty($_POST['dueDate']) ) {
                    AddRegistryDueDate($newID);
                }
            }
            // echo "-> $destfile\n";
            copy($srcFile, $destfile);
        }
    }
}

// to[], cc[], originalOwner, currentOwner

function RegisterDocumentInDB($document){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $document[0]['CC']                      = (isset($document[0]['CC'])) ? $document[0]['CC'] : 0;
    $document[0]['ID_ORIGINAL_DOCUMENT']    = (isset($document[0]['ID_ORIGINAL_DOCUMENT'])) ? $document[0]['ID_ORIGINAL_DOCUMENT'] : 0;
    $document[0]['ID_PARENT_DOCUMENT']      = (isset($document[0]['ID_PARENT_DOCUMENT'])) ? $document[0]['ID_PARENT_DOCUMENT'] : 0;
    $document[0]['FOLIO']                   = (isset($document[0]['FOLIO'])) ? $document[0]['FOLIO'] : null;
    $dataArray = [
        "TITLE"                 => $document[0]['TITLE'],
        "SENT_FROM"             => $document[0]['SENT_FROM'],
        "DATE_CREATED"          => $document[0]['DATE_CREATED'],
        'DATE_RECEIVED'         => $document[0]['DATE_CREATED'],
        "CURRENT_OWNER"         => $document[0]['CURRENT_OWNER'],
        "ORIGINAL_OWNER"        => $document[0]['ORIGINAL_OWNER'],
        "CC"                    => $document[0]['CC'],
        "ID_ORIGINAL_DOCUMENT"  => $document[0]['ID_ORIGINAL_DOCUMENT'],
        "ID_PARENT_DOCUMENT"    => $document[0]['ID_PARENT_DOCUMENT'],
        "FOLIO"                 => $document[0]['FOLIO'],
    ];
    $ci->db->insert('t_documents', $dataArray);
    $recentId = $ci->db->insert_id();
    $document[0]['NAME'] = $recentId . "_" . $document[0]['NAME'];
    $dataArray = [
        "NAME"           => $document[0]['NAME'],
    ];
    $ci->db->where('id', $recentId);
    $ci->db->update('t_documents', $dataArray);
    
    Attachments($recentId, $document[0]['CURRENT_OWNER']);
    return $recentId;
}

function AddRegistryMail($IDDocument, $CC){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $type = ($CC) ? 2 : 1;
    $dataArray = [
        "ID_DOCUMENT"   => $IDDocument,
        "ID_TYPE"       => $type,
        'SUBJECT'       => $_POST['subject'],
        'BODY'          => $_POST['textMail'],
    ];
    $ci->db->insert('t_mail', $dataArray);
}

function AddRegistryNotification($document, $IDDocument){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $type = ($document[0]['CC']) ? 2 : 1;
    $dataArray = [
        "ID_USER"   => $document[0]['CURRENT_OWNER'],
        "ID_TYPE"   => $type,
        "ID_DOC"    => $IDDocument,
        'DATE'      => $document[0]['DATE_CREATED'],
    ];
    $ci->db->insert('t_notifications', $dataArray);
}

function AddRegistryStatus($IDDocument){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $status = (isset($_POST['status'])) ? $_POST['status'] : 2 ;
    $dataArray = [
        "ID_DOCUMENT"   => $IDDocument,
        "STATUS"        => $status
    ];
    $ci->db->insert('t_status', $dataArray);
}

function Attachments($recentId, $user){
    if (isset($_FILES) && !empty($_FILES)) {
        $path = "data/userdata/$user/";
        if (!file_exists($path)) {
            mkdir($path, 0777);
        }
        $path = "data/userdata/$user/documents/";
        if (!file_exists($path)) {
            mkdir($path, 0777);
        }
        $path = "data/userdata/$user/documents/$recentId/";
        if (!file_exists($path)) {
            mkdir($path, 0777);
        }
        foreach ($_FILES as $key => $value) {
            if ($_FILES[$key]['error'] == 0) {
                $fileName = $_FILES[$key]['name'];
                $file_tmp = $_FILES[$key]['tmp_name'];
                $file = $path . $fileName;
                copy($file_tmp, $file);
            } else {
                if ($_FILES[$key]['error'] == 4){
                    // echo "No archivos selecionados. \n";
                }   
            }
        }
    }
}

function AddRegistryDueDate($ID_document){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    // If hora
    // $hora = ($hora) ? $hora : "23:59:59" ;
    $hora = $_POST['dueDate'] . " 23:59:59";
    $dataArray = [
        "ID_DOCUMENT"   => $ID_document,
        "DATE"          => $hora
    ];
    $ci->db->insert('t_documents_due_date', $dataArray);
}

function DeleteDraft($ID_Draft){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $ci->db->where('ID', $ID_Draft);
    $ci->db->delete("t_drafts");

    $ci->db->where('ID_DRAFT', $ID_Draft);
    $ci->db->delete("r_drafts");
    
    exit;
}
?>