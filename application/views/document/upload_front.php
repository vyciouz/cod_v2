<div class="content-wrapper">
    <div class="content">
        <div class="row">
    
            <div class="col-lg-12">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2>Subir nuevo documento</h2>
                    </div>
                    <div class="card-body">
                        <form id = "upload-doc" method = "post">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Título</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Título" name = "title">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Documento</label>
                                <input type="file" class="form-control-file" id="main-file" name ="main-file"  accept="application/pdf"  required>
                            </div>
                            <!-- <div class="form-group">
                                <label for="exampleFormControlTextarea1">Cuerpo</label>
                                <textarea class="form-control" id="texBody" name = "textBody" rows="3"></textarea>
                            </div> -->
                            <div class="form-group" id="attachments-container">
                                    <label for=""><i class="mdi mdi-paperclip"></i>Archivo(s) adjunto(s):</label><br>
                                    <input type="file" name = "userfile_1" class="form-control-file 1" id="exampleFormControlFile1" style = "width: 50%; display: inline">
                                    <button type="submit" id="add_file_input"  class="mb-1 btn btn-sm btn-outline-primary">+</button>
                            </div>
                            <div class="form-footer pt-4 pt-5 mt-4 border-top">
                                <button type="submit" class="btn btn-primary btn-default">Subir</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="exampleModalTooltip" tabindex="-1" role="dialog" aria-labelledby="exampleModalTooltip" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle2">Modal Title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Popover in a modal</h5>
                <p>This
                    <a href="#" role="button" class="btn btn-secondary popover-test" data-toggle="popover" title="" data-content="Popover body content is set in this attribute." data-original-title="Popover title">button</a> triggers a popover on click.</p>
                <hr>
                <h5>Tooltips in a modal</h5>
                <p>
                    <a href="#" class="tooltip-test" data-toggle="tooltip" title="" data-original-title="Tooltip">This link</a> and
                    <a href="#" class="tooltip-test" data-toggle="tooltip" title="" data-original-title="Tooltip">that link</a> have tooltips on hover.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-pill" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-pill">Save Changes</button>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){

    var url = "<?php echo base_url()?>index.php/document/upload_back";
    const form = document.querySelector('form');
    var accetpFileTypes = "";



    $("#add_file_input").click(function(event) {
        event.preventDefault();
        var counter = $("#attachments-container input" ).last().attr("class");
        
        counter = counter.replace(/\D/g,'');
        counter = parseInt(counter, 10);
        counter++;
        var newInput = 
            '<div class="' + counter + '">' +
                '<input type="file" class="form-control-file ' + counter + '" id="exampleFormControlFile1" style = "width: 50%; display: inline" name = "userfile_' + counter + '"> ' +
                '<button type="submit" id="add_file_input" style="min-width : 25px" class="btn btn-outline-danger btn-sm btn-default remove ' + counter + '">-</button>' +
            '</div>';

        $("#attachments-container").append(newInput);

        $(".remove").click(function( event ) {
            event.preventDefault();
            removeClass = $(this).attr("class");
            removeClass = removeClass.replace(/\D/g,'');
            removeClass = "." + removeClass;
            $(removeClass).remove();
        })
        // $(this).remove();
    });
    
    
    $(".remove").click(function(){
        this.remove();
    })

  
    
    $("#upload-doc").on('submit', function(e){
      var form = new FormData(this);
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'text',
            beforeSend : function(data){
                //$("#preview").fadeOut();
                // $("#err").fadeOut();
                // console.log(data);
            },
            success: function(data)
            {
                if(data=='invalid'){
                    // invalid file format.
                    // $("#err").html("Invalid File !").fadeIn();
                }
                else{
                    // view uploaded file.
                    // $("#preview").html(data).fadeIn();
                    // $("#form")[0].reset(); 
                    <?php $url = base_url() . "index.php/user/profile/" . $_SESSION['ID'];?>
                    window.location.replace("<?php echo $url?>");
                    console.log(data);
                }
            },
            error: function(e){
                // $("#err").html(e).fadeIn();
            }          
        });
    });
});
</script>