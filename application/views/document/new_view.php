<?php
$document['Texto'] = str_replace("[SEPARATOR]", "</p><p><br>   ", $document['Texto']);
$downloadURL = base_url("index.php/document/download/") . $document['ID'];
$readURL = base_url("index.php/document/read/") . $document['ID'];
$signURL = base_url("index.php/document/sign/") . $document['ID'];
MarkAsRead($document);

?>
<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-default text-dark">
                    <div class="card-header">
                        <h1 class="mb-3"><?=$document['Título'] ?></h1>
                        <br>
                    </div>
                    <div class="card-body pt-4">
                        <p>
                            <?=$document['Texto'] ?>
                        </p>
                        <div>
                            <a class="btn btn-square btn-primary " href="<?= $readURL ?>">Vista Previa <i class="mdi mdi-eye"></i></a>
                            <!-- <a class="btn btn-square btn-primary " href="<?= $downloadURL ?>">Descargar <i class="mdi mdi-download"></i></a> -->
                            <a class="btn btn-square btn-primary " href="<?= $signURL ?>">Firmar <i class="mdi mdi-pencil"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

function MarkAsRead($document){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $query = $ci->db->get_where('new_t_document_status', ['ID_HEAD_DOCUMENT' => $document['ID'], "SEEN" => 0]);
    $result = $query->row_array();
    if ($result) {
        $ci->db->set('SEEN', 1);
        $ci->db->where('ID', $result['ID']);
        $ci->db->update('new_t_document_status');
    }
}


?>