<?php
// BACK
// Crear nuevo documento
// Solamente es el registro en la base de datos.
// El texto tiene que veinr con separadores u otras etiquetas.
// [SEPARATOR]
session_start();

switch ($method) {
    // Crea un nuevo document
    case 'new':
        NewDocument();
        $result = [ "result" => 1,
            "msg" => "Documento registrado."
        ];
        break;
    
    // Crea y guarda un borrador de documento
    case 'save_draft':
        $recentId = NewDocument();
        SaveDocumentDraft($recentId);
        $result = [ "result" => 1,
            "msg" => "Borrador creado.",
            "ID" => $recentId
        ];
        break;

    case 'save_copy':
        $recentId = NewDocument();
        SaveDocumentDraft($recentId);
        $result = [ "result" => 1,
            "msg" => "Borrador creado.",
            "ID" => $recentId
        ];
        break;

    // "Termina" el documento a base del borrador
    // El registro del borrador es eliminado
    case 'finish_draft':
        // FinishDraft();
        SaveDraftChanges($ID_Draft);
        DeleteDraft($ID_Draft);
        $result = [ "result" => 1,
            "msg" => "Borrador eliminado.",
            "ID" => $ID_Draft
        ];
        break;

    // Guarda los cambios hechos al borrador
    case 'save_changes':
        SaveDraftChanges($ID_Draft);
        $result = [ "result" => 1,
            "msg" => "Cambios guardados exitosamente."
        ];
        break;

    case 'share_draft':
        ShareDraft($ID_Draft);
        $result = [ "result" => 1,
            "msg" => "Borrador compartido a usuarios.",
            "ID" => $ID_Draft
        ];
        break;
    
    default:
        # code...
        break;
}


echo json_encode($result);

function DeleteDraft($ID_Draft){
    $ci     =& get_instance();
    $ci->db->where('ID_DOCUMENT', $ID_Draft);
    $ci->db->delete('new_t_document_draft');
}

function ShareDraft($ID_Draft){
    $ci     =& get_instance();
    foreach ($_POST as $key => $value) {
        $query = $ci->db->get_where('new_t_document_draft', ["ID_DOCUMENT" => $ID_Draft, "ID_EDITOR" => $value]);
        $result = $query->result_array();
        if (!$result) {
            $dataArray = [
                'ID_DOCUMENT' => $ID_Draft,
                'ID_EDITOR'    => $value,
            ];
            $ci =& get_instance();
            $ci->db->insert('new_t_document_draft', $dataArray);
            RegisterNotificationDraft($ID_Draft, $value);
        }
        
    }
}

function NewDocument(){
    $ci     =& get_instance();
    $user   = getUserData($_SESSION['ID']);
    $conNo  = getConsecutiveDepartmentNo($user['DEPENDENCIA']);
    
    $dataDocument                   = [];
    $dateCurrent                    = new DateTime(date("Y-m-d H:i:s"));
    $dataDocument['TITLE']          = $_POST['title'];
    $dataDocument['FOLIO_2']        = $_POST['folio_2'];
    $dataDocument['FOR']            = $_POST['for'];
    $dataDocument['CC']             = $_POST['cc'];
    $dataDocument['DATE_CREATED']   = $dateCurrent->format("Y-m-d H:i:s");
    $dataDocument['ID_TYPE']        = 1; #Valor default hasta que se implementen formatos
    $dataDocument['OWNER']          = $_SESSION['ID'];
    $dataDocument['ID_AUTHOR']      = $_SESSION['ID'];
    $dataDocument['FOLIO']          = $user['NOM_DEP'] . "/" . $user['NOM_UA'] . "-" . $dateCurrent->format("Y") . "-" . $conNo;
    $dataDocument['BODY']           = ProcessText();
    
    $ci->db->insert('new_t_documents', $dataDocument);
    $recentId = $ci->db->insert_id();
    return $recentId;
}

function SaveDraftChanges($ID_Draft){
    $ci     =& get_instance();
    $dataDocument           = [];
    $dataDocument['TITLE']  = $_POST['title'];
    $dataDocument['FOR']  = $_POST['for'];
    $dataDocument['CC']  = $_POST['cc'];
    $dataDocument['FOLIO_2']  = $_POST['folio_2'];
    $dataDocument['BODY']   = ProcessText();
    $ci->db->where('ID', $ID_Draft);
    $ci->db->update('new_t_documents', $dataDocument); 
}

function SaveDocumentDraft($recentId){
    $ci     =& get_instance();
    $dataDraft = [
        "ID_DOCUMENT" => $recentId,
        "ID_EDITOR" => $_SESSION['ID']
    ];
    $ci->db->insert('new_t_document_draft', $dataDraft);
}

function ProcessText(){
    $array = [];
    $finalText = "";
    foreach ($_POST as $key => $value):
        if(strpos($key,'page') !== false):
            // echo $value;
            // echo "[SEPARATOR]";
            array_push($array,$value);
            array_push($array,"[SEPARATOR]");
        endif;
    endforeach;
    array_pop($array);
    foreach ($array as $key => $value) {
        $finalText .= $value;
    }
    // print_r($array);
    return $finalText;
}


function getConsecutiveDepartmentNo($ID_Department){
    $ci =& get_instance();
    // $ci->db->select('DEPENDENCIA');
    $ci->db->where('ID_DEP', $ID_Department);
    $dataCosecutiveCounter = [];
    $query = $ci->db->get('t_documents_consecutive');
    $result = $query->row_array();
    if ($result) {
        // Actualizar contador
        $docNo = $result['COUNT'];
        $result['COUNT']++;
        $ci->db->where('ID_DEP', $ID_Department);
        $ci->db->update('t_documents_consecutive', $result);
        $result = $result['COUNT'];
    }else{
        $result = 1;
        $dataArray = [
            "ID_DEP"  => $ID_Department,
            "COUNT" => $result,
        ];
        $ci->db->insert('t_documents_consecutive', $dataArray);
    }
	return $result;
}

function RegisterNotificationDraft($ID_Draft, $ID_user){
    $ci =& get_instance();
    $dateCurrent = new DateTime(date("Y-m-d H:i:s"));
    $date  = $dateCurrent->format("Y-m-d H:i:s");
    $dataArray = [
        'ID_DOC' => $ID_Draft,
        'ID_USER'    => $ID_user,
        "ID_TYPE" => 4,
        "DATE" => $date
    ];
    $ci =& get_instance();
    $ci->db->insert('t_notifications', $dataArray);
}
?>