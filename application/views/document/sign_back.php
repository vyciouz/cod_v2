<?php
session_start();
use \setasign\Fpdi\Fpdi;
if (!isset($_POST)) {
    header("Location: " . base_url() . "index.php/system/error_404");
    exit;
}

switch ($method) {
    case 'upload_files':
        UploadFiles();
        break;
    
    case 'files_check':
        FilesCheck();
        break;
            
    case 'sign':
        Sign($document);
        break;

    default:
    $result = [
        "result" => 0,
            "msg" => "Error desconocido."
        ];
        echo json_encode($result);
        break;
}

function UploadFiles(){
    // Check de credenciales con sesión iniciada.
    // print_r($_POST);
    // print_r($_FILES);
    // echo microtime();
    list($usec, $sec) = explode(" ", microtime());
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomChar = $characters[rand(0, $charactersLength - 1)];
    $tempName = $randomChar . ((float)$usec + (float)$sec);
    // echo $tempName;
    
    $cerTempName = $_FILES["cerfile"]["tmp_name"];
    $keyTempName = $_FILES["keyfile"]["tmp_name"];
    move_uploaded_file($cerTempName, "data/temp/$tempName.cer");
    move_uploaded_file($keyTempName, "data/temp/$tempName.key");
    $cerTempSrc = "data/temp/$tempName.cer";
    $keyTempSrc = "data/temp/$tempName.key";
    
    // Crear PEM para verificar si la contraseña y archivo key coinciden.
    $generateKEYPEM = "pkcs8"
        . " -inform DER"
        . " -in " . $keyTempSrc
        . " -passin pass:" . $_POST['password']
        . " -out $keyTempSrc.pem";
    exec(OPENSSL_ROUTE . $generateKEYPEM, $arr, $statusKeyPem);
 
    if ($statusKeyPem === 0){
        chmod("$keyTempSrc.pem", 0777);
    }else{
        $result = [
            "result" => 0,
            "msg" => "Contraseña incorrecta para la llave."
        ];
        unlink($cerTempSrc);
        unlink($keyTempSrc);
        unlink($keyTempSrc . ".pem");
        echo json_encode($result);
        exit;
    }
    
    // Modulus se usa para verificar que CER y KEY coincidan.
    $getModulusCer = "x509"
        . " -inform DER"
        . " -in " . $cerTempSrc
        . " -noout -modulus";
    $ModulusCer = exec(OPENSSL_ROUTE .$getModulusCer, $arr, $status);
    $getModulusKey = "rsa"
        . " -in $keyTempSrc.pem"
        . " -noout -modulus";
    $ModulusKey = exec(OPENSSL_ROUTE.$getModulusKey, $arr, $status);

    if($ModulusKey != $ModulusCer){
        if (empty($ModulusCer)):
            $result = [
                "result" => 0,
                "msg" => "Error en el archivo CER."
            ];
            echo json_encode($result);
            unlink($cerTempSrc);
            unlink($keyTempSrc);
            exit;
        endif;
        if (empty($ModulusKey)):
            $result = [
                "result" => 0,
                "msg" => "Error en el archivo KEY."
            ];
            echo json_encode($result);
            unlink($cerTempSrc);
            unlink($keyTempSrc);
            exit;
        endif;
        $result = [
            "result" => 0,
            "msg" => "Llave o certificado no corresponden."
        ];
        echo json_encode($result);
        unlink($cerTempSrc);
        unlink($keyTempSrc . ".pem");
        unlink($keyTempSrc);
        exit;
    }

    // Verificar si el CURP incluido en CER corresponde al registrado.
    $generarCERPEM  = "x509"
        . " -in " . $cerTempSrc
        . " -inform DER"
        . " -outform PEM"
        . " -out " . $cerTempSrc . ".pem";

    $generarTXT = "x509"
        . " -in " . $cerTempSrc . ".pem"
        . " > " . $cerTempSrc .".txt -text";
    exec(OPENSSL_ROUTE . $generarCERPEM, $arrCERPEM, $statusCERPEM);
    exec(OPENSSL_ROUTE . $generarTXT, $arrTXT, $statusTXT);

    if ($statusTXT === 0){
        $sslcert = file_get_contents( $cerTempSrc . ".txt");
        $sslcert = array(openssl_x509_parse($sslcert,TRUE));
        
        $curp = $sslcert[0]['subject']['serialNumber'];
        
        $ci             =& get_instance();
        $CertQuery      = $ci->db->get_where('t_users', ['CURP' => $curp, 'ID' => $_SESSION['ID']]);
        $CertQueryResult     = $CertQuery->row_array();

        if (!$CertQueryResult) {
            $result = [
                "result" => 0,
                "msg" => "Certificado e información de usuario no coinciden."
            ];
            echo json_encode($result);
            unlink($cerTempSrc);
            unlink($keyTempSrc);
            exit;
        }

    }else{
        $result = [
            "result" => 0,
            "msg" => "Error al crear archivo txt."
        ];
        echo json_encode($result);
        unlink($cerTempSrc);
        unlink($keyTempSrc);
        exit;
    }
    // En este punto corroboramos que los archivos son los correctos.
    $userDirectory = "data/userdata/".$_SESSION['ID']."/";
    
    if (!file_exists($userDirectory)) {
        mkdir($userDirectory, 0777);
    }

    $result = [
        "result" => 1,
        "msg" => "Archivos subidos correctamente."
    ];
    echo json_encode($result);
    unlink($cerTempSrc . ".pem");
    unlink($cerTempSrc . ".txt");
    unlink($keyTempSrc . ".pem");
    rename($keyTempSrc, $userDirectory . $_SESSION['ID'] . ".key");
    rename($cerTempSrc, $userDirectory . $_SESSION['ID'] . ".cer");
    exit;
}

function FilesCheck(){
    // print_r($_POST);
    $id = $_POST['represented'];
    $keyFile = glob("./data/userdata/$id/*.key");
    $cerFile = glob("./data/userdata/$id/*.cer");
    // var_dump($cerFile);
    // var_dump($keyFile);
    if ($cerFile && $keyFile) {
        $result = [
            "result" => 1,
            "msg" => "Archivos encontrados."
        ];

    } else {
        $result = [
            "result" => 0,
            "msg" => "Faltan archivos."
        ];
    }
    echo json_encode($result);
}

function Sign($document){
    $dateCurrent    = new DateTime(date("Y-m-d H:i:s"));
    $signedDate     = $dateCurrent->format("Y-m-d H:i:s");
    $ID_user        = $_SESSION['ID'];
    $userInfo       = getUserData($ID_user);
    $keyTempSrc     = "data/userdata/$ID_user/$ID_user.key";
    
    // Crear PEM para verificar si la contraseña y archivo key coinciden.
    $generateKEYPEM = "pkcs8"
        . " -inform DER"
        . " -in " . $keyTempSrc
        . " -passin pass:" . $_POST['password']
        . " -out $keyTempSrc.pem";
    exec(OPENSSL_ROUTE . $generateKEYPEM, $arr, $statusKeyPem);
 
    if ($statusKeyPem === 0){
        $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
        chmod("$keyTempSrc.pem", 0777);
        $result = [
            "result" => 1,
            "msg" => "Archivo firmado."
        ];
        $digitalKey = hash("sha256", $userInfo['CURP'] . $signedDate );
        $name = $userInfo['NAMES'] . " "  . $userInfo['LAST_NAME'] . " "  . $userInfo['MAIDEN_NAME'];
        $signedDocumentData = [
            "USER_NAME" => $name,
            "SIGNED_DATE" => $signedDate,
            "DIGITAL_SIGNATURE" => $digitalKey,
            "ID_DOCUMENT" => $document['ID']

        ];
        $ci->db->insert('new_t_documents_signed', $signedDocumentData);
    }else{
        $result = [
            "result" => 0,
            "msg" => "Contraseña incorrecta para la llave."
        ];

    }
    unlink($keyTempSrc . ".pem");
    echo json_encode($result);
}

function AddRegistryNotification($document, $dateCurrent){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $dataArray = [
        "ID_USER"   => $document['SENT_FROM'],
        "ID_TYPE"   => 3,
        "ID_DOC"    => $document['ID'],
        'DATE'      => $dateCurrent->format("Y-m-d H:i:s")
    ];
    $ci->db->insert('t_notifications', $dataArray);
}
?>