<?php
// "My documents" siempre mostrará los documentos de quien tenga sesión iniciada.
$ci =& get_instance();
$ci->load->model('documents_model');
$url = base_url("index.php/document/");
switch ($documentsType) {
    case 'personal':
        $dataArray = $ci->documents_model->myDocuments($_SESSION['ID']);
        $index = "ID";
        $orderByCol = 2;
        $actions = true;
        $url = $url . "view/";
        break;
    case 'signed':
        $dataArray = $ci->documents_model->myDocumentsSigned($_SESSION['ID']);
        $index = "ID_DOC";
        $orderByCol = 2;
        $actions = true;
        $url = $url . "read/";
        break;
    case 'tracking':
        $dataArray = $ci->documents_model->tracking($_SESSION['ID']);
        $index = "SUB_THREAD";
        $orderByCol = 2;
        $actions = true;
        $url = $url . "track/";
        break;
    case 'drafts':
        $dataArray = $ci->documents_model->drafts($_SESSION['ID']);
        $index = "ID";
        $orderByCol = 2;
        $actions = true;
        $url = $url . "draft/";
        break;
    case 'all':
        $dataArray = $ci->documents_model->getAll();
        $index = "ID";
        $orderByCol = 1;
        $actions = false;
        $url = $url . "view/";
        $url = base_url("index.php/document/view/");
        break;
    
    default:
        $index = "ID";
        break;
}

DrawTable($dataArray, $controllerName, $index, $actions, $documentsType);?> 

<script>
$(function () {
    $('#documents').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        "pageLength": 25,
        "order": [[ <?= $orderByCol ?>, "desc" ]],
        "language": {
            "search": "Búsqueda:",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "info":           "Mostrando de _START_ a _END_ entradas de un total de _TOTAL_ resultados",
            "infoEmpty":      "Mostrando de 0 a 0 entradas de 0 resultados",
        },
    })
})
  
$(".mail-element").click(function () {
    var id = $(this).prop("id");
    console.log(id);
    var url = "<?= $url ?>" + id;
    window.location.href = url;
});

</script>


<?php
/********************************************************************************************/
/* ************************************** FUNCTIONS  ************************************** */
/********************************************************************************************/

function DrawTable($data, $controller, $index, $actions, $documentsType){
    // print_r($data);
    echo '<table class="table table-hover mail table-striped" id ="documents">';
    echo "<thead>";
    if ($data) {
        foreach ($data[0] as $key => $value) {
            echo ( $key !== "SUB_THREAD" && $key !== "ID" && $key !== "ID_DOC" && $key !== "ID_SIG" && $key !== "OWNER" && $key !== "SENT_FROM" && $key !== "ID_PARENT_DOCUMENT" && $key !== "CURRENT_OWNER" && $key !== "READ" ) ? "<th>" . $key . "</th>" : "" ;
        }

        echo ($actions) ? "<th> Acciones </th>" : "" ;
        echo "</thead>";
        echo "<tbody>";
    
        $odd = true;
        $trClass = "odd";
        foreach ($data as $key => $value):
            if ($documentsType == 'signed') {
                $value[$index] .= "/" . $value['ID_SIG'];
            }
            echo "<tr class = '$controller $trClass  mail-element' id = '" . $value[$index] . "'>";
            foreach ($value as $k => $v):
                $v = (strcmp($v,"")) ? $v : "--" ;
                $v = str_replace("[SEPARATOR]", " ", $v);
                if (strlen($v)>=50) {
                    $vLenght = 50;
                    $v = substr($v,0, $vLenght ) . "...";
                }
                echo ( $k !== "SUB_THREAD" && $k !== "ID" && $k !== "ID_DOC" && $k !== "OWNER" && $k !== "ID_SIG" && $k !== "SENT_FROM" && $k !== "ID_PARENT_DOCUMENT" && $k !== "CURRENT_OWNER" && $k !== "READ" ) ? "<td>" . $v . "</td>" : "" ;
            endforeach;
            if ($actions) {
                echo "<td>";
                echo "<a href='" .base_url("index.php/$controller/view/" . $value[$index]) . "' class='btn btn-xs'><i class='fa fa-search'></i></a>";
                if ($documentsType == 'drafts') {
                    echo "<a href='" .base_url("index.php/$controller/draft/" . $value[$index]) . "' class='btn btn-xs'><i class='fas fa-pencil-alt'></i></a>";
                }
                
                // echo "<a href='" .base_url("index.php/$controller/send/" . $value[$index]) . "'  class='btn btn-xs'><i class='fas fa-paper-plane'></i></a>";
                echo "</td>";
            }
            echo "</tr>";
            $odd = ($odd) ? false : true ;
            $trClass = ($odd) ? "odd" : "even" ;
        endforeach;
    }else{
        echo "<th>Tabla vacía</th>";
        echo "</thead>";
        echo "<tbody>";
        echo "<tr>";
        echo "<td>No hay información</td>";
        echo "</tr>";
    }
    
    echo "</tbody>";
    echo "</table>";
}

?>