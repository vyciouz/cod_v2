<?php
$today = new DateTime(date("Y-m-d"));


?>
<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-lg-10">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2>Borrador</h2>
                    </div>
                    <div class="card-body">
                        <form class="" id = "create_new_document" method = "POST">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p><i class = "mdi mdi-email-outline"></i> Datos de correo</p>
                                </div>
                                <div class="col-sm-6">
                                    <p><i class= "mdi mdi-file-document-outline"></i> Datos de Documento</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="">Destinatario</label>
                                        <div class = "pseudo-input" id = "recipient-holder">
                                            <div id = "rec_1" style ="display: inline">
                                            </div>
                                            <div id = "rec_2" style ="display: inline">
                                                <input type="text" id ="find-user" class = "crouching-div-hidden-input">
                                            </div>
                                        </div>
                                        <!-- <input type="text" class="form-control" placeholder="Enter Email"> -->
                                    </div>
                                    <div class="form-group">
                                        <label for="">CC</label>
                                        <div class = "pseudo-input" id = "copied-holder">
                                            <div id = "cc_1" style ="display: inline">
                                            </div>
                                            <div id = "cc_2" style ="display: inline">
                                                <input type="text" id ="cc" class = "crouching-div-hidden-input">
                                            </div>
                                        </div>
                                        <!-- <input type="text" class="form-control" placeholder="Enter Email"> -->                                    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Asunto</label>
                                        <input type="text" class="form-control" placeholder="Asunto" id ="subject" name = "subject" required value = "<?= $draft['TITLE']?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Texto correo</label>
                                        <textarea class="form-control" rows="3" name="textMail" id="text-mail"><?= $draft['BODY']?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="lname">Título</label>
                                        <input type="text" class="form-control" placeholder="Título de documento" id ="title" name = "title" required value = "<?= $draft['DOC_TITLE']?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="lname">Texto</label>
                                        <textarea class="form-control" rows="3" name="textBody" id="text-body" required><?= $draft['DOC_BODY']?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = "col-sm-12">
                                    <p>
                                    <p><i class = "mdi mdi-email-plus-outline"> Opcional</i></p>
                                    </p>
                                </div>
                                <div class = "col-sm-12">
                                    <div class="form-group">
                                        <input type="checkbox" id ="with-status"><label for="status-select">Enviar con estatus</label>
                                        <select class="form-control" id="status-select" name = "status" style="display: none" disabled>
                                            <option></option>
                                            <?php
                                            foreach ($statuses as $key => $value):
                                                echo "<option value = '".$value['ID']."'>".$value['DESC']."</option>";
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="checkbox" id ="with-due-date"><label for="exampleFormControlSelect12">Fecha límite</label>
                                        <input type="date" class = "form-control" name = "dueDate" id = "dueDate" style="display: none"  min="<?= $today->format("Y-m-d")?>" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = "col-sm-12">
                                    <div class = "form-group">
                                        <label for="">Archivo(s):</label>
                                    </div>
                                </div>
                            </div>
                            <div class = "row" >
                                <div class="col-sm-12">
                                    <div class="form-group"  id = "attachments-container">
                                        <input type="file" name = "userfile_1" class="form-control-file 1" id="exampleFormControlFile1" style = "width: 50%; display: inline">
                                        <button type="submit" id="add_file_input"  class="mb-1 btn btn-sm btn-outline-primary">+</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-footer pt-5 border-top">
                                <button id = "send_doc" type="submit" class="btn btn-primary btn-default send-doc">Crear y enviar</button> <button id = "save_draft" type="submit" class="btn btn-primary btn-default">Guardar cambios</button>
                            </div>

                        </form>
                    </div>
                </div>
                
            </div>
            
            <div class="col-lg-2">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2><i class = "mdi mdi-account-box-multiple"></i> Editores</h2>
                    </div>
                    <div class="card-body slim-scroll-special" style = "padding : 10px">
                        <?php
                        foreach ($editors as $key => $value):
                            echo "<div class ='row'>";
                            echo "<div class ='col-sm-6 col-md-12' style = 'padding: 10px 20px 10px'>" . $value['Editor'] . "</div>";
                            // echo "<div class ='col-sm-6 col-md-12 font-size-12 text-right'><i class='mdi mdi-clock-outline'></i> 10 AM</div>";
                            echo "</div>";
                        endforeach;
                        ?>
                            
                    
                    </div>
                    <div class="card-body border-top" style = "padding: 10px 0px 10px; text-align: center">
                        <button class="btn btn-primary btn-default" data-toggle="modal" data-target="#exampleModal" style = "width: 90%">Compartir Borrador</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="modal fade show" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: block; padding-right: 17px;" aria-modal="true"> -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Compartir Borrador</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Compartir con:</label>
                            <div class = "pseudo-input" id = "share-holder">
                                <div id = "share_1" style ="display: inline">
                                </div>
                                <div id = "share_2" style ="display: inline">
                                    <input type="text" id ="share-to" class = "crouching-div-hidden-input">
                                </div>
                            </div>
                            <!-- <input type="text" class="form-control" placeholder="Enter Email"> -->
                        </div>
                        <ul class="list-group" id = "ul-share-to">
                         
						</ul>
                    </div>
                    <div class="col-lg-6">
                        <div class = "col-12">
                            <div class="form-group" id = "new-share-to-list">
                                <label for="">Listado</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-pill" data-dismiss="modal">Cerrar</button>
                <button id = "share-draft" type="button" class="btn btn-primary btn-pill">Compartir</button>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    var url = "<?php echo base_url() . 'index.php/document/create_back/create_send/' . $ID_draft?>";
    const form = document.querySelector('form');

    $("#add_file_input").click(function(event) {
        event.preventDefault();
        var counter = $("#attachments-container input" ).last().attr("class");
        counter = counter.replace(/\D/g,'');
        counter = parseInt(counter, 10);
        counter++;
        var newInput = 
                '<input type="file" class="form-control-file ' + counter + '" id="exampleFormControlFile1" style = "width: 50%; display: inline" name = "userfile_' + counter + '"> ' +
                '<button type="submit" id="add_file_input" style="min-width : 25px" class="btn btn-outline-danger btn-sm btn-default remove ' + counter + '">-</button>';

        $("#attachments-container").append(newInput);

        $(".remove").click(function( event ) {
            event.preventDefault();
            removeClass = $(this).attr("class");
            removeClass = removeClass.replace(/\D/g,'');
            removeClass = "." + removeClass;
            $(removeClass).remove();
        })
        // $(this).remove();
    });

    $("#save_draft").click(function(event) {
        url = "<?= base_url() . 'index.php/document/draft_back/save_changes/' . $ID_draft?>";
        event.preventDefault();
        var form = $("#create_new_document")[0];
        var formData = new FormData(form);
        console.log(formData);
        $.ajax({
            url: url,
            method: "post",
            data:  formData,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            beforeSend : function(data){
            },
            success: function(data){
                console.log(data);
                if (data.result) {
                    window.location.href = "<?= base_url() . 'index.php/document/draft/'?>" + data.result;
                }
            },
            error: function(e){
            }          
        });
    });
    
    $("#create_new_document").on('submit', function(e){
        var form = new FormData(this);
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'text',
            beforeSend : function(data){
                //$("#preview").fadeOut();
                // $("#err").fadeOut();
                // console.log(data);
            },
            success: function(data){
                console.log(data);
            },
            error: function(e){
                // $("#err").html(e).fadeIn();
            }          
        });
    });
    
    $(".remove").click(function(){
        this.remove();
    })

    $( "#find-user" ).autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: "<?php echo base_url().'index.php/Users/json_get_users_by_name_test/'; ?>",
                dataType: "json",
                method: "GET",
                data: {
                    name : request.term
                },
                success: function( data ) {
                    // response( availableTags );
                    // console.log(data);
                    var d = $.map(data,function(names){
                        return{
                            label: names['NAMES'] + " " + names['LAST_NAME'],
                            value: names['ID']
                        }     
                    });
                    response( d );
                    // console.log(d);
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            // console.log( "Selected: " + ui.item.value + " aka " + ui.item.label );
            $("#rec_1").append("<div class='recipient-user'>" + ui.item.label + "   <a href='#' class='remove'>×</a> <input class='user-id-value' type='text' name='to-" + ui.item.value + "' value='"+ ui.item.value +"'></div>");
            this.value = "";
            $(".remove").click(function( event ) {
                event.preventDefault();
                $(this).parent().remove();
                // console.log("hello");
            })
            return false;
        }
    });

    $( "#cc" ).autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: "<?php echo base_url().'index.php/Users/json_get_users_by_name_test/'; ?>",
                dataType: "json",
                method: "GET",
                data: {
                    name : request.term
                },
                success: function( data ) {
                    // response( availableTags );
                    // console.log(data);
                    var d = $.map(data,function(names){
                        return{
                            label: names['NAMES'] + " " + names['LAST_NAME'],
                            value: names['ID']
                        }     
                    });
                    response( d );
                    // console.log(d);
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            // console.log( "Selected: " + ui.item.value + " aka " + ui.item.label );
            $("#cc_1").append("<div class='recipient-user'>" + ui.item.label + "   <a href='#' class='remove'>×</a> <input class='user-id-value' type='text' name='cc-" + ui.item.value + "' value='"+ ui.item.value +"'></div>");
            this.value = "";
            $(".remove").click(function( event ) {
                event.preventDefault();
                $(this).parent().remove();
            })
            return false;
        }
    });

    $("#share-draft").click(function(){
        console.log("share!\n");
        var fruits = [];
        
        // share-to-id-value
        $(".share-to-id-value").each(function() {
            // console.log($(this).val());
            fruits.push($(this).val());
        });
        // console.log(fruits[0]);
        $.ajax({
            url: "<?= base_url("index.php/document/draft_back/share_draft/$ID_draft")?>",
            method: "post",
            data:  {
                fruits: fruits
            },
            dataType: 'json',
            success: function(data){
                console.log(data);
                if (data.result) {
                    location.reload(true);
                }else{
                    console.log(data.msg);
                }
            },
            error: function(e){
                // $("#err").html(e).fadeIn();
            }          
        });

        
    });

    $( "#recipient-holder" ).click(function() {
        $( "#find-user" ).focus();
    });
    $( "#copied-holder" ).click(function() {
        $( "#cc" ).focus();
    });
    $( "#share-holder" ).click(function() {
        $( "#share-to" ).focus();
    });

    $('.slim-scroll-special').slimScroll({
        // allowPageScroll: true
    });
    
    AutoCompleteUsers("#share-to", "share_1", "share-to-");
    CheckboxToggleDisplay("#with-due-date", "#dueDate");
    CheckboxToggleDisplay("#with-status", "#status-select");
});

function SendBack(method){
    url = "<?= base_url()?>index.php/test/draft_back/" + method;
    event.preventDefault();
    var form = $("#create_new_document")[0];
    var formData = new FormData(form);
    console.log(formData);
    $.ajax({
        url: url,
        method: "post",
        data:  formData,
        contentType: false,
        cache: false,
        processData:false,
        dataType: 'text',
        beforeSend : function(data){
        },
        success: function(data){
            console.log(data);
        },
        error: function(e){
            // $("#err").html(e).fadeIn();
        }          
    });
}
function AutoCompleteUsers(seacrhField, resultsField, resultsNomenclature){
    
    $(seacrhField).on('input', function() {
        var sFieldValue = $(seacrhField).val();
        var newValue = "";
        var id = 0;
        $("#ul-share-to").html("");
        if (sFieldValue.length > 2) {
            console.log(sFieldValue);
            $.ajax( {
                url: "<?php echo base_url('index.php/Users/json_users/share_draft/5'); ?>",
                dataType: "json",
                method: "POST",
                data: {
                    "searchValue" : sFieldValue
                },
                success: function( data ) {
                    if (data.result) {
                        data.valuesReturned.forEach(element => {
                            newValue = "<li class='list-group-item d-flex justify-content-between align-items-center'>"
                                + element.fullName
                                + "<span style='vertical-align: middle ;'><button id='share_to_id_" + element.ID + "' class='mb-1 btn share-to btn-sm btn-outline-primary'>+</button></span>"
                                + "</li>";
                               
                            // console.log(element.fullName);
                            // console.log(element.ID);
                            $("#ul-share-to").append(newValue);
                        });
                        $(".share-to").click(function(){
                            id = $(this).attr("id");
                            id = id.replace(/\D/g,'');
                            id = parseInt(id, 10);
                            name = $(this).parent().parent().text();
                            name = name.replace(/\+/g, '');
                            // console.log(name);
                            console.log(id);
                            var newShareToUser = "<div class='btn btn-block btn-outline-primary' style = 'text-align:left; height: 44px; padding: 0'>"
                                + "<div style= 'display:inline-block; width: 70%; padding: 10px 10px 10px;'>"
                                + name
                                + "</div> "
                                + "<div style= 'display:inline-block; text-align:center; width: 25%; font-size: 15px; padding: 10px 0 10px;' class = 'remove'>"
                                + "<i class='fa fa-times' aria-hidden='true'></i>"
                                + "<input class='user-id-value share-to-id-value' type='text' name='to-share_" +id + "' value='" + id + "'>"
                                + "</div>"
                                + "</div>";
                            $("#new-share-to-list").append(newShareToUser);
                            $(".remove").click(function(){
                                console.log("removed: " + id);
                                $(this).parent().remove();
                            });
                            
                        });
                        

                    }else{
                        //
                    }
                    

                }
            });
        }
        
    });
}
function CheckboxToggleDisplay(ID_checkbox, ID_inputField ){
    $(ID_checkbox).click(function(){
        if($(this).prop("checked") == true){
            $(ID_inputField).toggle();
            $(ID_inputField).prop("disabled", false);
        }
        else if($(this).prop("checked") == false){   
            $(ID_inputField).toggle();
            $(ID_inputField).prop("disabled", true);
        }
    });
}
</script>