<?php
$today = new DateTime(date("Y-m-d"));
?>
<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2>Crear Nuevo Documento</h2>
                    </div>
                    <div class="card-body">
                        <form class="" id = "create_new_document" method = "POST">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p><i class = "mdi mdi-email-outline"></i> Datos de correo</p>
                                </div>
                                <div class="col-sm-6">
                                    <p><i class= "mdi mdi-file-document-outline"></i> Datos de Documento</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="">Destinatario</label>
                                        <div class = "pseudo-input" id = "recipient-holder">
                                            <div id = "rec_1" style ="display: inline">
                                            </div>
                                            <div id = "rec_2" style ="display: inline">
                                                <input type="text" id ="find-user" class = "crouching-div-hidden-input">
                                            </div>
                                        </div>
                                        <!-- <input type="text" class="form-control" placeholder="Enter Email"> -->
                                    </div>
                                    <div class="form-group">
                                        <label for="">CC</label>
                                        <div class = "pseudo-input" id = "copied-holder">
                                            <div id = "cc_1" style ="display: inline">
                                            </div>
                                            <div id = "cc_2" style ="display: inline">
                                                <input type="text" id ="cc" class = "crouching-div-hidden-input">
                                            </div>
                                        </div>
                                        <!-- <input type="text" class="form-control" placeholder="Enter Email"> -->                                    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Asunto</label>
                                        <input type="text" class="form-control" placeholder="Asunto" id ="subject" name = "subject" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Texto correo</label>
                                        <textarea class="form-control" rows="3" name="textMail" id="text-mail" required></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="lname">Título</label>
                                        <input type="text" class="form-control" placeholder="Título de documento" id ="title" name = "title" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="lname">Texto</label>
                                        <textarea class="form-control" rows="3" name="textBody" id="text-body" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = "col-sm-12">
                                    <p>
                                    <p><i class = "mdi mdi-email-plus-outline"> Opcional</i></p>
                                    </p>
                                </div>
                                <div class = "col-sm-12">
                                    <div class="form-group">
                                        <input type="checkbox" id ="with-status"><label for="status-select">Enviar con estatus</label>
                                        <select class="form-control" id="status-select" name = "status" style="display: none" disabled>
                                            <option></option>
                                            <?php
                                            foreach ($statuses as $key => $value):
                                                echo "<option value = '".$value['ID']."'>".$value['DESC']."</option>";
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="checkbox" id ="with-due-date"><label for="exampleFormControlSelect12">Fecha límite</label>
                                        <input type="date" class = "form-control" name = "dueDate" id = "dueDate" style="display: none"  min="<?= $today->format("Y-m-d")?>" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class = "row">
                                <div class = "col-sm-12">
                                    <div class = "form-group">
                                        <label for="">Archivo(s):</label>
                                    </div>
                                </div>
                            </div>
                            <div class = "row" >
                                <div class="col-sm-12">
                                    <div class="form-group"  id = "attachments-container">
                                        <input type="file" name = "userfile_1" class="form-control-file 1" id="user-file-1" style = "width: 50%; display: inline">
                                        <button id="add_file_input"  class="mb-1 btn btn-sm btn-outline-primary">+</button>
                                        <button id="delete_first-file" style=" padding: 0; font-size: 18px; width: 30px;" class="mb-1 btn btn-sm btn-outline-danger"><i class = "mdi mdi-trash-can-outline"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-footer pt-5 border-top">
                                <button id = "send_doc" type="submit" class="btn btn-primary btn-default send-doc">Crear y enviar</button> <button id = "save_draft" type="submit" class="btn btn-primary btn-default">Guardar Borrador</button>
                            </div>

                        </form>
                    </div>
                </div>
                
            </div>
            
        </div>  <!-- Row -->
    </div>
</div>

<script>
$(document).ready(function(){
    var url = "<?php echo base_url()?>index.php/document/create_back/create_send";
    const form = document.querySelector('form');

    $("#add_file_input").click(function(event) {
        event.preventDefault();
        var counter = $("#attachments-container input" ).last().attr("class");
        counter = counter.replace(/\D/g,'');
        counter = parseInt(counter, 10);
        counter++;
        var newInput = 
                '<input type="file" class="form-control-file ' + counter + '" id="exampleFormControlFile1" style = "width: 50%; display: inline" name = "userfile_' + counter + '"> ' +
                '<button type="submit" id="add_file_input" style="min-width : 25px" class="btn btn-outline-danger btn-sm btn-default remove ' + counter + '">-</button>';

        $("#attachments-container").append(newInput);

        $(".remove").click(function( event ) {
            event.preventDefault();
            removeClass = $(this).attr("class");
            removeClass = removeClass.replace(/\D/g,'');
            removeClass = "." + removeClass;
            $(removeClass).remove();
        })
        // $(this).remove();
    });

    $("#save_draft").click(function(event) {
        url = "<?= base_url()?>index.php/document/create_back/save_draft";
        event.preventDefault();
        var form = $("#create_new_document")[0];
        var formData = new FormData(form);
        // console.log(formData);
        $.ajax({
            url: url,
            method: "post",
            data:  formData,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            beforeSend : function(data){
            },
            success: function(data){
                console.log(data);
                if (data.result) {
                    window.location.href = "<?= base_url() . 'index.php/document/draft/'?>" + data.result;
                }
            },
            error: function(e){
            }          
        });
        
    });
    
    $("#create_new_document").on('submit', function(e){
        var form = new FormData(this);
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'text',
            beforeSend : function(data){
                //$("#preview").fadeOut();
                // $("#err").fadeOut();
                // console.log(data);
                var buttonLoading = '<div class="sk-circle" style= "margin: auto">'
                    + '<div class="sk-circle1 sk-child"></div>'
                    + '<div class="sk-circle2 sk-child"></div>'
                    + '<div class="sk-circle3 sk-child"></div>'
                    + '<div class="sk-circle4 sk-child"></div>'
                    + '<div class="sk-circle5 sk-child"></div>'
                    + '<div class="sk-circle6 sk-child"></div>'
                    + '<div class="sk-circle7 sk-child"></div>'
                    + '<div class="sk-circle8 sk-child"></div>'
                    + '<div class="sk-circle9 sk-child"></div>'
                    + '<div class="sk-circle10 sk-child"></div>'
                    + '<div class="sk-circle11 sk-child"></div>'
                    + '<div class="sk-circle12 sk-child"></div>'
                + '</div>';
                $("#send_doc").html(buttonLoading);
                $("#send_doc").attr("disabled", true);
            },
            success: function(data)
            {
                <?php $url = base_url() . "index.php/user/profile/" . $_SESSION['ID'];?>
                window.location.href = "<?php echo $url?>";
                console.log(data);
                
            },
            complete: function(){
                $('#send_doc').removeAttr("disabled");
                $("#send_doc").html("Crear y enviar");
            },
            error: function(e){
                // $("#err").html(e).fadeIn();
            }
        });
    });
    
    $(".remove").click(function(){
        this.remove();
    })

    $( "#find-user" ).autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: "<?php echo base_url().'index.php/Users/json_get_users_by_name_test/'; ?>",
                dataType: "json",
                method: "GET",
                data: {
                    name : request.term
                },
                success: function( data ) {
                    // response( availableTags );
                    // console.log(data);
                    var d = $.map(data,function(names){
                        return{
                            label: names['NAMES'] + " " + names['LAST_NAME'],
                            value: names['ID']
                        }     
                    });
                    response( d );
                    // console.log(d);
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            // console.log( "Selected: " + ui.item.value + " aka " + ui.item.label );
            $("#rec_1").append("<div class='recipient-user'>" + ui.item.label + "   <a href='#' class='remove'>×</a> <input class='user-id-value' type='text' name='to-" + ui.item.value + "' value='"+ ui.item.value +"'></div>");
            this.value = "";
            $(".remove").click(function( event ) {
                event.preventDefault();
                $(this).parent().remove();
                // console.log("hello");
            })
            return false;
        }
    });
    $( "#cc" ).autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: "<?php echo base_url().'index.php/Users/json_get_users_by_name_test/'; ?>",
                dataType: "json",
                method: "GET",
                data: {
                    name : request.term
                },
                success: function( data ) {
                    // response( availableTags );
                    // console.log(data);
                    var d = $.map(data,function(names){
                        return{
                            label: names['NAMES'] + " " + names['LAST_NAME'],
                            value: names['ID']
                        }     
                    });
                    response( d );
                    // console.log(d);
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            // console.log( "Selected: " + ui.item.value + " aka " + ui.item.label );
            $("#cc_1").append("<div class='recipient-user'>" + ui.item.label + "   <a href='#' class='remove'>×</a> <input class='user-id-value' type='text' name='cc-" + ui.item.value + "' value='"+ ui.item.value +"'></div>");
            this.value = "";
            $(".remove").click(function( event ) {
                event.preventDefault();
                $(this).parent().remove();
            })
            return false;
        }
    });

    $("#delete_first-file").on('click', function() { 
        event.preventDefault();
        $("#user-file-1").val(''); 
    });

    $( "#recipient-holder" ).click(function() {
        $( "#find-user" ).focus();
    });
    $( "#copied-holder" ).click(function() {
        $( "#cc" ).focus();
    });

    CheckboxToggleDisplay("#with-due-date", "#dueDate");
    CheckboxToggleDisplay("#with-status", "#status-select");
});

function SendBack(method){
    url = "<?= base_url()?>index.php/test/draft_back/" + method;
    event.preventDefault();
    var form = $("#create_new_document")[0];
    var formData = new FormData(form);
    console.log(formData);
    $.ajax({
        url: url,
        method: "post",
        data:  formData,
        contentType: false,
        cache: false,
        processData:false,
        dataType: 'text',
        beforeSend : function(data){
        },
        success: function(data){
            console.log(data);
        },
        error: function(e){
            // $("#err").html(e).fadeIn();
        }          
    });
}

function CheckboxToggleDisplay(ID_checkbox, ID_inputField ){
    $(ID_checkbox).click(function(){
        if($(this).prop("checked") == true){
            $(ID_inputField).toggle();
            $(ID_inputField).prop("disabled", false);
        }
        else if($(this).prop("checked") == false){   
            $(ID_inputField).toggle();
            $(ID_inputField).prop("disabled", true);
        }
    });
}
</script>