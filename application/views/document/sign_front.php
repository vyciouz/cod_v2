<?php
/*
En caso de no contar con certificado y llave, mostrar una pantalla de
subida de archivos.

Subir los archivos con jquery-ajax, checar si la contraseña corresponde
a los archivos subidos.
En caso de que no sea así, eliniar los archivos y mostrar pantala de error.


En caso de contar con certificado y llave, mostrar campo de contraseña
para la firma de archivo.
*/

if (!isset($_SESSION['ID'])) {
    header("Location: " . base_url());
    exit;
}
$roles = $this->roles_model->get_roles_by_user($_SESSION['ID']);
$rolesArray = ReturnRolesArray($roles);
$roleForProcuration = [7];
$id = $_SESSION['ID'];
$keyFile = glob("./data/userdata/$id/*.key");
$cerFile = glob("./data/userdata/$id/*.cer");
if ($procurator) {
    $ci =& get_instance();
    $ci->load->model('users_model');
    $procurationList = $ci->users_model->procuration($_SESSION['ID']);
}

// var_dump($cerFile);
// var_dump($keyFile);
#Verificar si existe una llave en la carpeta de archivo del usuario.
?>
<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-lg-12 mb3">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2>Firmar documento</h2>
                    </div>
                    <div class="card-body" id="form-container">
                        
                        <?php
                        if (CheckAccess($rolesArray, $roleForProcuration) && $procurator):
                            Procuration($procurationList, $document);
                            // SignForm($document);
                        else:
                            if ($keyFile && $cerFile):
                                SignForm($document);
                            else:
                                NoCredentials();
                            endif;
                        endif;

                        if (CheckAccess($rolesArray, $roleForProcuration) && !$procurator):
                            echo " <a class='btn btn-primary' href = '" . base_url("index.php/document/sign/$ID_Document/1") . "' >Firmar en nombre de alguien más </a>";
                            echo "</div>";
                            echo "</form>";
                        elseif (CheckAccess($rolesArray, $roleForProcuration) && $procurator):
                            echo " <a class='btn btn-primary' href = '" . base_url("index.php/document/sign/$ID_Document/") . "' >Firmar a mi nombre </a>";
                            echo "</div>";
                            echo "</form>";
                        else:
                            echo "</div>";
                        endif; ?>
                        
                    </div>
                </div>
            </div>
        </div>


        
    </div>
</div>


<script>
$(document).ready(function(){
    
    $("#select-procuration-list").change(function(){    
        var ID_Represented = $("#select-procuration-list option:selected").val();
        var url = "<?= base_url('index.php/document/sign_back/' . $ID_Document . '/files_check/') ?>" ;
        // console.log(ID_Represented);
        $.ajax({
            url: url,
            method: "post",
            data:{
                "represented" : ID_Represented
            },
            dataType: 'json',
            success: function(data){
                if (data.result) {
                    console.log(data);
                    $("#proc-result").text("");
                }else{
                    $("#proc-result").text(data.msg);
                }
            }
        });
        // $("#proc-alert").text($("#select-procuration-list option:selected").text());
    })

    $("#form-credentials").on('submit', function(e){
        var form = new FormData(this);
        var url = "<?= base_url('index.php/document/sign_back/0/upload_files')?>";
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            success: function(data){
                if (data.result) {
                    location.reload();
                }else{
                    $("#upload-result").text(data.msg);
                }
            },
            error: function(e){
                console.log(e);
            }          
        });
    });

    $("#form-sign-document").on('submit', function(e){
        var form = new FormData(this);
        var url = "<?= base_url("index.php/document/sign_back/$ID_Document/sign")?>";
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            beforeSend: function(){
                $("#sign-result").text("");
            },
            success: function(data){
                if (data.result) {
                    $("#sign-result").text("Archivo firmado correctamente!");
                    setTimeout(function () {
                        window.location.href = "<?= base_url()?>";
                    }, 3000);
                }else{
                    $("#sign-result").text(data.msg);
                }
            },
            error: function(e){
                console.log(e);
            }          
        });
    });

    

});
</script>

<?php

function NoCredentials(){?>
    <p>No cuenta con llave y/o certificado para firmar.</p>                  
    <form id = "form-credentials" action="<?php echo base_url();?>" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="cerfile">Certificado:</label>
            <input class="form-control-file" type="file" name="cerfile" id="cerfile" accept=".cer" required>
        </div>
        <div class="form-group">
            <label for="keyfile">Llave:</label>
            <input class="form-control-file" type="file" name="keyfile" id="keyfile" accept=".key" required>
        </div>
        <div class="form-group">
            <label for="userpw">Contraseña</label>
            <input class="form-control" type="password" id="userpw" name="password" value="" required>
            
        </div>
        <p id = "upload-result" class = "invalid-feedback" style = "display:block"></p>
        <div class="form-footer pt-4 pt-5 mt-4 border-top">
            <input class="btn btn-primary" type="submit" value="Subir" name="submit">
            
<?php 
}

function SignForm($document){?>
    <p>Firmar documento <?php echo $document['Título']?>:</p>
    <form id = "form-sign-document" action="<?php echo base_url('index.php/document/sign_back/' . $document['ID'])?>" method="post">
        <div class="form-group">
            <label for="">Contraseña</label>
            <input class = "form-control" type="password" name="password" id="password">
        </div>
        <p id = "sign-result" class = "invalid-feedback" style = "display:block"></p>
        <button class="btn btn-primary" action="submit">Firmar</button>
<?php
}

function Procuration($procurationList, $document){?>
    <p>Firmar documento <?php echo $document['Título']?>:</p>
    <form id = "form-sign-document" action="<?php echo base_url('index.php/document/sign_back/' . $document['ID'])?>" method="post">
    <div class="form-group">
        <label for="exampleFormControlSelect12">Firmar a nombre de:</label>
        <select name = "signee" class="form-control" id="select-procuration-list">
            <option value="0">--</option>
        <?php foreach ($procurationList as $key => $value) {
            echo "<option value = '" . $value['ID'] . "'>" . $value['fullName'] . "</option>";
        }?>
        </select>
        <p id = "proc-result" class = "invalid-feedback" style = "display: block !important"></p>
    </div>
        <div class="form-group">
            <label for="">Contraseña</label>
            <input class = "form-control" type="password" name="password" id="password">
        </div>
        <p id = "sign-result" class = "invalid-feedback" style = "display:block"></p>
        <button class="btn btn-primary" action="submit">Firmar</button>
<?php
}
?>