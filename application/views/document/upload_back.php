<?php
/*
Verificar archivos (sean tipos válidos)
-> 
-> Hacer Registro en BD
-> Obtener ID de registro
-> Mover archivos a carpeta
-> Crear carpeta con número de registro obtenido y mover adjuntos a carpeta
*/
session_start();
header('Content-Type: application/json; charset=utf-8');
if (isset($_POST) && !empty($_POST)) {
    Stuff();
} else {
    header("Location: " . base_url() . "index.php/system/error_404");
    exit();
}

function Stuff(){
    $result = [];
    $attachments = [];
    
    $dateCurrent                    = new DateTime(date("Y-m-d H:i:s"));
    $document[0]['TITLE']           = ($_POST['title'] == "") ? $date_array['year'].'-'.$date_array['mon'].'-'.$date_array['mday'] : $_POST['title'] ;
    $document[0]['NAME']            = str_replace(' ', '_',$document[0]['TITLE']) . ".pdf"; # pero cambiando ' ' por '_'
    $document[0]['ORIGINAL_OWNER']  = $_SESSION['ID'];
    $document[0]['CURRENT_OWNER']   = $_SESSION['ID'];
    $document[0]['DATE_CREATED']    = $dateCurrent->format("Y-m-d H:i:s");;

    foreach ($_POST as $key => $value) {
        if(strpos($key,'title') !== false){
            array_push($result,['title' => $value]);
        }
    }

    foreach ($_FILES as $key => $value):
        $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type
        
        if (IsValidFile()) {
            if(strpos($key,'main-file') !== false){
                // echo "mainDocument " . $value['name'] . "\n";
                $mainFile = [
                    'name' => $value['name'],
                    'slug' => CleanName($value['name']),
                    'type' => finfo_file($finfo,  $value['tmp_name'])
                ];
                $_FILES[$key]['mainFile'] = 1;
                array_push($result,['mainFile' => $mainFile]);
            }else{
                if (isset($value['name']) && !empty($value['name'])):
                    $aux = [
                        'name' => $value['name'],
                        'slug' => CleanName($value['name']),
                        'type' => finfo_file($finfo,  $value['tmp_name'])
                    ];
                    $_FILES[$key]['mainFile'] = 0;
                endif;
                if (isset($aux) && !empty($aux)):
                    array_push($attachments,$aux);
                endif;
            }
        }else{
            $error = "Archivo inválido";
            PrintError($error);
        }
    endforeach;
    /*
    Los archivos han sido revisados y son válidos. Listo para hacer el
    registro, obtener el ID del registro y 
    */
    $recentId = RegisterDocumentInDB($document);

    array_push($result, ['attachments' => $attachments]);
    $result = json_encode($result);
    echo $result;
}

function CleanName($str, $sep='_'){
    $res = strtolower($str);
    // $res = preg_replace('/[^[:alnum:]]/', ' ', $res);
    $res = preg_replace('/[[:space:]]+/', $sep, $res);
    return trim($res, $sep);
}

function IsValidFile(){
    $acceptedFileTypes = [
        "application/xml",
        "application/zip",
        "application/pdf",
        "application/vnd*",
        "audio/mpeg",
        "audio/ogg",
        "text/xml",
        "text/csv",
        "text/plain",
        "image/png",
        "image/jpeg",
        "image/gif",
    ];
    // foreach ($acceptedFileTypes as $value) {
    // }
    return true;
}

function PrintError($error){
    $error = ['error'=>$error];
    $error = json_encode($error);
    echo $error;
    exit;
}

function RegisterDocumentInDB($document){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $dataArray = [
        "TITLE"                 => $document[0]['TITLE'],
        "DATE_CREATED"          => $document[0]['DATE_CREATED'],
        'DATE_RECEIVED'         => $document[0]['DATE_CREATED'],
        "CURRENT_OWNER"         => $document[0]['CURRENT_OWNER'],
        "ORIGINAL_OWNER"        => $document[0]['ORIGINAL_OWNER'],
    ];
    $ci->db->insert('t_documents', $dataArray);
    $recentId = $ci->db->insert_id();
    $document[0]['NAME'] = $recentId . "_" . $document[0]['NAME'];
    $dataArray = [
        "NAME"                  => $document[0]['NAME'],
    ];
    $ci->db->where('id', $recentId);
    $ci->db->update('t_documents', $dataArray);
    Attachments($recentId, $document[0]['CURRENT_OWNER'], $document);

    return $recentId;
}

function Attachments($recentId, $user, $document){
    // print_r($_FILES);
    $path = "data/userdata/$user/";
    if (!file_exists($path)) {
        mkdir($path, 0777);
    }
    $path = "data/userdata/$user/documents/";
    if (!file_exists($path)) {
        mkdir($path, 0777);
    }
    
    foreach ($_FILES as $key => $value) {
        if (isset($_FILES[$key]['mainFile'])  && $_FILES[$key]['mainFile'] === 1) {
            $fileName = $_FILES[$key]['name'];
            $file_tmp = $_FILES[$key]['tmp_name'];
            $file = $path . $document[0]['NAME'];
            copy($file_tmp, $file);
        } elseif($_FILES[$key]['name']) {
            $pathAttachments = "data/userdata/$user/documents/$recentId/";
            if (!file_exists($pathAttachments)) {
                mkdir($pathAttachments, 0777);
            }
            $fileName = $_FILES[$key]['name'];
            $file_tmp = $_FILES[$key]['tmp_name'];
            $file = $pathAttachments . $fileName;
            copy($file_tmp, $file);
        }
    }
    
}
?>