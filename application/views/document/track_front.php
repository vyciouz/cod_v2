<?php
$ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
$query = $ci->db->get_where('new_t_document_status', ['SUB_THREAD' => $sub_thread]);
$result = $query->row_array();
$queryDoc = $ci->db->get_where('new_t_documents', ['ID' => $result['ID_HEAD_DOCUMENT']]);
$resultDoc = $queryDoc->row_array();
// echo $result['MAIN_THREAD'];
$query = $ci->db->query("SELECT
new_t_document_status.SEEN,
t_users.`NAMES`,
t_users.LAST_NAME
FROM
new_t_document_status
INNER JOIN t_users ON new_t_document_status.ID_THREAD_OWNER = t_users.ID
WHERE
new_t_document_status.MAIN_THREAD = \"" . $result['MAIN_THREAD'] . "\"");
$result = $query->result_array();

?>
<style>
th{
    background-color: #95a;
    color: white !important;
    font-weight: bold !important;
}
.mdi-eye{
    color: green;
}
.mdi-eye-off{
    color: red;
}
</style>

<div class="content-wrapper">
        <div class="content">							
            <div class="row">
                <div class="col-lg-12">
				    <div class="card card-default">
					    <div class="card-header card-header-border-bottom">
						    <h2>Seguimiento</h2>
						</div>
						<div class="card-body">
						    <p class="mb-5">Lista de usuarios que recibieron el oficio "<?= $resultDoc['TITLE']?>"</p>
							    <table class="table table-striped">
								    <thead>
									    <tr>
										    <th scope="col">Nombre</th>
											<th scope="col">Visto</th>
										</tr>
									</thead>
									<tbody>
                                        <?php DrawTable($result) ?>
									</tbody>
								</table>
							</div>
						</div>
                    </div>
				</div>
            </div>
        </div>
    </div>
</div>

<?php

function DrawTable($data){
    
    foreach ($data as $key => $value) {
        echo "<tr>";
        echo "<td>" . $value['NAMES'] . " " . $value['LAST_NAME'] . "</td>";
        if ($value['SEEN']) {
            $seenIcon = "<i  class = 'mdi mdi-eye'></i>";
        }else{
            $seenIcon = "<i class = 'mdi mdi-eye-off'></i>";
        }
        echo "<td>" . $seenIcon . "</td>";
        echo "</tr>";
    }
    




}

?>