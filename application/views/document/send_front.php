<?php
$sender = getUserData($document['SENT_FROM']);
$mailInfo = getMailInfo($document['ID']);

?>
<div class="content-wrapper">
    <div class="content">
        <div class="bg-white border rounded">
            <div class="row no-gutters">
                <div class="col-lg-4 col-xl-3">
                    <div class="profile-content-left pt-5 pb-3 px-3 px-xl-5">
                        <div class="card text-center widget-profile px-0 border-0">
                            <div class="card-img mx-auto rounded-circle">
                                <img src="<?php echo base_url()?>assets/img/system/pdf_file.svg" alt="pdf image">
                            </div>
                            <div class="card-body">
                                <h4 class="py-2 text-dark"> <?php echo $document['TITLE']?> </h4>
                                <?php
                                if ($sender):?>
                                <p>Remitente: <?php echo $sender['NAMES']?></p><?php
                                endif;
                                ?>
                                <a class="btn btn-primary btn-pill btn-lg my-4" href="<?= base_url() . 'index.php/document/view/' . $document['ID'] ?>">Ver</a>
                            </div>
                        </div>
        
                        <hr class="w-100">
                        <div class="contact-info pt-4">
                            <h5 class="text-dark mb-1">Información del documento</h5>
                            <?php
                            if ($mailInfo):?>
                            <p class="text-dark font-weight-medium pt-4 mb-2">Asunto</p>
                            <p><?= $mailInfo['SUBJECT'] ?></p>    
                            <?php
                            endif;
                            ?>
                            <p class="text-dark font-weight-medium pt-4 mb-2">Nombre del documento</p>
                            <p><?= $document['TITLE'] ?></p>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-xl-9">
                    <div class="profile-content-right py-5">
                        
                        <div class="tab-content px-3 px-xl-5" id="myTabContent">
                            <div class="card card-default" style = "border: none">
                                <div class="card-header card-header-border-bottom">
                                    <h2>Enviar documento </h2>
                                </div>
                                <div class="card-body">
                                    <form class="horizontal-form" id = "send_document" method = "POST">
                                        <div class="col-md-12">
                                            
                                            <div class="form-group row">
                                                <div class="col-12 col-md-3 text-right">
                                                    <label for="motive">Motivo de turnado: (Si no es turnado va vacio)</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" class="form-control" placeholder="Motivo" id ="motive" name = "motive">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-12 col-md-3 text-right">
                                                    <label for="">Destinatario</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <div class = "pseudo-input" id = "recipent-holder">
                                                        <div id = "rec_1" style ="display: inline">
                                                        </div>
                                                        <div id = "rec_2" style ="display: inline">
                                                            <input type="text" id ="find-user" class = "crouching-div-hidden-input">
                                                        </div>
                                                    </div>
                                                    <!-- <input type="text" class="form-control" placeholder="Enter Email"> -->
                                                </div>    
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12 col-md-3 text-right">
                                                    <label for="">CC:</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <div class = "pseudo-input" id = "recipent-holder">
                                                        <div id = "cc_1" style ="display: inline">

                                                        </div>
                                                        
                                                        <div id = "cc_2" style ="display: inline">
                                                            <input type="text" id ="cc" class = "crouching-div-hidden-input">
                                                        </div>
                                                        
                                                    </div>
                                                    <!-- <input type="text" class="form-control" placeholder="Enter Email"> -->
                                                </div>
                                            </div>
                                            
                                            <div class="form-footer pt-5 border-top">
                                                <button type="submit" class="btn btn-primary btn-default send-doc">Enviar</button>
                                            </div>

                                            
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
$(document).ready(function(){
    var url = "<?php echo base_url() . 'index.php/document/send_back/' . $document['ID']?>";
    const form = document.querySelector('form');
    var accetpFileTypes = "";

    $("#send_document").on('submit', function(e){
      var form = new FormData(this);
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'text',
            beforeSend : function(data){
                //$("#preview").fadeOut();
                // $("#err").fadeOut();
                // console.log(data);
            },
            success: function(data)
            {
                if(data=='invalid'){
                    // invalid file format.
                    // $("#err").html("Invalid File !").fadeIn();
                }
                else{
                    // view uploaded file.
                    // $("#preview").html(data).fadeIn();
                    // $("#form")[0].reset(); 
                    <?php $url = base_url() . "index.php/user/profile/" . $_SESSION['ID'];?>
                    window.location.href = "<?php echo $url?>";
                    console.log(data);
                }
            },
            error: function(e){
                // $("#err").html(e).fadeIn();
            }          
        });
    });
    
    $(".remove").click(function(){
        this.remove();
    })

    $( "#find-user" ).autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: "<?php echo base_url().'index.php/Users/json_get_users_by_name_test/'; ?>",
                dataType: "json",
                method: "GET",
                data: {
                    name : request.term
                },
                success: function( data ) {
                    // response( availableTags );
                    // console.log(data);
                    var d = $.map(data,function(names){
                        return{
                            label: names['NAMES'],
                            value: names['ID']
                        }     
                    });
                    response( d );
                    // console.log(d);
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            // console.log( "Selected: " + ui.item.value + " aka " + ui.item.label );
            $("#rec_1").append("<div class='recipent-user'>" + ui.item.label + "&nbsp;&nbsp;&nbsp;<a href='#' class='remove'>×</a> <input class='user-id-value' type='text' name='to-" + ui.item.value + "' value='"+ ui.item.value +"'></div>");
            this.value = "";
            $(".remove").click(function( event ) {
                event.preventDefault();
                $(this).parent().remove();
                console.log("hello");
            })
            return false;
        }
    });

    $( "#cc" ).autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: "<?php echo base_url().'index.php/Users/json_get_users_by_name_test/'; ?>",
                dataType: "json",
                method: "GET",
                data: {
                    name : request.term
                },
                success: function( data ) {
                    // response( availableTags );
                    // console.log(data);
                    var d = $.map(data,function(names){
                        return{
                            label: names['NAMES'] + " " + names['LAST_NAME'],
                            value: names['ID']
                        }     
                    });
                    response( d );
                    // console.log(d);
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            // console.log( "Selected: " + ui.item.value + " aka " + ui.item.label );
            $("#cc_1").append("<div class='recipent-user'>" + ui.item.label + "&nbsp;&nbsp;&nbsp;<a href='#' class='remove'>×</a> <input class='user-id-value' type='text' name='cc-" + ui.item.value + "' value='"+ ui.item.value +"'></div>");
            this.value = "";
            $(".remove").click(function( event ) {
                event.preventDefault();
                $(this).parent().remove();
            })
            return false;
        }
    });

});
</script>
<?php

function GetMailInfo($ID_Document){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $query = $ci->db->get_where('t_mail', ['ID_DOCUMENT' => $ID_Document]);
    $result = $query->row_array();
    return $result;
}

?>