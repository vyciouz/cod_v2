<?php 
session_start();
include("./assets/plugins/fpdf/fpdf.php");
include("./assets/plugins/qrlib/qrlib.php");
// $ID_signature = true para ver firmado

GenerarPDF($document, $ID_signature);

function GenerarPDF($document, $ID_signature){
    $ci =& get_instance();
    $author         = getUserData($document['Autor']);
    $signeeInfo     = getUserData($document['OWNER']);
    $defaultNoLogo  = "assets/img/system/no_logo.png";
    $logoDependency = "data/dependencies/". $signeeInfo['DEPENDENCIA']. "/logo.png";
    $logoD          = (file_exists($logoDependency)) ? $logoDependency :$defaultNoLogo;
    $logoCampaign   = "assets/img/system/campaign.png";
    $logoC          = (file_exists($logoCampaign)) ? $logoCampaign :$defaultNoLogo;
    $logoFooter     = "assets/img/system/doc_footer.png";
    $simpleDate     = substr($document['Fecha Creación'],0, 10 );
    $CC = $document['CC'];
    $ccFlag = false;
    $ccLimit = 5;
    $aux = "";
    

    $para = $document['FOR'];
    $rem = "ATENTAMENTE
" . $author['NAMES'] ." " . $author['LAST_NAME'] ." " .$author['MAIDEN_NAME'] ." " ."
". $author['PUESTO']. "
". $author['NAME_SUA']. "
". $author['NAME_UA']. "
". $author['NAME_DEP'];


    // Se registra la primer entrada del docmuento, la cual correponde
    // al emisor.
    if ($ID_signature) {
        $query = $ci->db->get_where('new_t_documents_signed', ["ID_DOCUMENT" => $document['ID'], "ID" => $ID_signature]);
        $result = $query->row_array();
    }
    
    
    // $textString = $document['Texto'];
    $textArray = explode("[SEPARATOR]", $document['Texto']);

    $pdf = new FPDF();

    foreach ($textArray as $key => $value) {
        $pdf->AddPage('P', 'A4');
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->SetTopMargin(15);
        $pdf->SetLeftMargin(15);
        $pdf->SetRightMargin(15);

        PDFHeader($pdf, $document);
        #Si es la primera página
        if ($key == 0) {
            $y = 45;
            $pdf->SetXY(10,$y);
            // $pdf->MultiCell(95,5,$document['Título'],0,'L',false);
            $rightHeader = "Monterrey, N.L., a $simpleDate
            Oficio Electrónico No. " . $document['Folio'];

            $pdf->SetXY(105,$y);
            $rightHeader = iconv('UTF-8', 'windows-1252//IGNORE', $rightHeader);
            $pdf->MultiCell(95,5,$rightHeader,0,'R',false);

            $y = 65;
            $pdf->SetXY(15,$y);
            $para = iconv('UTF-8', 'windows-1252//IGNORE', $para);
            $pdf->MultiCell(95,5,$para,0,'L',false);
        }
        
       
        // Cuerpo de la carta
        $pdf->setY(100);
        $pdf->SetFont('Arial', '', 12);
        $value = iconv('UTF-8', 'windows-1252//IGNORE', $value);
        $pdf->MultiCell(180,5,$value,0,'L',false);


        PDFFooter($pdf);

        if (isset($ID_signature)) {
            if ($result) {
                $y = 244;
                $pdf->setXY(35,$y);
                $digSign = $result['DIGITAL_SIGNATURE'];
                $signName = iconv('UTF-8', 'windows-1252//IGNORE', $result['USER_NAME']);
                $pdf->MultiCell(20,5,$signName,0,'L',false);
                $pdf->SetXY(55,$y);
                $pdf->MultiCell(100,5,$digSign,0,'L',false);
                $pdf->SetXY(155,$y);
                $pdf->MultiCell(45,5,$result['SIGNED_DATE'],0,'L',false);
                $QRData     = base_url("index.php/document/validate/$digSign");
                $routeDir   = "./data/userdata/";
                $QRDir      = $routeDir . "QR/";
                (file_exists ( $routeDir )) ? "" : mkdir($routeDir, 0777);
                (file_exists ( $QRDir )) ? "" : mkdir( $QRDir, 0777) ;
                $fileQRImage = $QRDir . "$digSign.png";
                if (!file_exists($fileQRImage)) {
                    QRcode::png($QRData, $fileQRImage);
                    chmod($fileQRImage, 0777);
                }
                $pdf->Image($fileQRImage, 14, 234, 20, 0);
            }
        }

        #Si es la última página
        if ($key == sizeof($textArray)-1) {
            $y = 200;
            $pdf->SetXY(105,$y);
            $rem = iconv('UTF-8', 'windows-1252//IGNORE', $rem);
            $pdf->MultiCell(95,5,$rem,0,'C',false);

            $y = 215;
            $pdf->SetXY(15,$y);
            $pdf->SetFont('Arial', '', 8);
            $ccArray = explode(PHP_EOL, $CC);
            if (sizeof($ccArray) > $ccLimit ) {
                for ($i=0; $i < $ccLimit; $i++) { 
                    $aux .= $ccArray[$i];
                    $aux .= ($i>=$ccLimit) ? "" : PHP_EOL ;
                }
                $ccFlag = true;
            }else{
                $aux = $CC;
            }
            $aux = iconv('UTF-8', 'windows-1252//IGNORE', $aux);
            $pdf->MultiCell(95,3,$aux,0,'L',false);
            
        }
        
        
    }
    if ($ccFlag) {
        $pdf->AddPage('P', 'A4');
        $pdf->SetFont('Arial', '', 8);
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->SetTopMargin(15);
        $pdf->SetLeftMargin(15);
        $pdf->SetRightMargin(15);
        $y = 15;
        $pdf->SetXY(15,$y);
        $aux = "";
        for ($i = $ccLimit; $i < sizeof($ccArray); $i++) { 
            $aux .= $ccArray[$i];
            $aux .= ($i>=sizeof($ccArray)) ? "" : PHP_EOL ;
        }
        $aux = iconv('UTF-8', 'windows-1252//IGNORE', $aux);
        $pdf->MultiCell(95,3,$aux,0,'L',false);
        PDFFooter($pdf);
    }
    $pdf->Output('I');
}


function PDFHeader($pdf, $document){
    $defaultNoLogo  = "assets/img/system/no_logo.png";
    $author         = getUserData($document['Autor']);
    $imageFolder = "./assets/img/system/";
    $logoDependency = "data/dependencies/". $author['DEPENDENCIA']. "/logo.png";
    $logoD  = (file_exists($logoDependency)) ? $logoDependency :$defaultNoLogo;
        
    $ci =& get_instance();
    $slogan = $ci->db->select('*')->order_by('id',"desc")->limit(1)->get('_plataforma')->row_array();
    $y = 10;
    $x = 10;

    $fontSize = 10;
    $pdf->SetFont('Helvetica', '', $fontSize);
    $slogan['SLOGAN'] = iconv('UTF-8', 'windows-1252//IGNORE', $slogan['SLOGAN']);
    $pdf->setXY($x,$y);
    $pdf->Cell(190, 5, $slogan['SLOGAN'], 0, 1, 'R', false);
    $pdf->Image($logoD, 15, 15, 0, 40);
}


function PDFFooter($pdf){
    $imageFolder = "./assets/img/system/";
    $x = 5;
    $imageSize = 50 ;
    $pdf->Image($imageFolder . "line.png", $x, 255, 200, 1);

    $x = 20;
    $fontSize = 8;
    $orientation = "L";
    $pdf->SetXY($x,264);
    $pdf->SetFont('Helvetica', '', $fontSize);
    $aux = 
        "Zuazua 665 Sur, Piso 11,"
        . "\nCol. Centro, Monterrey, N.L."
        . "\nC.P. 64000";
    $aux = iconv('UTF-8', 'windows-1252//IGNORE', $aux);
    $pdf->MultiCell(120, 5, $aux, 0, $orientation, false);

    ###########################################################################
    $x= 170;
    $pdf->SetXY($x,270);
    $pdf->SetFont('Helvetica', '', 20);
    $aux = "Nuevo León";
    $aux = iconv('UTF-8', 'windows-1252//IGNORE', $aux);
    $pdf->Cell(20, 5, $aux, 0, 1, 'R', false);

    $pdf->SetXY($x,275);
    $pdf->SetFont('Helvetica', '', 11);
    $aux = "Siempre Ascendiendo";
    $aux = iconv('UTF-8', 'windows-1252//IGNORE', $aux);
    $pdf->Cell(20, 5, $aux, 0, 1, 'R', false);
    ###########################################################################
    $x = 85;
    $fontSize = 8;
    $orientation = "L";
    $pdf->SetXY($x,264);
    $pdf->SetFont('Helvetica', '', $fontSize);
    $aux = "nl.gob.mx";
    $aux = iconv('UTF-8', 'windows-1252//IGNORE', $aux);
    $pdf->Cell(20, 5, $aux, 0, 1, $orientation, false);

    $pdf->SetXY($x,270);
    // $pdf->SetFont('Helvetica', '', $fontSize);
    $aux = "(81) 2020.1703 / (81) 2020.1687";
    $aux = iconv('UTF-8', 'windows-1252//IGNORE', $aux);
    $pdf->Cell(20, 5, $aux, 0, 1, $orientation, false);

    $pdf->SetXY($x,275);
    // $pdf->SetFont('Helvetica', '', $fontSize);
    $aux = "prensadegobierno@nuevoleon.gob.mx";
    $aux = iconv('UTF-8', 'windows-1252//IGNORE', $aux);
    $pdf->Cell(20, 5, $aux, 0, 1, $orientation, false);
    $imageSize = 4;
    $x = $x-6;
    
    $pdf->Image($imageFolder . "globe.png", $x, 265, $imageSize, 0);
    $pdf->Image($imageFolder . "phone.png", $x, 271, $imageSize, 0);
    $pdf->Image($imageFolder . "mail.png", $x, 276, $imageSize, 0);

    ###########################################################################

}

?>