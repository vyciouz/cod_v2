<?php
// Variables
// print_r($document);

if (!isset($document) || empty($document)) {
    // Redirigir a un 404
    header('Location: ' . base_url() . "index.php/system/error_404");
    exit;
}

$mailInfo       = GetMailInfo($document['ID']);
$currentOwner   = getUserData($document['CURRENT_OWNER']);
$sender         = getUserData($document['SENT_FROM']);
$originalOwner  = getUserData($document['ORIGINAL_OWNER']);
$date_a         = new DateTime($document['DATE_RECEIVED']);
// $interval       = date_diff($date_a);
$interval = FormatInterval($date_a);
$pre            = ($mailInfo) ? "Para: " : "Documento de: " ;
$recipients     = getRecipients($docID);

MarkAsRead($document);

$attachmentsDirectory = "./data/userdata/" . $document['CURRENT_OWNER'] . "/documents/$docID/";
$fileList = (is_dir($attachmentsDirectory)) ? Attachments($attachmentsDirectory) : false ;
?>

<div class="content">
    <div class="content-wrapper">
    
        <div class="row">
            <div class="col-12">
                <div class="card card-default">

                    <div class="card-header card-header-border-bottom">
                        <h2>
                            <?php echo ($mailInfo) ? $mailInfo['SUBJECT'] : $document['TITLE'] ;?>
                        </h2>
                    </div>
                    
                    <div class="card-body">
                        <div class = "row">
                            <div class = "col-12">
                                <p class = "mb-5">
                                    <span class="float-right"><?php echo $document['DATE_RECEIVED'] . "(hace " . $interval->format('%d') . " días)"?></span>
                                    <span class="float-left">
                                        <?php echo ($sender) ? $sender['NAMES'] . " " . $sender['LAST_NAME'] . " " . $sender['MAIDEN_NAME']  . " <'" . $sender['EMAIL'] ."'>" : "" ;?>
                                    </span>
                                </p>
                                
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "col-12">
                                <p class="card-text pb-3"><?=$pre . $currentOwner['NAMES'] . " " . $currentOwner['LAST_NAME'] . " " . $currentOwner['MAIDEN_NAME']; ?></p>
                            </div>
                        </div>
                        <?php
                        if ($mailInfo):?>
                            <hr>
                            <div class = "row">
                                <div class = "col-12">
                                    <?=$mailInfo['BODY'] ?>
                                </div>
                            </div> 
                        <?php
                        endif;
                        ?>
                        <hr>
                        <div class = "row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <a href="<?= base_url() . "/data/userdata/" . $document['CURRENT_OWNER'] . "/documents/" . $document['NAME']?>" style = "display: inline-block">
                                    <button class = "btn btn-outline-success"> <i class="mdi mdi-download t mr-4 "></i>Descargar</button>
                                </a> 
                            <?php
                            if ($mailInfo):?>
                                <!-- <a href="<?= base_url() . "index.php/document/reply/" . $document['ID'] ?>" style = "display: inline-block"> -->
                                    <!-- <button class = "btn btn-outline-success"> <i class="mdi mdi-download t mr-4 text-white"></i>Responder</button> -->
                                <!-- </a>  -->
                            <?php
                            endif;?>
                                <a href="<?= base_url() . "index.php/document/send/" . $document['ID'] ?>" style = "display: inline-block">
                                    <button class = "btn btn-outline-success"> <i class="mdi mdi-send t mr-4 "></i>Reenviar</button>
                                </a>
                            </div>
                        </div>
                        <?php
                        if ($fileList):
                            ShowAttachments($fileList, $document, $docID);
                        endif;?>
                            
                            

                        
                    </div>

                </div>
            </div>
        </div>
        

        <div class="row">
            <div class="col-12">
               <?php
               if ($recipients) {
                   FollowUp($recipients);
               }
               ?>
            </div>
        </div>
    

    </div>
</div>
<?php



$turned = WasItTurned($document['ID']);
if ($turned) {
    echo "<br>Motivo de turnado: " . $turned->MOTIVO;
}

?>



<?php

function WasItTurned($id){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $where = ["ID" => $id];
    $query = $ci->db->get_where('t_turned', $where);
    $row = $query->row();
    return $row;
}


function MarkAsRead($document){
    $readStatus = $document['READ'];
    if ($readStatus == 0) {
        $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
        $date_array = getdate();
        $dataArray = [
            "READ"          => 1,
            'READ_DATE'     => $date_array['year'].'-'.$date_array['mon'].'-'.$date_array['mday']
        ];
        $ci->db->where('ID', $document['ID']);
        $ci->db->update('t_documents', $dataArray);
    }
}

function getRecipients($id){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $searchQuery = "SELECT * FROM t_documents WHERE ID_PARENT_DOCUMENT = $id;";
    $query = $ci->db->query($searchQuery);
    $result = $query->result_array();
    // print_r($result);
    return $result;
}

function fillTable($recipients){
    foreach ($recipients as $key => $value) {
        $recipient = getUserData($value['CURRENT_OWNER']);
        $status = GetStatus($value['ID']);
        $read = ($value['READ']) ? '<i class="mdi mdi-eye"></i>' : '<i class="mdi mdi-eye-off"></i>' ;
        ?>
        <tr>
            <td><?php echo $value['ID'] ?></td>
           
            <td class="d-none d-md-table-cell"><?php echo $recipient['NAMES'] ?></td>
            <td class="d-none d-md-table-cell"><?php echo $value['DATE_RECEIVED'] ?></td>
            <td class="d-none d-md-table-cell"><?php echo $value['CC'] ?></td>
            <td class="d-none d-md-table-cell"><?php echo $read ?></td>
            <td>
                <?php
                if (!empty($status)) {?>
                    <span class="badge badge-success"><?php GetStatusLabel($status) ?></span>
                    <?php
                }
                ?>
                
            </td>
            <td class="text-right">
                <div class="dropdown show d-inline-block widget-dropdown">
                    <a class="dropdown-toggle icon-burger-mini" href="" role="button" id="dropdown-recent-order1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static"></a>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-recent-order1">
                        <li class="dropdown-item">
                            <a href="#">View</a>
                        </li>
                        <li class="dropdown-item">
                            <a href="#">Remove</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
    <?php
    }
    ?>
    <tr>
                                    
<?php
}

function FollowUp($recipients){?>
 <!-- Recent Order Table -->
 <div class="card card-table-border-none" id="recent-orders">
    <div class="card-header justify-content-between">
        <h2>Seguimiento</h2>
    </div>
    <div class="card-body pt-0 pb-5">
        <table class="table card-table table-responsive table-responsive-large" style="width:100%">
            <thead>
                <tr>
                    <th>Folio</th>
                    <th class="d-none d-md-table-cell">Destinatario</th>
                    <th class="d-none d-md-table-cell">Fecha Recibido</th>
                    <th class="d-none d-md-table-cell">Con Copia</th>
                    <th class="d-none d-md-table-cell">Visto</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php fillTable($recipients);?>
            </tbody>
        </table>
    </div>
</div>
<?php
}

function ShowAttachments($files, $document, $docID){
    $attachmentsDirectory = "data/userdata/" . $document['CURRENT_OWNER'] . "/documents/$docID/";
    $userDirectory = "data/userdata/" . $document['CURRENT_OWNER'] . "/documents/";
    ?>
    <hr>
        <div id="accordion3" class="accordion accordion-bordered ">
            <div class="card">
                <div class="card-header" id="heading1">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                        <i class="mdi mdi-attachment"></i> Archivos adjuntos
                    </button>
                </div>

                <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordion3" style="">
                    <div class="card-body">
                        <div class="row">
                            <?php
                            foreach ($files as $key => $value):?>
                            <div class="col-md-6 col-lg-3 col-xl-3">
                                <div class="row">
                                    <div class="col-md-2">
                                        <?php echo $value['icon'];?>
                                    </div>
                                    <div class="col-md-9">
                                        <a href="<?= base_url() . $attachmentsDirectory . $value['name']?>">
                                            <?php echo $value['displayName'];?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php
                            endforeach;
                            
                            $zipFile = $docID . $document['TITLE'] . ".zip";
                            if (!file_exists($attachmentsDirectory .$zipFile)) {
                                $z = new ZipArchive();
                                $zipFile = $docID . $document['TITLE'] . ".zip";
                                $zipRes = $z->open($userDirectory . $zipFile, ZIPARCHIVE::CREATE);
                                if ($zipRes === TRUE) {
                                    foreach ($files as $key => $value){
                                        $z->addFile($attachmentsDirectory . $value['name'], $value['name']);
                                    }
                                    $z->close();
                                }else {
                                    echo "No se puede crear zip.";
                                }
                            }
                            ?>
                        </div>
                        
                          
                            
                    </div>
                    <div  style = "magin-top: 10px;">
                        <a href="#" class ="btn btn-block btn-danger">
                            Descargar compreso
                        </a>
                    </div>
                    
                </div>
            </div> 
        </div>

<?php
}

function Attachments($dir){
    $files = scandir($dir);
    $fileList = [];
    array_shift($files); #Remove ./
    array_shift($files); #Remove ../
    // print_r($files);
    
    $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type
    foreach ($files as $key => $value) {
        $fileList[$key]['name'] = $value;
        $fileList[$key]['displayName'] = (strlen($value) <= 24) ? $value : substr($value, 0, 14) . "..." . substr($value, -8) ;
        $fileList[$key]['type'] = finfo_file($finfo,  $dir ."/" .$value);
        $string = $fileList[$key]['type'];
        switch (true) {
            case stristr($string, "image"):
                $fileList[$key]['icon'] = '<i class="mdi mdi-file-image font-size-20"></i>';
                break;
            
            default:
                $fileList[$key]['icon'] = '<img src="https://img.icons8.com/material/24/000000/question-mark.png">';
                break;
        }
    }
    
    return $fileList;
}

function GetMailInfo($ID_Document){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $query = $ci->db->get_where('t_mail', ['ID_DOCUMENT' => $ID_Document]);
    $result = $query->row_array();
    return $result;
}

function GetStatus($ID_Document){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $query = $ci->db->get_where('t_status', ['ID_DOCUMENT' => $ID_Document]);
    $result = $query->row_array();
    // print($result);
    return $result['STATUS'];
}

function GetStatusLabel($ID_Status){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $query = $ci->db->get_where('cat_status', ['ID' => $ID_Status]);
    $result = $query->row_array();
    // print($result);
    echo $result['DESC'];
}

function FormatInterval($date_a){
    $date_b = new DateTime(date("Y-m-d H:i:s"));
    $dd = date_diff($date_a,$date_b);
    
    // print_r($date_b->format("Ymd_His"));
    return $dd;
}
?>