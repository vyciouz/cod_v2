<?php
// FRONT
// Crear nuevo documento.
/*
1: Crear (nuevo)
2: Crear y finalizar (borrador propio)
3: Guardar Borrador (nuevo)
4: Guardar Cambios (borrador)
5: guardar copia (borrador ajeno)
*/
 
$modePrimary = ($mode == 'new') ? 1 : 2 ;
$modeSecondary = ($mode == 'new') ? 3 : 4 ;
if (isset($document)) {
    // print_r($document);
    $textArray = explode("[SEPARATOR]",$document['Texto']);
    $textTitle = $document['Título'];
    $folio2 = $document['Folio'];
    $forUser = $document['FOR'];
    $ccValue = $document['CC'];

    if ($_SESSION['ID'] != $document['OWNER']) {
        $primaryButtonText  = ($mode == 'new') ? "Crear" : "Guardar Copia";
        $primaryButtonID  = ($mode == 'new') ? "create" : "save-copy";
        $modePrimary = 5;
    }else {
        $primaryButtonText  = ($mode == 'new') ? "Crear" : "Guardar y finalizar edición";
        $primaryButtonID  = ($mode == 'new') ? "create" : "create-end";
        $modePrimary = 2;
    }
    
}else{
    $textArray = null;
    $textTitle = null;
    $primaryButtonText  =  "Crear";
    $primaryButtonID  = "create" ;
    $folio2 = "";
    $forUser = "";
    $ccValue = "";
}


$formID  = ($mode == 'new') ? "new" : "new";


$secondayButtonID  = ($mode == 'new') ? "save-draft" : "save-changes";
$secondayButtonText  = ($mode == 'new') ? "Guardar Borrador" : "Guardar Cambios";
?>

<style>
.add-this-user{
    color: #7a808d;
    
}
.add-this-user:hover{
    color: #37a;
    padding-left: 5px;
    font-weight: bold;
    font-size: 15px;
}
.delete-this-user{
    color: #7a808d;
}
.delete-this-user:hover{
    color: #f43;
    padding-left: 5px;
    font-weight: bold;
    font-size: 15px;
}
.hidden{
    display: none;
}

</style>
<div class="content-wrapper">
    <div class="content">
        <?php
        if ($mode != 'new' && $_SESSION['ID'] == $document['OWNER']) {
            ShareDraft();
        }
        ?>
        <div class="row">
            <div class="col-lg-12">
            <div class="card card-default">
                <div class="card-header card-header-border-bottom">
                    <h2>Nuevo Documento</h2>
                </div>
                <div class="card-body">
                    <form id = "<?= $formID ?>">
                        <div class="form-group row">
                            <div class = "col-6">
                                <label for="">Para:</label>
                                <textarea name="for" class="form-control" id="" cols="30" rows="4"><?= $forUser ?></textarea>
                            </div>
                            <div class = "col-6">
                                <label for="">Folio:</label>
                                <input type="text" class = "form-control" name = "folio_2" value = "<?= $folio2 ?>" required>
                            </div>
                        </div>
                        <span id = "textbox-container">
                            <div class="form-group ">
                                <label for="title">Título</label>
                                <input type="text" class = "form-control" name = "title" value = "<?= $textTitle ?>" required>
                            </div>
                            <hr>
                            <?php DrawPagesInput($modePrimary, $textArray) ?>
                        </span>
                        <div class="form-group ">
                            <label for="title">CC:</label>
                            <textarea name="cc" class="form-control" id="" cols="30" rows="2"><?=$ccValue ?></textarea>
                        </div>
                        <div class="form-footer pt-4 pt-5 mt-4 border-top">
                            <button type="submit" id = "<?= $primaryButtonID ?>" class="btn btn-primary btn-default"><?= $primaryButtonText ?></button>
                            <button id = "<?= $secondayButtonID ?>" class="btn btn-primary btn-default"><?= $secondayButtonText ?></button>
                            <button id = "new-page" class="btn btn-primary btn-default">Nueva Página</button>
                            
                            <!-- <button type="submit" class="btn btn-primary btn-default">Vista Previa</button> -->
                            <a href="<?= base_url() ?>" class="btn btn-secondary btn-default">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    // Check input length
    NewPage();
    AddNewPage();
    DeletePage();
<?php
if ($mode != "new") {?>
    ShareDraft();
<?php
}
?>
    
    
    SearchUsers();
    AddThisUser();

    $("#new").on('submit', function(e){
        e.preventDefault();
        var formData = new FormData(this);
        FormAction(formData, <?= $modePrimary ?>);
    });

    $("#<?= $secondayButtonID ?>").on('click', function(e){
        e.preventDefault();
        var formData = new FormData($("#<?= $formID ?>").get(0));
        FormAction(formData, <?= $modeSecondary ?>);
    });

});


function DeleteThisUser(){
    $(".delete-this-user").on('click', function(){
        $(this).remove();
    });
}

function AddThisUser(){
    $(".add-this-user").on('click', function(){
        var userID = $(this).attr("id");
        var usrName = $(this).attr("uname");
        usrRow = "<a href='#' class = ' delete-this-user ' id = '" + userID + "'>"
            + "<i class = 'mdi mdi-delete-outline'></i> " + usrName
            + " <input class = 'hidden' name = 'usr_" + userID + "' value = '" + userID + "' type='text'></a>";
        $("#user-holder").append( "<div>" + usrRow + "</div>");
        DeleteThisUser();
    });

}

function SearchUsers(){
    $("#search-user").on('input', function(){
        var searchVal = $(this).val();
        var urlMyDocs = "<?= base_url('index.php/JsonResponse/jsonUsersArgs/') ?>" + searchVal;
        var usrID = "";
        var usrName = "";
        if (searchVal.length == 0) { $("#user-result").html(""); }
        if (searchVal.length > 2) {
            $("#searchResult").text("");
            $.ajax({
                url: urlMyDocs,
                method: "post",
                data:  {
                },
                dataType: 'json',
                success: function(data){
                    if (data.length > 10) { limit = 10; }
                    if (data.length < 10){ limit = data.length; }
                    if (data.length == 0) { $("#user-result").html(""); }
                    $("#user-result").html("");
                    for (var key = 0; key < limit; key++) {
                        
                        usrID = data[key].ID;
                        usrName = data[key].NAMES + " " + data[key].LAST_NAME + " " + data[key].MAIDEN_NAME;
                        usrRow = "<a href='#' class = ' add-this-user ' uname = '" + usrName + "' id = '" + usrID + "'><i class = 'mdi mdi-account-plus'></i> " + usrName+ " </a>";
                        $("#user-result").append( "<div>" + usrRow + "</div>");
                    }
                    AddThisUser();
                },
                error: function(e){
                    // $("#err").html(e).fadeIn();
                }          
            });
        }
    });
}

function FormAction(formData, action){
    var url = "";
    if (action == 1) { url = "<?= base_url('index.php/document/new_back/new')?>";}
    if (action == 3) { url = "<?= base_url('index.php/document/new_back/save_draft/')?>";}
    <?php 
    #Acciones que sólo se pueden mostrar cuando se está editando un borrador
    if ($modePrimary != 1): ?>
    if (action == 2) { url = "<?= base_url('index.php/document/draft_back/finish_draft/' . $document['ID'])?>";}
    if (action == 4) { url = "<?= base_url('index.php/document/draft_back/save_changes/' . $document['ID'])?>";}
    if (action == 5) { url = "<?= base_url('index.php/document/draft_back/save_copy/' . $document['ID'])?>";}
    <?php endif; ?>
    
    $.ajax({
        url: url,
        method: "post",
        data:  formData,
        contentType: false,
        cache: false,
        processData:false,
        dataType: 'json',
        beforeSend : function(data){
            var buttonLoading = '<div class="sk-circle" style= "margin: auto">'
                + '<div class="sk-circle1 sk-child"></div>'
                + '<div class="sk-circle2 sk-child"></div>'
                + '<div class="sk-circle3 sk-child"></div>'
                + '<div class="sk-circle4 sk-child"></div>'
                + '<div class="sk-circle5 sk-child"></div>'
                + '<div class="sk-circle6 sk-child"></div>'
                + '<div class="sk-circle7 sk-child"></div>'
                + '<div class="sk-circle8 sk-child"></div>'
                + '<div class="sk-circle9 sk-child"></div>'
                + '<div class="sk-circle10 sk-child"></div>'
                + '<div class="sk-circle11 sk-child"></div>'
                + '<div class="sk-circle12 sk-child"></div>'
            + '</div>';
            $("#create").html(buttonLoading);
            $("button").attr("disabled", true);
        },
        success: function(data){
            <?php $url = base_url("index.php/documents/mydocuments/") ?>
            if (data.result) {
                if (action == 1) { window.location.href = "<?php echo $url?>";
                }
                if (action == 3 || action == 5) { window.location.href = "<?= base_url('index.php/document/draft/')?>" + data.ID;
                }
                
                <?php if ($modePrimary != 1): ?>
                if (action == 2) { window.location.href = "<?= base_url('index.php/document/view/')?>" + data.ID;
                }
                if (action == 4) { window.location.href = "<?= base_url('index.php/document/draft/' . $document['ID'])?>";
                }
                <?php endif; ?>
            }else{
                switch (data.error) {
                    case 1:
                        alert(data.msg);
                        break;
                
                    default:
                        break;
                }
            }
        },
        complete: function(){
        },
        error: function(e){
        }
    });
}

function AddNewPage(){
    $("#new-page").on('click', function(e){
        e.preventDefault();

        var counter = $("#textbox-container textarea" ).last().attr("id");
        // console.log(counter);
        // console.log($(this).attr("id"));
        counter = parseInt(counter, 10);
        // console.log("Siguiente elemento: " + (parseInt(counter, 10) + 1));
        // console.log("Elemento actual: " + (parseInt(currentTB, 10)));
        counter++;
        
        var textbox =
        "<div class='form-group'>"
            + "<label for='" + counter + "'>Página " + counter + "</label> <span id ='limit_" + counter + "'>0/1800</span>"
            + "<textarea class='form-control page' id='" + counter + "' name = 'page_" + counter + "' rows='3'></textarea>"
            + "<button class = 'btn btn-danger btn-default remove'><i class = 'mdi mdi-trash-can-outline'></i></button>"
        + "</div>";
        $("#textbox-container").append(textbox);
        NewPage();
        DeletePage();
    });
    
}

function NewPage(){
    $( ".page" ).on('input', function() {
        var counter = $("#textbox-container textarea" ).last().attr("id");
        var currentTB = ($(this).attr("id"));
        counter = parseInt(counter, 10);
        counter++;

        tLength = ($(this).val().length);        
        $("#limit_" + currentTB).text(tLength + "/1800");
        var textbox =
            "<div class='form-group'>"
                + "<label for='" + counter + "'>Página " + counter + "</label> <span id ='limit_" + counter + "'>0/1800</span>"
                + "<textarea class='form-control page' id='" + counter + "' name = 'page_" + counter + "' rows='3'></textarea>"
                + "<button class = 'btn btn-danger btn-default remove'><i class = 'mdi mdi-trash-can-outline'></i></button>"
            + "</div>";
        
        // Add textarea
        if ( tLength >= 1800 && !document.getElementById((parseInt(currentTB, 10) + 1))) {
            $("#textbox-container").append(textbox);
            NewPage();
            DeletePage();
        }
    });
}

function pageLenght(){
    var x = document.getElementById("myInput").value;
    // console.log();
}

function DeletePage(){
    // foreach page, change certain values
    // change span, id, name to counter number
    $(".remove").click(function( event ) {
        event.preventDefault();
        $(this).parent().remove();
        var pageNo = 1;
        $( ".page" ).each(function() {
            var counter = $(this ).attr("id");
            $(this ).attr("id", pageNo);
            $(this).siblings("label").text("Página " + pageNo);
            console.log(counter);
            pageNo++;
        });
    })
}
<?php
if ($mode != 'new') {?>
function ShareDraft(){
    $("#share-draft").on('submit', function(e){
        var form = new FormData(this);
        var url = "<?= base_url('index.php/document/draft_back/share_draft/' . $document['ID'])?>";
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            beforeSend : function(data){
                $("button").attr("disabled", true);
                //$("#preview").fadeOut();
                // $("#err").fadeOut();
                // console.log(data);
            },
            success: function(data){
                if(data.result){
                    window.location.href = "<?= base_url() . 'index.php/document/draft/'?>" + data.ID;
                }
                else{
                    // console.log(data);
                }
            }
        });
    });
}
<?php
}
?>



</script>

<?php

function DrawPagesInput($mode, $textArray = null){
    if ($mode == 1) {?>
        <div class="form-group">
            <label for="1">Página 1</label> <span id ="limit_1">0/1800</span>
            <textarea class="form-control page 1" id="1" name = "page_1" rows="3"></textarea>
        </div>
    <?php
    }else{
        foreach ($textArray as $key => $value) {?>
            <div class="form-group">
                <label for="<?= $key+1?>">Página <?= $key+1?></label> <span id ="limit_<?= $key+1?>">0/1800</span>
                <textarea class="form-control page" id="<?= $key+1?>" name = "page_<?= $key+1?>" rows="3"><?= $value?></textarea>
                <?php
                if ($key != 0):?>
<button class = 'btn btn-danger btn-default remove'><i class = 'mdi mdi-trash-can-outline'></i></button>
                <?php
                endif;
                ?>
                
            </div>
            
        <?php
        }
        
    }
}

function ShareDraft(){?>
    <div class="row">
            <div class="col-lg-12">
            <div class="card card-default">
            <div class="card-body">

<div class="form-group">
<label for="">Buscar y seleccionar usuario a quien compartir borrador:   </label>
<input type="text" class="form-control" id="search-user" placeholder="Buscar Usuario">
</div>
<form action="" id = "share-draft">
<div class="form-group" id = "user-result">
</div>
<hr>
<p>Usuarios seleccionados</p>
<div class="form-group" id = "user-holder">

</div>

<div class="form-group" id = "user-holder">
</div>
<div class="form-group">
<button type="submit" class = "btn btn-primary">Compartir</button>
</div>
</form>
            </div>
            </div>
            </div>
            </div>
            
<?php
}

?>