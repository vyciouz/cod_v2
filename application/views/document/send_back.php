<?php
session_start();
// Evitar que se ingrese a la URL de manera directa
if (!isset($docID) || empty($docID) || $docID == null) {
    echo "no hay";
    // header("Location: " . base_url() . "index.php/system/error_403");
    exit;
}
$document = GetDocument($docID);
if (!$document) {
    echo "Documento 404";
    // header("Location: " . base_url() . "index.php/system/error_404/1");
    exit;
}

if (empty($_POST)) {
    echo "no se envió información";
    header("Location: " . base_url() . "index.php/system/error_404/1");
    exit;
}

$sourceDocument = "./data/userdata/" . $_SESSION['ID'] . "/documents/" . $document[0]['NAME'];
if (!file_exists($sourceDocument)) {
    SendError("no existe archivo $sourceDocument");
    exit;
}

$to             = 0;
$cc             = [];
$title          = "";
$addressees     = [];
$sender         = (isset($_POST['REP']) && !empty($_POST['REP'])) ? $_POST['REP'] : $_SESSION['ID'] ;

foreach ($_POST as $key => $value) {
    if(strpos($key,'to') !== false){
        echo "Directo: $value\n";
        $to = $value;
        array_push($addressees,['ID'=>$value, 'CC' => 0]);
    }
    if(strpos($key,'cc') !== false){
        echo "Con copia: $value\n";
        array_push($cc,$value);
        array_push($addressees,['ID'=>$value, 'CC' => 1]);
    }
    if(strpos($key,'title') !== false){
        echo "Título: $value\n";
    }
    if(strpos($key,'textBody') !== false){
        echo "Texto: $value\n";
    }
}
if (!$addressees) {
    SendError("Elija destinatarios.");
    exit;
}
print_r($document);
echo "-----------------------------------------------------------------\n";
SendAndAddToDB ($document, $docID, $addressees, $sender);

if (!empty($_FILES)) {
    print_r($_FILES);
}

/*
Esta función toma el documento previamente generado y cicla por los ID's de
los usarios a quienes les será enviado el documento con copia. */
function SendAndAddToDB ($document, $currentDocumentID, $addressees, $sender){
    $dateCurrent    = new DateTime(date("Y-m-d H:i:s"));
    $baseFilesRoute = "./data/userdata/"; #Ruta base donde se guardan los documentos

    // Información que es igual para todos los remitentes.
    $document[0]['ID_PARENT_DOCUMENT']      = $currentDocumentID;
    $document[0]['ID_ORIGINAL_DOCUMENT']    = ($document[0]['ID_ORIGINAL_DOCUMENT'] == 0) ? $currentDocumentID : $document[0]['ID_ORIGINAL_DOCUMENT'];
    $document[0]['SENT_FROM']               = $sender;
    $document[0]['DATE_RECEIVED']           = $dateCurrent->format("Y-m-d H:i:s");;
    foreach ($addressees as $key => $value) {
        echo "Nuevo dueño: " . $value['ID'] . "\n";
        $document[0]['CURRENT_OWNER']   = $value['ID'];
        $document[0]['CC']              = ($value['CC'] == 1) ? 1 : 0;

        print_r($document);
        $srcFile = $baseFilesRoute . "$sender/documents/" . $document[0]['ID'] . "_" . str_replace(' ', '_',$document[0]['TITLE']) . ".pdf";
        echo "$srcFile\n";
        echo (file_exists($srcFile)) ? "existe" : "error documento 404" ;
        $newID = RegisterDocumentInDB($document);
        $destfile = $baseFilesRoute . $value['ID'] . "/documents/" . $newID . "_" . str_replace(' ', '_',$document[0]['TITLE']) . ".pdf";
        // echo "-> $destfile\n";
        copy($srcFile, $destfile);
    }
}


function RegisterDocumentInDB($document){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $dataArray = [
        "TITLE"                 => $document[0]['TITLE'],
        "SENT_FROM"             => $document[0]['SENT_FROM'],
        "DATE_CREATED"          => $document[0]['DATE_RECEIVED'],
        'DATE_RECEIVED'         => $document[0]['DATE_RECEIVED'],
        "CURRENT_OWNER"         => $document[0]['CURRENT_OWNER'],
        "ORIGINAL_OWNER"        => $document[0]['ORIGINAL_OWNER'],
        "CC"                    => $document[0]['CC'],
        "ID_ORIGINAL_DOCUMENT"  => $document[0]['ID_ORIGINAL_DOCUMENT']
    ];
    $ci->db->insert('t_documents', $dataArray);
    $recentId = $ci->db->insert_id();
    $document[0]['NAME'] = $recentId . "_" . str_replace(' ', '_',$document[0]['TITLE']) . ".pdf";
    $dataArray = [
        "NAME"           => $document[0]['NAME'],
    ];
    $ci->db->where('id', $recentId);
    $ci->db->update('t_documents', $dataArray);
    
    // Attachments($recentId, $document[0]['CURRENT_OWNER']);
    return $recentId;
}

function GetDocument($docID){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $ci->db->where('ID', $docID);
    $query = $ci->db->get('t_documents');
    $document = $query->result_array();
    return $document;
}

function SendError($message){
    $error = ['error' => ['msg' => $message]];
    $error = json_encode($error);
    echo $error;
}
?>