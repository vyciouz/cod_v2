<?php
$userInfo = getUserData($_SESSION['ID']);

$dependency = GetDependency($userInfo['DEPENDENCIA']);
$dependencyUsers = getDependencyUsers($userInfo['DEPENDENCIA']);
$dependencyLogo = "data/dependencies/". $userInfo['DEPENDENCIA']. "/logo.png";
$defaultNoLogo = "assets/img/system/no_logo.png";
$logo = (file_exists($dependencyLogo)) ? $dependencyLogo :$defaultNoLogo;

?>
<div class="row justify-content-center mt-5">
    <div class="col-xl-9">
        <div class="card card-default">
            <div class="card-header card-header-border-bottom">
                <h2> <i class="mdi mdi-account-card-details"></i> <?= $dependency['NAME'] ." (" . $dependency['NOMENCLATURE'] . ")" ?> </h2>
            </div>
            <div class="card-body">
                <div class="d-inline-block rounded overflow-hidden mt-4 mr-0 mr-lg-4">
                    <img src="<?= base_url() . $logo ?>" alt="Logo" id = "logo" style = "max-height: 100px">
                </div>
                <form id = "new-logo-form">
                    <div class="form-group" id = "new_logo">
                        <div class = "row" id="edit_logo_container" style="display: none">
                            <div class = " col-xl-6">
                                <input type="file" id = "input_newlogo" name = "newLogo" class = "form-control-input" accept="image/png">
                            </div>
                            <div class = " col-xl-6" >
                                <button type = "submit" id="upload_newlogo" class="mb-1 btn btn-sm btn-outline-primary"><i class = "mdi mdi-publish"></i></button>
                                <button id="upload_newlogo_cancel" class="mb-1 btn btn-sm btn-outline-danger">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="form-footer pt-5 border-top">
                    <a href="<?php echo base_url() . "index.php/dependency/edit/" . $userInfo['DEPENDENCIA']?>">
                        <button class="btn btn-primary btn-default">Editar Información</button>
                    </a>
                    <button class="btn btn-primary btn-default" id="edit_logo">Editar Logo</button>
                    
                </div>
                
                <hr>
                <p class="mb-5">
                    <h3 style = "color: black">
                        Vista previa de usuarios pertenecientes a esta dependencia.
                    </h3>
                </p>
                <?php DrawTable($dependencyUsers) ?>
            </div>
        </div>

    </div>
</div>
<script>

$(document).ready(function(){
    $('#upload_newlogo_cancel').on('click', function(e){
        e.preventDefault();
        console.log('cancel');
        $('#input_newlogo').val('');
        $("#edit_logo_container").toggle();
    });

    $('#edit_logo').on('click', function(e){
        e.preventDefault();
        $("#edit_logo_container").toggle();
    });

    $("#new-logo-form").on('submit', function(e){
        url = "<?php echo base_url() . "index.php/dependency/dependency_back/change_logo/" . $userInfo['DEPENDENCIA']?>";
        var form = new FormData(this);
        console.log(url);
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'text',
            beforeSend : function(data){
                //$("#preview").fadeOut();
                // $("#err").fadeOut();
                console.log(data);
            },
            success: function(data){
                <?php $url = base_url() . "index.php/user/profile/" . $_SESSION['ID'];?>
                // window.location.replace("<?php echo $url?>");
                d = new Date();
                $("#logo").attr("src", "<?= base_url() . $logo ?>?"+d.getTime());
                $('#input_newlogo').val('');
                $("#edit_logo_container").toggle();
                // console.log(data);
            },
            error: function(e){
            }
        });
    });
});
</script>
<?php
function DrawTable($users){?>
<table class="table table-hover">
<thead>
<?php

foreach ($users[0] as $arrayKeys => $arrayValues):
    echo "<th>" . $arrayKeys . "</th>";
endforeach;
echo "</thead>";
echo "<tbody>";

foreach ($users as $arrayKey => $arrayValues):
    echo "<tr>";
    foreach ($arrayValues as $key => $value):
        echo "<td>" . $value. "</td>";
    endforeach;
    echo "</tr>";
endforeach;

echo "</tbody>";
echo "</table>";
}

function GetDependencyUsers($id = null){
    $ci =& get_instance();
    $queryString = "SELECT
    t_users.LAST_NAME AS `Primer Apellido`,
    t_users.MAIDEN_NAME AS `Segundo Apellido`,
    t_users.`NAMES` AS Nombres,
    cat_dependencias.`NAME` AS Dependencia,
    cat_u_administrativas.`NAME` AS `Unidad Administrativa`,
    cat_sub_u_administrativas.`NAME` AS `Sub-unidad Administrativa`,
    t_users.PUESTO AS Puesto
    FROM
    t_users
    INNER JOIN cat_dependencias ON t_users.DEPENDENCIA = cat_dependencias.ID
    INNER JOIN cat_u_administrativas ON t_users.UNIDAD_ADMINISTRATIVA = cat_u_administrativas.ID
    INNER JOIN cat_sub_u_administrativas ON t_users.SUBUNIDAD_ADMINISTRATIVA = cat_sub_u_administrativas.ID
    WHERE
    t_users.DEPENDENCIA = 1
    ORDER BY
    `Primer Apellido` ASC
    LIMIT 5;
    ";
    $query = $ci->db->query($queryString);
    $resultUser = $query->result_array();
    return $resultUser;
}

function GetDependency($id = null){
    $ci =& get_instance();
    $ci->db->where('ID', $_SESSION['ID']);
    $queryUser = $ci->db->get('t_users');
    $resultUser = $queryUser->row_array();

    $ci->db->where('ID', $resultUser['DEPENDENCIA']);
    $queryDep = $ci->db->get('cat_dependencias');
    $resultDep = $queryDep->row_array();
    return $resultDep;
}
?>