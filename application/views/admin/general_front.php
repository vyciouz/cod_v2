<?php

$codLogo = "assets/img/system/campaign.png";
$defaultNoLogo = "assets/img/system/no_logo.png";
$logo = (file_exists($codLogo)) ? $codLogo :$defaultNoLogo;
$slogan = $this->db->select('*')->order_by('id',"desc")->limit(1)->get('_plataforma')->row_array();

?>
<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2>Administración General</h2>
                    </div>
                    <div class="card-body">
                        <div class="d-inline-block rounded overflow-hidden mt-4 mr-0 mr-lg-4">
                            <img src="<?= base_url() . $logo ?>" alt="Logo" id = "logo" style = "max-height: 100px">
                        </div>
                        <form id = "new-logo-form">
                            <div class="form-group" id = "new_logo">
                                <div class = "row" id="edit_logo_container" style="display: none">
                                    <div class = " col-xl-6">
                                        <input type="file" id = "input_newlogo" name = "newLogo" class = "form-control-input" accept=".png">
                                    </div>
                                    <div class = " col-xl-6" >
                                        <button type = "submit" id="upload_newlogo" class="mb-1 btn btn-sm btn-outline-primary"><i class = "mdi mdi-publish"></i></button>
                                        <button id="upload_newlogo_cancel" class="mb-1 btn btn-sm btn-outline-danger">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="form-footer pt-5 border-top">
                            <button class="btn btn-primary btn-default" id="edit_logo">Editar Logo</button>
                        </div>
                        <hr>
                        <div class = "row">
                            <div class ="col-12">
                                <h3 id = "slogan_text"><?= $slogan['SLOGAN']?>
                                </h3>
                            </div>
                            <form id = "new-slogan-form">
                                <div class="form-group col-sm-12" id = "new_slogan">
                                    <div class = "row" id="edit_slogan_container" style="display: inline-bloxk">
                                        <div class = " col-xl-6">
                                            <input type="text" id = "input_slogan" name = "newSlogan" class = "form-control" >
                                        </div>
                                        <div class = " col-xl-6" >
                                            <button type = "submit" id="change_slogan" class="mb-1 btn btn-sm btn-outline-primary"><i class = "mdi mdi-publish"></i> Cambiar frase</button>
                                            <button id="change_slogan_cancel" class="mb-1 btn btn-sm btn-outline-danger">Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class = "col-12">
                                <div class="form-footer pt-5 border-top">
                                    <button class="btn btn-primary btn-default" id="edit_slogan">Cambiar frase de campaña</button>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

$(document).ready(function(){
    EditToggle("edit_logo", "edit_logo_container");
    EditToggle("edit_slogan", "edit_slogan_container");
    CancelChange("upload_newlogo_cancel", "input_newlogo", "edit_logo_container");
    CancelChange("change_slogan_cancel", "input_slogan", "edit_slogan_container");

    $("#new-logo-form").on('submit', function(e){
        url = "<?php echo base_url() . "index.php/general/admin_back/change_logo"?>";
        var form = new FormData(this);
        e.preventDefault();
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'text',
            success: function(data){
                <?php $url = base_url() . "index.php/user/profile/" . $_SESSION['ID'];?>
                // window.location.replace("<?php echo $url?>");
                d = new Date();
                $("#logo").attr("src", "<?= base_url() . $logo ?>?"+d.getTime());
                $('#input_newlogo').val('');
                $("#edit_logo_container").toggle();
            },
            error: function(e){
                console.log(e.msg);
            }
        });
    });

    $("#new-slogan-form").on('submit', function(e){
        e.preventDefault();
        url = "<?php echo base_url() . "index.php/general/admin_back/change_slogan"?>";
        var form = new FormData(this);
        console.log(url);
        
        $.ajax({
            url: url,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'text',
            success: function(data){
                <?php $url = base_url() . "index.php/user/profile/" . $_SESSION['ID'];?>
                // window.location.replace("<?php echo $url?>");
                $("#slogan_text").text(data);
                $('#input_slogan').val('');
                $("#edit_slogan_container").toggle();
                console.log(data);
            },
            error: function(e){
                console.log(e.msg);
            }
        });
    });
});
function EditToggle(button, container){
    $('#' + button).on('click', function(e){
        e.preventDefault();
        $("#" + container).toggle();
    });
}

function CancelChange(button, inputField, container){
    $('#' + button).on('click', function(e){
        e.preventDefault();
        $('#' + inputField).val('');
        $('#' + container).toggle();
    });
}
</script>
