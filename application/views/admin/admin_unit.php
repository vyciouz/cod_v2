<div class="row justify-content-center mt-5">
    <div class="col-xl-9">
        <div class="card card-default">
            <div class="card-header card-header-border-bottom">
                <h2> <i class="mdi mdi-account-card-details"></i> <?php echo $title ?> </h2>
            </div>
            <div class="card-body">
                <div class="section section-white">

                    <div class="col-md-12 section-title">
                        <h3><?php echo $title ?></h3>
                    </div>

                    <div class="col-md-12 section-element">
                        <p>Nomenclatura: <?php echo $dependency[0]['NOMENCLATURE']?></p>
                        <p>Dirección: <?php echo $dependency[0]['ADDRESS']?></p>
                        <p>Teléfono 1: <?php echo $dependency[0]['TELEPHONE_1']?></p>
                        <p>Teléfono 2: <?php echo $dependency[0]['TELEPHONE_2']?></p>
                        <p>Teléfono 3: <?php echo $dependency[0]['TELEPHONE_3']?></p>
                    </div>
                    <hr>
                    <div class="col-md-12 section-element" style="text-align:center">
                        <a href="<?php echo base_url() . "index.php/admin_unit/edit/$id"?>"><button
                                class="btn btn-dark">Actualizar información</button></a>
                    </div>
                    <div class="col-md-12 section-element">
                        Usuarios de la dependencia
                        <div class="section-element">
                            <table id="example2" class="table table-hover dataTable" role="grid"
                                aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <?php
                                /* Columnas de la tabla a dibujar. */
                                /*
                                list.php ocupa llamarse desde el controlador con 4 argumentos para funcionar correctamente.
                                Ocupa $dataArray como el arreglo de información traído de la base de datos, $dataColumns como arreglo de las columnas a usar,
                                */
                                $columnsToUse = [
                                    'NAMES'=>'Nombres',
                                    'LAST_NAME'=>'Apellido Paterno',
                                    'MAIDEN_NAME'=>'Apellido Materno',
                                    'EMAIL'=>"Correo Electrónico",
                                    'PUESTO'=>"Puesto"
                                ];
                                DrawTableHeaders($columnsToUse);
                                ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                            /* Generación del cuerpo de la tabla*/
                            $catalogDependency = array(
                                "SECRETARIA"=>array(
                                    "TABLE"=>"cat_secretarias","searchInCOL"=>"ID","resultInCOL"=>"NAME"
                                )
                            );
                            $controllerName = "users";
                            DrawTable($users, $columnsToUse,$catalogDependency,$controllerName);?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>


<?php

function DrawTableHeaders($array){
    $count = 0;
    foreach ($array as $key=>$value): 
        if ($count == 0) {?>
<th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending"
    aria-label="<?php echo $value?>">
    <?php echo $value?>
</th>
<?php
        }else{?>
<th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="<?php echo $value?>">
    <?php echo $value?>
</th>
<?php
        }?>
<?php endforeach ?>
<?php
}

function DrawTable($data, $COLUMNS ,$arrayColNameToTable, $controller){
    //Cada registro encontrado
    foreach ($data as $key => $value): ?>
<tr role="row" class="odd"><?php
        //Cada columna a buscar
        foreach ($COLUMNS as $colKEY => $colVALUE) {
            //Si la columna a buscar existe dentro del arreglo arrayColNameToTable como arrayColNameToTable[$colNAME], 
            if (isset($arrayColNameToTable[$colKEY]) && !empty($arrayColNameToTable[$colKEY])) {
                $ci =& get_instance();
                $searchValue = $value[$colKEY];
                if ($searchValue!="") {
                    $TABLE = $arrayColNameToTable[$colKEY]['TABLE'];
                    $COL = $arrayColNameToTable[$colKEY]['searchInCOL'];
                    $resCOL = $arrayColNameToTable[$colKEY]['resultInCOL'];
                    $query = $ci->db->query("SELECT * FROM $TABLE WHERE $COL = $searchValue;");
                    $queryResult = $query->result_array();
                    $result = $queryResult;
                ?><td><?php echo $result[0][$resCOL];?></td><?php
                }else{
                    ?><td><?php echo "N/A";?></td><?php
                }
                
            } else {?>
    <td><?php echo $value[$colKEY] ?></td><?php
            }
        }?>
</tr><?php
    endforeach;
}


?>