<?php
switch ($method) {
    case 'change_logo':
        
        ChangeLogo();
        break;
    
    case 'change_slogan':
        ChangeSlogan();
        break;
    default:
        echo "error";
        break;
}

function ChangeLogo(){
    if (!isset($_FILES) || empty($_FILES)) {
        echo "error";
        exit();
    }
    $imagesDirectory = "./assets/img/system/";
    (file_exists ( $imagesDirectory )) ? "" : mkdir($imagesDirectory) ;
    if (!move_uploaded_file($_FILES["newLogo"]["tmp_name"], $imagesDirectory . "/campaign.png")) {
        echo "error";
        exit;
    }
    // print_r($_FILES);
}

function ChangeSlogan(){
    $dateCurrent   = new DateTime(date("Y-m-d H:i:s"));
    $formattedDate = $dateCurrent->format("Y-m-d H:i:s");
    $ci =& get_instance();
    $slogan = $ci->db->select('*')->order_by('id',"desc")->limit(1)->get('_plataforma')->row_array();
    
    $dataArray = [
        "SLOGAN"        => $_POST['newSlogan'],
        "INITIAL_DATE"  => $formattedDate
    ];
    $ci->db->insert('_plataforma', $dataArray);
    // $recentId = $ci->db->insert_id();
    $ci->db->where('id', $slogan['ID']);
    $dataArray = [
        "FINAL_DATE"  => $formattedDate
    ];
    $ci->db->update('_plataforma', $dataArray);
    echo $_POST['newSlogan'];
}
?>