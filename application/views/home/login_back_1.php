<?php
session_start();


// print_r($_POST);
// print_r($_FILES);

if (isset($_SESSION) && !empty($_SESSION)) {?>
    <div style="text-align:center"><h3>Ya se ha iniciado sesión.</h3></div><?php
    // header("Location: " . base_url());
    // die();
} else {
    if(!empty($_FILES) ){
        GenerateDefaults();
        /*
        CheckKeyCer() verifica que 
        regresa CURP en caso de que los archivos coincidan.
        */
        $randomNumber = UploadFilesToTemp();
        $loginInfo = new FIEL();
        $loginInfo->CERFILE = basename("$randomNumber.cer");
        $loginInfo->KEYFILE = basename("$randomNumber.key");
        $loginInfo->PW = $_POST['userpw'];
        $row = CheckKeyCer($loginInfo);
        StartSession($row, $loginInfo);
    }
}

function GenerateDefaults(){
    // Carpeta para archivos temporales del sistema
    $folder = "./data/temp/";
    (file_exists ( $folder )) ? "" : mkdir($folder) ;
}



function CheckKeyCer($loginInfo){
    /*
    Verificado que se hayan subido los archivos correctos, 
    se crea un nuevo objeto inicializando CERFILE y KEYFILE
    con los nombres base de los archivos, y PW con la contraseña
    de la llave privada.
    */
    
    // print_r($loginInfo);

    /* Inica manipulación de archivo .CER */
    /*
    Obtener el archivo .cer.pem del archivo .cer el cual es
    luego procesado a txt para obtener la información
    almacenada dentro del certificado.
    
    El archivo .CER por si solo, se usará para obtener su MODULUS,
    el cual será comparado con el MODULUS el archivo .KEY.PEM.
    */
    $generarCERPEM  = "x509"
        . " -in " . TEMP_ROUTE . $loginInfo->CERFILE    
        . " -inform DER"
        . " -outform PEM"
        . " -out " . TEMP_ROUTE . "$loginInfo->CERFILE.pem";

    // TEXT es usado para obtener los datos dentro del archivo .cer
    // dato a obtener: CURP
    $generarTXT = "x509"
        . " -in " . TEMP_ROUTE . "$loginInfo->CERFILE.pem"
        . " > " . TEMP_ROUTE . $loginInfo->CERFILE . ".txt -text";
    exec(OPENSSL_ROUTE . $generarCERPEM, $arrCERPEM, $statusCERPEM);
    exec(OPENSSL_ROUTE . $generarTXT, $arrTXT, $statusTXT);

    $sslcert = file_get_contents(TEMP_ROUTE . $loginInfo->CERFILE . ".txt");
    $sslcert = array(openssl_x509_parse($sslcert,TRUE));
    $names = $sslcert[0]['subject']['CN'];
    $curp = $sslcert[0]['subject']['serialNumber'];
    $rfc = $sslcert[0]['subject']['x500UniqueIdentifier'];
    $row = SearchCURP($curp);

    // SuccessMsg("Before EXEC");
    if ($statusCERPEM == 0){
        chmod(TEMP_ROUTE . "$loginInfo->CERFILE.pem", 0777);

        if ($statusTXT == 0){
            chmod(TEMP_ROUTE . $loginInfo->CERFILE . ".txt", 0777);
            /*
            Aquí se procesa el .KEY y genera un archivo PEM, el cúal será
            usado para obtener su modulus
            */
            $generateKEYPEM = "pkcs8"
                . " -inform DER"
                . " -in " . TEMP_ROUTE. $loginInfo->KEYFILE #file.key
                . " -passin pass:" . $loginInfo->PW
                . " -out ". TEMP_ROUTE ."$loginInfo->KEYFILE.pem"; #file.key.pem
            exec(OPENSSL_ROUTE . $generateKEYPEM, $arr, $statusKeyPem);
            if ($statusKeyPem === 0){
                # Dar permisos de lectura y escritura (necesario en sistemas que se ejecuten en Linux).
                chmod(TEMP_ROUTE ."$loginInfo->KEYFILE.pem", 0777);
            }else{
                RemoveTempFiles($loginInfo);
                $result = [
                    "result" => 0,
                    "msg" => "Contraseña incorrecta."
                ];
                RemoveTempFiles($loginInfo);
                echo json_encode($result);
                exit;
            }

            #Se obtiene el modulus del certificado subido
            $getModulusCer = "x509"
                . " -inform DER"
                . " -in ". TEMP_ROUTE. $loginInfo->CERFILE
                . " -noout -modulus";
            $ModulusCer = exec(OPENSSL_ROUTE .$getModulusCer, $arr, $status);
            #Se obtiene el modulus de la llave privada subida
            $getModulusKey = "rsa"
                . " -in " . TEMP_ROUTE . "$loginInfo->KEYFILE.pem"
                . " -noout -modulus";
            $ModulusKey = exec(OPENSSL_ROUTE.$getModulusKey, $arr, $status);

            $sslcert = file_get_contents(TEMP_ROUTE . $loginInfo->CERFILE . ".txt");
            $sslcert = array(openssl_x509_parse($sslcert,TRUE));
            $names = $sslcert[0]['subject']['CN'];
            $curp = $sslcert[0]['subject']['serialNumber'];
            $rfc = $sslcert[0]['subject']['x500UniqueIdentifier'];

            if($ModulusKey != $ModulusCer){
                $GLOBALS['error_pw'] = true;
                if (empty($ModulusCer)) {
                    $result = [
                        "result" => 0,
                        "msg" => "Formato de certificado incorrecto."
                    ];
                    RemoveTempFiles($loginInfo);
                    echo json_encode($result);
                    exit;
                }
                if (empty($ModulusKey)) {
                    $result = [
                        "result" => 0,
                        "msg" => "Formato de llave incorrecto."
                    ];
                    RemoveTempFiles($loginInfo);
                    echo json_encode($result);
                    exit;
                }
                // throw new RuntimeException("Llave no corresponde a certificado.", 1);
                $result = [
                    "result" => 0,
                    "msg" => "Combinación de certificado/llave incorrecta."
                ];
                RemoveTempFiles($loginInfo);
                echo json_encode($result);
                exit;
            }
        }else{
        }
    }else{
    }
    return $row;
}

function RemoveTempFiles($loginInfo){
    /* Borrado de archivos temporales. */
    $filesToDelete = [
        $loginInfo->CERFILE,
        $loginInfo->CERFILE . ".pem",
        $loginInfo->KEYFILE,
        $loginInfo->KEYFILE . ".pem",
        $loginInfo->CERFILE . ".txt"
    ];
    foreach ($filesToDelete as $key => $value) {
        if (file_exists(TEMP_ROUTE . $value)) {
            unlink(TEMP_ROUTE . $value);
        }
    }
}

function SearchCURP($curp){
    $ci =& get_instance();
    $query = $ci->db->query("SELECT * FROM t_users WHERE CURP = '$curp'");
    $row = $query->row_array();

    if (!$row){
        $result = [
            "result" => 0,
            "msg" => "CURP: $curp no encontrado."
        ];
        echo json_encode($result);
        exit;
    }
    return $row;
}

function StartSession($row, $loginInfo){
    $_SESSION['NAMES']   = ($row["NAMES"]) ? $row["NAMES"] : "N/A";
    $_SESSION['EMAIL']   = ($row["EMAIL"]) ? $row["EMAIL"] : "N/A";
    $_SESSION['CURP']    = ($row["CURP"])  ? $row["CURP"]  : "N/A";
    $_SESSION['RFC']     = ($row["RFC"])   ? $row["RFC"]   : "N/A";
    $_SESSION['ID']      = ($row["ID"])    ? $row["ID"]    : "0";

    $userFolder = "./data/userdata/".$row['ID'];
    // Crear directorio de usuario
   
    $cerFileOld = TEMP_ROUTE . $loginInfo->CERFILE;
    $cerFileNew = "$userFolder/".$_SESSION['ID'].".cer";
    $keyFileOld = TEMP_ROUTE . $loginInfo->KEYFILE;
    $keyFileNew = "$userFolder/" . $_SESSION['ID'].".key";

    if (!file_exists($cerFileNew)) {
        if (!copy($cerFileOld, $cerFileNew)) {
        $result = [
            "result" => 0,
            "msg" => "No se pudo mover archivo $cerFileNew."
        ];
        echo json_encode($result);
        exit;
        }
    }
    if (!file_exists($keyFileNew)) {
        if (!copy($keyFileOld, $keyFileNew)) {
        $result = [
            "result" => 0,
            "msg" => "No se pudo mover archivo $keyFileNew."
        ];
        echo json_encode($result);
        exit;
        }
    }
    $result = [
        "result" => 1,
        "msg" => "Sesión iniciada como " . $row['NAMES'],
        "ID" => $row["ID"]
    ];
    RemoveTempFiles($loginInfo);
    echo json_encode($result);
    exit;
}

function UploadFilesToTemp(){
    $flag = true;
    // Undefined | Multiple Files | $_FILES Corruption Attack
    /*
    Verificaciónde subida de archivos. Si falta alguno de los
    2 requeridos, o si ocurre algún error, no permite continuar.
    */
    $folder = "./data/temp/";
    $dateCurrent                    = new DateTime(date("Y-m-d H:i:s"));
    $randomNumber = rand(1, 9999999) . "_" . $dateCurrent->format("Ymd_His");

    // chmod($folder, 0777);
    if( isset( $_FILES["userfile"]) && isset( $_FILES["keyfile"])) {
        $keyUp = $randomNumber . ".key";
        $cerUp = $randomNumber . ".cer";
        $uploadOk = 1;
        if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $folder . $cerUp)) {
            $result = [
                "result" => 0,
                "msg" => "Faltó o no se pudo subir el certificado."
            ];
            echo json_encode($result);
            exit;
        }else{
            chmod($folder . $cerUp, 0777);
        }
        if (!move_uploaded_file($_FILES["keyfile"]["tmp_name"], $folder . $keyUp)) {
            $result = [
                "result" => 0,
                "msg" => "Faltó o no se pudo subir la llave."
            ];
            echo json_encode($result);
            exit;
        }else{
            chmod($folder . $keyUp, 0777);
        }
    }else{
        $result = [
            "result" => 0,
            "msg" => "Faltó o no se pudo subir el certificado."
        ];
        echo json_encode($result);
        exit;
    }
    return $randomNumber;
}
?>

