<?php
session_start();
if (empty($_SESSION)) {
    header("Location: " . base_url());
}
session_destroy();
$url = base_url();
header("Location: $url");
?>