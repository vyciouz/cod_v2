<?php
session_start();
if (isset($_SESSION) && !empty($_SESSION)) {
    header("Location: " . base_url() . "index.php/user/profile/" . $_SESSION['ID']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="Sleek Dashboard - Free Bootstrap 4 Admin Dashboard Template and UI Kit. It is very powerful bootstrap admin dashboard, which allows you to build products like admin panels, content management systems and CRMs etc.">


  <title>Iniciar sesión</title>

  <!-- GOOGLE FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500" rel="stylesheet" />
  <link href="https://cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css" rel="stylesheet" />

  <script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
  <!-- PLUGINS CSS STYLE -->
  <link href="<?php echo base_url()?>assets/plugins/nprogress/nprogress.css" rel="stylesheet" />

  

  <!-- SLEEK CSS -->
  <link id="sleek-css" rel="stylesheet" href="<?php echo base_url()?>assets/css/sleek.css" />

  

  <!-- FAVICON -->
  <link href="<?php echo base_url()?>assets/img/favicon.png" rel="shortcut icon" />

  

  <!--
    HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries
  -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script src="<?php echo base_url()?>assets/plugins/nprogress/nprogress.js"></script>
  <script src="<?php echo base_url()?>assets/js/login.js"></script>

</head>
<body class="certificado" id="body">
    <div class="container d-flex flex-column justify-content-between vh-100">
        <div class="row justify-content-center mt-5">
            <div class="col-xl-5 col-lg-6 col-md-10">
                <div class="card">
                    <div class="card-header bg-primary">
                        <div class="app-brand">
                            <a href="<?php echo base_url()?>">
				                <img class="brand-name" src="<?php echo base_url()?>assets/img/system/logo_w.png" alt="Gobierno del Estado de Nuevo León">
                            </a>
                        </div>
                    </div>
                    <div class="card-body p-5">
                        <h4 class="text-dark mb-5">Entrar con certificado</h4>
                            <form id = "login-cred" action="<?php echo base_url()?>index.php/welcome/lb_c" method = 'POST' enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-md-12 mb-4">
                                        <label for="" style = "font-weight: bold">Certificado:</label>
                                        <input type="file" class="" name="userfile" id="userfile" accept=".cer" required>
                                    </div>
                                    <div class="form-group col-md-12 mb-4">
                                        <label for="" style = "font-weight: bold">Llave:</label>
                                        <input type="file" class="" name="keyfile" id="keyfile" accept=".key" required>
                                    </div>
                                    <div class="form-group col-md-12 ">
                                        <input type="password" class="form-control input-lg" id="userpw" placeholder="Password" name = "userpw">
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-lg btn-primary btn-block mb-4">Iniciar sesión</button>
                                        <p><i class="mdi mdi-account-outline"></i> Inicar sesión con 
                                            <a class="text-blue" href="<?php echo base_url()?>index.php/welcome/user">usuario.</a>
                                        </p>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-md-12" id = "response-container" style = "text-align:center; padding-top:20px; display: block !important">
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright pl-0">
                <p class="text-center">&copy; 2018 Copyright Sleek Dashboard Bootstrap Template by
                    <a class="text-primary" href="http://www.iamabdus.com/" target="_blank">Abdus</a>.
                </p>
            </div>    
        </div>
    </div>

    <script>
$(document).ready(function(){
    loginForm("#login-cred","<?= base_url('index.php/welcome/lb_c')?>", "<?= base_url()?>");
});

</script>
</body>
</html>