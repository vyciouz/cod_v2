<?php
session_start();
$uPW = $_POST['userpw'];
$uName = $_POST['username'];
$logUser = "SELECT * FROM t_users WHERE USERNAME = '$uName' AND PASSWORD = '$uPW'";
$query = $this->db->query($logUser);
$row = $query->row_array();

if (!$row){
    $result = [
        "result" => 0,
        "msg" => "Usuario o contraseña incorrecta."
    ];
    echo json_encode($result);
    exit;
}

$_SESSION['NAMES'] = $row["NAMES"];
$_SESSION['EMAIL'] = $row["EMAIL"];
$_SESSION['CURP'] = $row["CURP"];
$_SESSION['RFC'] = $row["RFC"];
$_SESSION['ID'] = $row["ID"];
$folder = "./data/userdata/".$row['ID'];

if (!file_exists($folder)) {
    mkdir ($folder, 0777);
    mkdir ($folder."/documents", 0777);
}
$result = [
    "result" => 1,
    "msg" => "Sesión iniciada como " . $row['NAMES'],
    "ID" => $row["ID"]
];
echo json_encode($result);
?>