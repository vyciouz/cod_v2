        </div>
    </div>
    
    <script src="<?php echo base_url()?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/toastr/toastr.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/slimscrollbar/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/ladda/spin.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/ladda/ladda.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery-mask-input/jquery.mask.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/select2/js/select2.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/daterangepicker/moment.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jekyll-search.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/sleek.js"></script>
    <script src="<?php echo base_url()?>assets/js/chart.js"></script>
    <script src="<?php echo base_url()?>assets/js/date-range.js"></script>
    <script>
        $( document ).ready(function() {
            $('.slim-scroll').slimScroll({
                // allowPageScroll: true
            });
    });
    </script>
    </body>
</html>