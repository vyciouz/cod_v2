<?php
if (empty($_SESSION)) {
    // echo $host;
    // echo base_url();
    header("Location: " . base_url());
}

?>
<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2><?php echo $title?></h2>
                    </div>
                    <div class="card-body">
                       
                            <?php
                                /* Generación del cuerpo de la tabla*/
                                DrawTable($dataArray,$controllerName, $index, true);?> 
                          
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                $url = base_url()."index.php/" . $controllerName . "/new";?>
                <a href="<?php echo $url?>"><button class="btn btn-dark">Nuevo</button></a>
            </div>
        </div>
    </div>
</div>

<script>
 $(function () {
    $('#list').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        "pageLength": 25,
        "order": [[ <?= $orderByCol ?>, "desc" ]],
        "language": {
            "search": "Búsqueda:",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "info":           "Mostrando de _START_ a _END_ entradas de un total de _TOTAL_ resultados",
            "infoEmpty":      "Mostrando de 0 a 0 entradas de 0 resultados",
        },
    })
})
</script>

<?php
/********************************************************************************************/
/* ************************************** FUNCTIONS  ************************************** */
/********************************************************************************************/


function DrawTable($data, $controller, $index, $actions){
    // print_r($data);
    echo '<table class="table table-hover mail table-striped" id ="list">';
    echo "<thead>";
    
    if ($data) {
        foreach ($data[0] as $key => $value) {
            echo ( $key !== "ID" && $key !== "SENT_FROM" && $key !== "ID_PARENT_DOCUMENT" && $key !== "CURRENT_OWNER" && $key !== "READ" ) ? "<th>" . $key . "</th>" : "" ;
        }

        echo ($actions) ? "<th> Acciones </th>" : "" ;
        echo "</thead>";
        echo "<tbody>";
    
        $odd = true;
        $trClass = "odd";
        foreach ($data as $key => $value):
            
            echo "<tr class = ' $trClass' id = '" . $value[$index] . "'>";
            foreach ($value as $k => $v):
                $v = (strcmp($v,"")) ? $v : "--" ;
                echo ( $k !== "ID" && $k !== "SENT_FROM" && $k !== "ID_PARENT_DOCUMENT" && $k !== "CURRENT_OWNER" && $k !== "READ" ) ? "<td>" . $v . "</td>" : "" ;
            endforeach;
            if ($actions) {
                echo "<td>";
                echo "<a href='" .base_url("index.php/$controller/view/" . $value[$index]) . "' class='btn btn-xs'><i class='fa fa-search'></i></a>";
                echo "<a href='" .base_url("index.php/$controller/edit/" . $value[$index]) . "' class='btn btn-xs'><i class='fa fa-edit'></i></a>";
                echo "<a href='" .base_url("index.php/$controller/delete/" . $value[$index]) . "'  class='btn btn-xs'><i class='fas fa-trash-alt'></i></a>";
                echo "</td>";
            }
            echo "</tr>";
            $odd = ($odd) ? false : true ;
            $trClass = ($odd) ? "odd" : "even" ;
        endforeach;
    }else{
        echo "<th>Tabla vacía</th>";
        echo "</thead>";
        echo "<tbody>";
        echo "<tr>";
        echo "<td>No hay información</td>";
        echo "</tr>";
    }
    
    echo "</tbody>";
    echo "</table>";
}
?>