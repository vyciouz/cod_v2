<div class="content">
    <div class="row justify-content-center mt-5">
        <div class="col-xl-9">
            <div class="card card-default">
                <div class="card-header card-header-border-bottom">
                    <h2> <i class="mdi mdi-account-card-details"></i> <?php echo $title ?> </h2>
                </div>
                <div class="card-body">
                    <div class="alert alert-danger" role="alert">
                        <i class="mdi mdi-alert mr-1"></i> Modulo en construccion
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>