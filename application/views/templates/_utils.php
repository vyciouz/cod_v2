<?php

#DEFINE OPENSSL
$openSSLSource = [
    "C:/xampp/apache/bin/openssl.exe ",
    "G:/xampp/apache/bin/openssl.exe ",
    "/usr/bin/openssl "
];
foreach ($openSSLSource as $key => $value) {
    if (file_exists(rtrim($value, " "))) {
        define ("OPENSSL_ROUTE", $value);
        break;
    }
}

// define("OPENSSL_ROUTE", "C:/xampp/apache/bin/openssl.exe "); #Other
// define("OPENSSL_ROUTE", "G:/xampp/apache/bin/openssl.exe "); #Home PC
// define("OPENSSL_ROUTE", "/usr/bin/openssl "); #Dev Machine

// Directorio en donde se encuentran los archivos .key y .cer correspondientes a la FIEL.
define("SRC_ROUTE", "./data/fiel/");
// Directorio en donde se almacenarán los archivos .key.pem resultantes de la desencriptación.
define("SAVE_ROUTE", "./data/fiel_temp/");

define("TEMP_ROUTE", "./data/temp/");

define("USER_DATA", "./data/userdata/");

class FIEL{
    var $PW;
    var $ROUTE;
    var $PEMFILE;
    var $TXTFILE;
    var $KEYFILE;
    var $CERFILE;
    function setPW($PW){
        $this->PW=$PW;
    }
    function setROUTE($ROUTE){
        $this->ROUTE=$ROUTE;
    }
    function setFIEL($FIEL){
        $this->FIEL=$FIEL;
    }
    function setPEM($PEM){
        $this->PEM=$PEM;
    }
    function setTXT($TXT){
        $this->TXT=$TXT;
    }
    function getPW($PW){
        return $this->PW;
    }
    function getROUTE($ROUTE){
        return $this->ROUTE;
    }
    function getFIEL($FIEL){
        return $this->FIEL;
    }
    function getPEM($PEM){
        return $this->PEM;
    }
    function getTXT($TXT){
        return $this->TXT;;
    }
}

function SuccessMsg($text){?>
    <div class="alert alert-success" role="alert">>
        <?php echo $text; ?>
    </div><?php
}

function ErrorMsg($error){?>
    <div class="alert alert-danger" role="alert">
        <?php echo "Error: " . $error->getMessage();?></h3>
    </div><?php
}

function FilesInDir(){
    $dir    = '../';
    $files1 = scandir($dir);
    $files2 = scandir($dir, 1);
    ?>
    <p>
        Índice:
    </p>
    <?php
    $aux = "";

    foreach ($files1 as $key => $value) {
        if (preg_match('/.php/', $value)) {?>
            <div>
                <a href="./<?php echo $value?>"><?php echo str_replace('_', ' ', $value) ;?></a>
            </div>
        <?php
        }
    }
}
?>