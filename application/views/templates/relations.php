<?php

function GetDependencyInfo($dependencyID){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $ci->db->where('ID', $dependencyID);
    $query = $ci->db->get('cat_dependencias');
    $dependencia = $query->result_array();
    $dependencia[0]['NAME'] = ($dependencia) ? $dependencia[0]['NAME'] : "N/A" ;
    return $dependencia;
}

function GetAdminUnitInfo($adminUnitID){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $ci->db->where('ID', $adminUnitID);
    $query = $ci->db->get('cat_u_administrativas');
    $unidadAdmin = $query->result_array();
    $unidadAdmin[0]['NAME'] = ($unidadAdmin) ? $unidadAdmin[0]['NAME'] : "N/A" ;
    return $unidadAdmin;
}
?>