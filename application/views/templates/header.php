<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION['ID'])) {
    header("Location: " . base_url());
    exit;
}

$notifications  = $this->notifications_model->Notifications($_SESSION['ID']);
$roles          = $this->roles_model->get_roles_by_user($_SESSION['ID']);
$rolesArray     = ReturnRolesArray($roles);
$rolesNeededForAdminMenus = [1,2];

// print_r($alerts);
// exit;

$listArray = [
    "Administración" => [
        "slug" => "admin",
        "icon" => '<i class="mdi mdi-monitor-dashboard"></i>',
        "values" => [
            "<i class='fas fa-desktop'></i> Sistema" => "index.php/system/index",
            "<i class='fas fa-inbox'></i> General" => "index.php/general/admin",
            "<i class='fas fa-inbox'></i> Dependencia" => "index.php/Dependency/dashboard",
            // "<i class='fas fa-user'></i> Nuevo Asistente" => "index.php/system/index",
            "<i class='fas fa-user'></i> Nuevo Usuario" => "index.php/user/new"
        ],
        "active" => false
    ],
    "Enviar/Recibir Oficios" => [
        "slug" => "mail",
        "icon" => '<i class="mdi mdi-email"></i>',
        "values" => [
            "<i class='fas fa-envelope'></i> Nuevo" => "index.php/mail/compose",
            "<i class='fas fa-inbox'></i> Entrada" => "index.php/mail/inbox",
            "<i class='fas fa-paper-plane'></i> Enviados" => "index.php/mail/sent",
            // "<i class='fas fa-file'></i> Borradores" => "index.php/mail/draft",
        ],
        "active" => false
    ],
    "Oficios" => [
        "slug" => "docs",
        "icon" => '<i class="mdi mdi-folder-multiple-outline"></i>',
        "values" => [
            "<i class='fas fa-plus'></i> Nuevo Oficio" => "index.php/document/new",
            "<i class='fas fa-suitcase'></i> Mis Oficios" => "index.php/documents/mydocuments",
            "<i class='fas fa-file-import'></i> Oficios recibidos" => "index.php/documents/mydocuments",
            "<i class='fas fa-file'></i></i> Borradores" => "index.php/documents/mydrafts",
            "<i class='fas fa-file-import'></i> Oficios Firmados" => "index.php/documents/mysigneddocuments",
            "<i class='fas fa-file-import'></i> Seguimiento" => "index.php/documents/tracking"
        ],
        "active" => false
    ],
    "Listados" => [
        "slug" => "lists",
        "icon" => '<i class="mdi mdi-format-list-bulleted"></i>',
        "values" => [
            "<i class='mdi mdi-file-document-box-multiple'></i> Documentos" => "index.php/documents/all",
            // "<i class='mdi mdi-office-building'></i> Documentos Firmados" => "index.php/admin_subunits/list",
            "<i class='mdi mdi-account-details'></i> Usuarios" => "index.php/users/list",
            // "<i class='mdi mdi-account-details'></i> Asistentes" => "index.php/users/list",
            "<i class='mdi mdi-office-building'></i> Dependencias" => "index.php/dependencies/list",
            "<i class='mdi mdi-office-building'></i> Unidades Administrativas" => "index.php/admin_units/list",
            "<i class='mdi mdi-office-building'></i> Subunidades Administrativas" => "index.php/admin_subunits/list"
            
        ],
        "active" => false
    ]
];

if (!(CheckAccess($rolesArray, $rolesNeededForAdminMenus))) {
    unset($listArray["Administración"]);
    unset($listArray["Listados"]);
}

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title><?php echo $title?></title>

        <!-- GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500" rel="stylesheet"/>
        <link href="https://cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css" rel="stylesheet" />
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800&display=swap" rel="stylesheet">

        <!-- PLUGINS CSS STYLE -->
        <link href="<?php echo base_url()?>assets/plugins/toastr/toastr.min.css" rel="stylesheet" />
        <link href="<?php echo base_url()?>assets/plugins/nprogress/nprogress.css" rel="stylesheet" />
        <link href="<?php echo base_url()?>assets/plugins/flag-icons/css/flag-icon.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url()?>assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" />
        <link href="<?php echo base_url()?>assets/plugins/ladda/ladda.min.css" rel="stylesheet" />
        <link href="<?php echo base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" />
        <link href="<?php echo base_url()?>assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800&display=swap" rel="stylesheet">
        <!-- SLEEK CSS -->
        <link id="sleek-css" rel="stylesheet" href="<?php echo base_url()?>assets/css/sleek.css" />

        <!-- COD CSS -->
        <link id="sleek-css" rel="stylesheet" href="<?= base_url()?>assets/css/cod.css" />

        <!-- FNT AWESOME -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <!-- FAVICON -->
        <!-- <link href="assets/img/favicon.png" rel="shortcut icon" /> -->

        <!--
            HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries
        -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="<?php echo base_url()?>assets/plugins/nprogress/nprogress.js"></script>
        
        <!-- JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

        <!-- JQuery UI -->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/jquery-ui/jquery-ui.css">

        <?php if (isset($css) && !empty($css)): ?>
		    <?php foreach ($css as $key => $value): ?>
		    <!-- <?php echo $value ?> -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?><?php echo $value ?>">
            <?php endforeach ?>
        <?php endif ?>

        <?php if (isset($eJS) && !empty($eJS)): ?>
            <?php foreach ($eJS as $key => $value): ?>
            <!-- <?php echo $value ?> -->
        <script src="<?php echo base_url() ?><?php echo $value ?>"></script>
            <?php endforeach ?>
        <?php endif ?>
    </head>


    <body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
        <script>
          // NProgress.configure({ showSpinner: false });
          // NProgress.start();
        </script>

        <div class="mobile-sticky-body-overlay"></div>
            <div class="wrapper">
                <!--
                ====================================
                ——— LEFT SIDEBAR WITH FOOTER
                =====================================
                -->
                <aside class="left-sidebar bg-sidebar">
                    <div id="sidebar" class="sidebar sidebar-with-footer">
                        <!-- Aplication Brand -->
                            <div class="app-brand">
                                <a href="<?php echo base_url()?>">
                                    <!--<svg 
                                        class="brand-icon"
                                        xmlns="http://www.w3.org/2000/svg"
                                        preserveAspectRatio="xMidYMid"
                                        width="30"
                                        height="33"
                                        viewBox="0 0 30 33">
                                        <g fill="none" fill-rule="evenodd">
                                            <path
                                                class="logo-fill-blue"
                                                fill="#7DBCFF"
                                                d="M0 4v25l8 4V0zM22 4v25l8 4V0z"/>
                                            <path class="logo-fill-white" fill="#FFF" d="M11 4v25l8 4V0z" />
                                        </g>
                                    </svg>-->
                                    <!-- <span class="brand-name">Sleek DasSleehboard</span> -->
                                    <img class="brand-name" src="<?php echo base_url()?>assets/img/system/logo_w.png" alt="Gobierno del Estado de Nuevo León">
                                </a>
                            </div>
                            <!-- begin sidebar scrollbar -->
                            <div class="sidebar-scrollbar">

                                <!-- sidebar menu -->
                                <ul class="nav sidebar-inner" id="sidebar-menu">
                                    <?php DisplaySideBarElements($listArray); ?>
                                </ul>
                            </div>
                            
                            <hr class="separator" />
                            <!--<div class="sidebar-footer">
                                <div class="sidebar-footer-content">
                                    <h6 class="text-uppercase">
                                        Cpu Uses <span class="float-right">40%</span>
                                    </h6>
                                    <div class="progress progress-xs">
                                        <div
                                            class="progress-bar active"
                                            style="width: 40%;"
                                            role="progressbar"
                                            >
                                        </div>
                                    </div>
                                    <h6 class="text-uppercase">
                                        Memory Uses <span class="float-right">65%</span>
                                    </h6>
                                    <div class="progress progress-xs">
                                        <div
                                            class="progress-bar progress-bar-warning"
                                            style="width: 65%;"
                                            role="progressbar"
                                            >
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </aside>
                    
                    <div class="page-wrapper">
                        <!-- Header -->
                        <header class="main-header " id="header">
                            <nav class="navbar navbar-static-top navbar-expand-lg">
                                <!-- Sidebar toggle button -->
                                <button id="sidebar-toggler" class="sidebar-toggle">
                                    <span class="sr-only">Toggle navigation</span>
                                </button>
                                <!-- search form -->
                                <div class="search-form d-none d-lg-inline-block">
                                    <div class="input-group">
                                        <button type="button" name="search" id="search-btn" class="btn btn-flat">
                                        </button>         
                                    </div>
                                    <div id="search-results-container">
                                        <ul id="search-results"></ul>
                                    </div>
                                </div>
                                <div class="navbar-right ">
                                    <ul class="nav navbar-nav">
                                        <!-- Alertas importantes, fechas cercanas a expirar -->
                                            <?php #DrawDatedNotifications($alerts, true); ?>
                                            <?php #DrawDatedNotifications($datedDocuments, false); ?>
                                        <!--  -->
                                        <li class="dropdown notifications-menu">
                                            <button class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="mdi mdi-bell-outline"></i>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <?php DrawNotifications($notifications); ?>
                                            </ul>
                                        </li>
                                        
                                        <!-- User Account -->
                                        <li class="dropdown user-menu">
                                            <button href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                                <!-- <img src="assets/img/user/user.png" class="user-image" alt="User Image" /> -->
                                                <span class="d-none d-lg-inline-block"><?php echo $_SESSION['NAMES']?></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <!-- User image -->
                                                <li class="dropdown-header">
                                                    <!-- <img src="assets/img/user/user.png" class="img-circle" alt="User Image" /> -->
                                                    <div class="d-inline-block">
                                                        <?php echo $_SESSION['NAMES']?> <small class="pt-1"><?= $_SESSION['EMAIL']?> </small>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url()?>index.php/user/profile">
                                                        <i class="mdi mdi-account"></i> Perfil
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?= base_url() . "index.php/user/settings" ?>"> <i class="mdi mdi-settings"></i> Configuarción de cuenta </a>
                                                </li>
                                                <li class="dropdown-footer">
                                                    <a href="<?php echo base_url()?>index.php/welcome/logout"> <i class="mdi mdi-logout"></i> Log Out </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </header>

<?php
function DrawNotifications($notifications = NULL){
    $numberOfNotifications = (!empty($notifications)) ? count($notifications) : 0 ;
    $notificationNotice =  (count($notifications) == 1) ? "notificación" : "notificaciones" ;
    
    ?>
    <li class="dropdown-header" style = "margin: 0">Tienes <?= $numberOfNotifications . " " . $notificationNotice?></li>
        <div class = "slim-scroll">
        <?php
        if (!empty($notifications)) {
            foreach ($notifications as $key => $value) {
                switch ($value["ID_TYPE"]) {
                    case '1':
                        $a = base_url("index.php/document/view/" . $value["ID_DOC"]);
                        break;
                    
                    case '3':
                        $a = base_url("index.php/document/view/" . $value["ID_DOC"]);
                        break;

                    case '4':
                        $a = base_url("index.php/document/draft/" . $value["ID_DOC"]);
                        $message = "Se compartió contigo un borrador.";
                        break;
                    
                    default:
                        $a = "#";
                        break;
                }
                // $dateReceived = date("d-m-Y h:m A", strtotime($value['DATE']));
                $date = new DateTime($value['DATE']);
                $dateReceived = $date->format('Y-m-d h:i A');
                ?>
                <a  href="<?= $a ?>" style = "color: #8a909d;">
                    <li style = "display: block; font-size: 0.88rem; padding: 0.625rem 1.25rem; border-bottom: 1px solid #eee;" class="notification-link">
                        <i class="mdi <?= $value['ICON']?>"></i> <?= $value['DESCRIPTION']?>
                        <span class=" font-size-12 d-inline-block float-right" style = "font-weight: normal"><i class="mdi mdi-clock-outline"></i> <?= $dateReceived?>
                        </span>
                        
                    </li>
                </a>
                <?php
            }
        } else {?>
            <li style = "display: block; font-size: 0.88rem; padding: 0.625rem 1.25rem; ">
                
                    <i class="mdi mdi-feature-search-outline"></i> Sin notificaciones.
                    <span class=" font-size-12 d-inline-block float-right">
                    </span>
                
            </li><?php
        }?>
        </div>
    <li class="dropdown-footer">
        <!-- <a class="text-center" href="#"> View All </a> -->
    </li><?php
}

function DrawDatedNotifications($alerts = NULL, $alert){
    $icon = ($alert) ? "mdi-alert-outline sk-pulse" : "mdi-calendar" ;
    $color = ($alert) ? "color: red" : "" ;

    if (!empty($alerts)):
        $numberOfAlerts = (!empty($alerts)) ? count($alerts) : 0 ;?>
        <li class="dropdown notifications-menu ">
            <button class="dropdown-toggle " data-toggle="dropdown" style = "border-right: 0; <?= $color ?>; content:none !important">
                <i class="mdi <?= $icon ?>" style = "background-color: white; "></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-right">
                <li class="dropdown-header">Tienes <?php echo $numberOfAlerts?> notificaciones.</li>
        <?php
        foreach ($alerts as $key => $value):
            $dudeDate = substr($value['DATE'], 0, 10);
            $date_a = new DateTime(date($value['DATE']));
            $date_b = new DateTime(date("Y-m-d H:i:s"));
            $dd = date_diff($date_a,$date_b);
            $text = ($alert) ? $value['alert'] : $value['notification'] ;
            // print_r($dd);
            ?>
            <li>
                <a href="#">
                    <i class="mdi <?= $value['icon']?>"></i> <?= $text?>
                    <span class=" font-size-12 d-inline-block float-right"><i class="mdi mdi-clock-outline"></i><?= $dd->format('%a')?> días
                    </span>
                </a>
            </li>   
        <?php
        endforeach;?>
                <li class="dropdown-footer">
                    <a class="text-center" href="#"> View All </a>
                </li>
            </ul>
        </li>
        <?php
    endif;
}

function DisplaySideBarElements($listArray){
    foreach ($listArray as $item => $LAValue) {
        $expand = ($LAValue['active']) ? "active expand" : "" ;
        $show = ($LAValue['active']) ? "show" : "" ;
        ?>
        <li class="has-sub <?php echo $expand?>" >
        <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#<?php echo $LAValue['slug'] ?>"
            aria-expanded="<?php echo $LAValue['slug']?>" aria-controls="<?php echo $LAValue['slug'] ?>">
            
            <?php echo $LAValue['icon'] ?>
            <span class="nav-text"><?php echo $item?></span> <b class="caret"></b>
        </a>
        <ul  class="collapse <?php echo $show?>"  id="<?php echo $LAValue['slug'] ?>"
            data-parent="#sidebar-menu">
            <div class="sub-menu">
                <?php 
                foreach ($LAValue['values'] as $key => $value) {
                    if (is_array($value)) {?>
                        <li class="has-sub">
                            <a id="newdoc" class="sidenav-item-link collapsed" href="javascript:void(0)" data-toggle="collapse" data-target="#components" aria-expanded="false" aria-controls="components">
                                <span class="nav-text"><?php echo $key ?></span> <b class="caret"></b>
                            </a>
                            <ul class="collapse" id="components" style="">
                                <div class="sub-menu">
                                    <?php
                                    foreach ($value as $k => $v) {?>
                                        <li>
                                            <a href="<?php echo base_url() . $v?>"><?php echo $k?></a>
                                        </li>                
                                    <?php
                                    }?>
                                </div>
                            </ul>
                        </li>
                    <?php
                    }else{?>
                        <li class="">
                            <a class="sidenav-item-link" href="<?php echo base_url() . $value?>">
                                <span class="nav-text"><?php echo $key ?></span>
                            </a>
                        </li>
                        <?php
                    }?>
                <?php
                }?>
            </div>
        </ul>
    </li><?php
    }
}

function CheckAccess($roles, $rolesNeeded){
    $result = false;
    foreach ($rolesNeeded as $kRN => $vRN) {
        // echo "roleNeeded = $vRN<br>";
        if (!empty($roles)) {
            foreach ($roles as $key => $value) {
                // echo "rol obtenido = $key <br>";
                if ($key == $vRN) {
                    $result = true;
                    break;
                }
            }
            if ($result) {
                break;
            }
        }
    }
    return $result;
}
// mdi-alert-outline
function ReturnRolesArray($roles){
    foreach ($roles as $key => $value) {
        $rol[$value['ID_ROLE']] = true;
    }
    $rol = (!empty($rol)) ? $rol : "" ;
    return $rol;
}
?>