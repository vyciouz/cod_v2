
<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2><?php echo $title?></h2>
                    </div>
                    <div class="card-body">
                        <form id="edit-info" action="" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <?php DrawEditFields($editFields) ?>
                            </div>
                            <button class="btn btn-dark">Actualizar Datos</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
if (!empty($_POST)) {
    // print_r($_POST);
    $result = ":)";
    foreach ($editFields as $key => $value) {
        echo "$key<br>";
        if ($_POST[$key] == "") {
            echo "$key =>". $_POST[$key] ."<br>";
            $result = ":(";
        }else{
            echo "$key =>". $_POST[$key] ."<br>";
            $this->db->where('ID', $id);
            $this->db->update($table, $_POST);
        }
    }
    header('Location: '.$_SERVER['REQUEST_URI']);
    // print_r($result);
}else{
    // echo "Nada";
}
        
function DrawEditFields($editFields){
    
    foreach ($editFields as $key => $value) {
        // print_r($value);
        switch ($value['type']) {
            case 'text':
                DrawTextInput($editFields[$key], $key, $value);
                break;

            case 'select':
                DrawSelectInput($editFields[$key], $key, $value);
                break;

            case 'password':
                DrawPasswordInput($editFields[$key], $key, $value);
                break;
            
            default:
                # code...
                break;
        }
    }
    
}

function DrawTextInput($array, $colName, $value){?>
    <label for=""><?php echo $array['name']?></label>
    <input class = "form-control" type="text" name="<?php echo $colName?>" id="<?php echo $colName?>" value="<?php echo $value['val']?>">
    <br>
<?php
}

function DrawSelectInput($array, $colName, $value){?>
    <label for=""><?php echo $array['name']?></label>
    <select  class ="form-control" name="<?php echo $colName?>" id="<?php echo $colName?>">
        <option value="0">N/A</option>
    <?php
    if ($array['dependency']!=null) {
        $ci =& get_instance();
        $values = $ci->db->get($array['dependency']['table']);
        foreach ($values->result_array() as $row) {
            if ($row[$array['dependency']['inCol']] == $value['val']) {?>
                <option selected value="<?php echo $row[$array['dependency']['inCol']] ?>"><?php echo $row[$array['dependency']['outCol']] ?></option><?php
            }else{?>
            <option value="<?php echo $row[$array['dependency']['inCol']] ?>"><?php echo $row[$array['dependency']['outCol']] ?></option><?php
            }?>
            
        <?php
        }
    }
    ?>
    
    </select>
    <br>
<?php
}

function DrawPasswordInput($array, $colName, $value){?>
    <label for=""><?php echo $array['name']?></label>
    <input class = "form-control" type="password" name="<?php echo $colName?>" id="<?php echo $colName?>" value="<?php echo $value['val']?>">
    <br>
    <label for="">Verificar <?php echo $array['name']?></label>
    <input class = "form-control" type="password" id="<?php echo $colName?>_verify" value="<?php echo $value['val']?>">
    <div id="divCheckPasswordMatch"></div>
    <br>
    
    <script>
        function checkPasswordMatch() {
            var password = $("#<?php echo $colName?>").val();
            var confirmPassword = $("#<?php echo $colName?>_verify").val();
            if (password != confirmPassword)
                $("#divCheckPasswordMatch").html("Contraseña no coincide!");
            else
                $("#divCheckPasswordMatch").html("");
        }

        $(document).ready(function () {
            $("#<?php echo $colName?>").change(checkPasswordMatch);
            $("#<?php echo $colName?>_verify").change(checkPasswordMatch);
        });
    </script>
<?php
}

?>