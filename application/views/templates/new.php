<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-default">
                    <div class="card-header card-header-border-bottom">
                        <h2><?php echo $title?></h2>
                    </div>
                    <div class="card-body">
                        <form id="edit-info" action="" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <?php DrawEditFields($columnsToUse) ?>
                            </div>
                            <button class="btn btn-dark">Añadir registro</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
if (!empty($_POST)) {
    // print_r($_POST);
    $result = true;
    foreach ($columnsToUse as $key => $value) {
        // echo "$key<br>";
        if ($_POST[$key] == "") {
            // echo "$key =>". $_POST[$key] ."<br>";
            $result = false;
        }else{
            // echo "$key =>". $_POST[$key] ."<br>";
        }
    }
    // print_r($result);
    if ($result) {
        $this->db->insert($table, $_POST);
        header("Location: " . base_url() . "index.php/$controllerName/list");
    }
}else{
    // echo "Nada";
}

function DrawEditFields($columnsToUse){
    
    foreach ($columnsToUse as $key => $value) {
        switch ($value['type']) {
            case 'text':
                DrawTextInput($columnsToUse[$key], $key);
                break;

            case 'select':
                DrawSelectInput($columnsToUse[$key], $key);
                break;

            case 'password':
                DrawPasswordInput($columnsToUse[$key], $key);
                break;
            
            default:
                # code...
                break;
        }
    }
    
}

function DrawTextInput($array, $colName){?>
    <label for=""><?php echo $array['name']?></label>
    <input class = "form-control" type="text" name="<?php echo $colName?>" id="<?php echo $colName?>" required>
    <br>
<?php
}

function DrawSelectInput($array, $colName){?>
    <label for=""><?php echo $array['name']?></label>
    <select  class ="form-control" name="<?php echo $colName?>" id="<?php echo $colName?>">
        <option value="0">N/A</option>
    <?php
    if ($array['dependency']!=null) {
        $ci =& get_instance();
        $values = $ci->db->get($array['dependency']['table']);
        foreach ($values->result_array() as $row) {?>
            <option value="<?php echo $row[$array['dependency']['inCol']] ?>"><?php echo $row[$array['dependency']['outCol']] ?></option>
        <?php
        }
    }
    ?>
    
    </select>
    <br>
<?php
}

function DrawPasswordInput($array, $colName){?>
    <label for=""><?php echo $array['name']?></label>
    <input class = "form-control" type="password" name="<?php echo $colName?>" id="<?php echo $colName?>">
    <br>
    <label for="">Verificar <?php echo $array['name']?></label>
    <input class = "form-control" type="password" id="<?php echo $colName?>_verify">
    <div id="divCheckPasswordMatch"></div>
    <br>
    <script>
        function checkPasswordMatch() {
            var password = $("#<?php echo $colName?>").val();
            var confirmPassword = $("#<?php echo $colName?>_verify").val();
            if (password != confirmPassword)
                $("#divCheckPasswordMatch").html("Contraseña no coincide!");
            else
                $("#divCheckPasswordMatch").html("");
        }

        $(document).ready(function () {
            $("#<?php echo $colName?>").change(checkPasswordMatch);
            $("#<?php echo $colName?>_verify").change(checkPasswordMatch);
        });
    </script>
<?php
}

?>