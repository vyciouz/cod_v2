<?php

function getUserRoles($id){

}

function getUserData($id){
    $ci =& get_instance(); #Se carga la instancia para poder realizar queries de búsqueda
    $query  = $ci->db->get_where('t_users', ['ID' => $id]);
    $result = $query->row_array();
    $user   = [];
    if ($result) {
        $queryUA    = $ci->db->get_where('cat_u_administrativas',       ["ID" => $result['UNIDAD_ADMINISTRATIVA']]);
        $querySUA   = $ci->db->get_where('cat_sub_u_administrativas',   ["ID" => $result['SUBUNIDAD_ADMINISTRATIVA']]);
        $queryDep   = $ci->db->get_where('cat_dependencias',            ["ID" => $result['DEPENDENCIA']]);
        $resultUA   = $queryUA->row_array();
        $resultSUA  = $querySUA->row_array();
        $resultDep  = $queryDep->row_array();
        
        $dep        = ($resultDep)  ? $resultDep['NAME']            : "N/A" ;
        $sua        = ($resultSUA)  ? $resultSUA['NAME']            : "N/A" ;
        $ua         = ($resultUA)   ? $resultUA['NAME']             : "N/A" ;
        $nomDep     = ($resultDep)  ? $resultDep['NOMENCLATURE']    : "N/A" ;
        $nomSua     = ($resultSUA)  ? $resultSUA['NOMENCLATURE']    : "N/A" ;
        $nomUa      = ($resultUA)   ? $resultUA['NOMENCLATURE']     : "N/A" ;
  
        foreach ($result as $key => $value) {
            $user[$key] = $value;
        }
        
        $user['NAME_DEP']   = $dep;
        $user['NAME_UA']    = $ua;
        $user['NAME_SUA']   = $sua;
        $user['NOM_DEP']    = $nomDep;
        $user['NOM_UA']     = $nomUa;
        $user['NOM_SUA']    = $nomSua;
    }
    return $user;
}
?>