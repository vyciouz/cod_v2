function loginForm(formId, target, redirectTarget){
    $(formId).on('submit', function(e){
        var form = new FormData(this);
        
        e.preventDefault();
        $.ajax({
            url: target,
            method: "post",
            data:  form,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            beforeSend : function(data){
            },
            success: function(data){
                // console.log(data);
                if (data.result) {
					$( "#response-container" ).removeClass( "invalid-feedback" );
                    $( "#response-container" ).addClass( "valid-feedback" );
                    $("#response-container").html(data.msg + "<br>Redireccionando...");
                    setTimeout(function () {
                        window.location.href = redirectTarget + "index.php/user/profile/" + data.ID;
                    }, 3000);
				}else{
					$( "#response-container" ).removeClass( "valid-feedback" );
                    $( "#response-container" ).addClass( "invalid-feedback" );
                    $("#response-container").html(data.msg);
				}
            },
            complete: function(){
            },
            error: function(e){
            }
        });
    });
}